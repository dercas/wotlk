#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <dlfcn.h>

#include "wotlkpre.h"

pthread_t thread;
int i;
void *handle;
int (*PostMessage)(void *, unsigned, unsigned, unsigned);

void *wine_dlopen(const char *filename, int flag, char *error, size_t errorsize)
{
    printf("dlopen......%s\n", filename);
    void *h = dlopen(filename, flag);
    dlerror();
    dlerror();
    return h;
}

void *wine_dlsym(void *handle, const char *symbol, char *error, size_t errorsize)
{
    printf("dlsym.......%s\n", symbol);
    void *f = dlsym(handle, symbol);
    dlerror();
    dlerror();
    return f;
}

static void *loop(void *par)
{
    sleep(1);
    handle = dlopen("/usr/lib32/wine/user32.dll.so", RTLD_LAZY | RTLD_GLOBAL);
    PostMessage = (int (*)(void *, unsigned, unsigned, unsigned)) dlsym(handle, "PostMessageA");
    puts("readin'");
    for (i=0; i<12; i++) {
        printf("waited %d\n", i);
        sleep(1);
    }
    printf("HWND: %p\n", *hwnd);
    // PostMessage(*hwnd, 0x100, 97, 0);
    return NULL;
}

__attribute__((constructor)) void wotlkpre_init(void)
{
    pthread_create(&thread, NULL, loop, NULL);
}


