#ifndef INJECT_H
#define INJECT_H

#include <sys/types.h>

#define HWND void *
#define uid_t int

#include "consts.h"

// Macro to safely derreference a pointer, an return 0 on a NULL pointer
#define R(p)    (((int) p > 0x10000) ? (*((void **) (p))) : 0)

typedef struct __ofs_t {
    void *addr;     
    int type;
    uid_t uid;
    uid_t guid;
    void *next;
} ofs_t;

ofs_t ofs_head(void);
ofs_t ofs_new(void *addr);
ofs_t ofs_find(uid_t guid);
int ofs_findn(uid_t guid, ofs_t *buf, ssize_t n);

#define IS_NULL_OFS(obj) (obj.addr == NULL \
                       || obj.type < 1 \
                       || obj.type > 7 \
                       || obj.guid == 0)
#define NULL_OFS ((ofs_t) {NULL,0,0,0,0})
#define for_ofs(item) \
    for(item = ofs_head(); ! IS_NULL_OFS(item); item = ofs_new(item.next))

// direct data offsets
extern int      *map_id; // = (int *) MAPNAM;
extern int      *loading;
extern HWND     *hwnd;
extern char     *map_name;
extern uid_t   *mouseover_g;
extern uid_t   *target_g;

// read-only data functions
void *  player_base (void);
int     sitting (void *base);
int     casting (void *base);
int     channeling (void *base);
char    bob_state (void *addr);

// final usable functions
void do_string (const char *arg);
void cast_spell (const char *spell_name);
char *get_localized_text (const char *var);
double get_time (void);
void rightclick_obj (void *addr);

// main loop function, called on every glFlush() call
extern void loopFun(void);

#endif
