// #include <windows.h>
// #include <winternl.h>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <libgen.h>

// #include <windows.h>
// #include <winternl.h>

typedef unsigned short USHORT;
typedef unsigned long ULONG;
typedef long NTSTATUS;
typedef void *PVOID, *HMODULE;
typedef char CHAR, *PCHAR;
typedef const char *LPCSTR;
typedef struct _STRING {
    USHORT Length;
    USHORT MaximumLength;
    PCHAR Buffer;
} STRING, *PSTRING, ANSI_STRING;

#ifndef __stdcall 
#define __stdcall __attribute__((ms_abi)) __attribute__((__force_align_arg_pointer__)) 
#endif
#ifndef WINAPI
#define WINAPI __stdcall
#endif

#define VOID(name) extern void name(void){}

// NTSTATUS WINAPI (*real_GPA)(HMODULE, const ANSI_STRING *, ULONG, PVOID *);
// NTSTATUS WINAPI LdrGetProcedureAddress(HMODULE module, const ANSI_STRING *name, 
//                                        ULONG ord, PVOID *address)
// {
//     // puts(name->Buffer);
//     puts("asda");
//     return 1;
// }

// WINAPI void *GetProcAddress(HMODULE module, LPCSTR func)
// {
//     return NULL;
// }

// extern void __wine_kernel_init(void)
// {
//     puts("init");
// }

// void *kernel32;

void *wine_dlopen(const char *filename, int flag, char *error, size_t errorsize)
{
    void *h;

//     if (!strcmp(basename((char *) filename), "ntdll.dll.so")) {
//         puts("Initializing kernel32");
//         // kernel32 = dlopen(NULL, RTLD_NOW | RTLD_GLOBAL);
//         // h = dlopen(filename, RTLD_GLOBAL | RTLD_NOW);
//         // _h = dlopen(NULL, RTLD_NOW | RTLD_GLOBAL);
//         // real_GPA = dlsym(h, "LdrGetProcedureAddress");
//         // printf("h == _h? %d\n", _h == h);
//         // return _h;
//     }
// 
//     if (!strcmp(basename((char *) filename), "kernel32.dll.so")) {
//         return dlopen(filename, flag);
//         puts("Replacing kernel32");
//         return kernel32;
//     }

    h = dlopen(filename, flag);
    dlerror();
    dlerror();
    return h;
}

// void *wine_dll_load(const char *filename, char *error, int errorsize, int *file_exists)
// {
//     puts(filename);
//     return NULL;
// }

