#ifndef INJECT_CONSTS_H
#define INJECT_CONSTS_H

// Player base tree (ends in OfsObj)
#define PLAYB	0x00CD87A8
#define PLAYOFF1	0x34
#define PLAYOFF2		0x24

// Mouseover GUID
#define MGUID	0x00DB07A0

// Target GUID
#define TGUID	0x00DB07B0

// OfsObj List
#define CONN	0x00C79CE0
#define LISTMGR		0x2ED0
#define MGRHEAD			0xAC

// Offsets for an OfsObj
#define ONEXT	0x3C
#define OGUID	0x30
#define OTYPE	0x14

#define UID		0x0224
#define UID2	0x0260
#define POSX	0x0798
#define POSY	0x079C
#define POSZ	0x07A0
#define POSR	0x07A8
#define POSE	0x07AC
#define PLAYST	0x07CF
#define COMBAT	0x1A46
#define SITTN	0x09F8
#define CASTN	0x0A60
#define CHANN   0x0980
#define BOBST   0x00BC

// Absolute pointers

#define MAPID 	0x00AB63BC
#define LOADS	0x00BD078A
#define MAPNAM	0x00CE07D0
#define WHWND	0x00D413F4

// Function addresses

#define DO_STRING           0x00819210
#define GET_PLAYER_OBJ      0x004038F0
#define GET_LOCALIZED_TEXT  0x007225E0
#define RIGHTCLICK_OBJ      0x00711140

// UIDs

#define BOBBER  35591


#endif
