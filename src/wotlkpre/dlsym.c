#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <pthread.h>
#include <linux/limits.h>

#include "dlsym.h"

#define STDCALL __attribute__((stdcall))

#define STARTS_WITH(value, symbol) \
    (sizeof(symbol) > 0 ? !strncmp(value, symbol, sizeof(symbol) - 1) : 1)

// void *(*real_glXGetProcAddress(const char *));
void *real_glXGetProcAddress;
void *fake_glXGetProcAddress(const char *proc_name)
{
    void *real_func = ((void *(*)(const char *)) real_glXGetProcAddress)(proc_name);
    void *filtered = wine_sym_filter(NULL, proc_name, real_func);
    return NULL == filtered ? real_func : filtered;
}

void *wine_dlsym(void *handle, const char *symbol, char *error, size_t errorsize)
{
    void *f = dlsym(handle, symbol);
    dlerror();
    dlerror();

    if (!wine_sym_filter)
        return f;

    if (STARTS_WITH(symbol, "glXGetProcAddress")) {
        printf("Hooking %s\n", symbol);
        real_glXGetProcAddress = f;
        return fake_glXGetProcAddress;
    }

    void *ret_f = wine_sym_filter(handle, symbol, f);

    if (!ret_f) {
        fprintf(stderr, "Got NULL pointer with function \"%s\""
                        ", using real pointer instead\n", symbol);
        return f;
    }

    return ret_f;
}

// void *wine_sym_filter(void *handle, const char *symbol, void *f)
// {
//     return f;
// }
// 
// int main(void)
// {
//     printf("%d\n", STARTS_WITH("glXGetProcAddressARB", "glXGetProcAddress"));
//     return 0;
// }
