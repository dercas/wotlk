#include <stdio.h>
#include <dlfcn.h>
#include <libgen.h>

void *wine_dlopen(const char *filename, int flag, char *error, size_t errorsize)
{
    printf("dlopen: %s\n", basename((char *) filename));
    void *h = dlopen(filename, flag);
    dlerror();
    dlerror();
    return h;
}
