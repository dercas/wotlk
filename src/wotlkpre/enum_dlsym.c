#include <stdio.h>
#include <dlfcn.h>

void *wine_dlsym(void *handle, const char *symbol, char *error, size_t errorsize)
{
    printf("dlsym: %s\n", symbol);
    void *f = dlsym(handle, symbol);
    dlerror();
    dlerror();
    return f;
}

