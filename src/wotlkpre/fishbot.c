#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "on_gl_draw.h"
#include "wotlk.h"

#define BUFFN 8

enum phase {
    WAITING,
    SEARCHING,
    FISHING,
};

void loopFun(void)
{
    puts("loopFun");
    static enum phase phase = WAITING;
    static ofs_t bobbers[BUFFN];
    static double times[BUFFN];
    static void *base;
    static double time = 0, delay = 0;
    static int retries = 0, fishes = 0;

    double new_time;
    int i, anything = 0;

    if (!(base = player_base())) {
        delay = 2.0;
        if (phase != WAITING) {
            puts("[Waiting]");
            phase = WAITING;
        }
        return;
    }

    new_time = get_time();

    if (time == 0) {
        time = new_time;
        return;
    }

    if (delay > new_time - time) {
        delay -= new_time - time;
        time = new_time;
        return;
    }

    time = new_time;

    if (!sitting(base)) {
        switch (phase) {
            case WAITING:
                fishes = 0;
                delay = 1.0;
                return;
            default:
                break;
        }
    }

    switch (phase) {
        case WAITING: 
            puts("[Searching]");
            do_string("delTrash()");
            do_string("RunMacroText('/click MainMenuBarBackpackButton')");
            cast_spell("Fishing");
            retries = 0;
            phase = SEARCHING;
            delay = 0.250;
            return;

        case SEARCHING:
            if (20 < retries++) {
                puts("\tno bobber found, recasting");
                puts("[Waiting]");
                phase = WAITING;
                delay = 0.5;
                return;
            }

            if (!channeling(base)) {
                delay = 0.1;
                return;
            }

            memset(bobbers, 0, sizeof(bobbers));
            anything = 0;
            i = ofs_findn(BOBBER, bobbers, sizeof(bobbers));
            anything = i > 0;

            if (!anything) {
                puts("\tno bobber found, retrying");
                delay = 0.1;
                return;
            } 

            memset(times, 0, sizeof(times));
            puts("[Fishing]");
            phase = FISHING;
            return;

        case FISHING:
            anything = 0;

            for (i = 0; i < BUFFN; i++) {
                if (bobbers[i].addr && times[i] == 0.0) {
                    anything = 1;
                    if (1 == bob_state(bobbers[i].addr)) {
                        printf("\t#%d splashed\n", i);
                        times[i] = time + 0.5;
                    }
                }
            }

            for (i = 0; i < BUFFN; i++) {
                if (times[i] > 0) {
                    anything = 1;
                    if (times[i] < time) {
                        printf("\tdrawing #%d (%d)\n", i, bobbers[i].guid);
                        rightclick_obj(bobbers[i].addr);
                        times[i] = -1.0; 
                    }
                } 
            }

            if (!channeling(base)) {
                if (!anything) {
                    printf("\tgot fish #%d\n", ++fishes);
                    puts("[Waiting]");
                    phase = WAITING;
                    delay = 1.5;
                    return;
                }

                puts("\ttimed out");
                puts("[Waiting]");
                phase = WAITING;
                delay = 2.5;
                return;
            }

            delay = 0.05;
            return;

        default:
            return;
    }
}

void (*loop_func)(void) = &loopFun;
