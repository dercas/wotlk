#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <pthread.h>
#include <linux/limits.h>

#include "wotlk.h"

#define STDCALL __attribute__((stdcall))


// ofs_t functions

ofs_t ofs_head (void)
{
    return ofs_new( R(R(R(CONN) + LISTMGR) + MGRHEAD) );
}

ofs_t ofs_new (void *addr)
{
    if (!addr)
        return NULL_OFS;
    return (ofs_t) {
        .addr = addr,
        .type = (int)     R( addr + OTYPE ),
        .uid  = (int)     R( addr + UID ),  
        .guid = (uid_t)   R( addr + OGUID ),
        .next = (void *)  R( addr + ONEXT )
    };
}

ofs_t ofs_find (uid_t uid)
{
    ofs_t ret = NULL_OFS, item;
    for_ofs (item) {
        if (item.uid == uid)
            ret = item;
    }
    return ret;
}

int ofs_findn (uid_t uid, ofs_t *arr, ssize_t n)
{
    int ret = 0;
    ofs_t item;
    for_ofs (item) {
        if (item.uid == uid) {
            arr[ret++] = item;
            if (ret >= n)
                break;
        }
    }
    return ret;
}



// direct data offsets

int     *map_id        =    (int *) MAPID;
int     *loading       =    (int *) LOADS;
HWND    *whwnd         =   (HWND *) WHWND;
char    *map_name      =   (char *) MAPNAM;
uid_t  *mouseover_g   = (uid_t *) MGUID;
uid_t  *target_g      = (uid_t *) TGUID;

// read-only data functions

void *player_base(void)
{
    return R(R(R(PLAYB) + PLAYOFF1) + PLAYOFF2);
}

int sitting(void *base)
{
    if (!base)
        base = player_base();
    return (int) R(base + SITTN);
}

int casting(void *base)
{
    if (!base)
        base = player_base();
    return (int) R(base + CASTN);
} 

int channeling(void *base)
{
    if (!base)
        base = player_base();
    return (int) R(base + CHANN);
}

char bob_state(void *addr)
{
    return (int) R(addr + BOBST);
}

// final usable complex functions

void do_string(const char *str)
{
	void (*__do_string)(int, int, int) = (void (*)(int, int, int)) DO_STRING;

    __do_string((int) str, (int) str, 0);
}

void cast_spell(const char *spell_name)
{
    const char *FORMATSTR = "CastSpellByName('%s')";
    char buff[sizeof(FORMATSTR) + strlen(spell_name)];
    sprintf(buff, FORMATSTR, spell_name);
    do_string(buff);
}

char *get_localized_text(const char *str)
{
    int ret;
    __asm__(
        "call *%1           \n\t"
        "mov %%eax, %%ecx   \n\t"
        "push $-1           \n\t"
        "push %3            \n\t"
        "call *%2           \n\t"
        "mov %%eax, %0          "
      : "=r" (ret)
      : "r" (GET_PLAYER_OBJ), "r" (GET_LOCALIZED_TEXT), "r" (str)
      : "eax", "ecx"
    );
    return (void *) ret;
}

double get_time(void)
{
    // do_string("time = GetTime()");
    // char *str = get_localized_text("time");
    // return atof(str);

    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + ((double) tv.tv_usec / 1.0e6);
}

void rightclick_obj(void *addr)
{
    __asm__(
        "mov %0, %%ecx  \n\t"
        "call *%1           "
      : 
      : "r" (addr), "r" (RIGHTCLICK_OBJ)
      : "ecx"
    );
}

