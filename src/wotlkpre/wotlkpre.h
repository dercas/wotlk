#include <sys/types.h>

void **hwnd = (void **) 0x121eac;

void (*LoadNewWorld)(int) = (void (*)(int)) 0x403B70;
int (*UpdatePosition)(void) = (int (*)(void)) 0x6e6ef0;
__attribute__((cdecl)) void (*logout)(void) = 
        (__attribute__((cdecl)) void (*)(void)) 0x6b18c0;

__attribute__((stdcall)) int (*StopMovement)(int) =
        (__attribute__((stdcall)) int (*)(int)) 0x98bd10;


extern void *wine_dlopen( const char *, int, char *, size_t);

extern void *wine_dlsym( void *, const char *, char *, size_t);
