#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

void usage(void)
{
    puts("Usage: find_symbol LIBFILE SYMBOLNAME");
}

int main(int argc, char **argv)
{
    if (argc < 3) {
        usage();
        exit(1);
    }

    void *handle = dlopen(argv[1], RTLD_LAZY | RTLD_LOCAL);
    if (!handle) {
        puts(dlerror());
        exit(1);
    }
    void *add = dlsym(handle, argv[2]);
    if (!add) {
        puts(dlerror());
        exit(1);
    }
    printf("%s at %p\n", argv[2], add);
    return 0;
}
