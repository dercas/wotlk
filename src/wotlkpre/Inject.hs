module Inject where

import Foreign
import Foreign.C

foreign import ccall "dynamic"
  unFun0 :: FunPtr (IO ()) 
              -> IO ()

foreign import ccall "dynamic"
  unFun1 :: FunPtr (CInt -> IO ()) 
              -> CInt -> IO ()

foreign import ccall "dynamic"
  unFun2 :: FunPtr (CInt -> CInt -> IO ()) 
              -> CInt -> CInt -> IO ()

foreign import ccall "dynamic"
  unFun3 :: FunPtr (CInt -> CInt -> CInt -> IO ()) 
              -> CInt -> CInt -> CInt ->IO ()

mkFun :: Integral a => a -> FunPtr b
mkFun = castPtrToFunPtr . wordPtrToPtr . fromIntegral

doString :: String -> IO ()
doString str = withCString str $ \ptr -> do
    let f = unFun3 $ mkFun 0x819210
        ptr' = fromIntegral $ ptrToWordPtr ptr
    f ptr' ptr' 0

inject :: IO ()
inject = do
    return ()

foreign import ccall "loopFun" loopFun :: Ptr ()

foreign export ccall "inject" inject :: IO ()
