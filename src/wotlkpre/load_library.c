// long LdrGetProcedureAddress(void *module, void *name, long ord, void **address)
// {
//     return 0;
// }


#include <stdarg.h>
#include <winternl.h>
#include <dlfcn.h>

#ifdef __x86_64
#define NTDLL_PATH "/usr/lib/wine/ntdll.dll.so"
#else
#define NTDLL_PATH "/usr/lib32/wine/ntdll.dll.so"
#endif

// NTSTATUS WINAPI (*real_LdrGetProcedureAddress)(HMODULE, const ANSI_STRING *, 
//                                                ULONG, PVOID *);
// 
// NTSTATUS _LdrGetProcedureAddress(HMODULE module, const ANSI_STRING *name,
//                                 ULONG ord, PVOID *address)
// {
//     return real_LdrGetProcedureAddress(module, name, ord, address);
// }

// __attribute__((constructor)) void __load_real_LdrGetProcedureAddress(void)
// {
//     void *handle = dlopen(NTDLL_PATH, RTLD_LAZY | RTLD_GLOBAL);
//     dlerror();
//     dlerror();
//     real_LdrGetProcedureAddress = 
//         (NTSTATUS WINAPI (*)(HMODULE, const ANSI_STRING *, ULONG, PVOID *))
//         dlsym(handle, "LdrGetProcedureAddress");
//     dlerror();
//     dlerror();
// }
