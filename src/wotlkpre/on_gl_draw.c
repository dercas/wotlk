#include "on_gl_draw.h"

#include <string.h>
#include <stdio.h>
#include <GL/glx.h>

void (*real_glXSwapBuffers)(Display *, GLXDrawable);
void fake_glXSwapBuffers(Display *dpy, GLXDrawable drawable)
{
    if (NULL != loop_func)
        loop_func();
    real_glXSwapBuffers(dpy, drawable);
}

void *_wine_sym_filter(void *handle, const char *symbol, void *func)
{
    if (!strcmp(symbol, "glXSwapBuffers")) {
        puts("Hooking glXSwapBuffers");
        real_glXSwapBuffers = func;
        return fake_glXSwapBuffers;
    }
    return func;
}

wine_sym_filter = _wine_sym_filter;
