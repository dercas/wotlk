#include "dlsym.h"

#include <string.h>
#include <GL/glx.h>
#include <stdio.h>
#include <unistd.h>

int starts_with(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? 0 : strncmp(pre, str, lenpre) == 0;
}
void __nop() { /* puts("nop"); */ }

typedef struct {
    char *name;
    void **real;
    void *fake;
} func_entry;

#define E(nam, rea, fak) (((func_entry) {nam, (void **) rea, (void *) fak}))

#define N(nam) (((func_entry) {nam, NULL, (void *) &__nop}))

// glXGetProcAddress 
// void *(*real_glXGetProcAddress)(const char *procName);
// 
// void *fake_glXGetProcAddress(const char *procName)
// {
//     return wine_sym_filter(NULL, procName, real_glXGetProcAddress(procName));
// }

// glXSwapBuffers 
static int _call_count = 0; // modular count of SwapBuffers calls, real function called on 0
static int _call_mod = 7;

void (*real_glXSwapBuffers)(Display *dpy, GLXDrawable drawable);

void fake_glXSwapBuffers(Display *dpy, GLXDrawable drawable)
{
    if (_call_count == 0)
        real_glXSwapBuffers(dpy, drawable); 
    else
        usleep(10000);
    _call_count = (_call_count + 1) % _call_mod;
}

// glDrawRangeElementsEXT 
void (*real_glDrawRangeElementsEXT)(GLenum, GLuint, GLuint, GLsizei, 
                                    GLenum, const void *);
void fake_glDrawRangeElementsEXT(GLenum mode, GLuint starts, GLuint end, 
                                 GLsizei count, GLenum type, const void *inds)
{
    if (_call_count == 0)
        real_glDrawRangeElementsEXT(mode, starts, end, count, type, inds);
}

// glDrawArrays 
void (*real_glDrawArrays)(GLenum, GLint, GLsizei);

void fake_glDrawArrays(GLenum mode, GLint first, GLsizei count)
{
    if (_call_count == 0)
        real_glDrawArrays(mode, first, count);
}

// glEnable
void (*real_glEnable)(GLenum);

void fake_glEnable(GLenum cap)
{
    if (_call_count == 0)
        real_glEnable(cap);
}

// glDisable
void (*real_glDisable)(GLenum);

void fake_glDisable(GLenum cap)
{
    if (_call_count == 0)
        real_glEnable(cap);
}

// glEnableClientState
void (*real_glEnableClientState)(GLenum cap);
void fake_glEnableClientState(GLenum cap)
{
    if (_call_count == 0)
        real_glEnableClientState(cap);
}

// glDisableClientState
void (*real_glDisableClientState)(GLenum cap);
void fake_glDisableClientState(GLenum cap)
{
    if (_call_count == 0)
        real_glDisableClientState(cap);
}

// glProgramStringARB
void (*real_glProgramStringARB)(GLenum, GLenum, GLsizei, const char *);
void fake_glProgramStringARB(GLenum target, GLenum format, GLsizei len, 
                             const char *program)
{
    // printf("ProgramString: %s\n", program);
    real_glProgramStringARB(target, 0, 0, "");
}

// glActiveTextureARB
void (*real_glActiveTextureARB)(GLenum); 
void fake_glActiveTextureARB(GLenum texture)
{
    if (_call_count == 0 && texture < 25)
        real_glActiveTextureARB(texture);
}

// glClientActiveTextureARB
void (*real_glClientActiveTextureARB)(GLenum);

void fake_glClientActiveTextureARB(GLenum texture)
{
    if (_call_count == 0)
        real_glClientActiveTextureARB(texture);
}

// glVertexAttribPointer
void (*real_glVertexAttribPointer)(GLuint, GLint, GLenum, GLboolean, 
                                   GLsizei, const GLvoid *);
void fake_glVertexAttribPointer(GLuint index, GLint size, GLenum type, 
                                 GLboolean normalized, GLsizei stride, 
                                 const GLvoid *pointer)
{
    if (_call_count == 0)
        real_glVertexAttribPointer(index, size, type, normalized, stride, pointer);
}

// glBufferDataARB
void (*real_glBufferData)(GLenum, GLsizeiptr, GLvoid *, GLenum);
void fake_glBufferData(GLenum tar, GLsizeiptr sz, GLvoid *data, GLenum usage)
{
    real_glBufferData(tar, 1, data, usage);
}

// glBufferSubDataARB
void (*real_glBufferSubData)(GLenum, GLintptr, GLsizeiptr, const GLvoid *);
void fake_glBufferSubData(GLenum target, GLintptr offset, 
                          GLsizeiptr size, const GLvoid *data)
{
    // if (_call_count == 0)
    real_glBufferSubData(target, offset, 1, data);
}

// glBindBufferARB *** UNSAFE ***
void (*real_glBindBuffer)(GLenum, GLuint);
void fake_glBindBuffer(GLenum target, GLuint buffer)
{
    if (_call_count == 0)
        real_glBindBuffer(target, buffer);
} 

// glBindProgramARB
void (*real_glBindProgramARB)(GLenum, GLuint);
void fake_glBindProgramARB(GLenum target, GLuint id)
{
    // printf("BindProgramARB: %d\n", id);
    real_glBindProgramARB(target, id);
}

// glMatrixMode
void (*real_glMatrixMode)(GLenum);
void fake_glMatrixMode(GLenum mode)
{
    if (_call_count == 0)
        real_glMatrixMode(mode);
}

// glClear
void (*real_glClear)(GLbitfield mask);
void fake_glClear(GLbitfield mask)
{
    if (_call_count == 0)
        real_glClear(mask);
}

// glClearColor
void (*real_glClearColor)(GLclampf, GLclampf, GLclampf, GLclampf);
void fake_glClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha)
{
    if (_call_count == 0)
        real_glClearColor(red, green, blue, alpha);
}

// glLoadMatrixf
void (*real_glLoadMatrixf)(const GLdouble *m);
void fake_glLoadMatrixf(const GLdouble *m)
{
    if (_call_count == 0)
        real_glLoadMatrixf(m);
}

// glDepthMask
void (*real_glDepthMask)(GLboolean flag);
void fake_glDepthMask(GLboolean flag)
{
    if (_call_count == 0)
        real_glDepthMask(flag);
}

func_entry replaces[] = {
//E("glXGetProcAddressARB",     &real_glXGetProcAddress,        &fake_glXGetProcAddress),
  E("glXSwapBuffers",           &real_glXSwapBuffers,           &fake_glXSwapBuffers),
  E("glDrawRangeElementsEXT",   &real_glDrawRangeElementsEXT,   &fake_glDrawRangeElementsEXT),
  E("glDrawArrays",             &real_glDrawArrays,             &fake_glDrawArrays),
//E("glEnable",                 &real_glEnable,                 &fake_glEnable),
//E("glDisable",                &real_glDisable,                &fake_glDisable),
//E("glEnableClientState",      &real_glEnableClientState,      &fake_glEnableClientState),
//E("glDisableClientState",     &real_glDisableClientState,     &fake_glDisableClientState),
//E("glTexGeni",                NULL,                           &__nop),
//E("glTexGenf",                NULL,                           &__nop),
//E("glTexGeniv",               NULL,                           &__nop),
//E("glTexGenfv",               NULL,                           &__nop),
//E("glTexEnvi",                NULL,                           &__nop),
//E("glTexEnvf",                NULL,                           &__nop),
//E("glTexEnviv",               NULL,                           &__nop),
//E("glTexEnvfv",               NULL,                           &__nop),
//E("glTexImage2D",             NULL,                           &__nop),
//E("glTexSubImage2D",          NULL,                           &__nop),
//E("glTexParameteri",          NULL,                           &__nop),
//E("glTexCoordPointer",        NULL,                           &__nop),
//E("glMaterialf",              NULL,                           &__nop),
//E("glMaterialfv",             NULL,                           &__nop),
//E("glFogf",                   NULL,                           &__nop),
//E("glFogfv",                  NULL,                           &__nop),
//E("glLightModelfv",           NULL,                           &__nop),
//E("glLightModeliv",           NULL,                           &__nop),
//E("glDepthRange",             NULL,                           &__nop),
//E("glEnableVertexAttribArrayARB",   NULL,                     &__nop),
//E("glDisableVertexAttribArrayARB",  NULL,                     &__nop),
//E("glCompressedTexImage2DARB",NULL,                           &__nop),
//E("glCompressedTexSubImage2DARB",NULL,                        &__nop),
//E("glActiveTextureARB",       &real_glActiveTextureARB,       &fake_glActiveTextureARB),
//E("glClientActiveTextureARB", &real_glClientActiveTextureARB, &fake_glClientActiveTextureARB),
//E("glBufferDataARB",          &real_glBufferData,             &fake_glBufferData),
//E("glBufferSubDataARB",       &real_glBufferSubData,          &fake_glBufferSubData),
//E("glBindBufferARB",          &real_glBindBuffer,             &fake_glBindBuffer),
//E("glGenProgramsARB",         NULL,                           &__nop),
//E("glBindProgramARB",         &real_glBindProgramARB,         &fake_glBindProgramARB),
//E("glProgramStringARB",       &real_glProgramStringARB,       &fake_glProgramStringARB),
//N("glProgramEnvParameters4fvEXT"),
//N("glVertexAttribPointerARB"),
//E("glGetProgramivARB",        NULL,                       &__nop),
//E("glMatrixMode",             &real_glMatrixMode,             &fake_glMatrixMode),
//E("glClear",                  &real_glClear,                  &fake_glClear),
//E("glClearColor",             &real_glClearColor,             &fake_glClearColor),
//E("glDepthMask",              &real_glDepthMask,              &fake_glDepthMask),
//E("glLoadMatrixf",            &real_glLoadMatrixf,            &fake_glLoadMatrixf),
//E("glLoadIdentity",           NULL,                           &__nop),
//E("glViewport",               NULL,                           &__nop),
//E("glPixelStorei",            NULL,                           &__nop),
//E("glColor4fv",               NULL,                           &__nop),
//E("glVertexPointer",          NULL,                           &__nop),
//E("glNormalPointer",          NULL,                           &__nop),
//E("glColorPointer",           NULL,                           &__nop),
//E("glPolygonOffset",          NULL,                           &__nop),
//E("glFrontFace",              NULL,                           &__nop),
//E("glBlendFunc",              NULL,                           &__nop),
//E("glAlphaFunc",              NULL,                           &__nop),
//E("glDepthFunc",              NULL,                           &__nop),
//E("glScissor",                NULL,                           &__nop),
};

void *wine_sym_filter(void *handle, const char *symbol, void *func)
{
    func_entry *entry;
    for (entry = replaces; entry - replaces < sizeof(replaces) / sizeof(func_entry); entry++) {
        if (!strcmp(symbol, entry->name)) {
            printf("Hooking %s\n", symbol);
            if (entry->real != NULL)
                *entry->real = func;
            return entry->fake;
        }
    }

    // printf("dlsym: %s\n", symbol);
    return func;
}
