{-# LANGUAGE OverloadedStrings #-}

module WoW.Wotlk 
    ( module WoW.Wotlk.Core
    , module WoW.Wotlk.TP
    , loadDefaultTPList
    ) where
    
import Control.Monad

import qualified Data.ByteString as BS
import Data.Serialize

import System.Directory

import WoW.Wotlk.Core
import WoW.Wotlk.TP

loadDefaultTPList :: IO TPList
loadDefaultTPList = do
    home <- getHomeDirectory
    dirs <- filterM doesFileExist $
        ["tplist.raw", home++"/tplist.raw"]
    if null dirs then do
        return []
    else do
        loadTPList (head dirs)

