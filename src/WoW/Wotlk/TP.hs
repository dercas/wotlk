{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}

module WoW.Wotlk.TP where

import Codec.Compression.GZip

import Control.Monad

import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString as BS
import Data.List (sortOn)
import Data.Maybe
import Data.Serialize

import GHC.Exts
import GHC.Generics

import WoW.Wotlk.Core

type TPList = [TPEntry]

data TPEntry = TPEntry
    { tp_name   :: !ByteString
    , tp_p      :: !Point
    } deriving (Show, Eq, Ord, Generic)

instance Serialize TPEntry where

failedEntry :: ByteString -> TPEntry
failedEntry err = TPEntry ("err " `BS.append` err) emptyPoint

decodeTPList :: BL.ByteString -> TPList
decodeTPList = decode' (pure . failedEntry . fromString) 

encodeTPList :: TPList -> BL.ByteString
encodeTPList = compress . encodeLazy

loadTPList :: FilePath -> IO TPList
loadTPList fp = do
    bs <- BL.readFile fp
    return $ decodeTPList bs

saveTPList :: FilePath -> TPList -> IO ()
saveTPList fp tpl = when (["err"] /= (tp_name <$> tpl)) $ do
    BL.writeFile fp $ encodeTPList tpl

load :: Serialize a => FilePath -> IO a
load fp = do 
    bs <- BL.readFile fp
    bs `seq` (return $ decode' error bs)

save :: Serialize a => FilePath -> a -> IO ()
save fp e = e `seq` BL.writeFile fp (encode' e)

type ConnectionList = [ConnectionEntry]

data ConnectionEntry = ConnectionEntry
    { c_o   ::  Point
    , c_d   ::  Float
    , c_f   ::  Point
    } deriving (Show, Eq, Ord, Generic)

instance Serialize ConnectionEntry where

getConnection :: ConnectionList -> Point -> Maybe Point
getConnection rs p =
    let d' p' = if c_d p' < 0 then 
                c_d p' + pointDist (c_o p') p
            else
                c_d p' - pointDist (c_o p') p
        ds = (\p' -> (c_f p', d' p')) <$> rs
        eqmap c = case p_mid p of
            Nothing -> True
            m -> m == p_mid c
    in fmap fst $ listToMaybe $ reverse 
        $ sortOn snd $ filter (\(c,d) -> d >= 0 && eqmap c) ds

decodeConnectionList :: BL.ByteString -> ConnectionList
decodeConnectionList bs = either (return []) id 
    (decodeLazy =<< try' (decompress bs))

encodeConnectionList :: ConnectionList -> BL.ByteString
encodeConnectionList = compress . encodeLazy

loadConnectionList :: FilePath -> IO ConnectionList
loadConnectionList fp = do
    decodeConnectionList <$> BL.readFile fp

saveConnectionList :: FilePath -> ConnectionList -> IO ()
saveConnectionList fp rl = BL.writeFile fp (encodeConnectionList rl)


type RouteList = [RouteEntry]

type RouteEntry = [(Point, Bool)]

loadRouteList :: FilePath -> IO RouteList
loadRouteList = load

saveRouteList :: FilePath -> RouteList -> IO ()
saveRouteList = save

-- utils
--------------------------------------------------------------------------------

decode' :: Serialize a => (String -> a) -> BL.ByteString -> a
decode' f bs = either (f . head . lines) id $ decodeLazy =<< try' (decompress bs)

encode' :: Serialize a => a -> BL.ByteString
encode' = compress . encodeLazy

portRoute :: FilePath -> FilePath -> IO ()
portRoute tup4 rout = do
    ps <- (fmap tuple4p) <$> load tup4 :: IO [Point]
    saveRouteList rout $ (\p -> [(p { p_z = Just (-495) }, True), (p,False)]) <$> ps

    where
        -- portP o = [(p { p_z = min (-495) 

