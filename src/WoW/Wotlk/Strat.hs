{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}

module WoW.Wotlk.Strat where

import Control.Monad
import Control.Monad.State

import qualified Data.ByteString.Char8 as B
import Data.Char
import Data.IORef
import Data.Function (on)
import Data.Maybe
import qualified Data.List as L
import qualified Data.Map as Ma

import System.Directory
import System.FilePath

import Text.Parsec as P

import UI.NCurses
import UI.NCurses.Menu hiding (setBackground)
import qualified UI.NCurses.Menu as M

import WoW.Wotlk.Core
import WoW.Wotlk.Box hiding (Key)

import XHotkey hiding (io, Window)

data TEntry = TEntry
  { t_name  :: String
  , t_ps    :: [(String, PS)]
  } deriving (Show, Eq)

type Prog = [TEntry]

p_groups :: Prog -> [String]
p_groups tes = L.nub $ tes >>= \te -> fst <$> t_ps te

-- data Prog = Prog
--   { p_groups  :: [String]
--   , p_es      :: [TEntry]
--   , p_in      :: [FilePath]
--   } deriving (Show, Eq)


instance Read TEntry where
    readsPrec _ = parseReads parseTEntry
    -- readPrec = do
    --     name <- T.readPrec
    --     T.lift skipSpaces
    --     '{' <- T.get
    --     T.lift skipSpaces
    --     ps <- T.choice
    --       [ do
    --           readps
    --       , do
    --           p <- T.readPrec
    --           when (null $ ps p) T.pfail
    --           return [("all", p)]]
    --     T.lift skipSpaces
    --     '}' <- T.get
    --     return $ TEntry name ps where
    --         readps = T.choice
    --           [ do
    --               nams <- readnams
    --               T.lift skipSpaces
    --               '{' <- T.get
    --               p <- T.readPrec
    --               T.lift skipSpaces
    --               '}' <- T.get
    --               T.lift skipSpaces
    --               ([(n,p) | n <- nams] ++) <$> readps
    --           , return []
    --           ]
    --         readnams = T.choice
    --           [ do
    --               nam <- T.readPrec
    --               nams <- readnams
    --               return (nam:nams)
    --           , return []]
                  
parseTEntry :: Stream s m Char => ParsecT s u m TEntry
parseTEntry = do
    liftM2 TEntry (spacesaft parseName)
      $ between (char '{') (char '}') $ do
          spaces
          tentries <- choice
            [ fmap join $ many1 $ spacesaft $ P.try $
                do spaces
                   groups <- P.try $ parseName `sepEndBy1` space
                   ps' <- between (spacesaft $ char '{') (char '}') (spacesaft $ parsePS)
                   return [(g, ps') | g <- groups]
            , pure . (,) "all" <$> parsePS
            ]
          spaces
          return tentries
--               spaces
--               groups <- option ["all"] (P.try $ many1 (spacesaft parseName))
--               spaces
--               ps <- between (spacesaft (char '{')) (char '}') (spacesaft parsePS)
--               return [(g, ps) | g <- groups]
    where
    parseName = P.try parseString <|> parseIdent


-- instance Monoid Prog where
--     mempty = Prog [] [] []
--     p1 `mappend` p2 = Prog 
--                         (L.union (p_groups p1) (p_groups p2))
--                         (L.union (p_es p1) (p_es p2))
--                         (L.union (p_in p1) (p_in p2))
-- 
-- instance Read Prog where
--     readPrec = T.choice
--       [ do
--           p <- T.readPrec
--           prog <- T.readPrec
--           return prog { p_es = p : p_es prog
--                       , p_groups = L.union (fst <$> t_ps p) (p_groups prog) }
--       , do
--           getstring "Include "
--           ws
--           fp <- T.readPrec
--           prog <- T.readPrec
--           return prog { p_in = fp : p_in prog }
--       , do
--           return $ Prog [] [] []
--       ]

parseProg :: Stream s IO Char => (FilePath -> IO s) -> ParsecT s FilePath IO Prog
parseProg reader = fmap join $ flip manyTill eof $ spacesaft $ choice $ P.try <$> 
  [ do "include" <- fmap toLower <$> many1 letter
       spaces
       file <- parseString
       cwd <- P.getState
       let newcwd = cwd </> takeDirectory file
           filepath = cwd </> file
       eith <- io $ runParserT (parseProg reader) newcwd (takeFileName file) 
               =<< reader filepath
       either (fail . show) return eith
  , pure <$> parseTEntry
  ]


readProg :: FilePath -> IO Prog
readProg fp = do
    source <- B.readFile fp
    either (error . show) id <$> runParserT (parseProg B.readFile) 
                                            (takeDirectory fp) fp source
    -- prog <- read <$> readFile fp
    -- progs <- forM (p_in prog) readProg
    -- return $ mconcat (progs ++ [prog])


data Colors = Colors
  { cblk  :: ColorID
  , cblk' :: ColorID
  , cwhi  :: ColorID
  , cwhi' :: ColorID
  , cblu  :: ColorID
  , cblu' :: ColorID
  }

newColors = do
    cblk <- newColorID ColorWhite ColorBlack 10
    cblk' <- newColorID ColorBlack ColorWhite 11
    cwhi <- newColorID ColorBlack ColorWhite 12
    cblu <- newColorID ColorWhite ColorBlue 13
    cblu' <- newColorID ColorBlue ColorWhite 14
    return $
      Colors cblk cblk' cwhi cwhi cblu cblu'

data UI = UI
  { cols      :: Colors 
  , currM     :: M
  , selectM   :: M
  , groupMs   :: Ma.Map String M
  , groups    :: Ma.Map String (IORef [WEnv])
  , entriesM  :: M
  , allwows   :: [WEnv]
  , entries   :: [TEntry]
  }

type RT = StateT UI Curses

data M = M
  { cmpwin    :: Window
  , refresh   :: RT ()
  , onevent   :: Event -> RT ()
  , waitev    :: RT ()
  }

instance Eq M where
    (==) = (==) `on` cmpwin

reloadAll :: RT ()
reloadAll = do
    ui <- get
    wows <- liftIO $ mapM newWEnv =<< xWows
    liftIO $ flip Ma.traverseWithKey (groups ui) $ \k ref -> do
        envs <- readIORef ref
        let envs' = L.filter (\i -> any (== i) wows) envs
        writeIORef ref envs'
    put ui { allwows = wows } 


selectProg :: Colors -> Curses Prog
selectProg cols = do
    (h,w) <- screenSize
    let h' = 20
        w' = 60
    win <- newWindow h' w' ((h-h') `quot` 2) ((w-w') `quot` 2)
    win' <- subWindow win (h'-4) (w'-4) ((h-h'+4) `quot` 2) ((w-w'+4) `quot` 2)

    files <- fmap L.sort $ liftIO $ getDirectoryContents "."
    names <- fmap catMaybes $ forM files $ \n -> do
        isdir <- io $ doesDirectoryExist n
        if n == "." then
            return Nothing
        else if isdir then
            return $ Just $ Right n
        else if L.isSuffixOf ".prog" n then
            return $ Just $ Left n
        else
            return Nothing
    its <- forM2 (shortcutchars ++ repeat ' ') names $ \c n -> do
        let c' = if c == ' ' then "  " else [c,':']
            name = pad 45 $  case n of
                Left n' -> " " ++ take (length n' - 5) n'
                Right n' -> "/" ++ n'
        newItem c' name

    menu <- newMenu its
    setWin menu win
    setSubWin menu win'
    setMark menu " "
    setFormat menu (fromIntegral h' - 4) 2
    disableOpts menu [NonCyclic]
    M.setBackground menu [AttributeColor $ cblu cols]
    M.setForeground menu [AttributeBold, AttributeColor $ cwhi cols]
    updateWindow win $ do
        setBackgroundFilled [AttributeColor $ cblu cols]
        drawBox Nothing Nothing
        moveCursor 0 2
        inBold $ drawString " Select Program "
    updateWindow win' $ do
        setBackgroundFilled [AttributeColor $ cblu cols]
    postMenu menu
    i <- getsortindex <$> getEventM menu
    desc <- fmap (names !!) $ (maybe (fromIntegral <$> getIndex menu) return i)
    case desc of
        Left prog -> io $ readProg prog
        Right dir -> do
            io $ setCurrentDirectory dir
            selectProg cols

new_pidM :: Colors -> String -> Curses M
new_pidM cols name = do
    win <- newWindow 14 30 0 0
    swin <- subWindow win 12 28 1 1
    menu <- newMenu []
    setWin menu win
    setSubWin menu swin
    setFormat menu 12 1
    setMark menu " "
    disableOpts menu [NonCyclic]
    setfor cols menu
    postMenu menu
    let refresh' = get >>= \ui -> do
        m <- maybe undefined return $ Ma.lookup name (groupMs ui)
        refresh m
    let m = M { cmpwin = win
      , refresh = get >>= \ui -> do
          reloadAll    
          UI { allwows = wows } <- get
          lift $ do
              inCurrentIndices menu $ do
                  unpostMenu menu
                  setcol ui menu m
                  sels <- io $ maybe (return []) readIORef 
                    $ Ma.lookup name (groups ui)
                  its <- forM (wows L.\\ sels) $ \env -> do
                      newItem (show $ wenv_pid env) (B.unpack $ wenv_name env)
                  setItems menu its
                  postMenu menu
                  updateWindow win $ do
                      drawBox Nothing Nothing
                      moveCursor 0 2
                      inBold $ drawString " unselected "
              refreshWindow win
      , onevent = \ev -> get >>= \ui -> case ev of
          EventCharacter ' ' -> do
              lift $ inCurrentIndices menu $ withCurrentItem menu $ \it -> do
                  p <- read <$> itemName it
                  io $ do
                      p' <- newWEnv p
                      let ref = maybe undefined id $ Ma.lookup name (groups ui)
                      xs <- readIORef ref
                      writeIORef ref (L.union [p'] xs)
              refresh'
              waitev m
          EventCharacter 'q' -> return ()
          _| any' ev [Left $ KeyFunction 5, Right 'r'] -> do
              refresh m
              waitev m
          _| any' ev [Left KeyRightArrow, Right 'l'] -> do
              let m = maybe undefined id $ Ma.lookup name (groupMs ui)
              put ui { currM = m }
              refresh'
              waitev m
          _ -> do
              lift $ onEventM menu ev
              waitev m
      , waitev = get >>= \ui -> do
          Just ev <- lift $ getEvent win Nothing
          onevent (currM ui) ev
      }
    return m

new_groupM :: Colors -> String -> Curses M
new_groupM cols name = do
    win <- newWindow 14 30 0 30
    swin <- subWindow win 12 28 1 31 
    menu <- newMenu []
    setWin menu win
    setSubWin menu swin
    setMark menu " "
    setFormat menu 12 1
    disableOpts menu [NonCyclic]
    setfor cols menu
    pid <- new_pidM  cols name
    let updateWows ui = do
        its <- mapM (fmap read . itemName) =<< menuItems menu
        wows <- io $ mapM newWEnv its
        let ref = maybe undefined id $ Ma.lookup name (groups ui)
        io $ writeIORef ref wows
    let m = M { cmpwin = win
      , refresh = get >>= \ui -> do
          refresh pid
          lift $ do
              inCurrentIndices menu $ do
                  unpostMenu menu
                  setcol ui menu m
                  wows <- io $ maybe (return []) readIORef 
                    $ Ma.lookup name (groups ui)
                  items <- forM wows $ \env -> do
                      newItem (show $ wenv_pid env) (B.unpack $ wenv_name env)
                  setItems menu items
                  updateWindow win $ do
                      drawBox Nothing Nothing
                      moveCursor 0 2
                      drawString $ " selected - " ++ name ++ " "
                  postMenu menu
              refreshWindow win
      , onevent = \ev -> get >>= \ui -> case ev of
          EventCharacter ' ' -> do
              lift $ inCurrentIndices menu $ withCurrentItem menu $ \it -> do
                  p <- read <$> itemName it
                  io $ do
                      let ref = maybe undefined id $ Ma.lookup name (groups ui)
                      xs <- readIORef ref
                      let xs' = L.filter (\e -> wenv_pid e /= p) xs
                      writeIORef ref xs'
              refresh m
              waitev m
          (EventCharacter 'r') -> do
              reloadAll
              waitev m
          EventCharacter 'q' -> return ()
          EventCharacter 'K' -> do
              lift $ onEventM menu ev
              lift $ updateWows ui
              waitev m
          _| any' ev [Left KeyLeftArrow, Right 'h'] -> do
              put ui { currM = pid }
              refresh m
              waitev pid
          _| any' ev [Left KeyRightArrow, Right 'l'] -> do
              let m = selectM ui
              put ui { currM = m }
              refresh m
              waitev m
          _ -> do
              lift $ onEventM menu ev
              waitev m
      , waitev = get >>= \ui -> do
          Just ev <- lift $ getEvent win Nothing
          onevent (currM ui) ev
      }
    return m

new_selectM :: Colors -> [String] -> Curses M
new_selectM cols names = do
    win <- newWindow 14 20 0 60
    swin <- subWindow win 12 18 1 61
    menu <- newMenu =<< mapM (\i -> newItem i "") names
    setWin menu win
    setSubWin menu swin
    setMark menu " "
    setFormat menu 12 1
    disableOpts menu [NonCyclic]
    setfor cols menu
    postMenu menu
    let m = M { cmpwin = win
      , refresh = get >>= \ui -> do
          lift $ inCurrentIndices menu $ do
              unpostMenu menu
              setcol ui menu m 
              updateWindow win $ do
                  drawBox Nothing Nothing
                  moveCursor 0 2
                  inBold $ drawString " groups "
              postMenu menu
              refreshWindow win
          str <- lift $ maybe (return "all") itemName =<< currentItem menu
          refresh $ maybe undefined id $ Ma.lookup str (groupMs ui)
      , onevent = \ev -> get >>= \ui -> case ev of
          EventCharacter 'r' -> do
              refresh m
              waitev m
          EventCharacter 'q' -> do
              return ()
          _| any' ev [Left KeyLeftArrow, Right 'h'] -> do
              str <- lift $ maybe (return "all") itemName =<< currentItem menu
              let m' = maybe undefined id $ Ma.lookup str (groupMs ui) 
              put ui { currM = m' }
              refresh m
              refresh m'
              waitev m'
          _ -> do
              lift $ onEventM menu ev
              refresh m
              waitev m
      , waitev = get >>= \ui -> do
          Just ev <- lift $ getEvent win Nothing
          onevent (currM ui) ev
      }
    return m

new_entriesM :: Colors -> [TEntry] -> Curses M
new_entriesM cols tes = do
    win <- newWindow 10 80 14 0
    swin <- subWindow win 8 78 15 1
    its <- forM2 shortcutchars tes $ \s e -> do
        newItem [s,':'] (t_name e)
    forM_ (every 2 its) $ \it -> do
        selectable it False
    menu <- newMenu its
    setWin menu win
    setSubWin menu swin
    setFormat menu 8 5
    setMark menu " "
    disableOpts menu [OneValue]
    M.setForeground menu [AttributeColor $ cblk $ cols]
    M.setBackground menu [AttributeColor $ cblk $ cols]
    M.setUnselectable menu [AttributeColor $ cblk $ cols, AttributeUnderline]
    updateWindow win $ setBackgroundFilled [AttributeColor $ cblk cols]
    postMenu menu
    let m = M { cmpwin = win
      , refresh = get >>= \ui -> lift $ do
          updateWindow win $ do
              drawBox Nothing Nothing
              moveCursor 0 2
              inBold $ drawString " entries "
          render
      , onevent = \ev -> get >>= \ui -> case ev of
          _ -> waitev m
      , waitev = get >>= \ui -> do
          Just ev <- lift $ getEvent win Nothing
          onevent (currM ui) ev
      }
    return m

triggerTE' :: Ma.Map String (IORef [WEnv]) -> TEntry -> IO ()
triggerTE' groups te = do
    forM_ (t_ps te) $ \(s,p) -> do
        if s == "self" then void $ inCurrentWoW $ do
            tp' (head $ ps_ps p) (ps_flags p)
        else void $ do
            ws <- maybe (return []) readIORef (Ma.lookup s groups)
            zipWithM (f $ ps_flags p) ws (cycle $ ps_ps p) where
                f flag wow p = do
                    runStateT (reload >> tp' p flag) wow


startProg :: Colors -> Prog -> Curses Bool
startProg cols prog = do
    fx <- io $ runForkedX $ do
        flushX
        mainLoop

    def <- defaultWindow
    updateWindow def clear

    let es = prog -- p_es prog
    let gs = L.sort (L.union ["all"] $ p_groups prog) L.\\ ["self"]

    groups' <- forM gs $ \g -> do
        m <- new_groupM cols g
        return (g,m)

    refs <- fmap Ma.fromList $ io $ forM gs $ \g -> do
        r <- newIORef []
        when (g == "all") $ do
            ws <- mapM newWEnv =<< xWows
            writeIORef r ws
        return (g,r)
    
    let bind' km te = bind km (io $ triggerTE' refs te)
    io $ queueX fx $ do
        zipWithM_ bind' shortcuts es

    sel <- new_selectM cols gs
    ent <- new_entriesM cols es 
    runStateT (reloadAll >> refresh ent >> refresh sel >> waitev sel) 
      $ UI cols sel sel (Ma.fromList groups') refs ent [] es 
    io $ exitForkedX fx
    return True
    

main :: IO ()
main = void $ do
    runCurses $ do
        colors <- newColors
        def <- defaultWindow
        updateWindow def $ do
            setBackgroundFilled [AttributeColor $ cblk colors]
        setEcho False
        setCursorMode CursorInvisible
        mouseInterval 50
        selectProg colors >>= startProg colors


onEventM :: Menu -> Event -> Curses Bool 
onEventM menu ev = do
    let lift' f = f >> return True
    case ev of
        -- up an item
        (EventSpecialKey KeyUpArrow) -> lift' $ do
            request menu UpItem
            upU menu
            downU menu
        (EventCharacter 'k') -> lift' $ do
            request menu UpItem
            upU menu
            downU menu
        -- up three items
        (EventMouse _ MouseState 
            {mouseShift=False, mouseButtons = (4,_):_}) -> lift' $ do
                request menu UpItem
                request menu UpItem
                request menu UpItem
                upU menu
                downU menu
        -- down an item
        (EventSpecialKey KeyDownArrow) -> lift' $ do
            request menu DownItem
            downU menu
            upU menu
        (EventCharacter 'j') -> lift' $ do
            request menu DownItem
            downU menu
            upU menu
        -- down three items
        (EventMouse _ MouseState {mouseShift=False, mouseButtons = (5,_):_}) -> lift' $ do
            request menu DownItem
            request menu DownItem
            request menu DownItem
            downU menu
            upU menu
        -- right an item
        (EventSpecialKey KeyRightArrow) -> do
            (_,c) <- menuFormat menu
            if c == 1 then
                return False
            else do
                request menu RightItem
                return True
        -- left an item
        (EventSpecialKey KeyLeftArrow) -> do
            (_,c) <- menuFormat menu
            if c == 1 then
                return False
            else do
                request menu LeftItem
                return True
        -- up a page
        (EventSpecialKey KeyPreviousPage) -> lift' $ do
            request menu UpPage
            downU menu
            upU menu
        (EventCharacter 'b') -> lift' $ do
            request menu UpPage
            upU menu
            downU menu
        -- down a page
        (EventSpecialKey KeyNextPage) -> lift' $ do
            request menu DownPage
            downU menu
            upU menu
        (EventCharacter 'f') -> lift' $ do
            request menu DownPage
            downU menu
            upU menu
        -- to the top
        (EventSpecialKey KeyHome) -> lift' $ do
            request menu FirstItem
            downU menu
            upU menu
        (EventCharacter 'g') -> lift' $ do
            request menu FirstItem
            downU menu
            upU menu
        -- to the bottom
        (EventSpecialKey KeyEnd) -> lift' $ do
            request menu LastItem
            upU menu
            downU menu
        (EventCharacter 'G') -> lift' $ do
            request menu LastItem
            upU menu
            downU menu
        -- up half a page
        (EventCharacter 'u') -> lift' $ do
            (n,_) <- menuFormat menu
            replicateM (fromIntegral n`quot`2) (request menu UpLine)
            upU menu
            downU menu
        (EventCharacter 'd') -> lift' $ do
            (n,_) <- menuFormat menu
            replicateM (fromIntegral n`quot`2) (request menu DownLine)
            downU menu
            upU menu
        (EventCharacter 'J') -> lift' $ do
            unpostMenu menu
            moveDown menu
            postMenu menu
        (EventCharacter 'K') -> lift' $ do
            unpostMenu menu
            moveUp menu
            postMenu menu
        EventCharacter '\EOT' -> lift' $ do 
            unpostMenu menu
            inCurrentIndices menu $ do
                mapM_ (removeItem menu) =<< currentItem menu
            postMenu menu
        _ -> return False

upU :: Menu -> Curses ()
upU = passBy UpItem isSelectable 

downU :: Menu -> Curses ()
downU = passBy DownItem isSelectable 

upS :: Menu -> Curses ()
upS = passBy UpItem (fmap not . isSelectable)

downS :: Menu -> Curses ()
downS = passBy DownItem (fmap not . isSelectable)

moveUp :: Menu -> Curses ()
moveUp menu = do
    _it <- currentItem menu
    forM_ _it $ \it -> do
        t <- topRow menu
        i <- fromIntegral <$> itemIndex it
        when (i>0) $ do
            swapIndices menu (i-1) i
            when (t>0) $
                setTopRow menu (t-1)
            setCurrent menu it

moveDown :: Menu -> Curses ()
moveDown menu = do
    _it <- currentItem menu
    forM_ _it $ \it -> do
        t <- topRow menu
        i <- fromIntegral <$> itemIndex it
        n <- itemCount menu
        when (i < fromIntegral n-1) $ do
            swapIndices menu (i+1) i
            when (t<n-1) $
                setTopRow menu (t+1)
            setCurrent menu it

passBy :: Request -> (Item -> Curses Bool) -> Menu -> Curses ()
passBy req cmp menu = do
    curr <- currentItem menu
    forM_ curr $ \it -> do
        sel <- cmp it
        unless sel $ do
            request menu req
            curr' <- currentItem menu
            unless (curr' == Just it) $ passBy req cmp menu

getEventM :: Menu -> Curses Event
getEventM menu = do
    w <- menuWin menu
    Just ev <- getEvent' w Nothing
    b <- onEventM menu ev
    if b then
        getEventM menu
    else
        return ev

getEventMH :: Menu -> [(Integer, RT ())] -> RT Event
getEventMH menu hs = do
    w <- lift $ menuWin menu
    ev <- getEventH (lift $ getEvent' w (Just 10)) 10 hs
    b <- lift $ onEventM menu ev
    if b then
        getEventMH menu hs
    else
        return ev

getEventH :: RT (Maybe Event) -> Integer -> [(Integer, RT ())] -> RT Event
getEventH getter step hs = oev 0 where
    oev i = do
        ev <- getter
        case ev of
            Nothing -> do
                traverse snd $ filter (\(m,_) -> i `mod` m < step) hs
                oev (i+step)
            Just ev' -> return ev'

forM2 :: Monad m => [a] -> [b] -> (a -> b -> m c) -> m [c]
forM2 xs ys f = zipWithM f xs ys

any' :: Event -> [Either Key Char] -> Bool
any' ev xs = any (==ev) $ either EventSpecialKey EventCharacter <$> xs

shortcuts :: [[KM]]
shortcuts = (\c -> ["scroll-num-Tab", scroll_ $ num_ $ fromChar c]) 
              <$> shortcutchars

shortcutchars :: String
shortcutchars = "12345qwertasdfgzxcvb67890yuoip;nm,./"

inBold :: Update a -> Update ()
inBold op = do
    setAttribute AttributeBold True
    op
    setAttribute AttributeBold False

getsortindex :: Event -> Maybe Int
getsortindex ev = case ev of
    EventCharacter c -> L.elemIndex c shortcutchars
    _ -> Nothing

every :: Int -> [a] -> [a]
every n xs = case drop (n-1) xs of
              (y:ys) -> y : every n ys
              [] -> []

setfor :: Colors -> Menu -> Curses ()
setfor columns menu = M.setForeground menu 
    [AttributeColor $ cblk columns, AttributeReverse, AttributeBold]

setcol :: UI -> Menu -> M -> Curses ()
setcol ui menu m = do
    w <- menuWin menu
    sw <- menuSubWin menu
    if currM ui == m then do
        updateWindow w 
          $ setBackgroundFilled [AttributeColor $ cblu $ cols ui]
        updateWindow sw 
          $ setBackgroundFilled [AttributeColor $ cblu $ cols ui]
        M.setBackground menu 
          [AttributeColor $ cblu $ cols ui]
    else do
        updateWindow w 
          $ setBackgroundFilled [AttributeColor $ cblk $ cols ui]
        updateWindow sw 
          $ setBackgroundFilled [AttributeColor $ cblk $ cols ui]
        M.setBackground menu 
          [AttributeColor $ cblk $ cols ui]

pad :: Int  -> String -> String
pad n str = take n str ++ replicate (length str - n - 1) '_'
