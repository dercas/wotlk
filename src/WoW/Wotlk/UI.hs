{-# LANGUAGE OverloadedStrings #-}

import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.State
import Control.Monad.IO.Class

import Data.ByteString.Char8 (pack, unpack)
import Data.Char
import Data.Foldable
import Data.IORef
import Data.List (union, delete)
import Data.Maybe

import Foreign hiding (void)

import Graphics.X11.Xlib.Extras (XErrorHandler, setErrorHandler)

import Numeric

import System.Environment
import System.Posix (CPid)
import System.Process (readProcess, callCommand)

import Text.Read (readMaybe)
import Text.Printf (printf)

import UI.NCurses
import UI.NCurses.Menu
import qualified UI.NCurses.Form as F

import WoW.Wotlk.Core hiding (tarPos)
import qualified WoW.Wotlk.Core as C
import WoW.Wotlk.TP
import WoW.Wotlk.X

import XHotkey hiding (io, Window)


data UI = UI
    { mainM   :: M
    , pidM    :: M
    , multiM  :: M
    , stateM  :: M
    , destM   :: M
    , hisM    :: M
    , tpM     :: M
    , routeM  :: M
    , miscM   :: M
    , confM   :: M
    , p1M     :: M
    , swow    :: WEnv
    , mwow    :: [WEnv]
    , lastPos :: Point
    , tarPos  :: Point
    , col     :: Colors
    , xcFP    :: FilePath
    , tpFP    :: FilePath
    , routeFP :: FilePath
    , conf    :: XConf
    , onlyX   :: Bool
    , onlyW   :: Bool
    } deriving Show

type RT = StateT UI Curses

data Colors = Colors
    { red :: ColorID
    , red' :: ColorID
    , grey :: ColorID
    , black :: ColorID
    , black' :: ColorID
    } deriving Show

data M = M
    { cmpwin    :: Window
    , refresh   :: RT ()
    , onevent   :: Event -> RT ()
    , waitev    :: RT ()
    } 

data F a = F
    { fedit :: a -> RT (Maybe a)
    , fnew  :: RT (Maybe a)
    }

data B = B
    { bdraw     :: RT ()
    , benclosed :: Int -> Int -> RT Bool
    , bpress    :: RT ()
    }
 
instance Show M where
    show _ = ""
instance Eq M where
    m == n = cmpwin m == cmpwin n

main :: IO ()
main = do
    setErrorHandler (\dpy errp -> putStrLn "X Error")

    args <- getArgs

    fx <- runForkedX $ do
        flushX
        mainLoop
    try' $ do
        yama <- readProcess "sysctl" ["-n", "kernel.yama.ptrace_scope"] ""
        when (yama == "1\n") $ do
            putStrLn "kernel.yama.ptrace_scope is enabled, memory reading and\
            \ writing might not work.\n\nPress any key to continue."
            void getChar
    let xcfp = "xconf.raw"
        tpfp = "tplist.raw"
        routefp = "routelist.raw"
    xconf <- io $ loadXConf xcfp
    wr <- io $ newIORef []
    pr <- io $ newIORef emptyPoint
    dr <- io $ newIORef (c_d0 xconf)

    let bind conf = queueX fx $ bindConf conf wr pr dr
    bind xconf

    runCurses $ do
        (h,w) <- screenSize
        when (h<24 || w<80) $ resizeTerminal 24 80
        setEcho False
        setCursorMode CursorInvisible
        mouseInterval 50
        col <- newColorID ColorRed ColorDefault 10
        col' <- newColorID ColorDefault ColorRed 11
        col'' <- newColorID ColorWhite ColorBlack 12
        grey <- newColorID ColorDefault ColorBlack 13
        black' <- newColorID ColorBlack ColorDefault 14
        let colors = Colors col col' grey col'' black'

        mainm <- new_mainM colors
        pidm <- new_pidM colors
        multim <- new_multiM colors
        statem <- new_stateM colors
        destm <- new_destM colors
        hism <- new_hisM colors
        tpm <- new_tpM colors tpfp
        routem <- new_routeM colors routefp
        miscm <- new_miscM colors
        confm <- new_confM colors bind
        let onlyX = any (== "-X") args
            onlyW = any (== "-wine") args

        let ui = UI mainm pidm multim statem destm hism tpm routem miscm confm 
                 pidm nullWEnv [] emptyPoint emptyPoint colors xcfp tpfp routefp 
                 xconf onlyX onlyW
        tryCurses $ runStateT (refresh mainm) ui 
    -- queueX fx (printBindings)
    exitForkedX fx
    return ()
    where
        try' :: IO a -> IO (Either IOException a)
        try' = try


new_mainM :: Colors -> Curses M
new_mainM color = do
    dft <- defaultWindow
    let m = M { cmpwin = dft
        , refresh = get >>= \ui -> do
            lift $ do
                (h,w) <- screenSize
                updateWindow dft $ do
                    clearLines [0..h-1]
                    drawBox Nothing Nothing
                    moveCursor 0 2
                    drawString "┤"
                    setAttribute AttributeBold True
                    drawString " Main "
                    setAttribute AttributeBold False
                    drawString "│ TP List │ Connection List │ Misc │ Config ├"
            refresh (stateM ui)
            refresh (multiM ui)
            refresh (pidM ui)
            refresh (hisM ui)
            refresh (destM ui)
            lift $ setCursorMode CursorInvisible
            waitev (p1M ui)
        , onevent = \ev -> get >>= \ui -> case ev of
            EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                , mouseButtons = (1,_):_} -> clickTabs y x (onevent (p1M ui) ev)
            EventCharacter c 
              | c == 'r' -> do
                refresh m
              | any (c ==) ['t','c','s'] -> do
                onevent (pidM ui) ev
              | any (c ==) ['p','a','o'] -> do
                onevent (multiM ui) ev
              | any (c ==) ['y','T','e','v','n'] -> do
                onevent (destM ui) ev
            EventCharacter 'q' -> mzero
            EventCharacter 'Y' -> do 
                io $ try' $ do
                    pos <- io $ runWoW (wenv_pid $ swow ui) getPos
                    callCommand $ printf "echo -n '%s' | xclip" 
                      (showPoint pos { p_e = Nothing } )
                waitev m
            EventCharacter '\t' -> do
                refresh (tpM ui)
                waitev (tpM ui)
            EventSpecialKey KeyBackTab -> do
                refresh (confM ui)
                waitev (confM ui)
            EventResized -> do
                refresh m
                waitev m
            _ -> onevent (p1M ui) ev
       , waitev = get >>= waitev . p1M
       }
    return m where
        try' :: IO a -> IO (Either IOException a)
        try' = try
            
p1select :: Int -> Int -> RT (Maybe M)
p1select y x = get >>= \ui -> lift $ do
    fmap listToMaybe $ flip filterM [pidM ui, multiM ui, destM ui, hisM ui] $ 
        \m -> enclosed (cmpwin m) y x

new_pidM :: Colors -> Curses M
new_pidM col = do
    win <- newWindow 13 21 1 1
    win' <- subWindow win 9 19 2 2 
    win'' <- newWindow 2 19 11 2
    menu <- newMenu []
    setWin menu win
    setSubWin menu win'
    setMark menu " "
    disableOpts menu [NonCyclic]
    setForeground menu [AttributeColor $ red col, AttributeReverse]
    postMenu menu
    b_tp <- new_B win'' 0 1 10
        (inBold $ do
            drawText "["
            inRed $ drawText "t"
            drawText "eleport]")
        (inBold $ inRev $ do
            drawText "["
            inRed' $ drawText "t"
            drawText "eleport]")
        100000
        (do
            UI { destM = destm } <- get
            onevent destm (EventUnknown 0)
            reloadWEnv
            ui <- get
            old <- liftIO $ runWoW' (swow ui) $ do
                p <- getPos
                setPos (tarPos ui)
                return p
            put ui {lastPos = old})

    b_b <- new_B win'' 0 12 6
        (inBold $ do
            drawText "[ba"
            inRed $ drawText "c"
            drawText "k]")
        (inBold $ inRev $ do
            drawText "[ba"
            inRed' $ drawText "c"
            drawText "k]")
        100000
        (void $ do
            ui <- get
            liftIO $ runWoW' (swow ui) (setPos (lastPos ui)))
    b_z <- new_B win'' 1 3 14
        (inBold $ do
            drawText "["
            inRed $ drawText "s"
            drawText "top falling]")
        (inBold $ inRev $ do
            drawText "["
            inRed' $ drawText "s"
            drawText "top falling]")
        100000
        (do
            reloadW
            UI { swow = wow } <- get
            io $ runWoW' wow stopFall)

    let bs = [b_tp, b_b, b_z]
    let updatePid = do
        ui <- get
        pid <- lift $ do
            pid <- currentData menu 
            return $ maybe nullWEnv id pid
        put ui {swow = pid}
    let redraw = \ui -> do
        let isSel = (pidM ui) == (p1M ui)
        (h,w) <- screenSize
        updateWindow win $ do
            moveWindow' 1 (w`quot`2-39)
            unless isSel$ setAttribute (AttributeColor $ red col) True
            drawBox Nothing Nothing
            moveCursor 0 2
            unless isSel $ setAttribute (AttributeColor $ red col) False
            setAttribute AttributeBold True
            drawString "pid"
            setAttribute AttributeBold False
        updateWindow win' $ do
            moveWindow' 2 (w`quot`2-38)
        updateWindow win'' $ do
            moveWindow' 11 (w`quot`2-38)

        -- render 
    let mvleft = get >>= \ui -> do
        updatePid
        let m' = (hisM ui)
            ui' = ui { p1M = m' }
        lift $ redraw ui' 
        put ui'
        refresh m' 
        waitev m' 
    let mvright = get >>= \ui -> do
        updatePid
        let m' = (multiM ui)
            ui' = ui { p1M = m' }
        lift $ redraw ui' 
        put ui'
        refresh m' 
        waitev m' 
    let mvdown = get >>= \ui -> do
        updatePid
        let m' = (destM ui)
            ui' = ui { p1M = m' }
        lift $ redraw ui' 
        put ui'
        refresh m' 
        waitev m' 
    let m = M { cmpwin = win'
        , refresh = do
            ui <- get
            wows <- getWows'
            pid <- lift $ inCurrentIndices menu $ do
                items <- forM wows $ \wow -> do
                    wenv <- liftIO $ newWEnv wow
                    newItemWith (show $ wenv_pid wenv) 
                        (unpack $ wenv_name wenv) wenv
                newpos <- searchItem (swow ui) items
                unpostMenu menu
                setItems menu items
                traverse (setCurrent menu) newpos
                postMenu menu
                redraw ui
            updatePid
            for_ bs bdraw
            
        , onevent = \ev -> get >>= \ui -> case ev of 
            (EventCharacter 'r') -> do
                refresh m
                refresh (stateM ui)
                waitev m
            EventCharacter 't' -> do
                bpress b_tp
                waitev (mainM ui)
            EventCharacter 'c' -> do
                bpress b_b
                waitev (mainM ui)
            EventCharacter 's' -> do
                bpress b_z
                waitev (mainM ui)
            EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                , mouseButtons = (1,ButtonPressed):_} -> do
                    clicked <- p1select y x
                    case clicked of
                        Nothing -> waitev m
                        Just m' -> if m'==m then do
                            pbs <- filterM (\b -> benclosed b y x) bs
                            traverse bpress pbs
                            when (null pbs) $ lift $ clickMenu menu 
                              (fromIntegral y) (fromIntegral x)
                            waitev m
                        else do
                            let ui' = ui {p1M = m'}
                            lift $ redraw ui'
                            put ui'
                            refresh m'
                            onevent m' ev
                            
            _| any' ev [Left KeyDownArrow, Right 'j'] -> do
                ifN menu 
                    (do
                        last <- lift $ liftM2 (==)
                            (currentItem menu) (lastItem menu)
                        if last then
                            mvdown
                        else do
                            onEventM menu ev
                            waitev m)
                    mvdown
            _| any' ev [Left KeyRightArrow, Right 'l'] -> 
                mvright
            _| any' ev [Left KeyLeftArrow, Right 'h'] -> 
                mvleft
            EventCharacter ' ' -> do
                wenv <- lift $ currentData menu
                let multim = (multiM ui)
                put ui { mwow = union (mwow ui) (maybe [] return wenv)}
                lift $ request menu NextItem
                refresh multim
                waitev (mainM ui)
            _ -> do
                onEventM menu ev
                waitev m
        , waitev = get >>= \ui -> do
            ev <- getEvent_main win
            updatePid
            onevent (mainM ui) ev
        }
    return m where
    inBold act = setAttribute AttributeBold True 
                 >> act >> setAttribute AttributeBold False
    inRed act = setAttribute (AttributeColor $ red $ col) True
                >> act >> setAttribute (AttributeColor $ red $ col) False
    inRed' act = setAttribute (AttributeColor $ red' $ col) True
                 >> act >> setAttribute (AttributeColor $ red' $ col) False
    inRev act = setAttribute AttributeReverse True
                 >> act >> setAttribute AttributeReverse False


new_multiM :: Colors -> Curses M
new_multiM colors = do
    win <- newWindow 13 21 1 22
    win' <- subWindow win 9 19 2 23 
    win'' <- subWindow win 2 19 11 23
    menu <- newMenu []
    setWin menu win
    setSubWin menu win'
    setMark menu " "
    disableOpts menu [NonCyclic]
    setForeground menu [AttributeColor $ red colors]
    b_tpa <- new_B win'' 0 1 8
        (inBold $ do
            drawText "[tp "
            inRed $ drawText "a"
            drawText "ll]")
        (inBold $ inRev $ do
            drawText "[tp "
            inRed' $ drawText "a"
            drawText "ll]")
        100000
        (do
            UI { destM = destm } <- get
            onevent destm (EventUnknown 0)
            UI { tarPos = p } <- get
            tpWoWs p)
    b_reg <- new_B win'' 0 10 9
        (inBold $ do
            drawText "[regrou"
            inRed $ drawText "p"
            drawText "]")
        (inBold $ inRev $ do
            drawText "[regrou"
            inRed' $ drawText "p"
            drawText "]")
        100000
        (do
            reloadWEnv
            ui <- get
            p <- liftIO $ do
                p <- runWoW' (swow ui) getPos
                forM_ (mwow ui) $ \wow -> do
                    runWoW' wow (setPos p)
                return p
            put ui { lastPos = p })
    b_sto <- new_B win'' 1 3 14
        (inBold $ do
            drawText "[st"
            inRed $ drawText "o"
            drawText "p falling]")
        (inBold $ inRev $ do
            drawText "[st"
            inRed' $ drawText "o"
            drawText "p falling]")
        100000
        (do
            reloadWs
            UI { mwow = wows } <- get
            io $ for_ wows (flip runWoW' stopFall))

    let bs = [b_tpa, b_reg, b_sto]
    let redraw ui = do
        let isSel = p1M ui == multiM ui
        (h,w) <- screenSize
        updateWindow win $ do
            moveWindow 1 (w`quot`2-18)
            unless isSel $ setAttribute (AttributeColor $ red $ colors) True
            drawBox Nothing Nothing
            moveCursor 0 2
            unless isSel $ setAttribute (AttributeColor $ red $ colors) False
            setAttribute AttributeBold True
            drawString "multi-pid"
            setAttribute AttributeBold False
        updateWindow win' $ do
            moveWindow 2 (w`quot`2-17)
        updateWindow win'' $ do
            moveWindow 11 (w`quot`2-17)
        -- render
    let mvleft ui = do
        let pidm = pidM ui
        put ui {p1M = pidm}
        lift $ redraw ui {p1M = pidm}
        refresh pidm
        waitev pidm
    let mvright ui = do
        let m' = destM ui
        put ui {p1M = m'}
        lift $ redraw ui {p1M = m'}
        refresh m'
        waitev m'
    let mvdown' = get >>= \ui -> do
        let m' = hisM ui
        put ui {p1M = m'}
        lift $ redraw ui {p1M = m'}
        refresh m'
        waitev m'
    let mvdown m = get >>= \ui -> do
        n <- lift $ itemCount menu
        if n==0 then
            mvdown'
        else do
            eq <- lift $ do
                ci <- currentItem menu
                li <- lastItem menu
                return (ci==li)
            if eq then
                mvdown'
            else do
                lift $ request menu DownItem
                waitev m
    let m = M { cmpwin = win
        , refresh = do
            reloadWEnv
            ui <- get
            wows <- filterM (io . isWoW . wenv_pid) (mwow ui) 
            put ui { mwow = wows }
            lift $ do
                items <- do
                    forM wows $ \wow -> do
                        newItemWith (show $ wenv_pid wow) (unpack $ wenv_name wow) wow
                unpostMenu menu
                setItems menu items
                postMenu menu
                redraw ui
            for_ bs bdraw
        , onevent = \ev -> get >>= \ui -> case ev of
            EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                , mouseButtons = (1,ButtonPressed):_} -> do
                    clicked <- p1select y x
                    case clicked of
                        Nothing -> waitev m 
                        Just m' -> if m'==m then do
                            pbs <- filterM (\b -> benclosed b y x) bs
                            traverse bpress pbs
                            when (null pbs) $ lift $ clickMenu menu 
                              (fromIntegral y) (fromIntegral x)
                            waitev m
                        else do
                            let ui' = ui {p1M = m'}
                            put ui'
                            lift $ redraw ui'
                            refresh m' 
                            onevent m' ev
            EventSpecialKey KeyLeftArrow -> do
                mvleft ui
            EventSpecialKey KeyRightArrow -> do
                mvright ui
            EventCharacter 'a' -> do
                bpress b_tpa
                waitev (mainM ui) 
            EventCharacter 'p' -> do
                bpress b_reg
                waitev (mainM ui)
            EventCharacter 'o' -> do
                bpress b_sto
                waitev (mainM ui)
            EventCharacter 'h' ->
                mvleft ui
            EventCharacter 'l' -> do
                mvright ui
            _| any' ev [Left KeyDownArrow, Right 'j'] -> do
                mvdown m
            EventCharacter ' ' -> do
                pid <- lift $ currentData menu 
                let items = maybe [] (flip delete (mwow ui)) pid 
                put ui { mwow = items }
                refresh m
                waitev m
            _ -> do
                onEventM menu ev
                waitev (mainM ui)
        , waitev = do
            ev <- getEvent_main win
            ui <- get
            onevent (mainM ui) ev
        }
    return m where
    inBold act = setAttribute AttributeBold True 
                 >> act >> setAttribute AttributeBold False
    inRed act = setAttribute (AttributeColor $ red $ colors) True
                >> act >> setAttribute (AttributeColor $ red $ colors) False
    inRed' act = setAttribute (AttributeColor $ red' $ colors) True
                 >> act >> setAttribute (AttributeColor $ red' $ colors) False
    inRev act = setAttribute AttributeReverse True
                 >> act >> setAttribute AttributeReverse False


new_stateM :: Colors -> Curses M
new_stateM colors = do
    win <- newWindow 13 36 1 43
    let redraw = \ui -> do
        (h,w) <- screenSize
        updateWindow win $ do
            moveWindow' 1 (w`quot`2+3)
            drawBorder (Just $ gr glyphLineV) Nothing (Just $ gr glyphLineH)
                  Nothing (Just $ gr glyphCornerUL) Nothing Nothing Nothing
            setAttribute (AttributeColor $ black' $ colors) False
            moveCursor 0 2
            setAttribute AttributeBold True
            drawString "player"
            setAttribute AttributeBold False
        render
    let m = M { cmpwin = win
        , refresh = reloadWEnv >> get >>= \ui -> lift $ do
            let name = wenv_name (swow ui)
            (pos, st, dist) <- liftIO $ runWoW' (swow ui) $ do
                pos <- getPos
                st <- getState
                tar <- C.tarPos'
                return (pos, st, pointDist pos tar)
            updateWindow win $ do
                -- clear
                setAttribute AttributeBold True
                moveCursor 2 2
                drawText " name"
                moveCursor 3 2
                drawText "mapID"
                moveCursor 4 2
                drawText "    x"
                moveCursor 5 2
                drawText "    y"
                moveCursor 6 2
                drawText "    z"
                moveCursor 7 2
                drawText "  yaw"
                moveCursor 8 2
                drawText "pitch"
                moveCursor 9 2
                drawText "state"
                moveCursor 10 2
                drawText " dist"
                setAttribute AttributeBold False
                moveCursor 2 8
                clearLine
                drawString (unpack name)
                moveCursor 3 8
                clearLine
                drawString (maybe "<unknown>" show $ p_mid pos)
                moveCursor 4 8
                clearLine
                drawString (maybe "-" show $ p_x pos)
                moveCursor 5 8
                clearLine
                drawString (maybe "-" show $ p_y pos)
                moveCursor 6 8
                clearLine
                drawString (maybe "-" show $ p_z pos)
                moveCursor 7 8
                clearLine
                drawString (maybe "-" show $ p_h pos)
                moveCursor 8 8
                clearLine
                drawString (maybe "-" show $ p_e pos)
                moveCursor 9 8
                clearLine
                drawString (showHex st "")
                moveCursor 10 8
                clearLine
                drawString (if dist /= 0 then show dist else "")
            redraw ui
        , onevent = \ev -> get >>= waitev . mainM
        , waitev = get >>= \ui -> waitev (mainM ui)
        } 
    return m where
    gr :: Glyph -> Glyph
    gr g = g { glyphAttributes = [AttributeColor $ red $ colors]}


new_destM :: Colors -> Curses M
new_destM colors = do
    win <- newWindow 9 30 14 1
    win' <- subWindow win 7 28 15 2
    fx <- F.newField 1 12 1 3 
    fy <- F.newField 1 12 2 3 
    fz <- F.newField 1 12 3 3 
    fh <- F.newField 1 7 2 19
    fe <- F.newField 1 7 3 19
    let fs = [fx,fy,fz,fh,fe]
    for_ fs $ \f -> do
        F.setBackground f [AttributeUnderline]
        F.disableOpts f [F.Static, F.Blank, F.Wrap]
    form <- F.newForm fs
    F.setWin form win
    F.setSubWin form win'
    F.postForm form

    let getF = do
        [x,y,z,h,e] <- traverse F.fieldContents [fx,fy,fz,fh,fe]
        return $ readPoint x y z h e ""

    let setF p = zipWithM_ F.setContents [fx,fy,fz,fh,fe]
          ((\f -> maybe' $ f p) <$> [p_x,p_y,p_z,p_h,p_e])
        

    b_y <- new_B win 6 2 8
        (inBold $ do
            drawText "[pla"
            inRed $ drawText "y"
            drawText "er]")
        (inBold $ inRev $ do
            drawText "[pla"
            inRed' $ drawText "y"
            drawText "er]")
        100000
        (do
            UI { swow = wow } <- get
            p <- liftIO $ runWoW' wow getPos
            lift $ setF p)
    b_tar <- new_B win 6 11 8
        (inBold $ do
            drawText "["
            inRed $ drawText "T"
            drawText "arget]")
        (inBold $ inRev $ do
            drawText "["
            inRed' $ drawText "T"
            drawText "arget]")
        100000
        (do
            UI { swow = wow } <- get
            p <- liftIO $ runWoW' wow C.tarPos'
            lift $ setF p)
    b_c <- new_B win 6 20 8
        (inBold $ do
            drawText "[corps"
            inRed $ drawText "e"
            drawText "]") 
        (inBold $ inRev $ do
            drawText "[corps"
            inRed' $ drawText "e"
            drawText "]") 
        100000
        (do
            UI { swow = wow } <- get
            p <- liftIO $ runWoW' wow getCorpse
            lift $ setF p)
    b_p <- new_B win 7 6 6
        (inBold $ do
            drawText "[pre"
            inRed $ drawText "v"
            drawText "]")
        (inBold $ inRev $ do
            drawText "[pre"
            inRed' $ drawText "v"
            drawText "]")
        100000
        (do
            UI { hisM = hism } <- get
            onevent hism (EventUnknown 1)
            UI { tarPos = tar } <- get
            lift $ setF tar)
    b_n <- new_B win 7 18 6
        (inBold $ do
            drawText "["
            inRed $ drawText "n"
            drawText "ext]")
        (inBold $ inRev $ do
            drawText "["
            inRed' $ drawText "n"
            drawText "ext]")
        100000
        (do
            UI { hisM = hism } <- get
            onevent hism (EventUnknown 2)
            UI { tarPos = tar } <- get
            lift $ setF tar)

    let bs = [b_y, b_tar, b_c, b_p, b_n]
    let redraw = \ui -> do
        let isSel = (p1M ui == destM ui)
        (h,w) <- screenSize
        when isSel $ void $setCursorMode CursorVisible
        updateWindow win $ do
            moveWindow' 14 (w`quot`2-39)
            unless isSel $ setAttribute (AttributeColor $ red $ colors) True
            drawBox Nothing Nothing
            unless isSel $ setAttribute (AttributeColor $ red $ colors) False
            setAttribute AttributeBold True
            moveCursor 0 2
            drawString "teleport to"
            setAttribute AttributeBold False
        updateWindow win' $ inBold $ do
            moveWindow' 15 (w`quot`2-38)
            moveCursor 1 1
            drawText "x"
            moveCursor 2 1
            drawText "y"
            moveCursor 3 1
            drawText "z"
            moveCursor 2 17
            drawText "θ"
            moveCursor 3 17
            drawText "ψ"
        render
    let mvleft ui = do
        let m' = multiM ui
        put ui {p1M = m'}
        lift $ do 
            setCursorMode CursorInvisible
            redraw ui {p1M = m'}
        refresh m'
        waitev m'
    let mvright ui = do
        let m' = hisM ui
        put ui {p1M = m'}
        lift $ do 
            setCursorMode CursorInvisible
            redraw ui {p1M = m'}
        refresh m'
        waitev m'
    let mvup ui = do
        let m' = pidM ui
        put ui {p1M = m'}
        lift $ do 
            setCursorMode CursorInvisible
            redraw ui {p1M = m'}
        refresh m'
        waitev m'
    let fix = lift $ do
        setCursorMode CursorVisible
        F.reposCursor form
        render
        return ()
    let setTar = do
        ui <- get
        tar <- lift $ do
            setCursorMode CursorInvisible
            F.request form F.Validate
            getF
        unless (tar == tarPos ui) $ do
            put ui { tarPos = tar }
            onevent (hisM ui) (EventUnknown 0)
    let m = M { cmpwin = win
        , refresh = do
            ui <- get
            lift $ do
                redraw ui
            for_ bs bdraw
            fix
        , onevent = \ev -> get >>= \ui -> fix >> case ev of
            EventMouse _ MouseState { mouseCoordinates = (x,y,_)
                , mouseButtons = (1,ButtonPressed):_} -> do
                    clicked <- p1select y x
                    case clicked of
                        Nothing -> waitev m
                        Just m' -> if m'==m then do
                            lift $ F.clickForm form 
                                (fromIntegral y) (fromIntegral x)
                            pbs <- filterM (\b -> benclosed b y x) bs
                            traverse bpress pbs
                            waitev m
                        else do
                            let ui' = ui {p1M = m'}
                            lift $ redraw ui'
                            put ui'
                            lift $ setCursorMode CursorInvisible
                            refresh m'
                            onevent m' ev
            EventSpecialKey KeyLeftArrow -> 
                mvleft ui
            EventSpecialKey KeyRightArrow -> 
                mvright ui
            EventSpecialKey KeyUpArrow -> do
                first <- lift $ liftM2 (==) 
                    (F.currentField form) (F.firstField form)
                if first then
                    mvup ui
                else do
                    onEventF form $ EventSpecialKey KeyBackTab
                    waitev m
            EventSpecialKey KeyDownArrow -> do
                onEventF form $ EventCharacter '\t'
                waitev m
            EventCharacter c
              -- | any (c==) ['.','\ETB'] || isNumber c -> do
                -- onEventF form ev
                -- waitev m
              | c == 'h' -> 
                mvleft ui
              | c == 'l' -> 
                mvright ui
              | c == 'k' -> do
                first <- lift $ liftM2 (==) 
                    (F.currentField form) (F.firstField form)
                if first then
                    mvup ui
                else do
                    onEventF form $ EventSpecialKey KeyBackTab
                    waitev m
              | c == 'j' -> do
                onEventF form $ EventCharacter '\t'
                waitev m
              | c == 'y' -> do
                bpress b_y
                waitev m
              | c == 'T' -> do
                bpress b_tar
                waitev m
              | c == 'e' -> do
                bpress b_c
                waitev m
              | c == 'v' -> do
                bpress b_p
                waitev m
              | c == 'n' -> do
                bpress b_n
                waitev m
              | c == '\n' -> do
                lift $ F.request form F.NextField
                waitev m
            EventUnknown 0 -> do
                setTar
            EventUnknown 1 -> do
                ui <- get
                lift $ setF (tarPos ui)
                lift $ do
                    setCursorMode CursorInvisible
                    updateWindow win (return ())
            _ -> do
                onEventF form ev
                waitev m
        , waitev = get >>= \ui -> do
            ev <- getEventH (lift $ getEvent' win (Just 10)) 10 
                [(750,(refresh $ stateM ui) >> fix)]
            lift $ setCursorMode CursorInvisible
            onevent (mainM ui) ev
        }
    return m where
    inBold act = setAttribute AttributeBold True >> act 
                 >> setAttribute AttributeBold False
    inRed act = setAttribute (AttributeColor $ red $ colors) True
                >> act >> setAttribute (AttributeColor $ red $ colors) False
    inRed' act = setAttribute (AttributeColor $ red' $ colors) True
                 >> act >> setAttribute (AttributeColor $ red' $ colors) False
    inRev act = setAttribute AttributeReverse True
                 >> act >> setAttribute AttributeReverse False

new_hisM :: Colors -> Curses M
new_hisM colors = do
    win <- newWindow 9 48 14 10
    win' <- subWindow win 7 46 15 11
    menu <- newMenu []
    setWin menu win
    setSubWin menu win'
    setMark menu " "
    setForeground menu [AttributeColor $ red colors, AttributeReverse]
    postMenu menu
    let bs = []
    let updateTar = do 
        mdat <- lift $ currentData menu
        for_ mdat $ \dat -> do
            ui <- get
            put ui {tarPos = dat} :: RT ()
    let m = M { cmpwin = win
      , refresh = do
          ui <- get
          let isSel = p1M ui == m
          lift $ do
              (h,w) <- screenSize
              updateWindow win $ do
                  moveWindow' 14 (w`quot`2-9)
                  (if isSel then id else inRed)
                      (drawBox Nothing Nothing)
                  moveCursor 0 2
                  inBold $ drawText "previous destinations"
              updateWindow win' $ do
                  moveWindow 15 (w`quot`2-8)
      , onevent = \ev -> get >>= \ui -> case ev of 
            EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                , mouseButtons = (1,st):_} -> do
                    clicked <- p1select y x
                    case clicked of
                        Nothing -> waitev m
                        Just m' -> if m'==m then do
                            pbs <- filterM (\b -> benclosed b y x) bs
                            traverse bpress pbs
                            when (null pbs) $ lift $ clickMenu menu 
                              (fromIntegral y) (fromIntegral x)
                            mdat <- lift $ currentData menu
                            for_ mdat $ \p -> liftIO $ forM (mwow ui) 
                                $ \w -> runWoW' w $ do
                                    stopFall 
                                    setPos p
                            waitev m
                        else do
                            let ui' = ui {p1M = m'}
                            put ui'
                            refresh m
                            refresh m'
                            onevent m' ev
            EventCharacter '\n' -> do
                mdat <- lift $ currentData menu
                for_ mdat $ \dat -> put ui { tarPos = dat }
                onevent (destM ui) (EventUnknown 1)
                waitev m
            EventUnknown 0 -> lift $ do
                let p = tarPos ui
                it <- newItemWith (show p) "" p
                unpostMenu menu
                addItems menu [it]
                postMenu menu
                updateWindow win (return ())
            EventUnknown 1 -> do
                mdat <- lift $ do
                    request menu UpItem
                    updateWindow win (return ())
                    currentData menu
                for_ mdat $ \dat -> do
                    put ui { tarPos = dat }
            EventUnknown 2 -> do
                mdat <- lift $ do
                    request menu DownItem
                    updateWindow win (return ())
                    currentData menu
                for_ mdat $ \dat -> do
                    put ui { tarPos = dat }
            _| any' ev [Left KeyLeftArrow, Right 'h'] ->
                mvleft
            _| any' ev [Left KeyRightArrow, Right 'l'] ->
                mvright
            _| any' ev [Left KeyUpArrow, Right 'k'] ->
                ifN menu 
                    (do
                        first <- lift $ liftM2 (==)
                            (currentItem menu) (firstItem menu)
                        if first then
                            mvup
                        else do
                            onEventM menu ev
                            waitev m)
                    mvup
            _ -> do
                onEventM menu ev
                ev <- getEvent_main win
                onevent (mainM ui) ev
      , waitev = do
          ui <- get
          ev <- getEvent_main win
          onevent (mainM ui) ev
    } where
        mvleft = do
            ui <- get
            let m' = destM ui
            put ui {p1M = m'}
            refresh m
            refresh m'
            waitev m'
        mvright = do
            ui <- get
            let m' = pidM ui
            put ui {p1M = m'}
            refresh m
            refresh m'
            waitev m'
        mvup = do
            ui <- get
            let m' = multiM ui
            put ui {p1M = m'}
            refresh m
            refresh m'
            waitev m'
    return m where
    inRed act = setAttribute (AttributeColor $ red $ colors) True
                >> act >> setAttribute (AttributeColor $ red $ colors) False
    inRed' act = setAttribute (AttributeColor $ red' $ colors) True
                 >> act >> setAttribute (AttributeColor $ red' $ colors) False

new_tpM :: Colors -> FilePath -> Curses M
new_tpM color fp = do
    (h,w) <- screenSize
    let menuheight = subtract 8
    win <- newWindow h w 0 0
    win' <- subWindow win (h-5) (w-2) 1 1
    win'' <- newWindow 2 60 (h-3) 1
    win''' <- newWindow 5 38 0 0
    tpl <- liftIO $ loadTPList fp
    pl <- forM tpl $ \entry -> do 
        it <- newItemWith (unpack (tp_name entry)) (show (tp_p entry)) entry
        when (tp_p entry == emptyPoint) $ selectable it False
        return it
    menu <- newMenu pl
    setWin menu win
    setSubWin menu win'
    disableOpts menu [OneValue]
    setForeground menu [AttributeColor $ red color, AttributeReverse]
    setUnselectable menu [AttributeColor $ red color, AttributeBold]
    setMark menu " "
    setFormat menu (fromIntegral h - 5) 1
    postMenu menu
    downU menu
    form <- new_msgbox color 
    b_tp <- new_B win'' 1 0 10 
        (do
            drawText "["
            setAttribute (AttributeColor $ red $ color) True
            drawText "t"
            setAttribute (AttributeColor $ red $ color) False
            drawText "eleport]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            setAttribute (AttributeColor $ red' $ color) True
            drawText "t"
            setAttribute (AttributeColor $ red' $ color) False
            drawText "eleport]"
            setAttribute AttributeReverse False)
        100000
        (reloadWEnv >> get >>= \ui -> lift $ void $ do
            p <- (maybe emptyPoint id) <$> (fmap tp_p) <$> currentData menu
            setForeground menu [AttributeColor $ red' $ col $ ui]
            updateWindow win $ return ()
            setForeground menu [AttributeReverse, AttributeColor $ red $ col $ ui] 
            liftIO $ runWoW' (swow ui) $ setPos p)
    b_tpa <- new_B win'' 1 11 8
        (do
            drawText "[tp "
            setAttribute (AttributeColor $ red $ color) True
            drawText "a"
            setAttribute (AttributeColor $ red $ color) False
            drawText "ll]")
        (do
            setAttribute AttributeReverse True
            drawText "[tp "
            setAttribute (AttributeColor $ red' $ color) True
            drawText "a"
            setAttribute (AttributeColor $ red' $ color) False
            drawText "ll]"
            setAttribute AttributeReverse False)
        100000
        (reloadWEnv >> get >>= \ui -> lift $ do
            p <- (maybe emptyPoint id) <$> (fmap tp_p) <$> currentData menu
            setForeground menu [AttributeColor $ red' $ col $ ui]
            updateWindow win $ return ()
            setForeground menu [AttributeReverse, AttributeColor $ red $ col $ ui] 
            liftIO $ for_ (mwow ui) $ \wow -> runWoW' wow $ setPos p)
    b_new <- new_B win'' 1 21 5
        (do
            drawText "["
            setAttribute (AttributeColor $ red $ color) True
            drawText "n"
            setAttribute (AttributeColor $ red $ color) False
            drawText "ew]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            setAttribute (AttributeColor $ red' $ color) True
            drawText "n"
            setAttribute (AttributeColor $ red' $ color) False
            drawText "ew]"
            setAttribute AttributeReverse False)
        0
        (do
            dat <- fnew form
            lift $ for_ dat $ \dat' -> do
                newit <- newItemWith (unpack $ tp_name dat')
                    (show  $tp_p dat') dat'
                when (emptyPoint == tp_p dat') $
                    selectable newit False
                top <- topRow menu
                j <- getIndex menu
                unpostMenu menu
                addItemsAt menu (fromIntegral j + 1) [newit]
                setTopRow menu top
                setIndex menu (fromIntegral j + 1)
                postMenu menu)
    b_edit <- new_B win'' 1 27 6
        (do
            drawText "["
            setAttribute (AttributeColor $ red $ color) True
            drawText "e"
            setAttribute (AttributeColor $ red $ color) False
            drawText "dit]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            setAttribute (AttributeColor $ red' $ color) True
            drawText "e"
            setAttribute (AttributeColor $ red' $ color) False
            drawText "dit]"
            setAttribute AttributeReverse False)
        0
        (get >>= \ui -> do
            it <- lift $ currentItem' menu
            dat <- lift $ getDataMaybe it
            dat' <- fedit form (maybe (TPEntry "" emptyPoint) id dat)
            lift $ case dat' of
                Nothing -> return ()
                Just dat'' -> do
                    newit <- newItemWith (unpack $ tp_name dat'') 
                        (show $ tp_p dat'') dat''
                    when (emptyPoint == tp_p dat'') $
                        selectable newit False
                    n <- itemIndex it
                    i_row <- topRow menu
                    unpostMenu menu
                    overrideIndex menu (fromIntegral n) newit
                    setTopRow menu i_row
                    unless (n<0) $ setIndex menu n
                    postMenu menu
                    render)
    b_del <- new_B win'' 1 34 8
        (drawText "[delete]")
        (do
            setAttribute AttributeReverse True
            drawText "[delete]"
            setAttribute AttributeReverse False)
        100000
        (lift $ do
            unpostMenu menu
            n <- itemCount menu
            i <- getIndex menu
            i_row <- topRow menu
            withCurrentItem menu (removeItem menu)
            postMenu menu
            setTopRow menu i_row
            if (i<n-1) then
                setIndex menu i
            else if (i>0) then
                setIndex menu (i-1)
            else
                return ())
    b_save <- new_B win'' 1 44 6
        (do
            drawText "["
            setAttribute (AttributeColor $ red $ color) True
            drawText "S"
            setAttribute (AttributeColor $ red $ color) False
            drawText "ave]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            setAttribute (AttributeColor $ red' $ color) True
            drawText "S"
            setAttribute (AttributeColor $ red' $ color) False
            drawText "ave]"
            setAttribute AttributeReverse False)
        100000
        (get >>= \ui -> lift $ do
            tpl <- menuData menu
            liftIO $ saveTPList (tpFP ui) tpl)
    b_rel <- new_B win'' 1 51 8
        (do
            drawText "["
            setAttribute (AttributeColor $ red $ color) True
            drawText "R"
            setAttribute (AttributeColor $ red $ color) False
            drawText "eload]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            setAttribute (AttributeColor $ red' $ color) True
            drawText "R"
            setAttribute (AttributeColor $ red' $ color) False
            drawText "eload]"
            setAttribute AttributeReverse False) 
        100000
        (lift $ do
            n <- itemCount menu
            i <- getIndex menu
            i_row <- topRow menu
            tpl' <- liftIO $ loadTPList fp
            pl' <- forM tpl $ \entry -> do 
                it <- newItemWith (unpack (tp_name entry)) (show (tp_p entry)) entry
                when (tp_p entry == emptyPoint) $ selectable it False
                return it
            unpostMenu menu
            setItems menu pl'
            postMenu menu
            setTopRow menu i_row
            if (i<n) then
                setIndex menu i
            else if (i>0) then
                setIndex menu (i-1)
            else
                return ()
            render )

    let bs = [b_tp, b_tpa, b_new, b_edit, b_del, b_save, b_rel]

    let m = M { cmpwin = win
        , refresh = get >>= \ui -> do
            (h,w) <- lift $ do
                (h,w) <- screenSize 
                dft <- defaultWindow
                p <- io $ runWoW' (swow ui) getPos
                updateWindow win $ do
                    resizeWindow h w
                    clearLines [h-3..h-2]
                    -- clear
                    drawBox Nothing Nothing
                    moveCursor 0 2 
                    drawString "┤ Main │"
                    setAttribute AttributeBold True
                    drawString " TP List "
                    setAttribute AttributeBold False
                    drawText "│ Connection List │ Misc │ Config ├"
                    moveCursor 0 0
                updateWindow win' $ 
                    resizeWindow (h-4) (w-2)
                updateWindow win'' $ do
                    moveWindow' (h-3) (w`quot`2-29)
                    clearLines [0,1]
                    setAttribute AttributeBold True
                return (h,w)
            for_ bs bdraw
            lift $ do
                i <- getIndex menu
                i_row <- topRow menu
                unpostMenu menu
                setTopRow menu i_row
                unless (i<0) $ setIndex menu i
                postMenu menu
                render
        , onevent = \ev -> get >>= \ui -> case ev of
            EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                , mouseButtons = (1,ButtonPressed):_} ->
                    clickTabs y x $ do
                        isIn <- lift $ enclosed win' y x
                        if isIn then lift $ do
                            clickMenu menu (fromIntegral y) 0 
                        else do
                            inW'' <- lift $ enclosed win'' y x
                            when inW'' $ forM_ bs $ \b -> do
                                    isIn <- benclosed b y x 
                                    when isIn (bpress b)
                        waitev m
            EventCharacter 'q' -> mzero
            EventCharacter '\EOT' -> do
                bpress b_del
                waitev m
            EventCharacter 'r' -> do
                refresh m
                waitev m 
            EventCharacter 'R' -> do
                bpress b_rel
                waitev m
            EventCharacter 'e' -> do
                bpress b_edit 
                waitev m 
            EventCharacter 'n' -> do
                bpress b_new 
                waitev m 
            EventCharacter 'S' -> do
                bpress b_save
                waitev m 
            EventCharacter ' ' -> do
                _it <- lift $ withCurrentItem menu $ \it -> do
                    setValue it True
                    setForeground menu 
                        [AttributeColor defaultColorID, AttributeReverse]
                    return it
                forM_ _it $ \it -> do
                    ev <- getEventM menu
                    lift $ case ev of 
                        EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                            , mouseButtons = (1,ButtonPressed):_} -> do
                                isIn <- enclosed win' y x
                                when isIn $do
                                    clickMenu menu (fromIntegral y) 0 
                                    top <- topRow menu
                                    i <- itemIndex it
                                    j <- getIndex menu
                                    unpostMenu menu
                                    moveToIndex menu (fromIntegral i) (fromIntegral j)
                                    postMenu menu
                                    setTopRow menu top
                                    setCurrent menu it
                        EventCharacter ' ' -> do
                            top <- topRow menu
                            i <- itemIndex it
                            j <- getIndex menu
                            unpostMenu menu
                            moveToIndex menu (fromIntegral i) (fromIntegral j)
                            postMenu menu
                            setTopRow menu top
                            setCurrent menu it
                        _ -> return ()
                    lift $ do
                        setForeground menu 
                            [AttributeColor $ red color, AttributeReverse]
                        setValue it False
                    waitev m
            EventCharacter '\t' -> do
                refresh (routeM ui)
                waitev (routeM ui)
            EventCharacter 'z' -> do
                lift $ do
                    j <- getIndex menu
                    (h,w) <- updateWindow win' windowSize
                    let newtop = max 0 $ j - (fromIntegral h `quot` 2)
                    setTopRow menu newtop
                    setIndex menu j
                waitev m
            EventSpecialKey KeyBackTab -> do
                refresh (mainM ui)
            EventSpecialKey KeyLeftArrow -> do
                lift $ (upS menu >> upU menu >> upS menu >> downU menu)
                waitev m
            EventSpecialKey KeyRightArrow -> do
                lift $ (downS menu >> downU menu)
                waitev m
            EventCharacter c | c=='\n' || c=='t' -> do
                when (nullWEnv /= swow ui) $ do
                    bpress b_tp
                waitev (tpM ui)
            EventCharacter 'a' -> do
                when (nullWEnv /= swow ui) $
                    bpress b_tpa
                waitev m
            EventResized -> do
                refresh m
                waitev m
            _ -> waitev (tpM ui)
        , waitev =
            onevent m =<< getEventM menu
        }
    return m

new_routeM :: Colors -> FilePath -> Curses M
new_routeM colors fp = do
    (h,w) <- screenSize
    dft <- defaultWindow
    win <- newWindow h w 0 0
    win' <- subWindow win (h-7) (w-2) 1 1
    win'' <- newWindow 2 64 0 0
    routel <- liftIO $ loadConnectionList fp
    items <- forM routel (\e -> newItemWith (show $ c_o e) (" →  " ++ (show $ c_f e)) e)
    menu <- newMenu items
    form <- new_editbox colors
    setWin menu win
    setSubWin menu win'
    setForeground menu [AttributeColor $ red colors, AttributeReverse]
    setMark menu " "
    postMenu menu
    downU menu

    let inRed act = do
        setAttribute (AttributeColor $ red $ colors) True
        act
        setAttribute (AttributeColor $ red $ colors) False
    let inRed' act = do
        setAttribute (AttributeColor $ red' $ colors) True
        act
        setAttribute (AttributeColor $ red' $ colors) False

    b_t <- new_B win'' 1 0 9 
        (do
            drawText "["
            inRed $ drawText "t"
            drawText "rigger]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            inRed' $ drawText "t"
            drawText "rigger]"
            setAttribute AttributeReverse False)
        100000
        (reloadWEnv >> get >>= \ui -> lift $ do
            entries <- menuData menu
            pos <- io $ runWoW' (swow ui) getPos
            let entry = getConnection entries pos
            for_ entry $ \route -> do
                io $ runWoW' (swow ui) (setPos route)
                its <- menuItems menu
                mit <- searchItemBy (\r -> c_f r == route) its
                for_ mit (setCurrent menu))
    b_ta <- new_B win'' 1 10 13
        (do
            drawText "[trigger "
            inRed $ drawText "a"
            drawText "ll]")
        (do
            setAttribute AttributeReverse True
            drawText "[trigger "
            inRed' $ drawText "a"
            drawText "ll]"
            setAttribute AttributeReverse False)
        100000
        (reloadWEnv >> get >>= \ui -> lift $ do
            entries <- menuData menu
            io $ for_ (mwow ui) $ \wow -> do
                pos <- runWoW' wow getPos
                traverse (runWoW' wow . setPos) (getConnection entries pos))
    b_new <- new_B win'' 1 25 5
        (do
            drawText "["
            inRed $ drawText "n"
            drawText "ew]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            inRed' $ drawText "n"
            drawText "ew]"
            setAttribute AttributeReverse False)
        0
        (do
            ui <- get
            dat <- fnew form
            lift $ for_ dat $ \entry -> do
                newit <- newItemWith (show $ c_o entry) 
                    (" →  " ++ (show $ c_f entry)) entry
                unpostMenu menu
                addItems menu [newit]
                postMenu menu
            refresh (routeM ui))
    b_edit <- new_B win'' 1 31 6
        (do
            drawText "["
            inRed $ drawText "e"
            drawText "dit]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            inRed' $ drawText "e"
            drawText "dit]"
            setAttribute AttributeReverse False)
        0
        (do
            ui <- get
            _it <- lift $ currentItem menu
            for_ _it $ \it -> do
                d <- traverse (fedit form) =<< (lift $ getDataMaybe it)
                for_ d $ \d' -> case d' of
                    Nothing -> return ()
                    Just dat -> lift $ do
                        newit <- newItemWith (show $ c_o dat) 
                            (" →  " ++ (show $ c_f dat)) dat
                        n <- itemIndex it
                        i_row <- topRow menu
                        unpostMenu menu
                        overrideIndex menu (fromIntegral n) newit
                        setTopRow menu i_row
                        unless (n<0) $ setIndex menu n
                        postMenu menu
            refresh (routeM ui))
    b_del <- new_B win'' 1 38 8
        (do
            drawText "[delete]")
        (do
            setAttribute AttributeReverse True
            drawText "[delete]"
            setAttribute AttributeReverse False)
        100000
        (lift $ do
            unpostMenu menu
            n <- itemCount menu
            i <- getIndex menu
            i_row <- topRow menu
            mapM_ (removeItem menu) =<< currentItem menu
            postMenu menu
            setTopRow menu i_row
            if (i<n-1) then
                setIndex menu i
            else if (i>0) then
                setIndex menu (i-1)
            else
                return ()
            )
    b_save <- new_B win'' 1 48 6
        (do
            drawText "["
            inRed $ drawText "S"
            drawText "ave]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            inRed' $ drawText "S"
            drawText "ave]"
            setAttribute AttributeReverse False)
        100000
        (get >>= \ui -> lift $ do
            tpl <- menuData menu
            liftIO $ saveConnectionList (routeFP ui) tpl)
    b_rel <- new_B win'' 1 55 8
        (do
            drawText "["
            inRed $ drawText "R"
            drawText "eload]")
        (do
            setAttribute AttributeReverse True
            drawText "["
            inRed' $ drawText "R"
            drawText "eload]"
            setAttribute AttributeReverse False) 
        100000
        (lift $ do
            n <- itemCount menu
            i <- getIndex menu
            i_row <- topRow menu
            routes <- liftIO $ loadConnectionList fp
            items <- forM routes
                (\e -> newItemWith (show $ c_o e) (" →  " ++ (show $ c_f e)) e)
            unpostMenu menu
            setItems menu items
            postMenu menu
            setTopRow menu i_row
            if (i<n) then
                setIndex menu i
            else if (i>0) then
                setIndex menu (i-1)
            else
                return ()
            render )

    let bs = [b_t, b_ta, b_edit, b_save, b_del, b_rel, b_new]
    let m = M { cmpwin = win
        , refresh = get >>= \ui -> do
            (h,w) <- lift $ do
                (h,w) <- screenSize 
                updateWindow dft $ clearLines [h-6..h-1]
                updateWindow win $ do
                    resizeWindow h w
                    clearLines [h-3..h-1]
                    drawBox Nothing Nothing
                    moveCursor 0 2 
                    drawText "┤ Main │ TP List │"
                    setAttribute AttributeBold True
                    drawText " Connection List "
                    setAttribute AttributeBold False
                    drawString "│ Misc │ Config ├"
                    moveCursor 0 0
                updateWindow win' $ 
                    resizeWindow (h-4) (w-2)
                updateWindow win'' $ do
                    moveWindow' (h-3) (w`quot`2-31)
                    clearLines [0,1]
                    setAttribute AttributeBold True
                return (h,w)
            for_ bs bdraw
            lift $ do
                i <- getIndex menu
                i_row <- topRow menu
                unpostMenu menu
                setTopRow menu i_row
                unless (i<0) $ setIndex menu i
                postMenu menu
                render
        , onevent = \ev -> get >>= \ui -> case ev of
            EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                , mouseButtons = (1,ButtonPressed):_} ->
                    clickTabs y x $ do 
                        isIn <- lift $ enclosed win' y x
                        if isIn then lift $ 
                            clickMenu menu (fromIntegral y) 0
                        else do
                            inWin <- lift $ enclosed win'' y x
                            when inWin $ for_ bs $ \b -> do
                                isIn <- benclosed b y x
                                when isIn (bpress b)
                        waitev m
            EventCharacter c 
              | c == 'r' -> do
                refresh m
                waitev m
              | c == 'R' -> do
                bpress b_rel
                waitev m
              | c == '\EOT' -> do
                bpress b_del
                waitev m
              | c == 'q' -> do
                mzero
              | c == '\t' -> do
                refresh (miscM ui)
                waitev (miscM ui)
              | c == 'e' -> do
                bpress b_edit
                waitev m
              | c == 'n' -> do
                bpress b_new
                waitev m
              | c == 'S' -> do
                bpress b_save
                waitev m
              | c == 't' || c == '\n'-> do
                bpress b_t 
                waitev m 
              | c == 'a' -> do
                bpress b_ta
                waitev m 
            EventSpecialKey KeyBackTab -> do
                refresh (tpM ui)
                waitev (tpM ui)
            EventResized -> do
                refresh m
                waitev m
            _ -> waitev m 
        , waitev = 
            onevent m =<< getEventM menu
        }
    return m

new_miscM :: Colors -> Curses M
new_miscM colors = do
    (h,w) <- screenSize
    win <- newWindow h w 0 0
    win' <- subWindow win 21 78 2 (w`quot`2-39)
    box <- subWindow win 20 25 2 (w`quot`2-39)
    ff <- F.newField 1 11 2 11
    fn <- F.newField 1 11 4 11
    fo <- F.newField 1 1 6 12
    fr <- F.newField 1 1 7 12
    fl <- F.newField 1 1 8 12
    ft <- F.newField 1 1 9 12
    fy <- F.newField 1 1 10 12
    fc <- F.newField 1 1 11 12
    fe <- F.newField 1 1 12 12
    ffps <- F.newField 1 11 14 11
    let fs' = [ff,fn]
        fs'' = [fo,fr,fl,ft,fy,fc,fe]
        fs''' = [ffps]
        fs = fs' ++ fs'' ++ fs'''
    for_ (fs' ++ fs''') $ \f -> do
        F.setBackground f [AttributeUnderline]
        F.disableOpts f [F.Static, F.Blank, F.Wrap]
    for_ fs'' $ \f -> do
        F.setBackground f [AttributeBold, AttributeColor $ red $ colors]
        F.disableOpts f [F.AutoSkip]
    form <- F.newForm fs
    F.setWin form win
    F.setSubWin form win'
    F.postForm form

    let toggleF field = do
        F.request form F.Validate
        c <- F.fieldContents field
        if null c then
            F.setContents field "X"
        else
            F.setContents field ""

    for_ [fo,fr,fl,ft,fc,fe] (flip F.setContents "X")

    let getF = do 
        F.request form F.Validate
        [f,n] <- (fmap readMaybe) <$> traverse F.fieldContents fs'
        [o,r,l,t,y,c,e] <- (fmap $ not . null) <$> traverse F.fieldContents fs''
        [fps] <- fmap readMaybe <$> traverse F.fieldContents fs'''
        return (f,n,o,r,l,t,y,c,e,fps) 
    b_ap <- new_B win' 20 28 7
        (inBold $ do
            drawText "["
            inRed $ drawText "a"
            drawText "pply]")
        (inBold $ inRev $ do
            drawText "["
            inRed' $ drawText "a"
            drawText "pply]")
        100000
        (get >>= \ui -> lift $ do
            (f,n,o,r,l,t,y,c,e,fps) <- getF
            io $ runWoW' (swow ui) $ do
                mM setFarclip f
                mM setNearclip n
                zipWithM ($) [setObjects, setRendering, setLighting, setTexture
                    , setRays, setNpcs, setEffects]
                    [o, r, l, t, y, c, e]
                mM setMaxfps fps
                return ())
    b_aa <- new_B win' 20 36 11
        (inBold $ do
            drawText "[apply al"
            inRed $ drawText "l"
            drawText "]")
        (inBold $ inRev $ do
            drawText "[apply al"
            inRed' $ drawText "l"
            drawText "]")
        100000
        (get >>= \ui -> lift $ do
            (f,n,o,r,l,t,y,c,e,fps) <- getF
            io $ forM_ (mwow ui) $ \wow -> runWoW' wow $ do
                mM setFarclip f
                mM setNearclip n
                zipWithM ($) [setObjects, setRendering, setLighting, setTexture
                    , setRays, setNpcs, setEffects]
                    [o, r, l, t, y, c, e]
                mM setMaxfps fps
                return ())
    b_ra <- new_B win' 20 49 7
        (inBold $ do
            drawText "[rese"
            inRed $ drawText "t"
            drawText " all]")
        (inBold $ inRev $ do
            drawText "[rese"
            inRed' $ drawText "t"
            drawText " all]")
        100000
        (get >>= \ui -> lift $ do
            io $ forM_ (mwow ui) $ \wow -> runWoW' wow (resetRender >> setMaxfps 60))

    let bs = [b_ap, b_aa, b_ra]

    let m = M { cmpwin = win
        , refresh = do
            ui <- get
            lift $ do
                (h,w) <- screenSize
                updateWindow win $ do
                    resizeWindow h w
                    clearLines [0..h-1]
                    drawBox Nothing Nothing
                    moveCursor 0 2 
                    drawText "┤ Main │ TP List │ Connection List │"
                    inBold $ drawText " Misc "
                    drawString "│ Config ├"
                F.unpostForm form
                F.postForm form
                updateWindow win' $ inBold $ do
                    moveWindow 2 (w`quot`2-39)
                    moveCursor 2 2
                    drawText " farclip"
                    moveCursor 4 2
                    drawText "nearclip"
                    moveCursor 6 2
                    drawText " objects ["
                    moveCursor 7 2
                    drawText "   world ["
                    moveCursor 8 2
                    drawText "lighting ["
                    moveCursor 9 2
                    drawText "textures ["
                    moveCursor 10 2
                    drawText "    rays ["
                    moveCursor 11 2
                    drawText "    npcs ["
                    moveCursor 12 2
                    drawText " effects ["
                    forM_ [6..12] $ \i -> do
                        moveCursor i 13
                        drawText "]"
                updateWindow box $ do
                    moveWindow 2 (w`quot`2-39)
                    inRed $ drawBox Nothing Nothing
                    moveCursor 0 2
                    inBold $ drawText "graphics"
                setCursorMode CursorVisible
            forM_ bs bdraw
            lift render
        , onevent = \ev -> get >>= \ui -> case ev of
            EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                , mouseButtons = (1,ButtonPressed):_} ->
                    clickTabs y x $ do
                        lift $ do
                            F.clickForm form (fromIntegral y) (fromIntegral x)
                        for_ bs $ \b -> do
                            isIn <- benclosed b y x
                            when isIn (bpress b)
                        lift $ F.reposCursor form
                        waitev m
            EventCharacter c
              | c == 'q' ->
                mzero
              | c == '\t' -> do
                lift $ setCursorMode CursorInvisible
                refresh (confM ui)
                waitev (confM ui)
              | c == 'a' -> do
                bpress b_ap
                lift $ F.reposCursor form
                waitev m
              | c == 'l' -> do
                bpress b_aa
                lift $ F.reposCursor form
                waitev m
              | c == 'r' -> do
                bpress b_ra
                lift $ F.reposCursor form
                waitev m
            EventSpecialKey KeyBackTab -> do
                lift $ setCursorMode CursorInvisible
                refresh (routeM ui)
                waitev (routeM ui)
            _ -> do
                lift $ F.reposCursor form
                field <- lift $ F.currentField form
                if any (==field) (fs' ++ fs''') then
                    onEventF form ev
                else case ev of 
                    _| any (ev==) f_mov -> do
                        onEventF form ev
                    _| any (ev==) (EventCharacter <$> [' ', '\DEL', '\n', 'x']) -> do
                        lift $ toggleF field
                        return True
                    _ -> return False
                    
                waitev m
        , waitev = do
            ev <- getEventH (lift $ getEvent' win (Just 10)) 10 []
            onevent m ev
        }
    return m where
    inRed act = setAttribute (AttributeColor $ red $ colors) True
                >> act >> setAttribute (AttributeColor $ red $ colors) False
    inRed' act = setAttribute (AttributeColor $ red' $ colors) True
                >> act >> setAttribute (AttributeColor $ red $ colors) False

new_confM :: Colors -> (XConf -> IO ()) -> Curses M
new_confM colors bind = do
    (h,w) <- screenSize
    win <- newWindow h w 0 0
    win' <- subWindow win 21 78 2 (w`quot`2-38)
    box <- subWindow win' 6 45 2 (w`quot`2-38)
    box' <- subWindow win' 20 30 2 (w`quot`2+8)
    box'' <- subWindow win' 8 29 14 (w`quot`2-22)

    fz <- F.newField 1 26 2 16
    fzm <- F.newField 1 26 3 16
    ff <- F.newField 1 16 2 57
    fb <- F.newField 1 16 3 57
    fl <- F.newField 1 16 4 57
    fr <- F.newField 1 16 5 57
    fu <- F.newField 1 16 6 57
    fd <- F.newField 1 16 7 57
    fsl <- F.newField 1 16 8 57
    fsr <- F.newField 1 16 9 57
    fdd <- F.newField 1 16 11 57
    fhd <- F.newField 1 16 12 57
    let fs = [fz,fzm,ff,fb,fl,fr,fu,fd,fsl,fsr,fdd,fhd]
    for_ fs $ \f -> do
        F.setBackground f [AttributeUnderline]
        F.disableOpts f [F.Static, F.Blank, F.Wrap]
    form <- F.newForm fs
    F.setWin form win
    F.setSubWin form win' 
    F.postForm form

    let getF = do
        ui <- get
        lift $ F.request form F.Validate
        [sf,sfm,f,b,l,r,u,d,sl,sr,dd,hd] <- lift $ traverse F.fieldContents 
            [fz,fzm,ff,fb,fl,fr,fu,fd,fsl,fsr,fdd,fhd]
        let m' s = (maybe [] id (readMaybe s))
            xconf = XConf
                (m' sf)
                (m' sfm)
                []
                []
                (m' f)
                (m' b)
                (m' l)
                (m' r)
                (m' u)
                (m' d)
                (m' sl)
                (m' sr)
                (m' dd)
                (m' hd)
                10.0
        put ui { conf = xconf }
    let setF = do
        ui <- get
        lift $ zipWithM_ F.setContents
            [fz,fzm,ff,fb,fl,fr,fu,fd,fsl,fsr,fdd,fhd] $
            show <$> ($ (conf ui)) <$> 
            [c_sf,c_sfm,c_mvf,c_mvb,c_mvl,c_mvr,c_mvu,c_mvd,c_mvsl
            ,c_mvsr,c_dd,c_hd]
    b_sav <- new_B win' 20 31 6
        (inBold $ do
            drawText "[save]")
        (inBold $ inRev $ do
            drawText "[save]")
        100000
        (do
            getF
            ui @ UI { conf = xconf, xcFP = fp } <- get
            io $ do
                bind xconf
                saveXConf fp xconf
            setF)
    b_ap <- new_B win' 20 38 7
        (inBold $ do
            drawText "[apply]")
        (inBold $ inRev $ do
            drawText "[apply]")
        100000
        (do
            getF
            ui @ UI { conf = xconf } <- get
            io $ bind xconf
            setF)
            -- refresh (confM ui))
    let bs = [b_sav, b_ap]

    let m = M { cmpwin = win
        , refresh = do
            ui <- get
            lift $ do
                (h,w) <- screenSize
                updateWindow win $ do
                    resizeWindow h w
                    clearLines [0..h-1]
                    drawBox Nothing Nothing
                    moveCursor 0 2 
                    drawText "┤ Main │ TP List │ Connection List │ Misc │"
                    inBold $ drawText " Config "
                    drawString "├"
                updateWindow win' $ inBold $ do
                    moveWindow 2 (w`quot`2-38) 
                    moveCursor 2 2
                    drawText "  current wow" 
                    moveCursor 3 2
                    drawText "multiple wows"

                    moveCursor 2 48
                    drawText " forward"
                    moveCursor 3 48
                    drawText "    back"
                    moveCursor 4 48
                    drawText "    left"
                    moveCursor 5 48
                    drawText "   right"
                    moveCursor 6 48
                    drawText "      up"
                    moveCursor 7 48
                    drawText "    down"
                    moveCursor 8 48
                    drawText "strafe L"
                    moveCursor 9 48
                    drawText "strafe R"

                    moveCursor 11 48
                    drawText " x2 step"
                    moveCursor 12 48
                    drawText " /2 step"
                updateWindow box $ do
                    moveWindow 2 (w`quot`2-38)
                    inRed $ drawBox Nothing Nothing
                    moveCursor 0 2 
                    inBold $ drawText "stop falling"
                updateWindow box' $ do
                    moveWindow 2 (w`quot`2+8)
                    inRed $ drawBox Nothing Nothing
                    moveCursor 0 2 
                    inBold $ drawText "movement"
                updateWindow box'' $ do
                    moveWindow 9 (w`quot`2-22)
                    inRed $ drawBox Nothing Nothing
                    moveCursor 0 2
                    inBold $ drawText "movement step"
            setF
            lift $ do
                setCursorMode CursorVisible
                F.reposCursor form 
            for_ bs bdraw
            lift $ render
        , onevent = \ev -> get >>= \ui -> case ev of
            EventMouse _ MouseState {mouseCoordinates = (x,y,_)
                , mouseButtons = (1,ButtonPressed):_} ->
                    clickTabs y x $ do
                        lift $ do
                            F.clickForm form (fromIntegral y) (fromIntegral x)
                        for_ bs $ \b -> do
                            isIn <- benclosed b y x
                            when isIn (bpress b)
                        lift $ F.reposCursor form
                        waitev m
            EventCharacter c
              | c == '\t' -> do
                refresh (mainM ui)
              | c == '\n' -> do
                bpress b_ap
                lift $ F.reposCursor form
                waitev m
            EventSpecialKey KeyLeftArrow -> do
                lift $ F.request form F.LeftField
                waitev m
            EventSpecialKey KeyBackTab -> do
                lift $ setCursorMode CursorInvisible
                refresh (miscM ui)
                waitev (miscM ui)
            _ -> do
                onEventF form ev
                waitev m
        , waitev = do
            ev <- getEventH (lift $ getEvent' win (Just 10)) 10 []
            onevent m ev
        }
    return m where
    inRed act = setAttribute (AttributeColor $ red $ colors) True
                >> act >> setAttribute (AttributeColor $ red $ colors) False
    inRed' act = setAttribute (AttributeColor $ red' $ colors) True
                >> act >> setAttribute (AttributeColor $ red $ colors) False


new_msgbox :: Colors -> Curses (F TPEntry)
new_msgbox colors = do
    win <- newWindow 8 70 0 0
    win' <- subWindow win 6 68 1 1
    fnam <- F.newField 1 60 1 7 
    fmid <- F.newField 1 8 2 7
    fx <- F.newField 1 14 3 7
    fy <- F.newField 1 14 3 26
    fz <- F.newField 1 14 3 45
    fh <- F.newField 1 14 4 7
    fe <- F.newField 1 14 4 30
    for_ [fnam, fmid, fx, fy, fz, fh, fe] $ \f -> do
        F.setBackground f [AttributeUnderline]
        F.disableOpts f [F.Static, F.Blank, F.Wrap]
    form <- F.newForm [fnam, fmid, fx, fy, fz, fh, fe]
    F.setWin form win
    F.setSubWin form win'
    F.postForm form
    let func ui = do
        lift $ do
            setCursorMode CursorVisible
            (h,w) <- screenSize
            -- when (w>=60 && h>=8) $ do
            updateWindow win $ do
                moveWindow' (h`quot`2-4) (w`quot`2 - 35)
                setAttribute (AttributeColor $ red colors) True
                drawBox Nothing Nothing
                setAttribute (AttributeColor $ red colors) False
                setAttribute AttributeBold True
                moveCursor 0 2
                drawText "edit"
                moveCursor 2 2
                drawText " name"
                moveCursor 3 2
                drawText "mapID"
                moveCursor 4 6
                drawText "x"
                moveCursor 4 25
                drawText "y"
                moveCursor 4 44
                drawText "z"
                moveCursor 5 4
                drawText "yaw"
                moveCursor 5 25
                drawText "pitch"
                setAttribute AttributeBold False
            updateWindow win' $ do
                moveWindow' (h`quot`2 - 3) (w`quot`2 -34)
            render
        ev <- getEventF form
        e' <- case ev of 
            EventCharacter '\n' -> lift $ do 
                F.request form F.Validate
                p@[name,x,y,z,h,e,mid] <- traverse F.fieldContents 
                    [fnam,fx,fy,fz,fh,fe,fmid]
                return $ Just $ TPEntry (pack name) 
                    (readPoint x y z h e mid)
            EventCharacter '\ESC' -> return Nothing
            _ -> return Nothing
        lift $ setCursorMode CursorInvisible
        return e'
    let f = F {
        fedit = \e -> get >>= \ui -> do
            lift $ do
                let p = tp_p e
                F.setContents fnam (unpack $ tp_name e)
                F.setContents fmid (maybe "" (show.map_id) $ p_mid $ p)
                F.setContents fx (maybe "" show $ p_x p)
                F.setContents fy (maybe "" show $ p_y p)
                F.setContents fz (maybe "" show $ p_z p)
                F.setContents fh (maybe "" show $ p_h p) 
                F.setContents fe (maybe "" show $ p_e p) 
            func ui
        , fnew = get >>= \ui -> do
            (mmname, p) <- liftIO $ runWoW' (swow ui) $ do
                mmname <- getMMapName
                p <- getPos
                return (mmname, p)
            lift $ do
                F.setContents fnam (unpack mmname)
                F.setContents fmid (maybe "" (show . map_id) $ p_mid p)
                F.setContents fx (maybe "" show $ p_x p)
                F.setContents fy (maybe "" show $ p_y p)
                F.setContents fz (maybe "" show $ p_z p)
                F.setContents fh (maybe "" show $ p_h p) 
                F.setContents fe (maybe "" show $ p_e p) 
            func ui
        }
    return f


new_editbox :: Colors -> Curses (F ConnectionEntry)
new_editbox colors = do
    win <- newWindow 16 66 0 0
    win' <- subWindow win 14 64 1 1
    -- bwin <- newWindow 16 66 1 1 

    -- updateWindow bwin $
        -- setBackgroundFilled [AttributeColor $ black $ colors]

    fm1 <- F.newField 1 8 2 8
    fx1 <- F.newField 1 14 3 8
    fy1 <- F.newField 1 14 3 27
    fz1 <- F.newField 1 14 3 46
    fh1 <- F.newField 1 10 4 8
    fe1 <- F.newField 1 10 4 31
    fd <- F.newField 1 14 7 8
    fm2 <- F.newField 1 8 10 8
    fx2 <- F.newField 1 14 11 8
    fy2 <- F.newField 1 14 11 27
    fz2 <- F.newField 1 14 11 46
    fh2 <- F.newField 1 10 12 8
    fe2 <- F.newField 1 10 12 27
    let fields = [fm1, fx1, fy1, fz1, fh1, fe1, fd, fm2, fx2, fy2, fz2, fh2, fe2]
    for_ fields $ \f -> do
        F.setBackground f [AttributeUnderline]
        F.disableOpts f [F.Static, F.Blank, F.Wrap]
    form <- F.newForm fields
    F.setWin form win
    F.setSubWin form win'
    F.postForm form
    let func ui title = do
        lift $ do
            setCursorMode CursorVisible
            (h,w) <- screenSize
            -- updateWindow bwin $
                -- moveWindow' (h`quot`2-7) (w`quot`2-32)
            updateWindow win $ do
                moveWindow' (h`quot`2-8) (w`quot`2-33)
                setAttribute (AttributeColor $ red colors) True
                drawBox Nothing Nothing
                setAttribute (AttributeColor $ red colors) False
                setAttribute AttributeBold True
                setAttribute AttributeUnderline True
                moveCursor 2 25
                drawText "point of origin"
                moveCursor 7 26
                drawText "trigger radius"
                moveCursor 10 23
                drawText "point of destination"
                setAttribute AttributeUnderline False
                moveCursor 0 2
                drawText title
                moveCursor 3 3
                drawText "mapID"
                moveCursor 4 7
                drawText "x"
                moveCursor 4 26
                drawText "y"
                moveCursor 4 45
                drawText "z"
                moveCursor 5 5
                drawText "yaw"
                moveCursor 5 26
                drawText "pitch"

                moveCursor 8 2
                drawText "radius"

                moveCursor 11 3
                drawText "mapID"
                moveCursor 12 7
                drawText "x"
                moveCursor 12 26
                drawText "y"
                moveCursor 12 45
                drawText "z"
                moveCursor 13 5
                drawText "yaw"
                moveCursor 13 22
                drawText "pitch"
                setAttribute AttributeBold False
            updateWindow win' $
                moveWindow' (h`quot`2 -7) (w`quot`2 -32)
            render
        ev <- getEventF form
        e' <- case ev of
            EventCharacter '\n' -> lift $ do
                F.request form F.Validate
                po <- fields2p fm1 fx1 fy1 fz1 fh1 fe1
                pd <- maybe 0 id <$> readMaybe <$> F.fieldContents fd
                pf <- fields2p fm2 fx2 fy2 fz2 fh2 fe2
                return $ Just $ ConnectionEntry po pd pf
            _ -> return Nothing
        lift $ setCursorMode CursorInvisible
        return e' 
    let f = F {
          fedit = \e -> get >>= \ui -> do
            lift $ do
                p2fields (c_o e) fm1 fx1 fy1 fz1 fh1 fe1
                F.setContents fd (show $ c_d e)
                p2fields (c_f e) fm2 fx2 fy2 fz2 fh2 fe2
            func ui "edit"
        , fnew = get >>= \ui -> do
            lift $ do
                p2fields (lastPos ui) fm1 fx1 fy1 fz1 fh1 fe1
                pos <- liftIO $ runWoW' (swow ui) getPos
                F.setCurrentField form fm1
                F.setContents fd "15.00"
                p2fields pos fm2 fx2 fy2 fz2 fh2 fe2
            func ui "new"
        }
    return f

        
-- util
    
getEventH :: RT (Maybe Event) -> Int -> [(Int, RT ())] -> RT Event
getEventH getter step hs = oev 0 where
    oev i = do
        ev <- getter
        case ev of
            Nothing -> do
                traverse snd $ filter (\(m,_) -> i `mod` m < step) hs
                oev (i+step)
            Just ev -> return ev
    

getEvent_main :: Window -> RT Event
getEvent_main win = do
    UI { stateM = statem } <- get
    getEventH (lift $ getEvent' win (Just 10)) 10
        [(750, refresh statem)]

getEventM :: Menu -> RT Event
getEventM menu = getEventMH menu []

getEventM_main :: Menu -> RT Event
getEventM_main menu = do
    UI { stateM = statem } <- get
    getEventMH menu [(750,refresh statem)]

getEventMH :: Menu -> [(Int, RT ())] -> RT Event
getEventMH menu hs = do
    w <- lift $ menuWin menu
    ev <- getEventH (lift $ getEvent' w (Just 10)) 10 hs
    b <- onEventM menu ev
    if b then
        getEventMH menu hs
    else
        return ev

onEventM :: Menu -> Event -> RT Bool 
onEventM menu ev = do
    let lift' f = lift f >> return True
    case ev of
        -- up an item
        (EventSpecialKey KeyUpArrow) -> lift' $ do
            request menu UpItem
            upU menu
            downU menu
        (EventCharacter 'k') -> lift' $ do
            request menu UpItem
            upU menu
            downU menu
        -- up three items
        (EventMouse _ MouseState 
            {mouseShift=False, mouseButtons = (4,_):_}) -> lift' $ do
                request menu UpItem
                request menu UpItem
                request menu UpItem
                upU menu
                downU menu
        -- down an item
        (EventSpecialKey KeyDownArrow) -> lift' $ do
            request menu DownItem
            downU menu
            upU menu
        (EventCharacter 'j') -> lift' $ do
            request menu DownItem
            downU menu
            upU menu
        -- down three items
        (EventMouse _ MouseState {mouseShift=False, mouseButtons = (5,_):_}) -> lift' $ do
            request menu DownItem
            request menu DownItem
            request menu DownItem
            downU menu
            upU menu
        -- up a page
        (EventSpecialKey KeyPreviousPage) -> lift' $ do
            request menu UpPage
            downU menu
            upU menu
        (EventCharacter 'b') -> lift' $ do
            request menu UpPage
            upU menu
            downU menu
        -- down a page
        (EventSpecialKey KeyNextPage) -> lift' $ do
            request menu DownPage
            downU menu
            upU menu
        (EventCharacter 'f') -> lift' $ do
            request menu DownPage
            downU menu
            upU menu
        -- to the top
        (EventSpecialKey KeyHome) -> lift' $ do
            request menu FirstItem
            downU menu
            upU menu
        (EventCharacter 'g') -> lift' $ do
            request menu FirstItem
            downU menu
            upU menu
        -- to the bottom
        (EventSpecialKey KeyEnd) -> lift' $ do
            request menu LastItem
            upU menu
            downU menu
        (EventCharacter 'G') -> lift' $ do
            request menu LastItem
            upU menu
            downU menu
        -- up half a page
        (EventCharacter 'u') -> lift' $ do
            (n,_) <- menuFormat menu
            replicateM (fromIntegral n`quot`2) (request menu UpLine)
            upU menu
            downU menu
        (EventCharacter 'd') -> lift' $ do
            (n,_) <- menuFormat menu
            replicateM (fromIntegral n`quot`2) (request menu DownLine)
            downU menu
            upU menu
        (EventCharacter 'J') -> lift' $ do
            unpostMenu menu
            moveDown menu
            postMenu menu
        (EventCharacter 'K') -> lift' $ do
            unpostMenu menu
            moveUp menu
            postMenu menu
        EventCharacter '\EOT' -> lift' $ do 
            unpostMenu menu
            inCurrentIndices menu $ do
                withCurrentItem menu (removeItem menu)
            postMenu menu
        ev' -> return False

getEventF :: F.Form -> RT Event
getEventF form = getEventFH form []

getEventFH :: F.Form -> [(Int, RT ())] -> RT Event
getEventFH form hs = do
    w <- lift $ F.formWin form
    ev <- getEventH (lift $ getEvent' w (Just 10)) 10 hs
    b <- onEventF form ev
    if b then
        getEventFH form hs
    else
        return ev

f_mov :: [Event]
f_mov = EventSpecialKey <$> [KeyDownArrow, KeyUpArrow]

onEventF :: F.Form -> Event -> RT Bool
onEventF form ev = do
    let lift' f = lift f >> return True
    case ev of 
        EventMouse _ MouseState {mouseButtons = (_,ButtonReleased):_} ->
            return False
        EventMouse _ MouseState {mouseShift=False, mouseButtons = (k,_):_}
         | k == 4 -> lift' $
            F.request form F.PrevField
         | k == 5 -> lift' $
            F.request form F.NextField
        EventSpecialKey KeyDeleteCharacter -> lift' $
            F.request form F.DeleteChar
        EventSpecialKey KeyBackspace -> lift' $
            F.request form F.DeletePrev
        EventCharacter c 
            | c == '\DEL' -> lift' $
                F.request form F.DeletePrev
            | isPrint c -> lift' $
                F.write form c
            | c == '\t' -> lift' $
                F.request form F.NextField
            | c == '\ETB' -> lift' $
                F.request form F.DeleteWord
            | c == '\v' -> lift' $
                F.request form F.ClearEOL
            | c == '\SOH' -> lift' $
                F.request form F.BeginLine
            | c == '\NAK' -> lift' $
                F.request form F.ClearField
            | c == '\ENQ' -> lift' $
                F.request form F.EndLine
        EventSpecialKey KeyBackTab -> lift' $
            F.request form F.PrevField
        EventSpecialKey KeyLeftArrow -> lift' $
            F.request form F.PrevChar
        EventSpecialKey KeyRightArrow -> lift' $
            F.request form F.NextChar
        EventSpecialKey KeyUpArrow -> lift' $ 
            F.request form F.UpField
        EventSpecialKey KeyDownArrow -> lift' $ 
            F.request form F.DownField
        EventSpecialKey KeyShiftedLeftArrow -> lift' $
            F.request form F.PrevWord
        EventSpecialKey KeyShiftedRightArrow -> lift' $ 
            F.request form F.NextWord
        ev' -> 
            return False

new_B :: Window -> Int -> Int -> Int -> Update () 
    -> Update () -> Int -> (RT ()) -> Curses B
new_B win y x width upd updc delay act = 
    let b = B { 
          bdraw = lift $ do
            (w,h) <- screenSize
            updateWindow win $ do
                (x',y') <- windowPosition
                do --when (y>=0 && y+y'<h && x>=0 && x+x'+width<w) $ do
                    moveCursor y x
                    upd
        , benclosed = \y' x' -> lift $ updateWindow win $ do
            (y'', x'') <- windowPosition
            return $ (y''+y)==y' && (x''+x)<=x' && (x''+x+width)>x'
        , bpress = do
            ui <- get
            lift $ do
                updateWindow win (moveCursor y x >> updc)
                render
            act
            lift $ do
                render
                io $ threadDelay delay
            bdraw b
            lift render
        }
    in return b
    
clickTabs :: Int -> Int -> RT () -> RT ()
clickTabs y x alternative = get >>= \ui -> do
    if y/=0 then
        alternative
    else
        if x>2 && x<9 then do
            lift $ setCursorMode CursorInvisible
            refresh (mainM ui)
        else if x>9 && x<19 then do
            refresh (tpM ui)
            lift $ setCursorMode CursorInvisible
            waitev (tpM ui)
        else if x>19 && x<37 then do
            refresh (routeM ui)
            lift $ setCursorMode CursorInvisible
            waitev (routeM ui)
        else if x>37 && x<44 then do
            refresh (miscM ui)
            waitev (miscM ui)
        else if x>44 && x<53 then do
            refresh (confM ui)
            waitev (confM ui)
        else
            alternative

tpWoWs :: Point -> RT ()
tpWoWs p = do
    reloadWEnv
    ui <- get
    case mwow ui of
        [] -> return ()
        wows -> do
            old <- liftIO $ do
                pos <- runWoW' (head wows) getPos
                for_ wows (flip runWoW' (setPos p))
                return pos
            put ui { lastPos = old }

reloadWEnv :: RT ()
reloadWEnv = do
    ui @ UI { swow = wenv, mwow = wenvs } <- get
    wenv' <- liftIO $ execStateT (reload >> get) wenv
    wenvs' <- liftIO $ forM wenvs (execStateT reload)
    put ui { swow = wenv', mwow = wenvs' }

reloadW :: RT ()
reloadW = do
    ui @ UI { swow = wow } <- get
    wow' <- io $ execStateT reload wow
    put ui { swow = wow' }

reloadWs :: RT ()
reloadWs = do
    ui @ UI { mwow = wows } <- get
    wows' <- io $ traverse (execStateT reload) wows
    put ui { mwow = wows' }

fields2p :: F.Field -> F.Field -> F.Field -> F.Field -> F.Field -> F.Field 
    -> Curses Point
fields2p fm fx fy fz fh fe = do
    [x,y,z,h,e,mid] <- traverse F.fieldContents [fx,fy,fz,fh,fe,fm]
    return $ readPoint x y z h e mid

p2fields :: Point -> F.Field -> F.Field -> F.Field -> F.Field -> F.Field
    -> F.Field -> Curses ()
p2fields (Point x y z h e mid) fm fx fy fz fh fe =
    zipWithM_ F.setContents [fx,fy,fz,fh,fe,fm] 
        ((maybe "" show <$> [x,y,z,h,e]) ++ [maybe "" (show . map_id) mid]) 

maybe' :: Show a => Maybe a -> String
maybe' = maybe "" show

maybe'' :: Show a => Maybe a -> String
maybe'' = maybe "_" show

any' :: Event -> [Either Key Char] -> Bool
any' ev xs = any (==ev) $ either EventSpecialKey EventCharacter <$> xs

withRT :: (UI -> Curses UI) -> RT ()
withRT f = get >>= lift . f >>= put

upU :: Menu -> Curses ()
upU = passBy UpItem isSelectable 

downU :: Menu -> Curses ()
downU = passBy DownItem isSelectable 

upS :: Menu -> Curses ()
upS = passBy UpItem (fmap not . isSelectable)

downS :: Menu -> Curses ()
downS = passBy DownItem (fmap not . isSelectable)

passBy :: Request -> (Item -> Curses Bool) -> Menu -> Curses ()
passBy req cmp menu = do
    _curr <- currentItem menu
    forM_ _curr $ \curr -> do
        sel <- cmp curr
        unless sel $ do
            request menu req
            curr' <- currentItem menu
            unless (curr' == Just curr) $ passBy req cmp menu

searchItem :: Eq a => a -> [Item] -> Curses (Maybe Item)
searchItem e xs = do
    datas <- filterM (\i -> (==e) <$> getData i) xs
    return $ listToMaybe datas

searchItemBy :: (a -> Bool) -> [Item] -> Curses (Maybe Item)
searchItemBy f xs = do
    datas <- filterM (\i -> f <$> getData i) xs
    return $ listToMaybe datas

moveUp :: Menu -> Curses ()
moveUp menu = do
    _it <- currentItem menu
    forM_ _it $ \it -> do
        t <- topRow menu
        i <- fromIntegral <$> itemIndex it
        when (i>0) $ do
            swapIndices menu (i-1) i
            when (t>0) $
                setTopRow menu (t-1)
            setCurrent menu it

moveDown :: Menu -> Curses ()
moveDown menu = do
    _it <- currentItem menu
    forM_ _it $ \it -> do
        t <- topRow menu
        i <- fromIntegral <$> itemIndex it
        n <- itemCount menu
        when (i < fromIntegral n-1) $ do
            swapIndices menu (i+1) i
            when (t<n-1) $
                setTopRow menu (t+1)
            setCurrent menu it

mM :: Monad m => (a -> m ()) -> Maybe a -> m ()
mM = maybe (return ())

ifN :: Menu -> RT () -> RT () -> RT ()
ifN menu iact eact = do
    n <- lift $ itemCount menu
    if (n>0) then
        iact
    else
        eact

getWows' :: RT [CPid]
getWows' = do
    ui <- get
    io $ do
        if onlyW ui then
            wineWows
        else if onlyX ui then
            xWows
        else
            getWows
        
inBold act = setAttribute AttributeBold True 
                >> act >> setAttribute AttributeBold False
inRev act = setAttribute AttributeReverse True
                >> act >> setAttribute AttributeReverse False
