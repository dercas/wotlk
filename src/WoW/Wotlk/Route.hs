{-# LANGUAGE CPP #-}

import Control.Concurrent

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans
import Control.Monad.State

import qualified Data.ByteString.Char8 as B
import Data.Char
import Data.Function
import Data.List
import qualified Data.List as L
import Data.Maybe
import Data.Text (Text)
import Data.Word

import Prelude hiding (interact)

import System.Console.Haskeline
import System.Environment
import System.Posix
import System.Exit
import System.Random

import Text.Read (readMaybe)
import Text.Printf (printf)

import UI.NCurses
import UI.NCurses.Form hiding (setBackground, trim)
import qualified UI.NCurses.Form as F

import WoW.Wotlk.Box
import WoW.Wotlk.Core 
import WoW.Wotlk.TP

data Colors = Colors
    { red :: ColorID
    , red' :: ColorID
    } deriving Show

data UI = UI
    { path :: FilePath
    , swow :: WEnv
    , colors :: Colors
    } deriving Show

type RT = StateT UI Curses

data B = B
    { bdraw     :: Curses ()
    , benclosed :: Int -> Int -> RT Bool
    , bpress    :: Curses ()
    }
 
runRT :: CPid -> RT a -> IO a
runRT pid f = do
    wenv <- newWEnv pid
    runCurses $ do
        col' <- newColorID ColorRed ColorDefault 10
        col'' <- newColorID ColorDefault ColorRed 11
        let ui = UI "" wenv $ Colors col' col''
        evalStateT f ui

data Opts = Opts
  { ofiles      :: [FilePath]
  , ointeract   :: Bool
  , oclicks     :: [Double]     -- 
  , opressafter :: [VKt]        -- key
  , oxlimited   :: Bool
  , owlimited   :: Bool
  , oplimited   :: Maybe [CPid]
  , oall        :: Bool
  , oauto       :: Bool
  , odelay      :: Double       -- only used in updated nodes
  , operiod     :: Double
  , ounsafe     :: Bool         -- disable safety measures
  , oclosest    :: Bool
  , oshuffle    :: Bool         -- randomize the entries
  , ocompart    :: Bool         -- randomize inside each .gz
  , ogrouped    :: Bool
  , opart       :: Float        -- Percentage of entries to take
  , opitch      :: Maybe Float  
  , ofarclip    :: Maybe Float
  , omaxfps     :: Maybe Word32
  , omaxfpsbk   :: Maybe Word32
  , ouid        :: Maybe Word32
  } deriving Show

defaultOpts :: Opts
defaultOpts = Opts 
    []      {- ofiles -}      False   {- ointeract -}   []      {- oclicks -}
    []      {- opressafter -} False   {- oxlimited -}   False   {- owlimited -}
    Nothing {- oplimited -}   False   {- oall -}        False   {- oauto -}
    0.5     {- odelay -}      0.0     {- operiod -}     False   {- ounsafe -}
    False   {- oclosest -}    False   {- oshuffle -}    False   {- ocompart -}
    False   {- ogrouped -}    1.0     {- opart -}       Nothing {- opitch -} 
    Nothing {- ofarclip -}    Nothing {- omaxfps -}     Nothing {- omaxfpsbk -}
    Nothing {- ouid -}

usage :: String
usage = unlines
  ["Usage: Route [option|file]" 
  ,"  Options affecting pid selection:"
  ,"    -X                      Limit the search for pids to $DISPLAY"
  ,"    -wine                   Limit the search for pids to $WINEPREFIX"
  ,"    -a, -all                Don't prompt for pids, use every available one"
  ,"    -w pid, -wow pid        Use provided pid. Can be passed multiple times to"
  ,"                            select multiple pids"
  ,"  Options affecting route ordering:"
  ,"    -near[est], -close[st]  Sort by distance to the current position"
  ,"    -shuffle                Randomize order of points"
  ,"    -part, -compart         Compartmentalize. Shuffle points contained in each"
  ,"                            file but respect the order they were passed in"
  ,"    -grouped                Teleport all characters to same point"
  ,"  Options affecting flow of execution:"
  ,"    -auto                   Start execution right away"
  ,"    -delay n                Delay (s) between teleports in a point"
  ,"    -period n               Seconds between runs"
  ,"    -farclip n              Set farclip value before every teleport"
  ,"  Options affecting behaviour around teleports"
  ,"    -i, -interact           Interact with any object in your same (x,y)"
  ,"    -fps n, -maxfps n       Set the fps limit before every teleport"
  ,"    -pitch f                Set the camera pitch before every teleport"
  ,"    -click f                Leftclick `f' seconds after teleporting" 
  ,"    -press key              Press `key` after clicks"
  ,"    -uid n                  Go on only if you're about to interact with an"
  ,"                            object of the given uid"
  ,"    -unsafe                 Enable unsafely reviving"
  ,""
  ,"    You can prefix any flag by `no' to unset the option. e.g:"
  ,"        Route -X -a -noX  # equivalent to Route -a"
  ]

isOpt :: String -> String -> Bool
-- isOpt ['-',c] arg = "-" `isPrefixOf` arg && not ("--" `isPrefixOf` arg) && c `elem` arg
isOpt opt arg = opt == arg

opt :: (Opts -> Opts) -> State ([String], Opts) ()
opt = modify . fmap 

findFlag :: String -> [String] -> Bool
findFlag opt args = any (isOpt opt) args

#define IN( arg, xs)            _| arg `elem` xs
#define SETREC(  field, value)  opt (\x -> x { field = value }) 
#define SETRECF( field, f)      opt (\x -> x { field = (f) (field x) }) 
#define READREC(  field )  \
    (shift >>= \val -> SETREC(field, readFail arg val))
#define READRECF( field, f)  \
    (shift >>= \val -> SETREC(field, (f) (readFail arg val)))
#define READRECJ( field) READRECF(field, Just)
parseArgs :: State ([String], Opts) ()
parseArgs = do
  remaining <- not . L.null . fst  <$> get
  when remaining $ do
    arg <- shift
    case arg of 
        _| arg `elem` ["-i", "-interact"] 
                      -> SETREC(ointeract, True)
        _| arg `elem` ["-noi", "-nointeract"]   
                      -> SETREC(ointeract, False)
        "-click"      -> do abstime <- readFail arg <$> shift
                            SETRECF(oclicks, \xs -> xs ++ [abstime - sum xs])
        "-noclick"    -> SETREC(oclicks, [])
        "-press"      -> do k <- readFail arg <$> shift
                            SETRECF(opressafter, (++ [k]))
        "-nopress"    -> SETREC(opressafter, [])
        "-X"          -> SETREC(oxlimited, True)
        "-noX"        -> SETREC(oxlimited, False)
        "-wine"       -> SETREC(owlimited, True)
        "-nowine"     -> SETREC(owlimited, False)
        _| arg `elem` ["-w", "-wow"]  
                      -> READRECF(oplimited, \w -> Just [w]) 
        _| arg `elem` ["-a", "-all"]      
                      -> SETREC(oall, True)
        _| arg `elem` ["-noa", "-noall"]  
                      -> SETREC(oall, False)
        "-auto"       -> SETREC(oauto, True)
        "-noauto"     -> SETREC(oauto, False)
        "-delay"      -> READREC(odelay)
        "-period"     -> READREC(operiod)
        "-unsafe"     -> SETREC(ounsafe, True)
        "-safe"       -> SETREC(ounsafe, False)
        _| arg `elem` ["-closest", "-close", "-nearest", "-near"]
                      -> SETREC(oclosest, True)
        _| arg `elem` ["-noclosest", "-noclose", "-nonearest", "-nonear"]
                      -> SETREC(oclosest, False)
        "-shuffle"    -> SETREC(oshuffle, True)
        "-noshuffle"  -> SETREC(oshuffle, False)
        _| arg `elem` ["-comp", "-compart"] 
                      -> SETREC(ocompart, True)
        _| arg `elem` ["-nocomp", "-nocompart"] 
                      -> SETREC(ocompart, False)
        "-part"       -> READREC(opart)
        "-pitch"      -> READRECJ(opitch)
        "-nopitch"    -> SETREC(opitch, Nothing)
        "-farclip"    -> READRECJ(ofarclip)
        "-nofarclip"  -> SETREC(ofarclip, Nothing)
        _| arg `elem` ["-fps", "-maxfps"]       
                      -> READRECJ(omaxfps)
        _| arg `elem` ["-nofps", "-nomaxfps"]   
                      -> SETREC(omaxfps, Nothing)
        _| arg `elem` ["-fpsb", "-maxfpsbk"]       
                      -> READRECJ(omaxfpsbk)
        _| arg `elem` ["-nofpsb", "-nomaxfpsbk"]   
                      -> SETREC(omaxfpsbk, Nothing)
        "-uid"        -> READRECJ(ouid)
        "-nouid"      -> SETREC(ouid, Nothing)
--         _| ".gz" `isSuffixOf` (toLower <$> arg) 
--                               -> opt (\o -> o { ofiles = ofiles o ++ [arg] })
        filename      -> SETRECF(ofiles, ++ [filename])        
--         _ -> error $ printf "unrecognized argument: %s" arg
    parseArgs
    where
    shift :: State ([String], Opts) String
    shift = do
        (arg:args, opts) <- get
        put (args, opts)
        return arg
    readFail :: Read a => String -> String -> a
    readFail text word = maybe 
        (error $ printf "%s: couldn't parse \"%s\"" text word) id (readMaybe word)

findValues :: String -> [String] -> [String]
findValues opt args = maybe [] (\(x,xs) -> commas x ++ findValues opt xs) rems 
    where
    remsincluding = uncons $ dropWhile (not . isOpt opt) args
    rems = uncons . snd =<< remsincluding
    commas s = case dropWhile (== ',')s of
                    "" -> []
                    s' -> w : commas s''
                          where (w, s'') = break (== ',') s'

findValue :: String -> [String] -> Maybe String
findValue x xs = listToMaybe $ reverse $ findValues x xs

readOpts :: [String] -> Opts
readOpts args = Opts 
                  files interact clicks ak xlimited wlimited plimited allwows 
                  auto delay period unsafe nearest shuffle compart grouped part
                  pitch farclip maxfps maxfpsbk uid 
                  where
    files = L.filter (L.isSuffixOf ".gz") args
    interact = findFlag "-i" args
    clicks = snd $ mapAccumL (\a b -> (b, b-a)) 0 $ sort 
             $ nub $ fmap (maybe 3.0 id . readMaybe) $ findValues "-click" args
    ak = maybeToList . readMaybe =<< findValues "-press" args
    xlimited = findFlag "-X" args
    wlimited = findFlag "-wine" args
    plimited = fmap pure . listToMaybe $ read <$> findValues "-w" args
    allwows = findFlag "-a" args
    auto = findFlag "-auto" args
    delay = maybe 0 id $ readMaybe =<< findValue "-delay" args
    period = maybe 0 id $ readMaybe =<< findValue "-period" args
    unsafe = findFlag "-unsafe" args
    nearest = findFlag "-near" args
    shuffle = findFlag "-shuffle" args
    compart = findFlag "-comp" args
    grouped = findFlag "-grouped" args
    part = maybe 1.0 id (findValue "-part" args >>= readMaybe)
    pitch = readMaybe =<< findValue "-pitch" args
    farclip = readMaybe =<< findValue "-farclip" args
    maxfps = readMaybe =<< findValue "-fps" args
    maxfpsbk = readMaybe =<< findValue "-maxfpsbk" args
    uid = readMaybe =<< findValue "-uid" args

skipentries :: RealFrac a => a -> [b] -> [b]
skipentries perc xs = L.take n xs where
  n = round (perc * fromIntegral (L.length xs))

main :: IO ()
main = void $ runInputT defaultSettings $ withInterrupt $ do
    args <- liftIO $ L.drop 1 <$> getArgs
    when (args `elem` [["-h"], ["--help"]]) $ do
        outputStrLn usage
        io exitFailure
    let eith = try' $ snd $ execState parseArgs (args, defaultOpts)
    opts <- case eith of
        Left str -> do outputStrLn str
                       outputStrLn usage
                       io $ exitFailure
        Right opts -> return opts 
    outputStrLn (show opts)
    wows <- getPids opts
    outputStrLn (printf "pids: %s" (show wows))
    fp <- if L.null (ofiles opts) then getRoutes else (io . loadRouteList) `mapM` ofiles opts 
    rl <- 
      if oshuffle opts then do
          if ocompart opts then do
              liftIO $ (concat . fmap (skipentries (opart opts))) 
                  <$> mapM shuffleList fp
          else do
              liftIO $ skipentries (opart opts) <$> shuffleList (concat fp)
      else if oclosest opts then do
          curpos <- liftIO $ runWoW (head wows) getPos
          let sortf points = pointDist curpos (fst $ last points)
          return $ sortOn sortf $ concat fp
      else liftIO $
          return $ concat fp
    outputDist (grouped rl)
    let rls = if ogrouped opts then
                  repeat rl
              else
                  formatting rl (length wows)
    io $ print (wows, length rl)

    chans <- liftIO $ replicateM (length wows) newEmptyMVar
    clickSem <- liftIO (newQSem 1)

    liftIO $ traverse forkIO $ zipWith3 (runRoute opts clickSem) rls wows chans
    main' opts chans where
    grouped :: RouteList -> [RouteList]
    grouped = groupOn (p_mid . fst . head) . sortOn (p_mid . fst . head)
    outputDist :: [RouteList] -> InputT IO ()
    outputDist xs = mapM_ (\(m,n) -> outputStrLn $ printf "%-4d %s" n (show m))
        $ reverse $ sortOn snd $ 
        (fmap (\ys -> (maybe (-1) id $ p_mid $ fst $ head $ head ys, length ys)) xs)

replLine :: String
replLine = "[q: quit] [^D: quit] [a: toggle auto] [any: cont] "

main' :: Opts -> [MVar Bool] -> InputT IO ()
main' opts chans = do
    mvar <- liftIO (newMVar False)
    liftIO $ forkIO $ (parLoop mvar opts chans)
    when (oauto opts) $ do
        liftIO $ putMVar mvar True
        getInputChar "starting autopilot (press any key to stop)"
        liftIO $ putMVar mvar False
    mainLoop mvar opts chans

mainLoop :: MVar Bool -> Opts -> [MVar Bool] -> InputT IO ()
mainLoop pvar opts chans = do
    i <- getInputChar replLine
    if i == Just 'q' || i == Nothing then
        liftIO $ mapM_ (flip tryPutMVar False) chans
    else if i == Just 'a' then do
        liftIO $ putMVar pvar True
        getInputChar "starting autopilot (press any key to stop)" 
        liftIO $ do
            putMVar pvar False
            mapM_ tryTakeMVar chans
        mainLoop pvar opts chans 
    else do
        liftIO $ mapM_ (flip tryPutMVar True) chans
        mainLoop pvar opts chans 

parLoop :: MVar Bool -> Opts -> [MVar Bool] -> IO ()
parLoop ref opts chans = forever $ if (operiod opts <= 0) then 
                         void $ takeMVar ref else do
    var <- tryTakeMVar ref
    if var == Just False then do
        takeMVar ref
        parLoop ref opts chans
    else do
        wrote <- liftIO $ or <$> mapM (flip tryPutMVar True) chans
        when wrote $ do
            putStrLn "(press any key to stop)"
        threadDelay (round $ operiod opts * 1000000)
        parLoop ref opts chans

        

getPids :: Opts -> InputT IO [CPid]
getPids opts = _get [] where 
    _get xs = do
        unfiltWows <- liftIO $ (\\ xs) <$> 
            if owlimited opts then 
                wineWows 
            else if oxlimited opts then 
                xWows 
            else 
                getWows
        let wows = maybe id 
                         (L.filter . (flip L.elem)) (oplimited opts) 
                   unfiltWows
        if oall opts || not (isNothing (oplimited opts)) then
            return wows
        else if null wows then
            return xs
        else do
            names <- liftIO $ traverse (flip runWoW getName) wows
            traverse (\(i,w) -> outputStrLn $ show i ++ ": " ++ 
                B.unpack w) $ zip [1 :: Int ..] names
            outputStrLn " (press any other key to continue)"
            i <- getInputChar "λ CPid <- "
            case do
                i' <- i
                n <- subtract 1 <$> readMaybe [i'] 
                if n < length wows then
                    return $ wows !! n
                else
                    Nothing of
                        Nothing -> return xs
                        Just w -> _get (w:xs)

getRoutes :: InputT IO [RouteList]
getRoutes = do
    r <- maybe "" (filter (/= '\\') . trim) <$> getInputLine "λ Route <- "
    exists <- liftIO $ fileExist r
    if exists then do
        rs <- liftIO $ loadRouteList r
        (rs:) <$> getRoutes
    else
        return []

newPoint :: RT (Point, Bool)
newPoint = get >>= \ui -> lift $ do
    setEcho False
    (hei,wid) <- screenSize
    win <- newWindow 10 61 (hei`quot`2-5) (wid`quot`2-30)
    win' <- subWindow win 8 59 (hei`quot`2-4) (wid`quot`2-29)
    fm <- newField 1 7 1 9
    fx <- newField 1 12 2 9
    fy <- newField 1 12 2 26
    fz <- newField 1 12 2 43
    fw <- newField 1 8 3 9
    fp <- newField 1 8 3 26
    fu <- newField 1 1 5 10
    let fs = [fm,fx,fy,fz,fw,fp,fu]
    forM_ fs $ \f -> do
        F.setBackground f [AttributeUnderline]
    F.setBackground fu [AttributeBold]
    form <- F.newForm fs
    F.setWin form win
    F.setSubWin form win'
    F.postForm form
    updateWindow win $ do
        drawBox Nothing Nothing
        bold $ do
            draw 0 2  "new point"
            draw 2 3  "map ID"
            draw 3 8  "x"
            draw 3 25 "y"
            draw 3 42 "z"
            draw 4 6 "yaw"
            draw 4 21 "pitch"
            draw 6 2 "updated"
            draw 6 10 "["
            draw 6 12 "]"
    let inRed f = do
        setAttribute (AttributeColor $ red $ colors $ ui) True
        f
        setAttribute (AttributeColor $ red $ colors $ ui) False
    let inRed' f = do
        setAttribute (AttributeColor $ red' $ colors $ ui) True
        f
        setAttribute (AttributeColor $ red' $ colors $ ui) False
    b_y <- new_B win 8 2 8
        (bold $ do
            drawText "[pla"
            inRed $ drawText "y"
            drawText "er]")
        (bold $ do
            drawText "[pla"
            inRed' $ drawText "y"
            drawText "er]")
        100000
        (do
            p <- liftIO $ runWoW' (swow ui) getPos
            setContents fm (maybe "" (show . map_id) (p_mid p))
            zipWithM_ setContents [fx,fy,fz,fw,fp]
                ((\f -> maybe' $ f p) <$> [p_x,p_y,p_z,p_h,p_e]))
            

    bdraw b_y
    render
    reposCursor form 

    let toggleU = do
        request form Validate
        u <- fieldContents fu
        if u/="" then
            setContents fu ""
        else
            setContents fu "X"
    let loop = do
        reposCursor form
        ev' <- getEvent win Nothing
        cf <- currentField form
        (\m -> maybe loop m ev') $ \ev -> case ev of
            EventCharacter c
                | c == '\ESC' -> return (emptyPoint, True)
                | c == '\n' -> do
                    request form Validate
                    [m,x,y,z,w,p] <- traverse fieldContents [fm,fx,fy,fz,fw,fp]
                    u <- fieldContents fu
                    return (readPoint x y z w p m, ""/=u)
                | c == 'y' -> do
                    bpress b_y
                    loop
                | c == '\t' -> do
                    request form F.NextField
                    loop
                | cf /= fu && isPrint c -> do
                    write form c
                    loop
                | c == ' ' || c == 'x' -> do
                    toggleU
                    loop
                | c == '\DEL' -> do
                    request form DeletePrev
                    loop
                | c == '\ETB' -> do
                    request form DeleteWord
                    loop
                | c == '\v' -> do
                    request form ClearEOL
                    loop
                | c == '\SOH' -> do
                    request form BeginLine
                    loop
                | c == '\NAK' -> do
                    request form ClearField
                    loop
                | c == '\ENQ' -> do
                    request form EndLine
                    loop
            EventSpecialKey k
                | k == KeyDeleteCharacter -> do
                    request form DeleteChar
                    loop
                | k == KeyBackTab -> do
                    request form PrevField
                    loop
                | k == KeyLeftArrow -> do
                    request form PrevChar
                    loop
                | k == KeyRightArrow -> do
                    request form NextChar
                    loop
                | k == KeyUpArrow -> do
                    request form UpField
                    loop
                | k == KeyDownArrow -> do
                    request form DownField
                    loop
            EventMouse _ MouseState {mouseShift=False, mouseButtons = (k,_):_}
             | k == 4 -> do
                request form PrevField
                loop
             | k == 5 -> do
                request form NextField
                loop
            _ -> loop
    res <- loop
    return res

runRoute :: Opts -> QSem -> RouteList -> CPid -> MVar Bool -> IO ()
runRoute _ _ [] pid _ = putStrLn $ printf "pid %s has an empty route" (show pid)
runRoute opts clickSem xs pid chan = step (cycle xs) where
  len = length xs
  step ys = do
    ext <- takeMVar $ chan
    if ext then do
        n <- stepRoute opts clickSem ys len pid
        step (drop n ys)
    else
        fail "exited"

stepRoute :: Opts -> QSem -> RouteList -> Int -> CPid -> IO Int
stepRoute opts clickSem xs n pid = do
    m <- runWoW pid getMapID 
    let (passed, tocheck) = break (any ((eqM m) . p_mid . fst)) (take n xs)
        len = length passed
    case tocheck of
        [] -> return len
        (entry:_) -> do
            putStrLn $ printf "[%s]" (show pid)
            runWoW pid $ do
                revive
                forM_ entry $ \(p,b) -> do
                  stopFall
                  if ointeract opts then when b $ do
                    tpRefreshed p
                    mobj <- interactZ (round $ odelay opts * 800000) (VKChar 'T')
                    afterops
                    forM mobj $ \(obj, objp) -> do
                        io $ threadDelay (round $ odelay opts * 200000) 
                        setObjPos obj objp
                    bail (ounsafe opts)
                  else if b then do
                    tpRefreshed p
                    io $ threadDelay (round $ odelay opts * 500000)
                    stopFall
                    io $ threadDelay (round $ odelay opts * 500000)
                    afterops
                  else void $ do
                    setPos p
                    afterops
            return (len+1)
    where
        afterops = do
                forM_ (opitch opts) $ \pitch -> do
                    c <- getCam
                    setCam c { c_pitch = pitch }
                forM_ (ofarclip opts) $ \farclip -> do
                    setFarclip farclip
                forM_ (omaxfps opts) $ \maxfps -> do
                    setMaxfps maxfps
                forM_ (omaxfpsbk opts) $ \maxfpsbk -> do
                    setMaxfpsBK maxfpsbk
                forM_ (oclicks opts) $ \delay -> do
                    liftIO $ threadDelay (round $ delay * 1000000)
                    bail (ounsafe opts)
                    cont <- flip (maybe (return True)) (ouid opts) $ \uid -> do
                        goUid uid
                    when cont $ do
                        io $ waitQSem clickSem
                        clickAt 3 0.5 0.5
                        io $ signalQSem clickSem
                    bail (ounsafe opts)
                forM_ (opressafter opts) $ \key -> do
                    press (vkt key)
                    io $ threadDelay 150000
                bail (ounsafe opts)
                io $ threadDelay 150000

-- utils
----------

draw :: Int -> Int -> Text -> Update ()
draw y x s = moveCursor y x >> drawText s

bold :: Update a -> Update ()
bold f = setAttribute AttributeBold True >> f >> setAttribute AttributeBold False

new_B :: Window -> Int -> Int -> Int -> Update () 
    -> Update () -> Int -> Curses () -> Curses B
new_B win y x width upd updc delay act = 
    let b = B { 
          bdraw = do
            -- (w,h) <- screenSize
            updateWindow win $ do
                -- (x',y') <- windowPosition
                do --when (y>=0 && y+y'<h && x>=0 && x+x'+width<w) $ do
                    moveCursor y x
                    upd
        , benclosed = \y' x' -> lift $ updateWindow win $ do
            (y'', x'') <- windowPosition
            return $ (y''+y)==y' && (x''+x)<=x' && (x''+x+width)>x'
        , bpress = do
            updateWindow win (moveCursor y x >> updc)
            render
            act
            render
            io $ threadDelay delay
            bdraw b
            render
        }
    in return b
    
maybe' :: Show a => Maybe a -> String
maybe' = maybe "" show

groupOn :: Eq b => (a -> b) -> [a] -> [[a]]
groupOn f = groupBy (\a b -> f a == f b)

eqM :: MapID -> Maybe MapID -> Bool
eqM _ Nothing = True
eqM m (Just m') = m == m'

formatting :: [a] -> Int -> [[a]]
formatting str n = 
    [concat $ take (length lsts) $ drop i (cycle lsts) | i <- [0..n-1]]
    where lsts = formatting' str n

formatting' :: [a] -> Int -> [[a]]
formatting' str n = let (a,b) = splitAt n str 
                   in zipWith (:) a (formatting' b n ++ (repeat []))

col :: (Num a, Show a) => [a] -> String
col [] = col [0 :: Int]
col xs = "\ESC[" ++ foldl1 (\s s' -> s ++ ";" ++ s') (show <$> xs) ++ "m\STX"

bail :: Bool -> WoW ()
bail unsafe = do
    reload
    incombat <- inCombat
    when (incombat && not unsafe {- || state /= 0x80000000 -} ) $ do 
        pos <- getPos
        stopFall
        tpRefreshed pos { p_z = Just (-495) }
        stopFall
    casting <- isCasting
    when casting $ do
        io $ threadDelay 100000
        bail unsafe
    -- io $ threadDelay 150000

goUid :: Word32 -> WoW Bool
goUid uid = do
    objs <- findUid uid
    if null objs then
        return False
    else do
        p <- objPos (head objs)
        p' <- getPos
        when (pointDist p p' > 10) $ do
            io $ putStrLn "relocating"
            tpRefreshed p { p_z = Just (-495) }
            io $ threadDelay 1000000
            setPos p
            io $ threadDelay 150000
        return True


shuffleList :: [a] -> IO [a]
shuffleList xs = if length xs < 2 then return xs else do
    r <- randomRIO (1, length xs - 1)
    let (left, right:rights) = splitAt r xs
    (right:) <$> shuffleList (left ++ rights)
