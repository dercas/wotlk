{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

module WoW.Wotlk.X where

import Codec.Compression.GZip

import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.Reader
import Control.Monad.State

import qualified Data.ByteString.Lazy as BL
import Data.Foldable
import Data.IORef
import Data.Maybe
import Data.NMap
import Data.Serialize

import Foreign hiding (void)

import GHC.Generics
import Graphics.X11 (get_EventType, keyPress)

import Text.Read

import WoW.Wotlk.Core
import WoW.Wotlk.TP

import XHotkey hiding (io)

data XConf = XConf
    { c_sf  :: [KM]
    , c_sfm :: [KM]
    , c_tp  :: [KM]
    , c_tpm :: [KM]
    , c_mvf :: [KM]
    , c_mvb :: [KM]
    , c_mvl :: [KM]
    , c_mvr :: [KM]
    , c_mvu :: [KM]
    , c_mvd :: [KM]
    , c_mvsl :: [KM]
    , c_mvsr :: [KM]
    , c_dd  :: [KM]
    , c_hd  :: [KM]
    , c_d0  :: Float
    } deriving (Show, Generic)

defaultXConf = XConf [] [] [] [] [] [] [] [] [] [] [] [] [] [] 10.0

instance Serialize XConf where

confBinds :: XConf -> IORef [WEnv] -> IORef Point -> IORef Float -> IO Bindings
confBinds c wr pr dr = do
    (mvf1,mvf2) <- mvF dr
    (mvb1, mvb2) <- mvB dr
    (mvl1, mvl2) <- rotL 
    (mvr1, mvr2) <- rotR 
    (mvu1, mvu2) <- mvU dr
    (mvd1, mvd2) <- mvD dr
    (sl1,  sl2) <- mvL dr
    (sr1,  sr2) <- mvR dr
    return $ foldl mappend (fromList []) $ 
        [ branch (c_sf c) stopFallCurrent
        , branch (c_sfm c) $ stopFallMult wr
        , branch (c_tp c) $ tpCurrent pr
        , branch (c_tpm c) $ tpMult pr wr
        , branch (c_mvf c) $ mvf1
        , branch (up_ <$> c_mvf c) $ mvf2
        , branch (c_mvb c) $ mvb1
        , branch (up_ <$> c_mvb c) $ mvb2
        , branch (c_mvl c) $ mvl1
        , branch (up_ <$> c_mvl c) $ mvl2
        , branch (c_mvr c) $ mvr1
        , branch (up_ <$> c_mvr c) $ mvr2
        , branch (c_mvu c) $ mvu1
        , branch (up_ <$> c_mvu c) $ mvu2
        , branch (c_mvd c) $ mvd1
        , branch (up_ <$> c_mvd c) $ mvd2
        , branch (c_mvsl c) $ sl1
        , branch (up_ <$> c_mvsl c) $ sl2
        , branch (c_mvsr c) $ sr1
        , branch (up_ <$> c_mvsr c) $ sr2
        , branch (c_dd c) $ doubleD dr
        , branch (c_hd c) $ halfD dr
        ]

bindConf :: XConf -> IORef [WEnv] -> IORef Point -> IORef Float -> X ()
bindConf c wr pr dr = setBindings =<< io (confBinds c wr pr dr)

newSet :: IO (IORef [WEnv], IORef Point, IORef Float)
newSet = do
    wr <- newIORef []
    pr <- newIORef emptyPoint
    dr <- newIORef 1
    return (wr,pr,dr)

stopFallCurrent :: X ()
stopFallCurrent = void $ inCurrentWoWX stopFall

stopFallMult:: Traversable t => IORef (t WEnv) -> X ()
stopFallMult ref = liftIO $ do
    wows <- readIORef ref
    forM_ wows (flip runWoW' stopFall)

tpCurrent :: IORef Point -> X ()
tpCurrent ref = do
    p <- liftIO $ readIORef ref
    void $ inCurrentWoWX (setPos p)

tpMult :: Traversable t => IORef Point -> IORef (t WEnv) -> X ()
tpMult ref rwows = liftIO $ do
    p <- readIORef ref
    wows <- readIORef rwows
    forM_ wows (flip runWoW' (setPos p))

mv' :: (Float -> WoW ()) -> IORef Float -> IO (X (), X ())
mv' act dr = do
    s <- newIORef False
    let floop wow = do
        rep <- readIORef s
        when rep $ do
            n <- readIORef dr
            runWoW' wow (act (n/40))
            threadDelay 25000
            floop wow
    let f1 = io $ void $ forkIO $ do
        wow <- currentWoW
        writeIORef s True
        traverse_ floop wow
    let f2 = io $ writeIORef s False
    return (f1,f2)

rot' :: (Float -> WoW ()) -> Float -> IO (X (), X ())
rot' act th = do
    s <- newIORef False
    let floop wow = do
        rep <- readIORef s
        when rep $ do
            runWoW' wow (act (th/100))
            threadDelay 10000
            floop wow
    let f1 = io $ void $ forkIO $ do
        wow <- currentWoW
        writeIORef s True
        traverse_ floop wow
    let f2 = io $ writeIORef s False
    return (f1,f2) 

mvF :: IORef Float -> IO (X (), X())
mvF = mv' moveF

mvB :: IORef Float -> IO (X (), X ())
mvB = mv' moveB 

mvL :: IORef Float -> IO (X (), X ())
mvL = mv' moveL

mvR :: IORef Float -> IO (X (), X ())
mvR = mv' moveR

mvU :: IORef Float -> IO (X (), X ())
mvU = mv' moveU

mvD :: IORef Float -> IO (X (), X ())
mvD = mv' moveD

rotL :: IO (X (), X ())
rotL = rot' rot 1.5

rotR :: IO (X (), X ())
rotR = rot' rot (-1.5)

doubleD :: IORef Float -> X ()
doubleD ref = io $ modifyIORef ref (*2)

halfD :: IORef Float -> X ()
halfD ref = io $ modifyIORef ref (/2)

loadXConf :: FilePath -> IO XConf
loadXConf fp = do
    bs <- BL.readFile fp
    return $ either (\_ -> defaultXConf) id
        (decodeLazy =<< try' (decompress bs))

saveXConf :: FilePath -> XConf -> IO ()
saveXConf fp xb = BL.writeFile fp (compress $ encodeLazy xb)


