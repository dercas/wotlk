{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Fisherman where

import Control.Applicative
import Control.Concurrent
import Control.Exception
import Control.Monad.RWS
import Control.Monad.Trans.State (StateT (..))

import Data.Maybe
import Data.List as L
import Data.Time.LocalTime

import Text.Printf

import WoW.Wotlk.Core
import WoW.Wotlk.Box

data Bobber = Bobber Int OfsObj
  deriving (Eq, Show)

getBobbers :: WoW [Bobber]
getBobbers = do 
    bobs <- findUid 35591 
    return (zipWith Bobber [1..] bobs)

getOwnBobbers :: WoW [Bobber]
getOwnBobbers = do
    g <- getGuid
    bs <- getBobbers
    filterM (\(Bobber _ o) -> (== g) <$> ar (ofs_addr o + 0x230)) bs

reorderBobbers :: [Bobber] -> [Bobber]
reorderBobbers bobs = zipWith Bobber [1..] ((\(Bobber _ b) -> b) <$> bobs)

bobberState :: Bobber -> WoW Word8
bobberState (Bobber _ o) = ar (ofs_addr o + 0xbc)

spellIsFishing :: Word32 -> Bool
spellIsFishing = (`elem` [7620, 7731, 7732, 18248, 33095, 51294])

isFishing :: WoW Bool
isFishing = spellIsFishing <$> channelingID

data Settings = Settings
  { b_cast      :: VKt 
  , b_interact  :: VKt
  , s_recasts   :: Int
  } deriving Show

defaultSettings :: Settings
defaultSettings = Settings (VKNum 1) (VKChar 'T') 10

data LogEntry = LogEntry TimeOfDay Bool String

instance Show LogEntry where
    show (LogEntry time success txt) = t ++ " " ++ txt
      where
        t = printf "[\ESC[%dm\STX%0.2d:%0.2d:%07.4f\ESC[m\STX]" 
                  (if success then 32 else 31 :: Int) (todHour time) 
                  (todMin time) (realToFrac $ todSec time :: Double)

type Log = [LogEntry]

newtype F a = F (RWST Settings Log (Maybe Bobber) WoW a)
    deriving (Functor, Applicative, MonadIO, MonadState (Maybe Bobber), 
              MonadReader Settings, MonadWriter Log) 

runF :: F a -> Settings -> Maybe Bobber -> WoW (a, Maybe Bobber, Log)
runF (F op) s b = runRWST op s b

liftW :: WoW a -> F a
liftW = F . lift

newtype FException = FException LogEntry
  deriving (Show)

instance Exception FException

instance Alternative F where
    empty = failF "Alternative.empty"
    op <|> op' = catchF op (\(FException loge) -> tell [loge] >> op')

instance MonadPlus F where
    mzero = empty
    mplus = (<|>)

catchF :: Exception e => F a -> (e -> F a) -> F a
catchF (F (RWST op)) handlr = F $ RWST $ \r s -> StateT $ \ws-> do
    catch (runStateT (op r s) ws)
          $ \e -> case handlr e of
              F (RWST f) -> runStateT (f r s) ws

instance Monad F where
    F m >>= f = F (m >>= (\(F m') -> m') . f)
    return = F . return
    fail = failF

logF :: Bool -> String -> F ()
logF success msg = do
    t <- localTimeOfDay . zonedTimeToLocalTime <$> io getZonedTime
    let e = LogEntry t success msg
    io $ print e
    tell [e]

failF :: String -> F a
failF msg = do
    t <- localTimeOfDay . zonedTimeToLocalTime <$> io getZonedTime
    let e = LogEntry t False msg
    io $ print e
    throw (FException e)

fish :: F Bool
fish = do
    n <- s_recasts <$> ask
    do skipbobbers
       cast n 
       findbobber 0
       reel 
       return True
    <|> return False
    where
    skipbobbers :: F ()
    skipbobbers = do
        bs <- liftW getOwnBobbers
        if L.null bs then do
            logF True "Casting"
        else do
            io (threadDelay 100000)
            skipbobbers
    cast :: Int -> F ()
    cast n
      | n > 0 = do
          key <- b_cast <$> ask
          liftW (press $ vkt key)
          io $ threadDelay 250000
          fishn <- liftW isFishing
          unless fishn $ do
              logF False " ├─Couldn't start casting"
              cast (n-1)
      | otherwise = do
          n' <- s_recasts <$> ask
          failF $ printf " └─Couldn't start fishing after %d casts" n'
    findbobber :: Int -> F ()
    findbobber n 
      | n > 30 = fail " └─Couldn't find bobber after 3s"
      | otherwise = do
          bs <- liftW getOwnBobbers
          case bs of
              [b] -> do
                  logF True " ├─Found bobber"
                  put (Just b)
              _ -> io (threadDelay 100000) >> findbobber (n+1)
    reel :: F ()
    reel = do
        inter <- b_interact <$> ask
        Just (b @ (Bobber _ bo)) <- get
        bobstate <- liftW $ bobberState b
        when (bobstate /= 0) $ do
            logF True " ├─Reeling in"
            io $ threadDelay 400000
            liftW $ do setTar bo
                       press (vkt inter)
            io $ threadDelay 100000
        io $ threadDelay 150000
        fishn <- liftW isFishing
        if fishn then do
            reel
        else
            logF True " └─Fished succesfully"

interactable :: OfsObj -> WoW (Addrs Word32)
interactable o = sequence [ (,) off <$> ar (ofs_addr o + off) 
                 | off <- [0x23c, 0x25c, 0x260, 0x264, 0x26c, 0x270, 0x274]]

checkBobbers :: WoW ()
checkBobbers = do
    bs <- getBobbers
    forM_ bs $ \(Bobber n o) -> do
        io $ printf "#%d:\n" n
        io . colprint =<< interactable o

browseb :: Bobber -> WoW [Addr Word32]
browseb (Bobber _ o) = objbrowsea o

browseb_ :: WoW [Addr Word32]
browseb_ = do
    bs <- getBobbers
    maybe [] id <$> forM (listToMaybe bs) browseb

main :: IO ()
main = void $ inCurrentWoW $ forever $ do
    (_, _, logs) <- runF fish defaultSettings Nothing
    -- io $ mapM_ print logs
    return ()
    
