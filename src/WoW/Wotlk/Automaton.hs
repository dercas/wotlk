module WoW.Wotlk.Automaton where

import Control.Concurrent
import Control.Exception
import Control.Monad
import qualified Control.Monad.State.Strict as ST

import Data.Bifunctor
import Data.ByteString.Char8
import Data.Foldable
import Data.Function
import Data.IORef 
import qualified Data.List as L
import qualified Data.Map as Ma
import Data.Maybe

import System.Directory

import Text.Printf

import UI.NCurses 
import qualified UI.NCurses.Menu as M

import WoW.Wotlk.Core
import WoW.Wotlk.Box

import MCurses
import WoW.Wotlk.Functions

data AEnv = AEnv
  { aenv_groups :: Ma.Map String [WEnv]
  , aenv_funcs  :: Ma.Map String (Auto Bool)
  , aenv_binds  :: Ma.Map String VK
  , aenv_wows   :: [WEnv]
  , aenv_curr   :: String
  , aenv_ref    :: IORef [WEnv]
  , aenv_fp     :: FilePath
  , aenv_r      :: RunEnv
  , aenv_run    :: Bool
  }

type Auto = StateW AEnv

instance Show AEnv where
    show a = printf "groups: %s, wows: %s, curr: %s"
            (show $ aenv_groups a) (show $ fmap wenv_pid $ aenv_wows a) (show $ aenv_curr a)

recompile :: Auto ()
recompile = do
    ae <- get
    prog <- io $ loadProg' (aenv_fp ae)
    let old = Ma.keys (r_wows (aenv_r ae)) L.\\ Ma.keys (r_wows prog)
        new = Ma.keys (r_wows prog) L.\\ Ma.keys (r_wows (aenv_r ae))
    forM old $ destroy' . printf " %s "
    forM new $ groupM . printf " %s "
    put ae { aenv_r = prog }
    populate' " Functions "

populatePIDs :: Auto ()
populatePIDs = do
    aenv <- get
    wows <- liftIO $ readIORef (aenv_ref aenv)
    let awows = aenv_wows aenv
    let wows' = 
          if awows `L.union` wows == awows then
              diffWows wows awows
          else
              Just wows
    case wows' of
        Nothing -> nop
        Just wows -> do
            (map, (groups, popgs)) <- flip runStateT ([], False)
              $ flip Ma.traverseWithKey (aenv_groups aenv) $ \gr wews -> do
                case diffWows wows wews of
                    Nothing -> do
                        return wews
                    Just wows' -> do
                        modify $ first (gr:)
                        when (L.length wews /= L.length wows) $ do
                            modify $ second (\_ -> True)
                        return wows'
            put aenv { aenv_wows = wows, aenv_groups = map }
            menv <- get'
            let order = flip L.takeWhile (menv_zorder menv) $ \p ->
                        p_name p /= " PIDs "
            forM_ groups $ \g -> populate' $ " " ++ g ++ " "
            when popgs $ populate' " Groups "
            populate' " PIDs "
            rend' " PIDs "
            forM_ (L.reverse order) rend

diffWows :: [WEnv] -> [WEnv] -> Maybe [WEnv]
diffWows full part = 
    let (changed, mwows) = L.unzip $ flip fmap part $ \w -> 
          case L.filter (== w) full of
              w' : _ -> if wenv_name w == wenv_name w' then
                            (False, Just w)
                        else
                            (True, Just w')
              _ -> (True, Nothing)
    in if or changed then
        Just (catMaybes mwows)
    else
        Nothing

titleM :: FilePath -> Auto ()
titleM nam = (selPanel =<<) $ do
    let (ha,re) = L.length nam `quotRem` 2
        (le, ri) = (fromIntegral $ ha + re, fromIntegral ha)
    new_P $ PanPars 
      "title" ((1/90,0), (1/2,-le), (1/90,1), (1/2,ri))
      (\w -> lift' $ updateWindow w $ do
          moveCursor 0 0
          drawString nam)

pidM :: Auto ()
pidM = (selPanel =<<) $ new_M $ MenuPars
  " PIDs "
  ((1/90, 1), (1/8, -9), (8/10, 22-23*8/10), (13/32, 28-79*13/32))
  (do
      aenv <- get
      return (" Groups ", aenv_curr aenv))
  []
  (\_ -> do
      aenv <- get
      let wows = aenv_wows aenv
          mgroup = Ma.lookup (adjSpace $ aenv_curr aenv) (aenv_groups aenv)
          newows = case mgroup of
              Nothing -> wows
              Just w -> wows L.\\ w
      return $ wenvToTuple <$> newows)
  (\(_, _, menu, n) ev -> case ev of
      EventCharacter 'q' -> mzero
      EventCharacter ' ' -> (>> return True) 
        $ replicateM (if n == 0 then 1 else n) $ do
          mpid <- lift' $ M.currentItem menu
          forM mpid $ \pid -> do
              wenv <- lift' (M.itemName pid) >>= liftIO . newWEnv . read 
              aenv <- get
              let curr = adjSpace $ aenv_curr aenv
                  newmap = Ma.adjust (flip L.union [wenv]) curr 
                                              (aenv_groups aenv)
              put aenv { aenv_groups = newmap }
              populate' (aenv_curr aenv)
              rend' (aenv_curr aenv)
              populate' " Groups "
              rend' " Groups "
              populate' " PIDs "
              rend' " PIDs "
      EventCharacter '\t' -> selPanel' " Functions " >> return True
      _ -> return False)
  (\_ _ -> nop)
  (Ma.fromList [(Right k, " Groups ") | k <- [KeyPreviousPage, KeyNextPage]])
  True
  menuStyle
  backStyle

groupM :: String -> Auto ()
groupM name = (selPanel =<<) $ new_M $ MenuPars
  (printf " %s " name)
  ((1/90, 1), (13/32, 30-79*13/32), (8/10, 22-23*8/10), (11/16, 56-79*11/16))
  (return (" PIDs ", " Groups "))
  []
  (\(_, _, menu, _) -> do
      AEnv { aenv_groups = groups } <- get 
      return $ wenvToTuple <$> lookup' name groups)
  (\(_, _, menu, n) ev -> let n1 = if n == 0 then 1 else n in case ev of
      EventCharacter 'q' -> mzero
      EventCharacter ' ' -> (>> return True) $ do
          m <- replicateM n1 $ do
              mpid <- lift' $ M.currentItem menu
              forM_ mpid $ \pid -> do
                  wenv <- lift' (M.itemName pid) >>= liftIO . newWEnv . read
                  aenv <- get
                  let curr = aenv_curr aenv
                      curr' = adjSpace curr
                      newmap = Ma.adjust (L.delete wenv) curr' (aenv_groups aenv)
                  put aenv { aenv_groups = newmap } 
                  populate' curr
              return (mpid /= Nothing)
          when (or m) $ do
              populate' " Groups "
              rend' " Groups "
              populate' " PIDs "
              rend' " PIDs "
              rend' $ printf " %s " name
      EventCharacter '\t' -> selPanel' " Functions " >> return True
      _ -> return False)
  (\(_, _, menu, _) ev -> case ev of
      EventCharacter c | c == 'K' || c == 'J' -> do
          its <- lift' $ M.menuItems menu
          pids <- fmap read <$> lift' (mapM M.itemName its)
          modify $ \aenv -> aenv { aenv_groups = Ma.adjust 
              (\w -> catMaybes $ (\p -> L.find ((== p) . wenv_pid) w) <$> pids)
              name (aenv_groups aenv) }
          nop
      _ -> nop)
  (Ma.fromList [(Right k, " Groups ") | k <- [KeyPreviousPage, KeyNextPage]])
  True
  menuStyle
  backStyle

groupsM :: Auto ()
groupsM = (selPanel =<<) $ new_M $ MenuPars 
  " Groups "
  ((1/90,1), (11/16, 58-79*11/16), (8/10, 22-23*8/10), (7/8, 78-79*7/8))
  (do
      aenv <- get
      return (aenv_curr aenv, " PIDs "))
  [B "Recompile" [Left 'R'] 
      (\_ -> recompile) emptyScheme emptyScheme emptyScheme]
  (\(_, _, menu, _) -> do
      AEnv { aenv_groups = map } <- get
      return $ flip3 Ma.foldrWithKey [] map $ \g w -> 
        let n = L.length w
            nam = if n == 0 then "" else printf "%d" n
        in ((nam, g):))
  (\(_,_,menu,n) ev -> let n1 = if n == 0 then 1 else n in case ev of
      EventCharacter 'q' -> mzero
      EventSpecialKey KeyNextPage -> do
          lift' $ replicateM_ n1 $ M.request menu M.DownItem 
          upd menu
          return True
      EventSpecialKey KeyPreviousPage -> do
          lift' $ replicateM_ n1 $ M.request menu M.UpItem 
          upd menu
          return True
      EventCharacter '\t' -> selPanel' " Functions " >> return True
      _ -> return False)
  (\(_,_,menu,_) _ -> do
      upd menu)
  (Ma.fromList [])
  True
  menuStyle
  backStyle where
  upd menu = do
      mgroup <- lift' $ mapM M.itemDesc =<< M.currentItem menu
      forM_ mgroup $ \group -> do
          let group' = " " ++ group ++ " "
          modify $ \aenv -> aenv { aenv_curr = group' }
          t <- topPanel

          populate' " PIDs "
          rend' " PIDs "
          rend' " Groups "
          atopPanel' group'
          forM_ t $ \t' -> do
              aenv <- get
              if (adjSpace $ p_name t') `Ma.member` aenv_groups aenv then do
                  selPanel' group'
              else
                  atopPanel t'
          rend' group' 

logM :: Auto (Panel AEnv)
logM = do
    new_P $ PanPars
        "log"
        ((1/4,4-23/4), (5/12,26.3-79*5/12), (7/12,18-23*7/12), (2/3, 76-79*2/3))
        (\w -> do
            ae <- get
            let st = if r_state (aenv_r ae) /= Just False then
                    ([ Left (ColorGreen, ColorBlack)
                     , Right AttributeBold ], "Ok")
                  else
                    ([ Left (ColorRed, ColorBlack)
                     , Right AttributeBold ], "Error")
                pairs = st : ((,) [ Left (ColorWhite, ColorBlack) ] 
                                  <$> (r_log $ aenv_r ae))
            lift' $ updateWindow w $ do
                (h,w) <- windowSize
                clearLines [0..h-1]
                setBackgroundFilled $ convAtts $ [Left (ColorBlack, ColorBlack)]
                setAttributes [AttributeBold]
                drawBox Nothing Nothing
                drawMult 1 1 pairs)

progM :: Auto (Panel AEnv)
progM = do
    let runf (_w,_w',menu,n) = let n1 = if n == 0 then 1 else n in do
          lift' (M.currentItem menu) >>= mapM_ ? \it -> do
              str <- lift' $ M.itemName it
              (`mplus` return ()) $ replicateM_ n1 $ do
                  ae <- get
                  fe <- io $ ST.execStateT (refresh_all  
                                          >> callFunc (adjSpace str)) 
                      (aenv_r ae) { r_wows = aenv_groups ae }
                  modify $ \a -> a { aenv_r = fe }
                  populate' "log"
                  guard `mapM_` r_state fe
    new_M $ MenuPars
      " Functions "
      ((5/12,6-23*5/12), (1/3,3-79/3), (3/4,20-23*3/4), (7/12,52.6-79*7/12))
      (do
          return ("", ""))
      [ b "[run function]" [Left '\n', Left 'r'] runf
      , b "[clear log]" [Left 'c'] $ \_ -> do
          modify $ \ae -> ae { aenv_r = (aenv_r ae) { r_log = [] } }
          populate' "log"]
      (\_ -> do
          ae <- get
          return $ x2t <$> (Ma.keys $ r_funcs $ aenv_r ae))
      (\(win, _, menu, n) ev -> case ev of
          EventCharacter 'q' -> mzero
          EventCharacter '\t' -> minPanel' " Functions " >> return True
          _ | ev == eu OnLeave -> do
              minPanel' " Functions "
              aenv <- get
              top <- maybe " Groups " p_name <$> topPanel
              mapM_ rend' $ L.delete top 
                      ["title", " PIDs ", " Groups ", aenv_curr aenv]
              lock False
              return True
          _ | ev == eu OnEnter -> do
              atopPanel' "log"
              populate' "log"
              lock True
              return True
          _ -> return False)
      (\_ _ -> rend' " Functions ")
      mempty
      True
      menuStyle'
      invScheme where
      b name binds op = B name binds op emptyScheme emptyScheme emptyScheme

type SelEnv = ([FilePath], FilePath, Ma.Map FilePath [String], [String], Prog)
data Selector = Selector
  { s_paren   :: [FilePath]
  , s_curr    :: FilePath
  , s_child   :: Ma.Map FilePath [String]
  , s_cs      :: (Bool, [String])
  , s_prog    :: (FilePath, RunEnv)
  } deriving Show

selectProg :: StateW s (FilePath, RunEnv)
selectProg = fmap fst $ port (Selector [] "" mempty (False, []) ("", mempty { r_funcs = mempty })) $ do
    lift' clearBackground
    let getfilt di = do
          let adj f = 
                if di /= "." then 
                    if L.last di == '/' then
                        di ++ f
                    else
                        L.concat [di, "/", f] 
                else f
          fs <- getDirectoryContents di 
          fmap catMaybes $ forM fs $ \fp -> do
              dir <- doesDirectoryExist $ adj fp
              if dir && fp /= "." && fp /= ".." then 
                  return $ Just $ L.concat [adj fp, "/"]
              else if ".bot" `L.isSuffixOf` fp then
                  return $ Just $ (adj fp)
              else
                  return Nothing
        upd pop = do
          s <- get
          (put =<<) $ io $ do
              curr <- getCurrentDirectory
              parents <- if curr == "/" then return [] 
                                        else getfilt ".."
              this <- getfilt "." 
              child <- forM this $ \f -> do
                  if L.last f == '/' then do
                      s <- getfilt f
                      return (f, basename <$> s)
                  else do
                      c <- L.lines <$> readFile' f
                      return (f,c)
              return s 
                { s_paren = parents
                , s_curr = curr
                , s_child = Ma.fromList child
                , s_cs = s_cs s }
          when pop $ do
              -- populate' "parent"
              populate' "current"
              populate' "children"
              populate' "parent"
              on_enter' "children"
    upd False
    new_M $ MenuPars 
      "current" ((0.0,0), (0.15,0), (1.0,0), (0.44, 0))
      (return ("","")) []
      (\(_,sw,_,_) -> do
          s <- get
          (_,w) <- lift' $ updateWindow sw windowSize
          let namsz = L.maximum (L.length <$> Ma.keys (s_child s))
              sz = (w - namsz - 2)
              pad = printf "%*.*s" sz sz
          return $ flip fmap (Ma.toList (s_child s)) $ \(nam, c) -> 
            let dir = L.last nam == '/'
            in (nam, (if dir then "[value=False]" else "") 
              ++ pad (if dir then 
                          show $ L.length c 
                      else 
                          show (L.length c) ++ " l") )) 
      (\(_,_,menu,_) ev -> case ev of
          _ | any' [Left 'h', Right KeyLeftArrow] ev -> do
              io $ setCurrentDirectory ".."
              upd True
              return True
          _ | any' [Left '\n', Left 'l', Right KeyRightArrow] ev -> do
              mit <- lift' $ M.currentItem menu
              case mit of
                  Nothing -> mzero 
                  Just it -> do
                      nam <- trim <$> lift' (M.itemName it)
                      if L.last nam == '/' then do
                          io $ setCurrentDirectory nam
                          upd True
                      else do
                          p <- io $ loadProg' nam
                          modify' $ \s -> s { s_prog = (nam, p) }
                      return True
          EventCharacter 'q' -> mzero
          _ -> return False)
      (\(_,_,m,_) ev -> do
          mitem <- lift' (M.currentItem m)
          lift' $ M.forItems_ m $ \i -> do
              nam <- M.itemName i
              unless (mitem == Just i) $
                  M.selectable i (L.last nam /= '/')
              return ()
          forM_ mitem $ \item -> do
              nam <- lift' $ trim <$> M.itemName item
              s <- get
              let dir = L.last nam == '/' 
              when dir $ lift' $ do
                  M.selectable item True
              lift' $ M.setForeground m $ convAtts $ 
                if dir then
                    [ Left (ColorBlue, ColorDefault)
                    , Right AttributeReverse
                    , Right AttributeBold ]
                else 
                    [ Left (ColorGreen, ColorDefault)
                    , Right AttributeReverse
                    , Right AttributeBold ]
              let xs = maybe [] id $ Ma.lookup nam (s_child s)
              put s { s_cs = (L.last nam == '/', xs) }
          populate' "children")
      mempty False 
      emptyScheme { c_back = [ Left (ColorGreen, ColorDefault)
                             , Right AttributeBold ]
                  , c_grey = [ Left (ColorBlue, ColorDefault)
                             , Right AttributeBold ] } 
      emptyScheme
    new_P $ PanPars
      "parent" ((0,0), (0,0), (1,0), (0.15,0))
      $ \win -> do
          Selector { s_paren = xs } <- get
          let pairs = do
              x <- xs
              let a = [Right AttributeBold
                    , Left (if L.last x == '/' then ColorBlue else ColorGreen
                    , ColorDefault)]
              return (a,x)
          lift' $ updateWindow win $ do
              clear
              drawMult 1 1 pairs
    new_P $ PanPars
      "children" ((0,0), (0.44,1), (1,0), (1,0))
      $ \win -> do
          Selector { s_cs = (b,xs) } <- get
          let pairs = do
              x <- xs
              let a = 
                    if b then
                        if L.last x == '/' then
                            [Left (ColorBlue, ColorDefault), Right AttributeBold]
                        else
                            [Left (ColorGreen, ColorDefault), Right AttributeBold]
                    else
                        []
                        
              return (a,x)
          lift' $ updateWindow win $ do
              clear
              -- setBackgroundFilled $ convAtts [Left (ColorBlack, ColorCyan)]
              drawMult 1 1 pairs
    selPanel' "parent"
    selPanel' "children"
    selPanel' "current"
    menv <- get'
    mainDo (not . Ma.null . r_funcs . snd . s_prog <$> get)
    s_prog <$> get

menuStyle = emptyScheme
  { c_back = [Left (ColorBlack, ColorCyan)]
  , c_fore = [Left (ColorWhite, ColorBlue), Right AttributeBold]
  , c_deco = [Left (ColorDefault, ColorCyan)]
  , c_title = [Left (ColorDefault, ColorDefault), Right AttributeBold, Right AttributeReverse]
  }
backStyle = menuStyle
  { c_fore = [Left (ColorWhite, ColorBlue)]
  , c_deco = [Left (ColorDefault, ColorDefault)]
  , c_title = [Left (ColorDefault, ColorDefault), Right AttributeBold]
  }
menuStyle' = emptyScheme
  { c_back = [ Left (ColorBlack, ColorGreen)]
  , c_fore = [ Left (ColorWhite, ColorBlack) ]
  , c_deco = [Left (ColorBlack, ColorBlack), Right AttributeBold]
  , c_title = [Left (ColorWhite, ColorBlack), Right AttributeBold]
  }
  
backWows :: Auto ThreadId
backWows = do
    ref <- aenv_ref <$> get
    let loop = do
        writeIORef ref =<< mapM newWEnv =<< wineWows
        threadDelay 1200000
        loop
    liftIO $ forkIO $ loop

runAuto :: Auto a -> IO (a, AEnv)
runAuto op = runCurses $ do
    clearBackground
    setCursorMode CursorInvisible
    setEcho False
    mouseInterval 20
    ref <- liftIO $ newIORef []
    runStateW op (AEnv mempty mempty mempty [] " Groups " ref "" mempty False)

startProg :: FilePath -> RunEnv -> Auto ()
startProg nam r = do
    lift' clearBackground
    wows <- io $ (mapM newWEnv =<< wineWows)
    modify $ \aenv -> aenv 
      { aenv_groups = Ma.adjust (\_ -> wows) "all" (r_wows r )
      , aenv_curr = " all " 
      , aenv_fp = nam
      , aenv_r = r
      }
    titleM nam
    progM
    logM 
    pidM
    groupsM
    forM_ (Ma.keys $ r_wows r) groupM
    selPanel' " all "
    selPanel' " PIDs "
    backWows

    regOp 1200 populatePIDs

    mainLoop

main :: IO ()
main = void $ runAuto $ do
    lift' $ do
        clearBackground
        setCursorMode CursorInvisible
        setEcho False
        def <- defaultWindow
        updateWindow def $ do
            let nam = "Program #8172"
            (_,w) <- windowSize
            moveCursor 0 $ (w - fromIntegral (L.length nam)) `quot` 2
            drawString nam
        render
    (nam, prog) <- selectProg
    when (Ma.keys (r_funcs prog) /= mempty) $ do
        startProg nam prog

-- util

refresh_all = do
    renv <- get
    map <- forM (r_wows renv) $ mapM 
                (io . newWEnv . wenv_pid )
    put renv { r_wows = map }

wenvToTuple wenv = (show $ wenv_pid wenv, unpack $ wenv_name wenv)

adjSpace :: String -> String
adjSpace str = L.reverse $ trim $ L.reverse $ trim str

flip3 :: (a -> b -> c -> d) -> b -> c -> a -> d
flip3 f a b c = f c a b

basename :: FilePath -> FilePath
basename fp = case L.break (== '/') fp of
    ("", fp) -> snd $ L.span (== '/') fp
    (_, fp) -> basename fp

infixr 9 ?
(?) = ($)

