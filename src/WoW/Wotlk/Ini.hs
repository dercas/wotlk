module WoW.Wotlk.Ini where

import Data.ByteString.Char8

import Text.Read

import WoW.Wotlk.Core hiding (readPoint)
import WoW.Wotlk.TP

newtype Ini = Ini { items :: [IniItem] }
    deriving Show
newtype IniItem = IniItem { item :: Entry }
    deriving Show

instance Read Ini where
    readPrec = Ini <$> choice
        [ do
            it <- readPrec
            its <- readPrec
            return (it:(items its))
        , do
            return [] ]
        

instance Read IniItem where
    readPrec = do
        nl
        name <- readName
        readType
        p <- readPoint
        comm <- readComment
        return $ IniItem $ Entry (pack name) p

readName :: ReadPrec String
readName = do
    '[' <- get
    readTil ']'

readType :: ReadPrec ()
readType = do
    nl
    checkS "Type:="
    return ()

readPoint :: ReadPrec Point
readPoint = do
    nl
    checkS "MapID:="
    mid <- (Just . MapID <$> readPrec) +++ (return Nothing)
    nl
    checkS "XPos:="
    x <- (Just <$> readPrec) +++ (return Nothing)
    nl
    checkS "YPos:="
    y <- (Just <$> readPrec) +++ (return Nothing)
    nl
    checkS "ZPos:="
    z <- (Just <$> readPrec) +++ (return Nothing)
    return $ Point x y z Nothing mid

readComment :: ReadPrec String
readComment = do
    nl
    checkS "Comment:="
    readTil '\n'
    

checkS :: String -> ReadPrec ()
checkS [] = return ()
checkS (c:str) = do
    c' <- get
    if c /= c' then
        pfail 
    else
        checkS str

readTil :: Char -> ReadPrec String
readTil c = do
    c' <- get
    if c' == c  then
        return ""
    else do
        str <- readTil c
        return (c':str)

nl :: ReadPrec ()
nl = choice
    [ do
        ' ' <- get
        nl
    , do
        '\n' <- get
        nl
    , do
        '\t' <- get
        nl
    , return ()
    ]

