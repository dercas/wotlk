{-# LANGUAGE FlexibleContexts #-}

module WoW.Wotlk.Box 
  ( module WoW.Wotlk.Box
  , module BoX11.Basic
  ) where

import BoX11.Basic hiding (getName)

-- import Control.Arrow as Arr
import Control.Concurrent
import Control.Monad
import Control.Monad.State

import Data.Bits
import Data.Char
import Data.Either
import Data.Fixed
import Data.List as L
import Data.Maybe

import Text.Parsec as P hiding (getState)

import WoW.Wotlk.Core

data TPFlag = Refresh | StopFall | Fast | Unsafe | Delay Int | InteractZ Int VKt
    deriving (Show, Eq)

instance Read TPFlag where
    readsPrec _ = parseReads parseTPFlag

parseTPFlag :: Stream s m Char => ParsecT s u m TPFlag
parseTPFlag = do
    flag <- parseIdenti
    case flag of
        "refresh"   -> return Refresh
        "stopfall"  -> return StopFall
        "fast"      -> return Fast
        "unsafe"    -> return Unsafe
        "delay"     -> do spaces
                          eith <- ($ ()) <$> parseNum' False mzero
                          return $ Delay $ either id (round . (*1e6)) 
                                                     (eith :: Either Int Double)
        "interactz" -> do spaces
                          eith <- ($ ()) <$> parseNum' False mzero
                          spaces
                          vk <- read <$> many1 (alphaNum <|> char '_')
                          return $ InteractZ 
                                     (either id (round . (*1e6)) (eith :: Either Int Double)) 
                                     vk
        _ -> fail ("no such flag: " ++ flag)
    where parseIdenti = P.try $ fmap toLower <$> parseIdent

tpflagInteractZ :: [TPFlag] -> Maybe (Int, VKt)
tpflagInteractZ = foldr folder Nothing where
    folder (InteractZ d k) = \_ -> Just (d, k)
    folder _ = id

tp' :: P -> [TPFlag] -> WoW ()
tp' p fs = get >>= \wenv -> do
    io $ forkIO $ evalStateT (tp p fs) wenv
    return ()

tp :: P -> [TPFlag] -> WoW ()
tp p flags = do
    when (any (== StopFall) flags) $ do
        stopFall
    p' <- pPoint p     
    w <- setPos p'
    when (w && any (== Refresh) flags) $ do
        when (p_x p' /= Nothing || p_y p' /= Nothing || p_z p' /= Nothing) $ do
            if Unsafe `elem` flags then
                refreshPosU
            else if Fast `elem` flags then 
                refreshPosL 
            else do
                io $ threadDelay 25000
                refreshPos
                io $ threadDelay 60000
    forM_ (tpflagInteractZ flags) $ uncurry interactZ

refreshPos :: WoW ()
refreshPos = do
    WEnv { wenv_hwnd = hWnd } <- get
    io $ do
        sendKey (fromIntegral $ fromEnum 'A') hWnd
        threadDelay 10000

refreshPosL :: WoW ()
refreshPosL = do
    WEnv { wenv_hwnd = hWnd } <- get
    io $ postKey (fromIntegral $ fromEnum 'A') hWnd

refreshPosU :: WoW ()
refreshPosU = do
    WEnv { wenv_hwnd = hWnd } <- get
    io $ postKeyUnsafe (fromIntegral $ fromEnum 'A') hWnd

tpRefreshed :: Point -> WoW Bool
tpRefreshed p = do
    w <- setPos p
    when w $ do
        io $ threadDelay 50000
        when (p_x p /= Nothing || p_y p /= Nothing || p_z p /= Nothing) $ do
            refreshPos
            io $ threadDelay 150000
    return w

tpRefreshed' :: Point -> StateT WEnv IO Bool
tpRefreshed' p = do
    w <- setPos p
    when w $ do
        io $ threadDelay 150000
        when (p_x p /= Nothing || p_y p /= Nothing || p_z p /= Nothing) $ do
            refreshPos
            io $ threadDelay 250000
    return w


clickAt :: VK -> Double -> Double -> WoW ()
clickAt k x y = do
    WEnv { wenv_hwnd = hWnd } <- get
    io $ click k x y hWnd

press :: Key -> WoW ()
press k = do
    WEnv { wenv_hwnd = hWnd } <- get
    io $ sendKey k hWnd 

pressDown :: Key -> WoW ()
pressDown k = do
    WEnv { wenv_hwnd = hWnd } <- get
    io $ sendKeyDown k hWnd

pressUp :: Key -> WoW ()
pressUp k = do
    WEnv { wenv_hwnd = hWnd } <- get
    io $ sendKeyUp k hWnd

writeChat :: String -> WoW ()
writeChat str = do
    hWnd <- wenv_hwnd <$> get
    let open = do
          chat <- typing
          when (not chat) $ do
              io $ sendKey vk_return hWnd
              open
        close = do
          chat <- typing
          when chat $ do
              io $ sendKey vk_return hWnd
              close
    do
        open
        io $ forM_ str $ flip sendChar hWnd
        close

target :: String -> WoW ()
target str = do
    writeChat ("/tar " ++ str)

revive :: WoW Bool
revive = do
    WEnv { wenv_hwnd = hWnd } <- get

    autoco <- isAutoCorpse
    corpse <- isCorpse
    if autoco then do
        stopFall
        io $ do
            threadDelay 15000
            replicateM 4 $ do
                postKey vk_esc hWnd
                threadDelay 100000
            replicateM 2 $ do
                postKey (vk_char 'M') hWnd
                threadDelay 100000
            threadDelay 350000
            sendKey vk_multiply hWnd
            threadDelay 150000
    else if corpse then do
        io $ do
            threadDelay 15000
            replicateM 4 $ do
                postKey vk_esc hWnd
                threadDelay 100000
            replicateM 2 $ do
                postKey (vk_char 'M') hWnd
                threadDelay 100000
            threadDelay 350000
            postKey vk_multiply hWnd
            threadDelay 150000
      
        wait 20
        reload
        pos <- getCorpse
        tpRefreshed pos
        io $ sendKey vk_multiply hWnd
        wait 20
    else do
        pos <- getCorpse
        didtp <- tpRefreshed pos
        when didtp $ io $ sendKey 106 hWnd
    not <$> isDead
    where
    wait :: Int -> WoW ()
    wait n = when (n > 0) $ do
        io $ threadDelay 500000
        l <- isLoading
        when l $ do
            wait (n-1)

browseEnt :: (Ent -> WoW Bool) -> Point -> Point -> Float -> Int -> WoW [Ent]
browseEnt f upleft downright r delay = fst <$> foldM fing ([], ()) 
                                       (quads upleft downright r)
  where
    fing (matches, oldguids) (x,y) = do
        stopFall
        tpRefreshed emptyPoint { p_x = Just x, p_y = Just y, p_z = Just (-495) }
        io $ threadDelay delay
        ents <- filterM f =<< entList
        return (matches `union` ents, oldguids)

quads :: Point -> Point -> Float -> [(Float, Float)]
quads upleft downright r = liftM2 (,) ranx rany
  where
    ranx = (x+) <$> distrib (x' - x) r
    rany = (y+) <$> distrib (y' - y) r
    [x, x', y, y'] = fj <$> ([p_x, p_y] <*> [upleft, downright])
    fj = maybe 0 id 

distrib :: (Enum a, RealFrac a) => a -> a -> [a]
distrib seg r
  | seg < 0 = negate <$> distrib (-seg) r
  | otherwise = (\x -> r * (1 + 2 * x) - off/2) <$> [ 0 .. n ]
    where
      n = fromIntegral $ (ceiling (seg / r / 2 - 1) :: Int)
      off = 2 * r * (1 + n) - seg

data PS = PS { ps_ps :: [P], ps_flags :: [TPFlag] }
    deriving (Show, Eq)

instance Read PS where
    readsPrec _ = parseReads parsePS
--     readPrec = do
--         (points, pflags) <- readpfs
--         return $ PS points pflags where
--             readpfs = T.choice
--               [ do
--                   p <- T.readPrec
--                   ret
--                   (points, pflags) <- readpfs
--                   return (p : points, pflags)
--               , do
--                   p <- T.readListPrec
--                   ret
--                   (points, pflags) <- readpfs
--                   return (p ++ points, pflags)
--               , do
--                   f <- T.readPrec
--                   ret
--                   (points, pflags) <- readpfs
--                   return (points, f : pflags)
--               , return ([], [])
--               ] 

parsePS :: Stream s m Char => ParsecT s u m PS
parsePS = makePS <$> sepEndBy (after peith) sep where
  makePS xs = PS (join $ lefts xs) (rights xs)
  peith = (Right <$> P.try parseTPFlag) 
      <|> (P.try $ Left . ($ ()) <$> parsePsOrP mzero mzero mzero mzero mzero)
  after p = p >>= \result ->  many (char ' ' <|> tab) >> return result
  sep = endOfLine >> spaces

interactZ :: Int -> VKt -> WoW (Maybe (OfsObj, Point))
interactZ delay key = attempt (step + 1) where
  step = 10
  attempt n 
    | n == 0 = return Nothing
    | otherwise = do
        p <- getPos
        objs <- do objects <- L.filter ((== 5) . ofs_type) <$> objList
                   mapM (\o -> (,) o <$> objPos o) objects
        let pairs = L.filter (\(_,p') -> xyDist p p' == 0) objs
        if L.null pairs then do
            io $ threadDelay (delay `quot` step)
            attempt (n - 1)
        else forM (listToMaybe pairs) $ \(obj, objp) -> do
            setTar obj
            setObjPos obj p
            press (vkt key)
            return (obj, objp) 

moveTowards p = do
    pos <- getPos
    when (pointDist pos p > 1.0) $ do
        endingpos <- lookTowards 0
        moveForward 1.0
    where
    lookTowards proy = do
        pos <- getPos
        speed <- getMovSpeed
        let tradius = speed * radiusf
            angle = calcAngle pos
            dist = pointDist pos p
            chord = calcChord angle tradius 
            sign = signum angle
            key = vkt $ if angle > 0 then VKLef else VKRig
            okey = vkt $ if angle <= 0 then VKLef else VKRig
        -- when (abs (calcDelta pos angle) > distthr || abs angle > pi/2) $ do
        side key True
        forward (abs angle < pi/2.5 && dist > 4) 
        step $ do
            pos <- getPos
            speed <- getMovSpeed
            let realangle = calcAngle pos 
                angle = if realangle < pi/12 then
                        realangle + proy 
                    else 
                        realangle
                chord = calcChord angle (radiusf * speed)
            -- if abs (calcDelta pos angle) > distthr || abs angle > pi/2 then
            forward (abs angle < pi/3 && chord < 0.9 * dist && xyDist pos p > 3)
            side key (signum realangle == sign)
            if signum realangle == sign then do
                when (angle > pi/8) $ do
                    tdelay (0.7 * abs angle / radspeed)
                return False
                -- return (False, p_h' pos)
            else do
                return True -- (True, p_h' pos)
        pressDown okey
        io (threadDelay 22500)
        pressUp okey

    moveForward distthr = do
        pressDown (vkt VKUp)
        step $ do
            pos <- getPos
            speed <- getMovSpeed
            let dist = pointDist pos p
            forward (dist > distthr)
            if dist > distthr then do
                when (abs (calcAngle pos) > pi/15) $ do
                    lookTowards 0
                when (dist > 4) $ do
                    tdelay (0.3 * dist / speed)
                return False
            else do
                return True
        
    calcAngle :: Point -> Float
    calcAngle pos = angleDiff (p_h' pos) (fromJust' "calcAngle" (lookAtP pos p))
    calcDelta :: Point -> Float -> Float
    calcDelta pos angle = pointDist pos p * sin angle
    calcChord :: Float -> Float -> Float
    calcChord angle radius = 2 * radius * sin (abs angle / 2)
    radiusf = 0.85
    radspeed = 3.12   -- rads/s turning speed
    side key whether = do st <- getState
                          let bit = testBit st 4 || testBit st 5
                          if whether && not bit then
                              pressDown key
                          else if not whether && bit then
                              pressUp key
                          else return ()
    forward whether = do st <- getState
                         let bit = testBit st 0
                         if whether && not bit then
                             pressDown (vkt VKUp)
                         else if not whether && bit then
                             pressUp (vkt VKUp)
                         else return ()
    step f = stepFold (fmap (\x -> (x,())) <$> const f) ()
    stepn f = stepFoldn (fmap (\x -> (x,())) <$> const f) ()
    stepFold f x = stepFoldn f x 12500 800
    stepFoldn f x del n = do
        (completed, y) <- f x
        if completed then
            return y
        else if n > 0 then do
            io (threadDelay del)
            stepFoldn f y del (n-1)
        else
            fail "overdid step_"
