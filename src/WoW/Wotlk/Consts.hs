{-# LANGUAGE OverloadedStrings #-}

module WoW.Wotlk.Consts where

import Data.ByteString
import qualified Data.Map.Strict as M
import Data.String
import Data.Word

default (Word32)

connstat     :: Word32  --
connstat    = 0xc79ce0  --  connstat
listmgr      :: Word32  --   │ 
listmgr     = 0x2ed0    --   └─listmgr
mgrhead      :: Word32  --      │
mgrhead     = 0xac      --      └─mgrhead

                        -- *ofsobj
ofsnext      :: Word32  --   │
ofsnext     = 0x3c      --   ├─ofsnext
ofsguid      :: Word32  --   │
ofsguid     = 0x30      --   ├─ofsguid
ofs3namp     :: Word32  --   │
ofs3namp    = 0x964     --   ├─ofs3namp
ofs3namp2    :: Word32  --   │  │
ofs3namp2   = 0x5c      --   │  └─ofs3namp2
ofs5namp     :: Word32  --   │         
ofs5namp    = 0x1a4     --   ├─ofs5namp
ofs5namp2    :: Word32  --   │  │
ofs5namp2   = 0x90      --   │  └─ofs5namp2
ofstype      :: Word32  --   │
ofstype     = 0x14      --   └─ofstype
playstat     :: Word32  --  
playstat    = 0xcd87a8  --  playstat
pboff1       :: Word32  --   │
pboff1      = 0x34      --   └─pboff1
pboff2       :: Word32
pboff2      = 0x24      --      └─pboff2 
moverg       :: Word32
moverg      = 0xbd07a0  --  moverg
puitg        :: Word32
puitg       = 0xbd0769  --  puitg
tarstat      :: Word32
tarstat     = 0xbd07b0  --  tarstat (guid *)
tarstatn     :: Word32
tarstatn    = 0xca1198  --  tarstatn
                        -- (ofsobj)
uido         :: Word32  --   │
uido        = 0x224     --   ├─uido
uid3         :: Word32  --   │
uid3        = 0x1014    --   ├─uid3
uid6         :: Word32  --   │
uid6        = 0x17c     --   ├─uid6
uid'         :: Word32  --   │
uid'        = 0x260     --   ├─uid'
hp           :: Word32  --   │
hp          = 0xfb0     --   ├─hp
mhp          :: Word32  --   │
mhp         = 0x19d8    --   ├─mhp
mhp3         :: Word32  --   │
mhp3        = 0x1068    --   ├─mhp3
mp           :: Word32  --   │
mp          = 0xfb4     --   ├─mp
mmp          :: Word32  --   │
mmp         = 0x19dc    --   ├─mmp
ene          :: Word32  --   │
ene         = 0xfc0     --   ├─ene
prevx        :: Word32  --   │
prevx       = 0x7d4     --   ├─prevx
mvspeed      :: Word32  --   │
mvspeed     = 0x814     --   ├─mvspeed
posx         :: Word32  --   │
posx        = 0x798     --   ├─posx
posy         :: Word32  --   │
posy        = 0x79c     --   ├─posy
posz         :: Word32  --   │
posz        = 0x7a0     --   ├─posz
posr         :: Word32  --   │
posr        = 0x7a8     --   ├─posr
eposx        :: Word32  --   │
eposx       = 0x7d4     --   ├─eposx
bufs0        :: Word32  --   │
bufs0       = 0xdd4     --   ├─
bufsp        :: Word32  --   │
bufsp       = 0xddc     --   ├─
ofs4tar      :: Word32  --   │
ofs4tar     = 0x19a0    --   ├─
ofs3tar      :: Word32  --   │
ofs3tar     = 0xfd8     --   │       
ofsthrtp     :: Word32  --   │
ofsthrtp    = 0xfec     --   ├─ofsthrtp 
ofsthrt      :: Word32  --   │  │
ofsthrt     = 0x2c      --   │  └─ofsthrt
playst       :: Word32  --   │
playst      = 0x7cf     --   ├─playst
playst2      :: Word32  --   │
playst2     = 0x7cc     --   ├─playst2
expe         :: Word32  --   │
expe        = 0x2340    --   ├─exp
lvl          :: Word32  --   │
lvl         = 0x1a30    --   ├─lvl
honor        :: Word32  --   │
honor       = 0x2c80    --   ├─honor
combat       :: Word32  --   │
combat      = 0x1a46    --   ├─combat
sitting      :: Word32  --   │
sitting     = 0x9f8     --   ├─sitting
castn        :: Word32  --   │
castn       = 0xa60     --   ├─casting
chann        :: Word32  --   │
chann       = 0xa80     --   └─channeling
playnam      :: Word32
playnam     = 0xc79d18  --  playnam
mapid        :: Word32
mapid       = 0xab63bc  --  mapid 
emapid       :: Word32
emapid      = 0xac3868  --  emapid
zoneid       :: Word32
zoneid      = 0xbd080c  --  zoneid
ezoneid      :: Word32
ezoneid     = 0xbe8f50  --  ezoneid
maploaded    :: Word32
maploaded   = 0xBEBA40  --  maploaded
loading      :: Word32
loading     = 0xb4ae40  --  loading
mapnam       :: Word32
mapnam      = 0xce07d0  --  mapnam
mipoff       :: Word32
mipoff      = 0xbd077c  --  mipoff
mipnam       :: Word32
mipnam      = 0         --   └─mipnam
rmflags      :: Word32
rmflags     = 0xcd774c  --  rmflags
rmf_objects  :: Word32
rmf_objects = 0x01      --   ├┅rmf_objects
rmf_render   :: Word32
rmf_render  = 0x0102    --   ├┅rmf_render
rmf_light    :: Word32
rmf_light   = 0x0200    --   ├┅rmf_light
rmf_texture  :: Word32
rmf_texture = 0x0800    --   ├┅rmf_texture
rmf_ray      :: Word32
rmf_ray     = 0x20000000--   └┅rmf_ray
roflags      :: Word32
roflags     = 0xcd7754  --  roflags
rof_npcs     :: Word32
rof_npcs    = 0x01      --   ├┅rof_npcs
rof_effects  :: Word32
rof_effects = 0x02      --   └┅rof_effects
camstat      :: Word32
camstat     = 0xb7436c  --  camstat
camoff       :: Word32
camoff      = 0x7e20    --   └─camoff
camx         :: Word32
camx        = 0x8       --      ├─camx
camy         :: Word32
camy        = 0xc       --      ├─camx
camz         :: Word32
camz        = 0x10      --      ├─camx 
camdist      :: Word32
camdist     = 0x118     --      ├─camdist
camyaw       :: Word32
camyaw      = 0x11c     --      ├─camyaw
campitch     :: Word32
campitch    = 0x120     --      └─campitch
rdfqueue     :: Word32
rdfqueue    = 0xacdac0
rdfpop       :: Word32
rdfpop      = 0xbea980

ofsx         :: Word32
ofsx        = 0xe8
ofsx'        :: Word32
ofsx'       = 0x1d8

typn         :: Word32
typn        = 0xd41660
wgroup       :: Word32
wgroup      = 0xb74484  --  wgroup
grouped      :: Word32
grouped     = 0xb74498  --  grouped
groupn       :: Word32
groupn      = 0xb744a4  --  groupn
hwnd         :: Word32
hwnd        = 0xd413f4  --  hwnd
fclip        :: Word32
fclip       = 0xcd7748  --  fclip
nclip        :: Word32
nclip       = 0xadeed4  --  nclip
maxfps       :: Word32
maxfps      = 0xc5df7c  --  maxfps
maxfpsbk     :: Word32
maxfpsbk    = 0xc5df74  --  maxfpsbk
corpx        :: Word32
corpx       = 0xbd0a58  --  corpx
corpy        :: Word32
corpy       = 0xbd0a5c  --  corpy
corpz        :: Word32
corpz       = 0xbd0a60  --  corpz
clkx         :: Word32
clkx        = 0xca1264  --  clkx
clky         :: Word32
clky        = 0xca1268  --  clky
clkz         :: Word32
clkz        = 0xca126c  --  clkz

bobbrid      :: Word32
bobbrid     = 35591

mapMap :: M.Map Word32 String
mapMap = M.fromList 
    [ (0,   "Eastern Kingdoms")
    , (1,   "Kalimdor")
    , (33,  "Shadowfang Keep")
    , (34,  "Stormwind Stockade")
    , (36,  "Deadmines")
    , (43,  "Wailing Cavers")
    , (47,  "Razorfen Kraul")
    , (48,  "Blackfathom Deeps")
    , (70,  "Uldaman")
    , (90,  "Gnomeregan")
    , (109, "Sunken Temple")
    , (129, "Razorfen Downs")
    , (189, "Scarlet Monastery")
    , (209, "Zul'Farrak")
    , (229, "Blackrock Spire")
    , (230, "Blackrock Depths")
    , (249, "Onyxia's Lair")
    , (289, "Scholomance")
    , (309, "Zul'gurub")
    , (329, "Stratholme")
    , (349, "Mauradon")
    , (369, "Deeprun Tram")
    , (389, "Ragefire Chasm")
    , (409, "Molten Core")
    , (429, "Dire Maul")
    , (469, "Blackwing Lair")
    , (509, "Ruins of Ahn'Qiraj")
    , (531, "Ahn'Qiraj Temple")
    , (533, "Naxxramas")
    -- Outland
    , (530, "Outland")
    , (540, "Shattered Halls")
    , (542, "Blood Furnace")
    , (543, "Hellfire Ramparts")
    , (545, "The Steamvault")
    , (546, "The Underbog")
    , (547, "Slave Pens")
    , (550, "Tempest Keep")
    , (552, "The Arcatraz")
    , (553, "The Botanica")
    , (554, "The Mechanar")
    , (555, "Shadow Labyrinth")
    , (556, "Sethekk Halls")
    , (557, "Mana-Tombs")
    , (558, "Auchenai Crypts")
    , (560, "Old Hillsbrad Foothills")
    , (565, "Gruul's Lair")
    -- Northrend
    , (571, "Northrend")
    , (574, "Utgarde Keep")
    , (575, "Utgarde Pinnacle")
    , (576, "The Nexus")
    , (578, "The Oculus")
    , (595, "The Culling of Statholme")
    , (599, "Halls of Stone")
    , (600, "Drak'Tharon Keep")
    , (601, "Azjol-Nerub")
    , (602, "Halls of Lightning")
    , (603, "Ulduar")
    , (604, "Gundrak")
    , (615, "The Obsidian Sanctum")
    , (616, "Eye of Eternity")
    , (619, "Ahn'kahet")
    , (624, "Vault of Archavon")
    , (631, "Icecrown Citadel")
    , (632, "The Forge of Souls")
    -- , (
    ]

mapMap' :: M.Map Word32 ByteString
mapMap' = fmap fromString mapMap

corpseMap :: M.Map (Float, Float, Float, Word32) (Float, Float, Float)
corpseMap = M.fromList $
    [ 
    -- Wailing Caverns
      ((-751.1312, -2209.2405, 149.23082, 1),     (-745.93, -2212.08, 15.83))
    -- Blackfathom Deeps
    , ((4249.1206, 748.3871, 147.26273, 1),       (4247.74, 746.879, -24.3))
    -- Dire Maul
    , ((-3908.0325, 1130.0035, 91.66601, 1),      (-3734.78, 934.67, 161.01))
    -- Stratholme
    , ((3392.317, -3378.4832, 134.04224, 0),      (3235.23, -4057.48, 108.44))
    -- Zul'Gurub
    , ((-11916.06, -1224.5771,   77.52504,    0), (-11916.50, -1229.74, 92.29))
    -- Onyxia's Lair
    , ((-4753.3125, -3752.4211, 122.84202, 1),    (-4753.31, -3752.42, 50))
    , ((-360.04987, 3067.8955, -64.877045,  530), (-363.35, 3082.55, -15))
    -- Blood Furnace
    , ((-295.41934, 3151.9927, -78.38922,   530), (-305.589, 3172.3, 30.989))
    -- Mana Tombs
    , ((-3089.5164, 4942.7563, -20.68425,   530), (-3074, 4942.89, -101.05))
    -- Shadow Labyrinth
    , ((-3640.6877, 4943.1626, -21.145424,  530), (-3650.18, 4942.99, -93.14))
    -- Utgarde Keep
    , ((1211.7594, -4868.422, 188.77936,    571), (1235.0, -4859.0, 41.2489))
    -- The Nexus
    , ((3899.9258, 6985.4355, 104.069916,   571), (3905.0, 6985.55, 69.49))
    -- Ahn'kahet
    , ((3641.458, 2032.0638, 78.86793, 571),      (3638.0, 2026.0, 2.53))
    -- Drak'Tharon Keep
    , ((4774.583, -2023.0491, 205.51202, 571),    (4774.6, -2023, 235.5))
    , ((8571.435, 792.2205, 547.7061, 571),       (8566, 792.52, 558.24))
    ]

herbIDs :: M.Map Word32 String
herbIDs = M.fromList
  [ (181166,  "Bloodthistle")
  , (1618,    "Peacebloom")
  , (3724,    "Peacebloom")
  , (1617,    "Silverleaf")
  , (3725,    "Silverleaf")
  , (1619,    "Earthroot")
  , (3726,    "Earthroot")
  , (1620,    "Mageroyal")
  , (3727,    "Mageroyal")
  , (1621,    "Briarthorn")
  , (3729,    "Briarthorn")
  , (2045,    "Stranglekelp")
  , (1622,    "Bruiseweed")
  , (3730,    "Bruiseweed")
  , (1623,    "Wild Steelbloom")
  , (1628,    "Grave Moss")
  , (1624,    "Kingsblood")
  , (2041,    "Liferoot")
  , (2042,    "Fadeleaf")
  , (2046,    "Goldthorn")
  , (2043,    "Khadgar's Whisker")
  , (2044,    "Wintersbite")
  , (2866,    "Firebloom")
  , (142140,  "Purple Lotus")
  , (180165,  "Purple Lotus")
  , (142141,  "Arthas' Tears")
  , (176642,  "Arthas' Tears")
  , (142142,  "Sungrass")
  , (176636,  "Sungrass")
  , (180164,  "Sungrass")
  , (142143,  "Blindweed")
  , (183046,  "Blindweed")
  , (142144,  "Ghost Mushroom")
  , (142145,  "Gromsblood")
  , (176637,  "Gromsblood")
  , (176583,  "Golden Sansam")
  , (176638,  "Golden Sansam")
  , (180167,  "Golden Sansam")
  , (176584,  "Dreamfoil")
  , (176639,  "Dreamfoil")
  , (180168,  "Dreamfoil")
  , (176586,  "Mountain Silversage")
  , (176640,  "Mountain Silversage")
  , (180166,  "Mountain Silversage")
  , (176587,  "Plaguebloom")
  , (176641,  "Plaguebloom")
  , (176588,  "Icecap")
  , (176589,  "Black Lotus")
  , (181270,  "Felweed")
  , (183044,  "Felweed")
  , (190174,  "Frozen Herb")
  , (181271,  "Dreaming Glory")
  , (183045,  "Dreaming Glory")
  , (181275,  "Ragveil")
  , (183043,  "Ragveil")
  , (181277,  "Terocone")
  , (181276,  "Flame Cap")
  , (181278,  "Ancient Lichen")
  , (189973,  "Goldclover")
  , (181279,  "Netherbloom")
  , (185881,  "Netherdust Bush")
  , (191303,  "Firethorn")
  , (181280,  "Nightmare Vine")
  , (181281,  "Mana Thistle")
  , (190169,  "Tiger Lily")
  , (190170,  "Talandra's Rose")
  , (191019,  "Adder's Tongue")
  , (190173,  "Frozen Herb")
  , (190175,  "Frozen Herb")
  , (190171,  "Lichbloom")
  , (190172,  "Icethorn")
  , (190176,  "Frost Lotus")
  ]
