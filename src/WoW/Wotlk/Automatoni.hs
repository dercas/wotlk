{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Main where

import Control.Exception as E hiding (handle)
import Control.Monad.State.Strict
import Control.Monad.Reader

import Data.ByteString.Char8 (pack)
-- import qualified Data.ByteString.Char8 as BS
import Data.Char 

import Data.Map.Strict as M
import Data.Maybe
import Data.List as L

import System.Console.Haskeline
import System.Console.Terminfo
import System.Environment (getArgs)
import System.Posix.Types
import System.Process (callProcess)

import Text.Parsec as P
import Text.ParserCombinators.Parsec.Numeric as P
import Text.Printf

import WoW.Wotlk.Core
import WoW.Wotlk.Functions

newtype FuncRepl a = FuncRepl (StateT (ParseEnv, RunEnv) (ReaderT Terminal IO) a)
  deriving (Functor, Applicative, Monad, MonadIO, MonadException
            , MonadReader Terminal, MonadState (ParseEnv, RunEnv))

runFuncRepl :: FuncRepl a -> IO a
runFuncRepl (FuncRepl op) = do
    term <- setupTermFromEnv 
    all <- mapM newWEnv =<< wineWows
    runReaderT 
      (evalStateT op (mempty, mempty { r_wows = M.singleton "all" all })) 
      term

main :: IO ()
main = runFuncRepl $ runInputT (setComplete completion defaultSettings) 
       $ withInterrupt (includes >> step)
    where
    step :: InputT FuncRepl ()
    step = do
      (penv, renv) <- lift get
      mline <- fmap nospaces <$> getInputLine "λ "
      forM_ mline $ \line -> do
        let args = words line
        case if L.null args then Nothing else M.lookup (L.head args) commands of
          Just cmd -> cmd (L.drop 1 args)
          Nothing -> do
            eith <- liftIO $ try' $ compileOrExec penv renv (pack line)
            case eith of 
              Left e -> outputStrLn (show e) 
              Right (Right (penv', renv)) -> do
                unless (L.null line) $ do
                    outputStrLn $ if r_state renv == Just False then "Err" else "Ok" 
                    forM_ (r_log renv) outputStrLn
                lift $ put $ (,) penv' renv 
                  { r_wows = r_wows renv `mappend`
                              M.fromList [(g,[]) | g <- parsGroups penv'] 
                  , r_state = Nothing 
                  , r_log = [] }
              Right (Left err) -> do
                let par = runParser (parsePar >>= \r -> eof >> return r) 
                              penv "parsePar" line
                case par of
                  Left err' -> do outputStrLn (show err)
                                  outputStrLn (show err')
                  Right papply -> outputStrLn (show $ applyPar papply [])
      unless (isNothing mline) step
    includes = do
      args <- L.drop 1 <$> io getArgs
      forM_ args $ \arg -> do
        (penv, renv) <- lift get
        meith <- parseArg (M.keys (r_wows renv)) arg
        forM_ meith $ \eith -> case eith of
          Left fnam -> do
              outputStrLn ("include \"" ++ arg ++ "\"")
              eith <- io $ try' $ runParserT 
                  (liftM2 (,) (includeFile fnam) P.getState) penv "includes" ""
              case eith of
                  Left e -> outputStrLn (show e)
                  Right (Left e) -> outputStrLn (show e)
                  Right (Right (prog, penv')) -> do
                      let renv' = execState (compileTopLevel `mapM_` prog) renv
                      lift $ put $ (,) penv' renv'
                        { r_wows = r_wows renv' `mappend`
                                    M.fromList [(g,[]) | g <- parsGroups penv'] 
                        , r_state = Nothing 
                        , r_log = [] } 
          Right (gnam, pid) -> do
              eith' <- io $ try' $ newWEnv pid
              case eith' of
                  Left e -> outputStrLn (show e)
                  Right wenv -> do
                      lift $ put $ (,) penv renv
                        { r_wows = M.adjust (`L.union` [wenv]) gnam (r_wows renv) } 

    try' :: IO a -> IO (Either SomeException a)
    try' = E.try

commands :: Map String ([String] -> InputT FuncRepl ())
commands = M.fromList $ join
  [ [":defs", ":defines"] ? \_ -> do
            (ParseEnv { parsDefines = defs }, renv) <- lift get
            if M.null defs then
                outputStrLn "No symbols"
            else
                -- forM_ (M.toList defs) $ \(k,v) -> outputStrLn $ printf "%s %s" k (show v)
                prettyPrint $ fmap show <$> M.toList defs
  , [":f", ":funcs"] ? \_ -> do
            (_, RunEnv { r_funcs = funcs }) <- lift get
            if M.null funcs then
                outputStrLn "No symbols"
            else
                prettyPrint $ flip (,) "" <$> M.keys funcs
  , [":g", ":groups"] ? \args -> case args of
          [] -> do
            (_, RunEnv { r_wows = wows }) <- lift get
            forM_ (M.toList wows) (outputStrLn . show)
          name : ops -> do
            (_, RunEnv { r_wows = wows }) <- lift get
            eith <- io $ runParserT (join <$> 
                forM ops parseGroupInclusions) (False, const) ":groups" ""
            case eith of
                Left err -> outputStrLn (show err)
                Right fs -> lift (modify $ \(p,r) -> (,) p r 
                  { r_wows = M.adjust (flip (L.foldl (flip ($))) fs) name (r_wows r) })
            wows <- r_wows . snd <$> lift get
            case M.lookup name wows of
                Nothing -> outputStrLn $ printf "No such group: %s" name
                Just ws -> do outputStrLn $ printf "%s:" name
                              forM_ ws (outputStrLn . printf "    %s" . show)
             
  , [":w", ":wows"] ? \_ -> do
          io (wineWows >>= mapM newWEnv) >>= mapM_ (outputStrLn . show)
  ] where
  xs ? f = [(x, f) | x <- xs]

parseGroupInclusions :: String -> ParsecT String (Bool, [WEnv] -> [WEnv] -> [WEnv]) IO [[WEnv] -> [WEnv]]
parseGroupInclusions body = P.setInput body >> (catMaybes <$> manyTill (spacesaft step) eof) where
  step :: ParsecT String (Bool, [WEnv] -> [WEnv] -> [WEnv]) IO (Maybe ([WEnv] -> [WEnv])) 
  step = choice
    [ do char '+'
         P.setState (True, L.union)
         return Nothing
    , do char '-'
         P.setState (True, flip (L.\\))
         return Nothing
    , do (c,f) <- P.getState
         pids <- sepBy1 P.int (char ',')
         unless c $ 
             P.setState (True, L.union)
         fmap (Just . f) $ forM pids $ \p -> do
             isw <- io $ isWoW p
             unless isw (fail $ printf "%d: Not a WoW process" $ toInteger p)
             io $ newWEnv p
    ]

keywords :: [String]
keywords = ["include", "define"]

completion :: CompletionFunc FuncRepl
completion = completeWord Nothing " \t,()[]@*" $ \str -> do
    let mtup = L.uncons str
    if maybe False ((== '"') . fst) mtup then do
        files <- listFiles (maybe "" snd mtup)
        return $ (\c -> c 
          { replacement = "\"" ++ replacement c 
              ++ (guard (L.last (replacement c) /= '/') >> "\"") }) 
          <$> L.filter (\c -> (".bot" `isSuffixOf` replacement c) 
                              || ("/" `isSuffixOf` replacement c))  files
    else do
        (penv, renv) <- get
        let str' = str -- L.takeWhile (/= '(') str
        return $ mconcat
          [ do def <- L.filter (isPrefixOf str') (M.keys $ parsDefines penv)
               return (Completion def def True)
          , do key <- L.filter (isPrefixOf str') (M.keys commands)
               return (Completion key key True)
          , do wor <- L.filter (isPrefixOf (toLower <$> str')) keywords
               let wor' = str' ++ L.drop (L.length str') wor
               return (Completion wor' wor' True)
          , do fun <- L.filter (isPrefixOf str') (M.keys $ r_funcs renv)
               return (Completion fun fun False)
          ]


prettyPrint :: [(String, String)] -> InputT FuncRepl ()
prettyPrint xs = unless (L.null xs) $ do
    term <- liftIO setupTermFromEnv
    let Just width = getCapability term (tiGetNum "cols")
    pr width where
      pr width = forM_ lines $ outputStrLn . unwords' where
        lines :: [[String]]
        lines 
          | n == 0 = (\(k,v) -> [printf "%-*s %s" keywidth k v]) <$> xs
          | otherwise = fmap (\(k,v) -> printf "%-*s %*s" keywidth k valwidth v) 
                        <$> splitin xs
        keywidth = maximum (length . fst <$> xs)
        valwidth = maximum (length . snd <$> xs)
        pad = if keywidth == 0 || valwidth == 0 then 2 else 3
        n = (width + 1) `quot` (keywidth + valwidth + pad)
        splitin ys = let (l,r) = splitAt n ys in l : if L.null r then [] else splitin r
        unwords' [] = ""
        unwords' ys = foldr1 (printf "%s  %s") ys

parseArg :: MonadIO io => [String] -> String -> InputT io 
        (Maybe (Either FilePath (String, CPid)))
parseArg groups arg = do
    let eith = parse p "parseArg" arg
    case eith of
        Left e -> outputStrLn (show e) >> return Nothing
        Right r -> return (Just r)
    where
    p = P.choice $ fmap P.try $ 
        [ do lh <- P.many1 (P.noneOf "=")
             guard (lh `L.elem` groups)
             P.char '='
             rh <- P.int
             eof
             return $ Right (lh,rh)
        , do name <- P.many1 anyChar
             eof
             return (Left name)
        ]

nospaces :: String -> String
nospaces = L.reverse . L.dropWhile isSpace . L.reverse . L.dropWhile isSpace
