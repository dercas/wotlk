module Wotlk.WoW.SearchGuid where

import System.Environment

import WoW.Wotlk.Core
import WoW.Wotlk.Box

main :: IO ()
main = void $ do
    args @ [sup, slo, srad, sdel, sid] <- drop 1 <$> getArgs
    print (quads (read sup) (read slo) (read srad))
    inCurrentWoW $ browseEnt (\e -> return (isObject e && ent_uid e == read sid)) 
              (read sup) (read slo) (read srad) (read sdel) >>= mapM_ (\o -> io $print (o, ent_pos o))
