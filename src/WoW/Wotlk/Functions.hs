{-# LANGUAGE GADTs, BangPatterns, FlexibleContexts, StandaloneDeriving #-}

module WoW.Wotlk.Functions where

import Control.Arrow hiding (first, second)
import qualified Control.Arrow as Arrow
import Control.Category hiding (id, (.))
import qualified Control.Category as Cat 
import Control.Concurrent
import Control.Exception
import Control.Monad.Except
import Control.Monad.State hiding (StateT, State)
import qualified Control.Monad.Trans.State.Strict as ST
import Control.Monad.Trans.Reader

import Data.Bifunctor
import qualified Data.ByteString.Char8 as BS
import Data.Char
import Data.Either
import Data.Function
import Data.IORef
import qualified Data.Map as M
import Data.Maybe
import qualified Data.List as L
import Data.Ratio
import Data.Typeable

-- import Debug.Trace

import System.FilePath
import System.Process.VM

import Text.Parsec hiding (try, State)
import qualified Text.Parsec as P
import Text.Printf 
import Text.Read (readMaybe)

import WoW.Wotlk.Core
import WoW.Wotlk.Box

type Bind = ([VK], VK)

type Func = [Par] -> Either (RunTime ()) (RTWoW (Maybe Bool))

data FuncException
  = MissingGroupException String
  | MissingBindException String
  | MissingArgException String
  | IllegalArgException Par
  | FailedCallException String
  deriving (Eq, Show)

instance Exception FuncException

findVar :: [Var] -> Var -> Int
findVar vars v = maybe (error $ printf "Missing Argument %s" v) 
    id $ L.findIndex (== v) vars

varpar :: [Var] -> Var -> [Par] -> Par
varpar !vs !v = 
    let n = findVar vs v
    in seq n $ \ps -> 
        if L.length ps < n+1 then
            error $ printf "Missing Argument %s" v 
        else
            ps !! n

type Group = Set (WEnv, Int) String

groupNames :: Group -> [String]
groupNames (Set name) = [name]
groupNames (Singleton _) = []
groupNames (SetUnion a b) = groupNames a ++ groupNames b
groupNames (SetDiff a b) = groupNames a ++ groupNames b
groupNames (SetInter a b) = groupNames a ++ groupNames b

groupWows :: Group -> RunTime [(WEnv, Int)]
groupWows gs = flip getwows gs . r_wows <$> get 

type RTWoW = ReaderT (WEnv, Int) RunTime

type Var = String

data VarApply a b = VarApply 
                      { appliedVars :: ![Var] 
                      , applyPar :: !(a -> b)
                      }

type ParApply = VarApply [Par]

instance Monoid b => Monoid (VarApply a b) where
    mempty = VarApply [] mempty
    VarApply vars f `mappend` VarApply vars' f' = 
        VarApply (vars `L.union` vars') (liftM2 mappend f f')

instance Functor (VarApply a) where
    fmap f (VarApply vars g) = VarApply vars (fmap f g)

instance Applicative (VarApply a) where
    pure = VarApply [] . pure
    VarApply vars f <*> VarApply vars' f' = 
        VarApply (vars `L.union` vars') (f <*> f')

instance (Typeable b, Monoid a, Show a, Show b) => Show (VarApply a b) where
    show (VarApply vars f)
      | L.null vars = show (f mempty)
      | otherwise = printf "(%s)" $ 
                foldr (printf "$%s -> %s") (show $ typeOf (f mempty)) vars

instance Category VarApply where
    id = VarApply [] id
    VarApply vars f . VarApply vars' f' =
        VarApply (vars `L.union` vars') (f . f')

instance Arrow.Arrow VarApply where
    arr = VarApply []
    first (VarApply vars f) = VarApply vars (Arrow.first f)
    second (VarApply vars f) = VarApply vars (Arrow.second f)
    VarApply vars f *** VarApply vars' f' =
        VarApply (vars `L.union` vars') (f Arrow.*** f')
    VarApply vars f &&& VarApply vars' f' =
        VarApply (vars `L.union` vars') (f Arrow.&&& f')

data LogicOp = Or | And
    deriving (Show, Eq)

logicOp :: Foldable t => LogicOp -> t Bool -> Bool
logicOp Or = or
logicOp And = and

data Par 
  = ParP P 
  | ParS String 
  | ParN Int
  | ParF Double
  | ParC Ordering Double 
  | ParL [Par]
  | ParI String
  | ParG Group
  deriving Show

instance Num Par where
    ParN n + ParN n' = ParN (n + fromIntegral n')
    ParN n + ParF f = ParF (fromIntegral n + f)
    ParN _ + p        = throw (IllegalArgException p)
    ParF f + ParN n = ParN n + ParF f
    ParF f + ParF f' = ParF (f + realToFrac f')
    ParF _ + p        = throw (IllegalArgException p)
    p      + _        = throw (IllegalArgException p)
    ParN n * ParN n' = ParN (n * fromIntegral n')
    ParN n * ParF f   = ParF (fromIntegral n * f)
    ParN _ * p        = throw (IllegalArgException p)
    ParF f * ParN n   = ParN n * ParF f
    ParF f * ParF f'  = ParF (f * realToFrac f')
    ParF _ * p        = throw (IllegalArgException p)
    p      * _        = throw (IllegalArgException p)
    abs (ParN n)      = ParN $ abs n
    abs (ParF f)      = ParF $ abs f
    abs p             = throw (IllegalArgException p)
    signum (ParN n)   = ParN $ signum n
    signum (ParF f)   = ParF $ signum f
    signum p          = throw (IllegalArgException p)
    negate (ParN n)   = ParN $ negate n
    negate (ParF f)   = ParF $ negate f
    negate p          = throw (IllegalArgException p)
    fromInteger n     = ParN (fromInteger n)

instance Fractional Par where
    ParN n / m = ParF (fi n) / m
    n / ParN m = n / ParF (fi m)
    ParF f / ParF f' = ParF (f / f')
    ParF _ / p = throw (IllegalArgException p)
    p / _ = throw (IllegalArgException p)
    recip (ParF f) = ParF (1 / f)
    recip p = throw (IllegalArgException p)
    fromRational = ParF . fromRational

instance Enum Par where
    fromEnum (ParN n) = fromEnum n
    fromEnum p = throw (IllegalArgException p)
    toEnum = ParN

instance Eq Par where
    ParN n == ParN n' = n == fromIntegral n'
    ParN _ == _ = False
    ParF f == ParF f' = f == realToFrac f'
    ParF _ == _ = False
    ParP p == ParP p' = p == p'
    ParP _ == _ = False
    ParS s == ParS s' = s == s'
    ParS _ == _ = False
    ParL l == ParL l' = l == l'
    ParL _ == _ = False
    ParI i == ParI i' = i == i'
    ParI _ == _ = False
    ParC o f == ParC o' f' = o == o' && f == f'
    ParC _ _ == _ = False
    ParG g == ParG g' = g == g'
    ParG _ == _ = False

instance Ord Par where
    p `compare` p' = (par2Int p :: Int) `compare` par2Int p'

instance Real Par where
    toRational (ParN n) = toRational n
    toRational (ParF f) = toRational f
    toRational p = throw (IllegalArgException p)

instance Integral Par where
    ParN n `quotRem` ParN n' = bimap ParN ParN (n `quotRem` fromIntegral n')
    ParN _ `quotRem` p = throw (IllegalArgException p)
    p `quotRem` _ = throw (IllegalArgException p)
    toInteger = par2Int 

writePar :: Word32 -> Par -> WoW ()
writePar addr par = case par of
    ParP p -> do pid <- wenv_pid <$> get
                 void . io . writePoint pid addr =<< pPoint p
    ParS _ -> return ()
    ParN n -> aw addr (fromIntegral n :: Word32)
    ParF f -> aw addr (realToFrac f :: Float)
    p -> throw (IllegalArgException p)

data ParseEnv = ParseEnv { parsDefines  :: M.Map String Par
                         , parsVars     :: [Var] 
                         , parsGroups   :: [String]
                         , parsCWD      :: FilePath
                         } deriving Show

instance Monoid ParseEnv where
    mempty = ParseEnv mempty [] [] "."
    ParseEnv d v g _ `mappend` ParseEnv d' v' g' fp =
        ParseEnv (d `mappend` d') (v `mappend` v') (g `mappend` g') fp
type Prog = [TopLevel]

data TopLevel 
  = Decl { declName   ::  !String
         , declPars   ::  ![Var]
         , declBody   ::  ![Statement]
         }
  | OAsi { oasig      ::  !Asignment }

data Statement =
    Cmp  { cmpLHS     ::  !(ParApply Par)
         , cmpRHS     ::  !(ParApply Par)
         , cmpEq      ::  !Bool
         , cmpNegte   ::  !Bool
         }
  | Call { callThrws  ::  !Bool               -- !
         , callNegte  ::  !Bool               -- 
         , callParll  ::  !Bool
         , callModer  ::  !(Maybe BListOp)
         , callFunam  ::  !(ParApply String)
         , callGrops  ::  !(Maybe (ParApply Group))
         , callParfs  ::  !(ParApply [Par])
         }
  | Loop { loopOnFal  ::  !OnFail
         , loopNegte  ::  !Bool
         , loopParll  ::  !Bool
         , loopUpdts  ::  !(Maybe (Bool, Bool)) -- (throws?, updates state?)
         , loopReps   ::  !(Maybe Int)
         , loopPerod  ::  !(Maybe (Double, IORef Double))
         , loopGroup  ::  !(Maybe (ParApply Group))
         , loopBody   ::  ![[Statement]]
         }
  | For  { forOnFal   ::  !OnFail
         , forNegte   ::  !Bool
         , forParll   ::  !Bool
         , forUpdts   ::  !(Maybe (Bool, Bool))
         , forVar     ::  !Var
         , forBody    ::  ![Statement]
         }
  | Asig { asig       ::  !Asignment }

data Asignment = Asignment { asigName   ::  !String
                           , asigValue  ::  !([VKt], VKt)
                           }

isNegated :: Statement -> Bool
isNegated ca @ Call {} = callNegte ca
isNegated lo @ Loop {} = loopNegte lo
isNegated cm @ Cmp {} = cmpNegte cm
isNegated _ = False

instance Show TopLevel where
    show (Decl na pa bo) = printf
      "Decl name=%s pars=%s body=\n%s" na (show pa) 
      (unlines $ ("  " ++) <$> lines (show `foldMap` bo))
    show (OAsi a) = show a

instance Show Statement where
    show (Cmp lhs rhs eq ne) = printf
      "Cmp %s %s %s negated=%s\n" (show lhs) (if eq then "==" else "/=" :: String) 
      (show rhs) (show ne)
    show (Call th ne pa mo fu gr pr) = printf 
      "Call throws=%s negated=%s parallel=%s modifier=%s name=%s group=%s pars=%s\n"
      (show th) (show ne) (show pa) (maybe "_" show mo) 
      (show fu) (maybe "_" show gr) (show pr)
    show (Loop nf ne pa up re pe gr bo) = printf
      "Loop fail=%s negated=%s parallel=%s updates=%s reps=%s period=%s group=%s body=\n%s"
      (show nf) (show ne) (show pa) 
      (maybe "_" (\(t,u) -> printf "(%s,%s)" (show t) (show u)) up :: String) 
      (maybe "*" show re) (maybe "_" (show.fst) pe) 
      (maybe "_" show gr)
      (unlines $ ("  " ++) <$> lines ((sep . fmap (foldMap show)) bo)) where
          sep [] = []
          sep xs = foldr1 (\a b -> a ++ ",\n" ++ b) xs
    show (For {}) = ""
    show (Asig a) = show a

instance Show Asignment where
    show (Asignment na (vks,vk)) = printf
      "Asig name=%s keys=%s" na (foldr (\a b -> a ++ "-" ++ b) (show vk) 
      (show <$> vks))

parseFile :: FilePath -> IO (Either ParseError ([TopLevel], ParseEnv))
parseFile fp = BS.readFile fp >>=
    runParserT parseProg mempty { parsCWD = takeDirectory fp } fp 

parseProg :: ParsecT ByteString ParseEnv IO ([TopLevel], ParseEnv)
parseProg = liftM2 (,) 
                   (parseTopLevels >>= \b -> spaces' >> eof >> return b) 
                   P.getState

includeFile :: FilePath -> ParsecT ByteString ParseEnv IO Prog
includeFile filename = do
    penv <- P.getState
    conts <- liftIO $ BS.readFile (parsCWD penv </> filename)
    let newfp = takeDirectory $ parsCWD penv </> filename
    (prog, newpenv) <- either (fail . show) return =<< 
            io (runParserT parseProg penv { parsCWD = newfp } filename conts)
    P.setState newpenv
    return prog

preprocessor :: ParsecT ByteString ParseEnv IO Prog
preprocessor = spaces' >> choice
    [ do
        P.try (string' "include") <|> string' "incl" 
        spaces'
        includeFile =<< parseString
    , do
        P.try (string' "define") <|> string' "def"
        space
        line <- manyTill anyChar (void newline <|> eof)
        rest <- getInput
        setInput (BS.pack line)
        spaces'
        name <- parseIdent
        spaces'
        e <- ($ []) . applyPar <$> parsePar 
        spaces' 
        void newline <|> eof
        modifyState $ 
            \st -> st { parsDefines = M.insert name e (parsDefines st) }
        setInput rest
        return []
    ]

parseTopLevels :: ParsecT ByteString ParseEnv IO [TopLevel]
parseTopLevels = many (P.try $ spaces' >> pars) >>= 
                  \p -> spaces' >> return (join p)
  where pars = P.try preprocessor <|> (pure <$> parseTopLevel )

parseTopLevel :: ParsecT ByteString ParseEnv IO TopLevel
parseTopLevel = choice 
  [ do -- Decl
      P.try $ P.try (string' "function") <|> string' "func"
      spaces'
      name <- parseName
      spaces'
      args <- between (char '(' >> spaces')
                      (spaces' >> char ')')
                      (parseIdent `sepBy` (spaces' >> char ',' >> spaces'))
      st <- P.getState
      P.putState st { parsVars = parsVars st ++ args }
      spaces'
      prog <- between (char '{' >> spaces') (spaces' >> char '}') parseBody
      modifyState $ \s -> s { parsVars = parsVars st }
      return (Decl name args prog)
  , OAsi <$> P.try parseAsignment
  ]

parseBody :: ParsecT ByteString ParseEnv IO [Statement]
parseBody = many (P.try $ spaces' >> pars) >>= \p -> spaces' >> return (catMaybes p)
  where pars = choice
              [ P.try $ do preprocessor
                           return Nothing
              , Just <$> parseStatement
              ]

parseStatement :: ParsecT ByteString ParseEnv IO Statement
parseStatement = choice $ P.try <$>
  [ do -- Cmp
      ngt <- option False $ do char '~' ; spaces' ; return True
      lhs <- parsePar
      spaces' 
      eq <- choice [ do string "==" ; return True
                   , do string "/=" ; return False ]
      spaces'
      rhs <- parsePar
      return (Cmp lhs rhs eq ngt)
  , do -- Call
      c <- step (Call False False False Nothing mempty Nothing mempty) $ 
               \c -> (spaces' >>) $ choice $ P.try <$>
        [ do char '!'
             return c { callThrws = True }
        , do char '~'
             return c { callNegte = True }
        , do char '&' 
             return c { callParll = True }
        , do char '/'
             return c { callParll = False }
        , (\m -> c { callModer = Just m } ) <$> parseBListOp
        ] 
      spaces'
      name <- choice
        [ do char '$'
             varname <- parseName
             vars <- parsVars <$> P.getState
             return (VarApply [varname] (par2I . varpar vars varname))
        , VarApply [] . const <$> parseName
        ]
      spaces'
      groups <- parseMaybe $ between (char '[' >> spaces')
                                     (spaces' >> char ']') 
                                     (parseGroup True)
      spaces'
      pars <- sequenceA <$> between (char '(' >> spaces') 
                                   (spaces' >> char ')')
                                   (parsePar `sepBy` 
                                            (P.try $ spaces' >> char ',' >> spaces'))
      spaces'
      return c { callGrops = groups, callParfs = pars, callFunam = name }
  , do -- Loop
      l <- step (Loop Fail False True (Just (False, False)) (Just 1) Nothing Nothing []) $ 
              \l -> (spaces' >>) $ choice $ P.try <$>
            [ do char '#'
                 return l { loopOnFal = SetFalse }
            , do char '>'
                 return l { loopOnFal = Break }
            , do char '~'
                 return l { loopNegte = True }
            , do char '!'
                 return l { loopUpdts = maybe (Just (True, False))
                                              (\(_,b) -> Just (True, b))
                                              (loopUpdts l) }
            , do char '?'
                 return l { loopUpdts = maybe (Just (False, True))
                                              (\(b,_) -> Just (b, True))
                                              (loopUpdts l) }
            , do char '@'
                 return l { loopUpdts = Nothing }
            , do char '/'
                 return l { loopParll = False }
            , do char '&'
                 return l { loopParll = True }
            , do char '*'
                 return l { loopReps = Nothing }
            , do n <- ($ ()) <$> parseInt False mzero
                 return l { loopReps = Just n }
            , do char '%'
                 spaces'
                 per <- ($ ()) <$> parseFloat False mzero
                 ioref <- io $ newIORef 0
                 return l { loopPerod = Just (per, ioref) }
            ]
      spaces'
      group <- parseMaybe $ between (char '[' >> spaces') 
                                    (spaces' >> char ']')
                                    (parseGroup True)
      unless (isNothing group) appendWowVars
      spaces'
      body <- between (char '{' >> spaces') (spaces' >> char '}') $
                  parseBody `sepBy` (spaces' >> char ',' >> spaces')
      return l { loopGroup = group, loopBody = body }
  , do -- Asig
      Asig <$> parseAsignment
  ] where
  step :: a -> (a -> ParsecT s u m a) -> ParsecT s u m a
  step e f = (P.try (f e) >>= flip step f) <|> return e
  appendWowVars :: Monad m => ParsecT s ParseEnv m ()
  appendWowVars = modifyState $ \st -> let vars = parsVars st in st
    { parsVars = vars ++ [prim vars "_" , prim vars "_n" , prim vars "_N"] }
  prim :: [Var] -> Var -> Var
  prim vars v = if v `elem` vars then
                    prim vars (v ++ "'")
                else
                    v

parseAsignment :: ParsecT ByteString ParseEnv IO Asignment
parseAsignment = do
    name <- parseName
    spaces'
    char '='
    spaces'
    vks <- flip sepBy1 (char '-') $ P.try $ do
        vk <- many1 (alphaNum <|> char '_')
        maybe (fail $ "No such VK: " ++ show vk) return (readMaybe vk)
    return (Asignment name (init vks, last vks))

spaces' :: Stream s m Char => ParsecT s u m ()
spaces' = void (P.try $ many1 $ P.try comment) <|> spaces where
    comment = void $ do
        many (space <|> tab) 
        string "--" 
        manyTill anyChar (void newline <|> eof)
        spaces

parseName :: Stream s m Char => ParsecT s u m String
parseName = do
    c <- letter <|> char '_' 
    b <- many (alphaNum <|> char '_' <|> char ':' <|> char '.')
    l <- many (char '\'')
    return (c : b ++ l)

parsedVar :: Stream s m Char => (Par -> c) -> ParsecT s ParseEnv m (ParApply c)
parsedVar f = P.getState >>= \st -> choice
  [ P.try $ do
      char '$'
      var <- parseName
      notFollowedBy (many (char ' ' <|> tab) >> (alphaNum <|> char '_'))
      guard (not $ var `elem` ["A","C","P","T","E","O","R","N","YawAt"])
      return $ VarApply [var] (f . varpar (parsVars st) var)
  , P.try $ do
      ident <- parseIdent 
      -- guard (not $ ident `elem` ["A","C","P","T","E","O","R","N","YawAt"])
      case M.lookup ident (parsDefines st) of
          Nothing -> parserFail (printf "Not defined: %s" ident)
          Just p -> return (pure $ f p)
  ]

parsePar :: Stream s m Char => ParsecT s ParseEnv m (ParApply Par)
parsePar = P.try $ spaces >> choice
  [ P.try $ parsedVar id
  , pure . ParS <$> parseString
  , (\xs -> sequenceArr xs >>^ ParL) <$> 
        between (char '[') (spaces >> char ']') 
            (sepBy parsePar (P.try $ spaces >> char ','))
  , P.try $ do
      n <- fmap (fmap $ either ParN ParF) $ parseNum' True $ parsedVar par2Num 
      notFollowedBy (spaces >> (alphaNum <|> char '_' <|> char '-'))
      return n
  , fmap ParP <$> parseP (parsedVar $ par2Int)
                         (parsedVar par2Frac)
                         (parsedVar $ \(ParP (P p)) -> p)
                         (parsedVar par2P)
                         (parsedVar par2Ps)
  , fmap (ParL . fmap ParP) <$> parsePs (parsedVar $ \(ParN n) -> n)
                                        (parsedVar par2Frac)
                                        (parsedVar $ \(ParP (P p)) -> p)
                                        (parsedVar par2P)
                                        (parsedVar par2Ps) 
  , fmap ParG <$> parseGroup False
  , arr . const . ParI <$> parseIdent
  , do 
      ordr <- P.choice [ do char '<'
                            return LT
                       , do char '>'
                            return GT
                       , do string "=="
                            return EQ
                       ]
      spaces
      (>>^ ParC ordr) <$> parseFloat False mzero
  ]

testParseParApply :: Parsec ByteString u (ParApply a) -> u -> ByteString -> [Par] -> a
testParseParApply p env source pars = either (error . show) (($ pars) . applyPar) $ 
    runParser (p >>= \r -> eof >> return r)
        env "testParseParApply" source

testParsePar :: ParseEnv -> ByteString -> ParApply Par
testParsePar penv source = either (error . show) id $
    runParser (parsePar >>= \r -> eof >> return r) 
        penv "testParsePar" source

testParsePar' :: ParseEnv -> ByteString -> [Par] -> Par
testParsePar' penv source pars = ($ pars) . applyPar  $ testParsePar penv source

parseGroup :: Stream s m Char => Bool -> ParsecT s ParseEnv m (ParApply Group)
parseGroup adds = do
    vars <- parsVars <$> P.getState
    groupf @ (VarApply _ g) <- parseSet mzero 
            (pureArr <$> (parseIdent <|> parseString)) 
            (parsedVar par2G)
    when adds $ modifyState $ \p -> p { parsGroups = parsGroups p `L.union` 
            groupNames (g ((ParG $ Singleton (nullWEnv, 0)) <$ vars)) }
    return groupf

par2Int :: Integral a => Par -> a
par2Int (ParN n) = fromIntegral n
par2Int (ParF f) = round f
par2Int (ParP (P Point { p_mid = Just m })) = fromIntegral m
par2Int p = error $ printf "%s cannot be used as an Integer" (show p)

par2Frac :: RealFrac a => Par -> a
par2Frac (ParF f) = realToFrac f
par2Frac (ParN n) = fromIntegral n
par2Frac (ParP (P Point { p_mid = Just m })) = fromIntegral m
par2Frac p = error $ "couldn't convert to frac: " ++ show p

par2Num :: (Integral i, RealFrac f) => Par -> Either i f
par2Num (ParF f) = Right $ realToFrac f
par2Num p = Left $ par2Int p

par2C :: Par -> Double -> Bool
par2C (ParC ordr n) = (== ordr) . flip compare n
par2C p = (== par2Frac p)

par2I :: Par -> String
par2I p = maybe (throw (IllegalArgException p))
                id
                (par2I' p)

par2I' :: Par -> Maybe String
par2I' (ParI i) = Just i
par2I' (ParG (Set i)) = Just i
par2I' _ = Nothing

par2G :: Par -> Group
par2G (ParS g) = Set g
par2G (ParI g) = Set g
par2G (ParG g) = g
par2G p = throw (IllegalArgException p)

pars2G :: [Par] -> Group
pars2G = foldr1 SetUnion . fmap par2G 

par2Ps :: Par -> [P]
par2Ps (ParL ps) = foldMap par2Ps ps
par2Ps (ParN n) = [par2P (ParN n)]
par2Ps (ParP p) = [p]
par2Ps _ = []

par2P :: Par -> P
par2P (ParN n) = P emptyPoint { p_mid = Just $ fi n }
par2P (ParP p) = p
par2P p = throw (IllegalArgException p)

par2L :: [Par] -> [Par]
par2L = L.foldr ((++) . fold) [] where
  fold (ParL ps) = ps
  fold p = [p]

data BListOp 
  = All
  | Any
  | Some
  | None
  deriving Show

instance Read BListOp where
    readsPrec _ = parseReads parseBListOp

parseBListOp :: Stream s m Char => ParsecT s u m BListOp
parseBListOp = do
    str <- parseIdenti
    case str of 
        "all"  -> return All
        "any"  -> return Any
        "some" -> return Some
        "none" -> return None
        _ -> mzero
    where
    parseIdenti = fmap toLower <$> parseIdent

opBList :: BListOp -> [Bool] -> Bool
opBList op xs = if L.null xs then True else case op of
    All   -> and xs
    Any   -> or xs
    Some  -> not (and xs)
    None  -> not $ or xs

data OnFail = Fail | SetFalse | Break
  deriving (Eq, Show)

data RunEnv = RunEnv { r_funcs  ::  !(M.Map String Func)
                     , r_binds  ::  !(M.Map Var Bind)
                     , r_wows   ::  !(M.Map String [WEnv])
                     , r_state  ::  !(Maybe Bool)
                     , r_log    ::  ![String]
                     } 

instance Show RunEnv where
    show r = printf "RunEnv\nfuncs:  %s\nbinds:  %s\ngroups: %s\nstate:  %s\nlog: %s" 
        (show $ M.keys (r_funcs r)) (show $ r_binds r) (show $ r_wows r)
        (show $ r_state r) (unlines $ r_log r)

instance Monoid RunEnv where
    mempty = RunEnv baseFunctions mempty mempty Nothing []
    RunEnv f b w s l `mappend` RunEnv f' b' w' _ l'
      = RunEnv (f `mappend` f') (b `mappend` b') 
               (w `mappend` w') s (l `mappend` l')


type CompileTime = ST.State RunEnv
type RunTime = ST.StateT RunEnv IO

loadProg' :: FilePath -> IO RunEnv
loadProg' fp = either (error . show) id <$> loadProg fp

loadProg :: FilePath -> IO (Either ParseError RunEnv)
loadProg fp = flip compileWithCWD (takeDirectory fp) =<< BS.readFile fp

compile' :: ByteString -> IO RunEnv
compile' source = either (error . show) id <$> compile source

compile :: ByteString -> IO (Either ParseError RunEnv)
compile source = compileWithCWD source "."

compileWithCWD :: ByteString -> FilePath -> IO (Either ParseError RunEnv)
compileWithCWD source cwd = do
    mprog <- runParserT parseProg mempty { parsCWD = cwd } "" source
    forM mprog $ \(prog, penv) -> do
        wows <- mapM newWEnv =<< wineWows
        let map' = M.fromList $ ("all", wows) : L.zip (parsGroups penv) 
                                                      (repeat [])
        return $ ST.execState (compileTopLevel `mapM_` prog) 
                              mempty { r_wows = map' }

compileOrExec :: ParseEnv -> RunEnv -> ByteString 
    -> IO (Either ParseError (ParseEnv, RunEnv))
compileOrExec env prevrt source = do
    mprog <- runParserT parseProg env "compileOrExec" source
    case mprog of
        Right (prog,penv) -> return $ Right $ 
                (penv, ST.execState (mapM_ compileTopLevel prog) prevrt)
        Left err -> do
            mexec <- runParserT (liftM2 (,) (parseBody >>= \p -> eof >> return p) P.getState) 
                                env "compileOrExec" source
            case mexec of
                Left _ -> return (Left err)
                Right (body, penv) -> Right . (,) penv <$> 
                        ST.execStateT (compileBody [] body) prevrt

callFunc :: String -> RunTime ()
callFunc name = do
    renv <- get
    case ($ []) <$> M.lookup name (r_funcs renv) of
        Nothing -> do log' $ "No such function: " ++ name
                      modRT False
        Just (Left f) -> void $ tryRT name True f
        Just (Right _) -> do log' $ "Cannot call function with group indexing: " ++ name


sequenceRunTime :: Bool -> [RunTime ()] -> RunTime ()
sequenceRunTime parallel ops = do
    logs <- r_log <$> get
    modify $ \r -> r { r_log = [] }
    res <- (if parallel then sequenceST else sequence . fmap (>> get)) ops
    logs' logs
    logs' (r_log `foldMap` res)
    if any (== Just False) (r_state <$> res) then
        modRT False
    else when (any (== Just True) (r_state <$> res)) $
        modRT True

compileTopLevel :: TopLevel -> CompileTime ()
compileTopLevel exp' = case exp' of
    Decl name _ body -> do
        modify' $ \r -> r 
          { r_funcs = M.insert name (Left . flip compileBody body) (r_funcs r) }
    OAsi (Asignment name (mods,k)) -> do
        modify' $ \r -> r
          { r_binds = M.insert name (vkt <$> mods, vkt k) (r_binds r) }

compileBody :: [Par] -> [Statement] -> RunTime ()
compileBody _ [] = return ()
compileBody pars sts = compileOP $! foldr folder [] sts where
  folder !l !r = (isNegated l, compileStatement pars l) : r
  compileOP [] = return () -- io $ yield
  compileOP ((_,op) : ops) = do
      op
      ok <- r_state <$> get
      if ok /= Just False then
          compileOP (L.dropWhile fst ops)
      else do
          unmodRT
          compileNOP (L.filter fst ops)
          modify $ \r -> r 
            { r_state = if isNothing (r_state r) then 
                            Just False 
                        else 
                            r_state r }
  compileNOP [] = return ()
  compileNOP ((_,op) : ops) = do
      op
      ok <- r_state <$> get
      when (ok /= Just False) $
          compileNOP ops

compileStatement :: [Par] -> Statement -> RunTime ()
compileStatement pars s = case s of
 -- Cmp
    Cmp lhs rhs eq _ -> 
        modRT $ (if eq then (==) else (/=)) 
                    (applyPar lhs pars) 
                    (applyPar rhs pars)
 -- Call
    Call throws _ parallel modifier (VarApply _ namef) groupf parsf -> do
      renv <- get
      let funcname = namef pars
      case ($ (applyPar parsf) pars) <$> M.lookup funcname (r_funcs renv) of
        Nothing -> fail $ "No such function: " ++ funcname
        Just (Left !statf) -> do
            unless (isNothing groupf) $
                   fail $ "Cannot index by wow groups in: " ++ funcname
            statf
        Just (Right !wowf) -> do
            let wows = maybe 
                        (error $ "Must index by wow groups in: " ++ funcname)
                        (getwows (r_wows renv) . ($ pars) . applyPar) groupf
                boolf = maybe (\_ -> Nothing) ((Just .) . opBList) modifier
            ret <- if parallel then do
                       io $ forP wows $ \w -> ST.evalStateT (runReaderT wowf w) renv
                   else
                       forM wows (runReaderT wowf)
            case boolf $ catMaybes ret of
                Nothing -> return ()
                Just st -> unless st $ do
                    if throws then
                        throw $ FailedCallException $ printf "%s failed" funcname
                    else
                        modRT False
 -- Asig
    Asig (Asignment bindname (mods, k)) -> do
      modify $ \r -> r
        { r_binds = M.insert bindname (vkt <$> mods, vkt k) (r_binds r) }
 -- Loop
    Loop _ _ parallel mupdates reps mperiod mgroupf bodies -> do
      mwows <- (groupWows . ($ pars) . applyPar) `mapM` mgroupf 
      body $ sequenceRunTime parallel $ case mwows of
          Nothing -> compileBody pars <$> bodies
          Just !ws -> (\(w, n) -> compileBody 
              (pars ++ [ParG (Singleton (w,n)), ParN n, ParN (L.length ws)])) 
              <$> ws <*> bodies
      where
          body op = do
              reset <- isreset
              if reset then do
                  step reps $ op
                  res <- get
                  when (r_state res == Just False)
                      exiter
              else 
                  exiter
          -- handler op = case onfail of
          --     Fail -> op >> return False
          --     SetFalse -> tryRT "<loop?" False op >> return False
          --     Break -> tryRT "<loop}" False op >> return True
          exiter = case mupdates of
              Just (False, False) -> unmodRT
              Just (True, _) -> fail "loop failed"
              _ -> return ()
          step rep op
            | rep == Just 0 = return ()
            | otherwise = do
                unmodRT
                op
                res <- get
                when (r_state res == Just False || isNothing mupdates) $
                    step (subtract 1 <$> rep) op
          isreset :: RunTime Bool
          isreset = case mperiod of
              Nothing -> return True
              Just (margin, ref) -> do 
                  t <- getTimes
                  v <- io (readIORef ref)
                  if v + margin <= t then do
                      io $ writeIORef ref t
                      return True
                  else
                      return False
 -- For
    For {} -> return ()
                        
getwows :: M.Map String [WEnv] -> Group -> [(WEnv, Int)]
getwows map' set = fix' $ setToListBy eq $ fmap lookgup set where
  eq = (==) `on` either fst id
  lookgup g = maybe (error $ "No such group: " ++ show g) id (M.lookup g map')
  fix' gs = l ++ (L.zip r ns) where
      l = lefts gs
      r = rights gs
      ns = [0..] L.\\ (snd <$> l)


hp_f :: Integral b => a -> (b, b) -> Double
hp_f _ (h, m) =  fi h / fi m -- (fi h / fi m) ** 2 ** (1 - fi m / mean)

baseFunctions :: M.Map String Func
baseFunctions = M.fromList
  [ ("in_party5",   \_ -> r $ liftW inGroup) 
  , ("in_raid",     \_ -> r $ liftW inRaid)
  , ("in_party",    \_ -> r $ liftW inGroupOrRaid)
  , ("party_full",  \_ -> r $ (>= 5) <$> liftW partySize)
  , ("dist_lt",     \[ParP (P p), ParF f] -> r $ liftW $ do
                        pos <- getPos
                        return (realToFrac f > pointDist p pos))
  , ("in_map",      \(p:_) -> r $ liftW $ do
                        let P po = par2P p
                        m <- getMapID
                        return (Just m == p_mid po))
  , ("in_instance", \_ -> r $ liftW inInstance)
  , ("in_combat",   \_ -> r $ liftW inCombat)
  , ("online",      \_ -> r $ liftW isOnline)
  , ("loading",     \_ -> r $ liftW isLoading)
  , ("world_loading", \_ -> r $ liftW worldLoading)
  , ("falling",     \_ -> r $ liftW isFalling)
  , ("casting",     \_ -> r $ liftW isCasting)
  , ("dist_lt",     \[p, d] -> r $ liftW $ do
                        pos <- getPos
                        p' <- pPoint $ par2P p
                        return $ pointDist pos p' < par2Frac d)
  , ("dist_gt",     \[ParP p, d] -> r $ liftW $ do
                        pos <- getPos
                        p' <- pPoint p
                        return $ pointDist pos p' > par2Frac d)
  , (,) "exists" $
          \pars -> r $ liftW $ do
              let (mop, defs) = pars2objs (par2L pars)
              objs <- fmap (not . L.null) <$> mapM findObj defs
              return (not (L.null objs) && maybe or logicOp mop objs)
  , (,) "is_dead" $
          \pars -> r $ liftW $ do
              let (mop, defs) = pars2objs (par2L pars)
                  op = maybe and logicOp mop
              hps <- mapM objHP =<< (join <$> mapM findObj defs)
              return $ op $ ((== 0) . fst) <$> hps
  , ("level_lt",    \[p] -> r $ liftW $ do
                        (< par2Int p) <$> getLevel)
  , ("level_gt",    \[p] -> r $ liftW $ do
                        (> par2Int p) <$> getLevel)
  , ("tp",          tp_ False False False Nothing) 
  , ("tp_l",        tp_ True  False False Nothing)   
  , ("tp_f",        tp_ False True  False Nothing)
  , ("tp_u",        tp_ False True  True  Nothing)
  , ("tp_gt",       tp_ False False False (Just (GT, 0)))
  , ("tp_l_gt",     tp_ True  False False (Just (GT, 0)))
  , ("tp_f_gt",     tp_ False False False (Just (GT, 0)))
  , ("tp_gt_xy",    tp_ False False False (Just (GT, 1)))
  , ("tp_l_gt_xy",  tp_ True  False False (Just (GT, 1)))
  , ("tp_f_gt_xy",  tp_ False True  False (Just (GT, 1)))
  , ("tp_gt_h",     tp_ False False False (Just (GT, 2)))
  , ("tp_l_gt_h",   tp_ True  False False (Just (GT, 2)))
  , ("has_buff",    \ps -> r $ liftW $ do
                        bs <- getBuffs
                        return $ not $ L.null $ L.intersect bs (par2Int <$> par2L ps))
  , ("stopfall",    \_ -> true $ liftW 
                        (stopFall >> io (threadDelay 5000) >> refreshPos))
  , ("stopfall_l",  \_ -> true $ liftW stopFall)
  , ("chat",        \[ParS str] -> true $ liftW (writeChat str))
  , ("istartar",    \_ -> r $ liftW $ do
                        guid <- getGuid
                        mguid <- mapM objTarGuid =<< getTar
                        return (mguid == Just guid))
  , ("target",      \p -> true $ liftW $ case p of
                        [ParS str] -> target str
                        [n] -> void $ targetByUid (par2Int n)
                        _ -> throw (IllegalArgException (ParL p)))
  , ("target_g",    \ps -> true $ liftW $ foldM_ (\alrdy p ->
                        if alrdy then return True else do
                            ret <- targetByUid (par2Int p)
                            return (not $ isNothing ret)) False (par2L ps))
  , ("target_g_a",  \ps -> true $ liftW $ foldM_ (\alrdy p ->
                        if alrdy then return True else do
                            ret <- targetByUidAlive (par2Int p)
                            return (not $ isNothing ret)) False (par2L ps))
  , ("target_p",    \[g] -> true $ do
                        tar <- lift (groupWows $ par2G g)
                        forM_ (listToMaybe tar) $ \t -> do
                            liftW $ setPlayerAsTarget $ wenv_guid $ fst t)
  , ("target_l",    \_ -> r $ do
                        (_, n) <- ask
                        liftW $ do
                          plays <- mapM (\o -> (,) o <$> objHP o) =<< playerList
                          let mean = fi (L.sum (snd . snd <$> plays)) 
                                        / fi (L.length plays) :: Double
                              sorted = L.sortOn (hp_f mean . snd) $ 
                                   L.filter (\(_,(h,mh)) -> h > 0 && h < mh) plays
                          unless (null sorted) $ do
                              setTar $ fst $ (L.cycle sorted !! n)
                          return (not (null sorted)))
  , ("target_b",    \ps -> r $ do
                        (_, n) <- ask
                        liftW $ do
                            ents <- filterM (fmap (L.any (`L.elem` 
                                  (par2Int <$> ps))) . objBuffs) =<< playerList
                            if L.null ents then
                                return False
                            else do
                                setTar $ (L.cycle ents !! n)
                                return True)
  , ("clear_target",  \_ -> true $ liftW clearTar)
  , ("hp_gt",       \ps -> r $ liftW $ do
                        (hp, mahp) <- getHP
                        return $ any (== GT) $ fmap (compf hp mahp) ps)
  , ("hp_lt",       \ps -> r $ liftW $ do
                        (hp, mahp) <- getHP
                        return $ any (== LT) $ fmap (compf hp mahp) ps)
  , ("mana_lt",     \ps -> r $ liftW $ do
                        (mp, mamp) <- getMP
                        return $ mamp > 0 
                              && any (== LT) (fmap (compf mp mamp) ps))
  , ("tar_exists",  \pars -> r $ liftW $ do
                        if L.null pars then do 
                            (/= Nothing) <$> getTar
                        else do
                            uid <- mapM objUid =<< getTar
                            return $ uid `L.elem` (Just . par2Int <$> par2L pars))
  , ("tar_hp_gt",   \ps -> r $ liftW $ do
                        mtup <- tarHP
                        flip (maybe $ return False) mtup
                          $ \(hp,mahp) -> return $ (mahp > 0 &&) 
                            $ flip any ps $ \p' -> case par2Num p' of
                              Left n -> fromIntegral (n :: Int) < hp
                              Right f -> f < (fromIntegral hp / fromIntegral mahp :: Double))
  , ("tar_hp_lt",   \ps -> r $ liftW $ do
                        mtup <- tarHP
                        flip (maybe $ return False) mtup
                          $ \(hp,mahp) -> return 
                            $ flip any ps $ \p' -> case par2Num p' of
                              Left n -> fromIntegral (n :: Int) > hp
                              Right f -> f > (fromIntegral hp / fromIntegral mahp :: Double))
  , ("tar_casting", \pars -> r $ liftW $ do
                        if L.null (par2L pars) then 
                            (/= 0) <$> tarCasting
                        else do
                            c <- tarCasting
                            return $ c `L.elem` (par2Int <$> par2L pars))
  , ("press",       \(p : pars) -> true $ do
                        renv <- get
                        (wenv, n) <- ask
                        let (mods, k) = maybe (error 
                              $ printf "couldn't find bind named: %s" (par2I p)) id
                              $ M.lookup (par2I p) (r_binds renv) 
                            f = liftIO $ withPosted mods (wenv_hwnd wenv) $ do
                                postKey k (wenv_hwnd wenv)
                        case pars of
                            [p'] -> when (n == par2Int p') f
                            _ -> f)
  , ("press'",      \[p] -> true $ do
                        renv <- get
                        (wenv, _) <- ask
                        let (mods, k) = maybe (error 
                              $ printf "couldn't find bind named: %s" (par2I p)) id
                              $ M.lookup (par2I p) (r_binds renv) 
                        liftIO $ withMods mods (wenv_hwnd wenv) $ do
                            sendKey k (wenv_hwnd wenv))
  , ("rightclick",  \[x,y] -> true $ do
                        (WEnv { wenv_hwnd = hWnd }, _) <- ask
                        io $ click 3 (par2Frac x) (par2Frac y) hWnd)
  , ("pos_updated", \_ -> r $ liftW posUpdated)
  , ("sitting",     \_ -> r $ liftW isSitting)
  , ("dead",        \_ -> r $ liftW isDead)
  , ("stunned",     \_ -> r $ liftW isStunned)
  , ("go_corpse",   \_ -> r $ liftW $ do
      c <- getCorpse
      if c == emptyPoint then
          return False
      else
          tpRefreshed c)
  ,(,) "moveto"   $ \ps -> r $ do
      (_, n) <- ask
      let p = L.cycle (par2Ps `foldMap`ps) !! n
      eith <- liftW (tryWoW $ moveTowards =<< pPoint p)
      case eith of Left e -> lift (log' (displayException e)) >> return False
                   Right _ -> return True
  , ("revive",      \_ -> r $ liftW revive)
  , ("farclip",     \[p] -> true $ liftW (setFarclip $ par2Frac p))
  , ("pitch",       \[p] -> true $ liftW (setCamPitch $ par2Frac p))
  , ("rdf_free",    \_ -> r $ liftW rdfFree)
  , ("rdf_queued",  \_ -> r $ liftW rdfQueued)
  , ("rdf_prompted",\_ -> r $ liftW rdfPrompted)
  , ("rdf_matched", \_ -> r $ liftW rdfMatched)
  , ("lookat3",     \(pa:_) -> true $ liftW $ do
                        p <- getPos
                        setCamYaw (par2Frac pa - maybe 0 id (p_h p)))
  , ("tar_z",       \_ -> true $ liftW $ do
                        p <- getPos
                        objs <- do objects <- L.filter ((== 5) . ofs_type) 
                                                                <$> objList
                                   mapM (\o -> (,) o <$> objPos o) objects
                        let pairs = L.filter (\(_,p') -> xyDist p p' == 0) objs
                        forM_ (listToMaybe (fst <$> pairs)) setTar)
  , ("bring_down",  \_ -> true $ liftW $ do
                        mtar <- getTar
                        forM_ mtar $ \tar -> do
                            p <- getPos
                            setObjPos tar emptyPoint { p_z = p_z p })
  , (,) "memwrite" $
          \ps -> true $ liftW $ do
              pid <- wenv_pid <$> get
              addr <- io $ chainOffset pid (par2Int <$> init (par2L ps))
              writePar addr (L.last (par2L ps))

  , (,) "cmp" $
          \[n,cmp] -> Left $ modRT (par2C cmp (par2Frac n))
  , (,) "and" $
          \(n:cmps) -> Left $ modRT (all (flip par2C $ par2Frac n) cmps)
  , ("refresh",     \pars -> Left $ updateGroups (pars2G pars))
  , ("compact",     \pars -> Left $ do
                        wows <- fmap fst <$> groupWows (pars2G pars)
                        uids <- io $ forM wows (evalStateT getGuid)
                        (xtr, mss) <- io $ evalStateT 
                                        (diffPlayers uids) (L.head wows)
                        unless (L.null xtr && L.null mss) $ do
                            modRT False)
  , ("delay",       \[par] -> Left $ case par2Num par of
                        Left n -> io $ delay $ fi (n :: Int)
                        Right d -> io $ delay $ round (d * 1e6 :: Double))
  , ("refresh_all", \_ -> Left $ do
                        renv <- get
                        map' <- forM (r_wows renv) $ mapM 
                                    (io . newWEnv . wenv_pid )
                        put renv { r_wows = map' })
  , (,) "group_size_gt" $ \[g,n] -> Left $ do
                        group <- groupWows (par2G g)
                        modRT (L.length group > par2Int n)
  , (,) "g_clear"      $ \pars -> Left $ do
                        let gs = (\(Set g) -> (g,[])) <$> par2G <$> pars
                        modify $ \re 
                            -> re { r_wows = M.fromList gs `mappend` r_wows re }
  , ("g_add_hlvl",  \[ParI source, ParI dest] -> Left $ do
                        renv <- get
                        wows <- io $ sortOnM 
                            (evalStateT (liftM2 (,) getLevel getXP)) 
                          $ maybe (error "no such group") id 
                          $ M.lookup source (r_wows renv)
                        let wows' = maybe (error "no such group") id
                              $ M.lookup dest (r_wows renv)
                            dif = wows L.\\ wows'
                        unless (L.null dif) $ do
                          put renv { r_wows = 
                              M.adjust (L.union [L.last dif]) dest (r_wows renv) })
  , ("false", \[] -> Left $ modRT False)
  , ("true", \[] -> Left $ modRT True)
  , ("log", \[ParS str] -> Left $ log' str)
  ] where
      r x = Right $! Just <$> x
      compf val ma (ParF f) = compare (fi val / fi ma) f
      compf val _ (ParN n) = compare val (fi n)
      compf _ _ par = error $ "cannot compf " ++ show par
      tp_ :: Bool -> Bool -> Bool -> Maybe (Ordering, Int) -> Func 
      tp_ lazy _ unsafe ordr ps = true $ do
          (_, n) <- ask
          let flags = [StopFall]
                   ++ if not lazy then [Refresh] else []
                   ++ if unsafe then [Unsafe] else []
          liftW $ case ordr of
              Nothing -> do
                  let p = L.cycle (par2Ps `foldMap` ps) !! n
                  tp p flags
              Just (orde, fu) -> do
                  let p = L.cycle (par2Ps `foldMap` L.init ps) !! n
                  pos <- getPos
                  po <- pPoint p
                  let dist = if fu == 0 then pointDist else 
                             if fu == 1 then xyDist else h_dist
                  when (compare (dist pos po) (par2Frac (L.last ps)) == orde)
                      $ tp (P po) flags
      h_dist p p' = maybe 0 id $ liftM2 (\a b -> abs (b - a)) (p_h p) (p_h p')
      delay :: Int -> IO ()
      delay n = do
          t <- fi <$> getTime
          threadDelay (n `quot` 2)
          delayTill (t + n `quot` 1000)
          where
          delayTill t = do
              t' <- fi <$> getTime
              when (t' < t) $ do
                  threadDelay (1000 * min (n `quot` 6000) (t - t'))
                  delayTill t

liftW :: WoW a -> RTWoW a
liftW op = ask >>= io . (flip runWoW' op) . fst 

pars2objs :: [Par] -> (Maybe LogicOp, [Either Word32 ByteString])
pars2objs ps = foldr folder (Nothing, []) (par2L ps) where
  folder p (op, objs) = case p of
      ParS str -> (op, objs ++ [Right $ BS.pack str])
      _ -> case par2I' p of
          Just i | i ~= "or"  || i ~= "any" -> (Just Or, objs)
                 | i ~= "and" || i ~= "all" -> (Just And, objs)
                 | otherwise -> (op, objs ++ [Right $ BS.pack i])
          Nothing -> (op, objs ++ [Left (par2Int p)])

infix 4 ~=
(~=) :: String -> String -> Bool
s ~= s' = fmap toLower s == fmap toLower s'

-- util

log' :: String -> RunTime ()
log' str = logs' [str] 

logs' :: [String] -> RunTime ()
logs' strs = modify $ \r -> r { r_log = r_log r ++ strs }

true :: RTWoW a -> Either b (RTWoW (Maybe Bool))
true op = Right (op >> return Nothing)

fi :: (Integral a, Num b) => a -> b
fi = fromIntegral

tryStateT :: ST.StateT s IO a -> ST.StateT s IO (Either String a)
tryStateT op = do
    s <- get
    eith <- fmap (first (displayException :: SomeException -> String)) 
            $ liftIO $ try (ST.runStateT op s)
    either (\_ -> return ()) (put . snd) eith
    return $ second fst eith

tryRT :: String -> Bool -> RunTime () -> RunTime Bool
tryRT tag verb op = do
    t0 <- getTime
    eith <- tryStateT op
    either
        (\exc -> do
            let str = maybe exc trim $ listToMaybe $ catMaybes $ 
                        mapM L.stripPrefix ["user error"] exc
            modRT False
            log' $ printf "Err %s: %s" tag str
            return False)
        (\_ -> if not verb then return True else do
            st <- r_state <$> get
            if st == Just False then
                log' $ printf "Err %s: failed" tag
            else do
                t1 <- getTime
                log' $ printf "Ok %s (%.2fs)" tag (fi (t1-t0) / 1000 :: Double)
            return True)
        eith

updateGroups :: Group -> RunTime ()
updateGroups gs = do
    wows <- fmap fst <$> groupWows gs
    r <- get
    map' <- flip M.traverseWithKey (r_wows r) $ \_ -> mapM $ \w -> do
        if w `L.elem` wows then
            io $ newWEnv (wenv_pid w)
        else
            return w

    put r { r_wows = map' }

modRT :: Bool -> RunTime ()
modRT b = modify $ \r -> r { r_state = Just b } 

unmodRT :: RunTime ()
unmodRT = modify $ \r -> r { r_state = Nothing }
