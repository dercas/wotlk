module WoW.Wotlk.Explore where

import Control.Concurrent
-- import Control.Exception
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.State hiding (get,put)

import Data.List
import Data.Maybe

import System.Environment
import System.Posix.Types

import Text.Printf
import Text.Read hiding (get, lift)

import WoW.Wotlk.Box
import WoW.Wotlk.Core

data EEntry = EEntry MapID Float Float | E3 MapID Float Float Float

instance Read EEntry where
    readPrec = liftM4 E3 readPrec readPrec readPrec readPrec +++ liftM3 EEntry readPrec readPrec readPrec

instance Show EEntry where
    show (EEntry (MapID m) x y) = show m ++ " " ++ show x ++ " " ++ show y
    show (E3 (MapID m) x y z) = show m ++ " " ++ show x ++ " " ++ show y ++ " " ++ show z

data Behaviour = All | Sync | Current
    deriving (Show, Eq)

data Opts = Opts
  { odelay      :: Double
  , obehaviour  :: Behaviour
  } deriving (Show, Eq)

defaultOpts :: Opts
defaultOpts = Opts 0.0 Current

readOpts :: StateT ([String], Opts) Maybe ()
readOpts = do
    arg <- shift
    case arg of
        "-a" -> opt $ \x -> x { obehaviour = All }
        "-s" -> opt $ \x -> x { obehaviour = Sync }
        "-d" -> optRead "-d" $ \x y -> x { odelay = y }
        _ -> error $ printf "unknown argument: %s" arg
    remaining <- not . null . fst  <$> get
    when remaining readOpts
    where
      shift :: StateT ([String], Opts) Maybe String
      shift = do
          (margs, opts) <- get
          (arg, args) <- lift $ uncons margs 
          put (args, opts)
          return arg
      opt :: (Opts -> Opts) -> StateT ([String], Opts) Maybe ()
      opt f = modify (\(as, os) -> (as, f os))
      readFail :: Read a => String -> String -> a
      readFail text word = maybe 
          (error $ printf "%s: couldn't parse %s" text word) id (readMaybe word)
      optRead :: Read a => String -> (Opts -> a -> Opts) -> StateT ([String], Opts) Maybe ()
      optRead text f = do
          x <- readFail text <$> shift
          opt (`f` x)

entryToPoint :: EEntry -> Point
entryToPoint (EEntry mid x y) = Point (Just x) (Just y) (Just $ -100) Nothing Nothing (Just mid)
entryToPoint (E3 mid x y z) = Point (Just x) (Just y) (Just z) Nothing Nothing (Just mid)

pointToEntry :: Point -> EEntry
pointToEntry p = E3 (maybe undefined id $ p_mid p) (m' $ p_x p) (m' $ p_y p) (m' $ p_z p)

loadEEntries :: FilePath -> IO [EEntry]
loadEEntries path = fmap (catMaybes . (fmap readMaybe) . lines) (readFile path)

newEEntry :: CPid -> IO EEntry
newEEntry pid = runWoW pid $ pointToEntry <$> getPos

appendEEntries :: FilePath -> [EEntry] -> IO ()
appendEEntries fp es = do
    appendFile fp $ unlines (show <$> es)

append' :: CPid -> IO ()
append' pid = do
    e <- newEEntry pid
    appendEEntries "explore.txt" [e]

fork :: CPid -> IO (MVar Point, MVar Bool)
fork pid = do
    m <- newEmptyMVar 
    n <- newMVar True

    let loop = do
        p <- io $ takeMVar m
        tpRefreshed' p
        ext <- io $ takeMVar n
        when ext loop
    forkIO $ runWoW pid $ do
        clip <- getFarclip
        setFarclip 15
        loop
        setFarclip clip
    return (m,n)

forkExplore :: Opts -> CPid -> IO (MVar ())
forkExplore opts wow = do
    m <- newEmptyMVar
    forkIO $ runWoW wow $ do
        points <- io $ fmap entryToPoint <$> loadEEntries "explore.txt"
        forM_ points (tpChecked opts)
        io $ putMVar m ()
    return m


main :: IO ()
main = do
    args <- drop 1 <$> getArgs
    let opts = maybe defaultOpts snd $ execStateT readOpts (args, defaultOpts)
    case obehaviour opts of
        All -> do
            wows <- filterM (\w -> runWoW w isOnline) =<< wineWows
            vars <- forM wows (forkExplore opts)
            forM_ vars takeMVar
        Sync -> do
            wows <- filterM (\w -> runWoW w isOnline) =<< wineWows
            points <- io $ fmap entryToPoint <$> loadEEntries "explore.txt"
            vs <- forM wows fork
            forM_ points $ \p -> do
                forM_ vs $ \(m,n) -> do
                    putMVar m p
                    putMVar n True
            forM_ (fst <$> vs) (flip putMVar emptyPoint)
            forM_ (snd <$> vs) (flip putMVar False)
            threadDelay 250000
        Current -> void $ inCurrentWoW $ do
            p <- io $ fmap entryToPoint <$> loadEEntries "explore.txt"
            forM_ p (tpChecked opts)

m' :: Maybe b -> b
m' = fromJust

tpChecked :: Opts -> Point -> WoW ()
tpChecked opts p = do
    xp <- getXP
    b <- tpRefreshed p
    when b $ do
      waitXP xp
      io $ delay (odelay opts)

waitXP :: Word32 -> WoW ()
waitXP xp = wait' 0 where
  wait' :: Word32 -> WoW ()
  wait' n = do
      xp' <- getXP
      if xp == xp' && n < 30 then do
          when (n `mod` 10 == 9) $ do
              refreshPos
          io $ threadDelay 15000
          wait' (n + 1)
      else
          return ()

      
delay :: Double -> IO ()
delay = threadDelay . round . (* 1000000)
