{-# LANGUAGE OverloadedStrings, DeriveGeneric, GeneralizedNewtypeDeriving,
    StandaloneDeriving, CPP, Arrows, FlexibleContexts, RankNTypes #-}

module WoW.Wotlk.Core 
    ( module WoW.Wotlk.Core 
    , module Control.Monad
    , module Data.Word
    , showHex
    , get, put
    , ByteString
    ) where
import WoW.Wotlk.Consts

import Control.Arrow hiding ((+++), first)
import Control.Concurrent
import Control.Exception
import Control.Monad
import Control.Monad.State
import qualified Control.Monad.Trans.State.Strict as ST

import Data.ByteString hiding (readFile, null, hGetContents)
import qualified Data.ByteString.Char8 as BS
import Data.Bifunctor as BiF
import Data.Bits hiding (setBit)
import Data.Char
import Data.Fixed
import Data.Foldable
import Data.Function
import Data.Functor.Identity ()
import qualified Data.List as L
import qualified Data.Map.Strict as M
import Data.Maybe
import qualified Data.Serialize as S
import Data.Word

import Foreign hiding (void)
import Foreign.C

import GHC.Generics (Generic)
-- import GHC.IO (unsafePerformIO)

import Numeric 

import Prelude 

import System.Posix.Directory
import System.Environment
import System.IO
import System.IO.Unsafe
import System.Process.VM.SafeCall
import System.Random (randomRIO)

import Text.Parsec as P hiding (try, getState, setState)
import qualified Text.Parsec as P
import Text.ParserCombinators.Parsec.Numeric
import Text.Printf --(printf)
import Text.Read (readMaybe)

import Time.System -- (timeCurrentP)
import Time.Types (Elapsed (..), ElapsedP (..), Seconds (..), NanoSeconds (..))

import XHotkey hiding (io, get, ask, execStateT, StateT (..))


-- import Debug.Trace

deriving instance Generic CUInt
instance S.Serialize CUInt where

deriving instance Generic KM
instance S.Serialize KMitem where

deriving instance Generic KMitem
instance S.Serialize KM where

#ifndef DEBUGLVL
#  define DEBUGLVL 0
#endif

-- debug :: PrintfType t => String -> t
-- debug = if DEBUGLVL > 0 then Prelude.putStrLn else \_ -> return ()

-- newtype Vec a = Vec { unVec :: [a] }
--     deriving (Eq, Ord, Monoid, Functor, Applicative, Monad, Generic, S.Serialize)
-- 
-- instance (Remote a, Show a) => Show (Vec a) where
--     show (Vec xs) = L.unlines $ snd $ L.mapAccumL show' (0 :: Int) xs where
--         show' n e = (n + sizeOf e, printf "0x%x %s" n (show e))

newtype MapID = MapID { map_id :: Word32 }
    deriving (Num, Enum, Ord, Real, Integral, Remote, S.Serialize)

instance Eq MapID where
    (==) = (==) `on` ((.&. 0x7fffffff) . map_id)

instance Read MapID where
    readsPrec n = fmap (first MapID) . readsPrec n

instance Show MapID where
    show mid = format $ lookupMapID mid where
        format str = printf ("%-3d " ++ str) (map_id mid)

lookupMapID :: MapID -> String
lookupMapID (MapID mid) = maybe "<unknown>" id $ M.lookup mid mapMap

data WEnv = WEnv
    { wenv_pid :: CPid
    , wenv_base :: Word32
    , wenv_guid :: Word32
    , wenv_name :: ByteString
    , wenv_hwnd :: Word64 
    } 

instance Eq WEnv where
    w1 == w2 = (wenv_pid w1) == (wenv_pid w2)

eqWEnv :: WEnv -> WEnv -> Bool
eqWEnv w1 w2 = and [wenv_pid w1 == wenv_pid w2, wenv_name w1 == wenv_name w2]

instance Show WEnv where
    show w = printf "WEnv { %-6d %-12s 0x%x }" (fromIntegral $ wenv_pid w :: Word) 
                              (BS.unpack $ wenv_name w) (wenv_base w)
type WoW = StateT WEnv IO

runWoW :: CPid -> WoW a -> IO a
runWoW pid act = do
    env <- newWEnv pid
    (ret, _) <- runStateT (reloadPlaybase >> act) env 
    return ret

runWoW' :: WEnv -> WoW a -> IO a
runWoW' env act = fmap fst $ runStateT act env

tryWoW :: WoW a -> WoW (Either SomeException a)
tryWoW op = do
    s <- get
    eith <- liftIO $ try (runStateT op s)
    either (\_ -> return ()) (put . snd) eith
    return $ BiF.second fst eith

inCurrentWoW :: WoW a -> IO (Maybe a)
inCurrentWoW act = currentWoW >>= traverse (flip runWoW' act)

inCurrentWoWX :: WoW a -> X (Maybe a)
inCurrentWoWX act = currentWoWX >>= traverse (io . flip runWoW' act)

execWoW :: WoW a -> WEnv -> IO WEnv
execWoW = execStateT

type Uid = Word32 

data Point = Point
    { p_x   :: Maybe Float  -- x coordinate
    , p_y   :: Maybe Float  -- y coordinate
    , p_z   :: Maybe Float  -- z coordinate
    , p_h   :: Maybe Float  -- heading angle. See Tait-Bryan angles
    , p_e   :: Maybe Float  -- elevation angle
    , p_mid :: Maybe MapID  -- map ID
    } deriving (Eq, Ord, Generic)

emptyPoint :: Point
emptyPoint = Point Nothing Nothing Nothing Nothing Nothing Nothing

showPoint :: Point -> String
showPoint p = showe where
  maypend str remainder = if remainder then printf "%s _" str else str 
  showe = case p_e p of
      Just e -> printf "%s %.2f" (showh True) e 
      Nothing -> showh False
  showh remainder = case p_h p of
      Just h -> printf "%s %.2f" (showz True) h 
      Nothing -> maypend (showz remainder) remainder
  showz remainder = case p_z p of
      Just z -> printf "%s %.2f" (showy True) z 
      Nothing -> maypend (showy remainder) remainder
  showy remainder = case p_y p of
      Just y -> printf "%s %.2f" (showx True) y
      Nothing -> maypend (showx remainder) remainder
  showx remainder = case p_x p of
      Just x -> printf "%s %.2f" showm x 
      Nothing -> maypend showm remainder
  showm = case p_mid p of
      Just m -> show $ map_id m
      Nothing -> "_"

showPoint' :: Point -> String
showPoint' Point { p_x = x, p_y = y, p_z = z, p_h = h, p_e = e, p_mid = m } = 
    L.foldr1 (\a b -> a ++ " " ++ b) (maybe "_" id <$> minimal)
    where
    minimal = L.reverse (L.dropWhile isNothing (L.reverse list))
    list = [i m, f x, f y, f z, f h, f e]
    i = fmap show
    f = fmap $ printf "%.2f"

instance Show Point where
    show (Point Nothing Nothing Nothing Nothing Nothing Nothing) = ""
    show (Point x y z h e mid) = 
        (maybe "" ((++ " ") . show) mid) ++ "(" ++ unmaybe x ++ ", " ++ 
            unmaybe y ++ ", " ++ unmaybe z ++ ")" ++ mh h ++ me e
        where
            unmaybe = maybe "_" show 
            mh = maybe "" (printf " θ: %f")
            me = maybe "" (printf " ψ: %f")

instance Read Point where
    readsPrec _ = parseReads (($ ()) <$> parsePoint mzero mzero)

instance Storable Point where
    sizeOf _ = 24
    alignment _ = 8
    peek ptr = do
        [x,y,z] <- fmap (\x -> if x==0 then Nothing else Just x) 
            <$> traverse (peekByteOff ptr) [0,4,8] :: IO ([Maybe Float])
        [h,e] <- fmap Just <$> traverse (peekByteOff ptr) [16,20] 
        return $ Point x y z h e Nothing
    poke ptr (Point x y z h e _) = do
        traverse_ (pokeByteOff ptr 0) x
        traverse_ (pokeByteOff ptr 4) y
        traverse_ (pokeByteOff ptr 8) z
        traverse_ (pokeByteOff ptr 16) h
        traverse_ (pokeByteOff ptr 20) e

instance Remote Point where
    peep p a = liftM6 Point (peepNon0 p a)            (peepNon0 p (a+4)) 
                            (peepNon0 p (a+8))        (Just <$> peep p (a+16)) 
                            (Just <$> peep p (a+20))  (return Nothing)
    plant p a (Point x y z h e _) = do
        forM_ x (plant p a)
        forM_ y (plant p $ a + 4)
        forM_ z (plant p $ a + 8)
        forM_ h (plant p $ a + 16)
        forM_ e (plant p $ a + 20)
    peepUnsafe p a = liftM6 Point (peepNon0_ p a)           (peepNon0_ p (a+4)) 
                                  (peepNon0_ p (a+8)) (Just <$> peep_ p (a+16)) 
                                  (Just <$> peep_ p (a+20))    (return Nothing)
    plantUnsafe p a (Point x y z h e _) = do
        forM_ x (plant_ p a)
        forM_ y (plant_ p $ a + 4)
        forM_ z (plant_ p $ a + 8)
        forM_ h (plant_ p $ a + 16)
        forM_ e (plant_ p $ a + 20)

parsePoint_ :: (Arrow a, Stream s m Char) 
        => ParsecT s u m (a b Word32) -> ParsecT s u m (a b Float) 
        -> [ParsecT s u m (a b Point)]
parsePoint_ f g = 
    [ (liftA6 $ \m x y z h e -> Point x y z h e m) <$> mint <*> mflo <*> mflo 
                                                   <*> mflo <*> mflo <*> mflo
    , (liftA5 $ \m x y z h -> Point x y z h n m)   <$> mint <*> mflo <*> mflo 
                                                   <*> mflo <*> mflo 
    , (liftA4 $ \m x y z -> Point x y z n n m)     <$> mint <*> mflo <*> mflo 
                                                   <*> mflo
    , (liftA3 $ \m x y -> Point x y n n n m)       <$> mint <*> mflo <*> mflo
    , (liftA2 $ \m x -> Point x n n n n m)         <$> mint <*> mflo
    , liftA (Point n n n n n) <$> mint
    ]
    where
    mint = mapcatm MapID $ parseUMaybe $ P.try $ 
        parseInt True f
    mflo = P.try $ many (tab <|> char ' ') >> (mapcatm id $ parseUMaybe $
        parseFloat True g)
    n = Nothing
    -- p m x y z h e = Point x y z e h m

parsePoint :: (Arrow a, Stream s m Char) => ParsecT s u m (a b Word32) 
    -> ParsecT s u m (a b Float) -> ParsecT s u m (a b Point)
parsePoint f g = parenthesis $ do
    m <- mint 
    do x <- mflo
       do y <- mflo
          do z <- mflo
             do h <- mflo
                do e <- mflo
                   return (liftA6 Point x y z h e m)
                 <|> return (liftA5 (\a b c d e -> Point b c d e n a) m x y z h)
              <|> return (liftA4 (\a b c d -> Point b c d n n a) m x y z)
           <|> return (liftA3 (\a b c -> Point b c n n n a) m x y)
        <|> return (liftA2 (\a b -> Point b n n n n a) m x)
     <|> return (liftA (\a -> Point n n n n n a) m)
    where
    mint = mapcatm MapID $ parseUMaybe $ P.try $ 
        parseInt True f
    mflo = P.try $ many (char ' ' <|> tab) >> (mapcatm id $ parseUMaybe $
        parseFloat True g)
    n = Nothing
    parenthesis pars = pars <|> parseparen pars

-- Unsafe field retrieving functions
p_x' :: Point -> Float
p_x' = fromJust' "p_x'" . p_x

p_y' :: Point -> Float
p_y' = fromJust' "p_y'" . p_y

p_z' :: Point -> Float
p_z' = fromJust' "p_z'" . p_z

p_h' :: Point -> Float
p_h' = fromJust' "p_h'" . p_h

p_e' :: Point -> Float
p_e' = fromJust' "p_e'" . p_e

data Set a b
  = Singleton a
  | Set b
  | SetUnion (Set a b) (Set a b)
  | SetDiff (Set a b) (Set a b)
  | SetInter (Set a b) (Set a b)
  deriving (Read, Eq)

instance (Show a, Show b) => Show (Set a b) where
    show (Set x) = show x
    show (SetDiff s s') = printf "(%s - %s)" (show s) (show s')
    show (SetUnion s s') = printf "(%s + %s)" (show s) (show s')
    show (SetInter s s') = printf "(%s ^ %s)" (show s) (show s')
    show (Singleton y) = show y

parseSet :: (Stream s m Char, Arrow a) => 
        ParsecT s u m (a b c) -> ParsecT s u m (a b d) 
     -> ParsecT s u m (a b (Set c d)) -> ParsecT s u m (a b (Set c d))
parseSet sinp setp comp = ops where
    ops = do
        lh <- value
        fmap (L.foldr apply lh) $ many $ P.try $ do
            spaces
            P.choice [ char '+' >> mapcat (flip SetUnion) value
                     , char '-' >> mapcat (flip SetDiff) value
                     , (char '^' <|> char '*') >> mapcat (flip SetInter) value
                     ]
    value = do
        spaces 
        choice $ P.try <$> 
          [ mapcat Singleton sinp
          , mapcat Set setp
          , comp 
          , parseparen (parseSet sinp setp comp) ]

instance Functor (Set a) where
    fmap f (Set x) = (Set (f x))
    fmap f (SetUnion x y) = SetUnion (fmap f x) (fmap f y)
    fmap f (SetDiff x y) = SetDiff (fmap f x) (fmap f y)
    fmap f (SetInter x y) = SetInter (fmap f x) (fmap f y)
    fmap _ (Singleton x) = Singleton x

setToListBy :: Foldable t => 
                  (Either a b -> Either a b -> Bool) -> Set a (t b) -> [Either a b]
setToListBy eq = g where
  g s = case s of
    Singleton x -> [Left x]
    Set x -> Right <$> Data.Foldable.toList x
    SetUnion x y -> (L.unionBy eq) (g x) (g y)
    SetDiff x y -> (L.deleteFirstsBy eq) (g x) (g y)
    SetInter x y -> (L.intersectBy eq) (g x) (g y) 

data Ent 
  = NPC 
      { ent_name  :: ByteString
      , ent_guid  :: Word32
      , ent_uid   :: Word32
      , ent_pos   :: Point
      }
  | Player 
      { ent_name  :: ByteString
      , ent_guid  :: Word32
      , ent_pos   :: Point
      }
  | Object 
      { ent_name  :: ByteString
      , ent_guid  :: Word32
      , ent_uid   :: Word32
      , ent_pos   :: Point
      }
  | Spell 
      { ent_guid  :: Word32
      , ent_uid   :: Word32
      , ent_pos   :: Point
      }
  | UnknownEnt 
  deriving (Ord)

instance Eq Ent where
    UnknownEnt == UnknownEnt = True
    UnknownEnt == _ = False
    _ == UnknownEnt = False
    e == e' = ent_guid e == ent_guid e' 

instance Show Ent where
    show (NPC n g u _) = printf "NPC {%d, %s, 0x%X}" u (BS.unpack n) g
    show (Player n g _) = printf "Player {%s, 0x%X}" (BS.unpack n) g
    show (Object n g u _) = printf "Object {%d, %s, 0x%X}" u (BS.unpack n) g
    show (Spell g u _) = printf "Spell {%d, 0x%X}" u g
    show _ = ""

isNPC :: Ent -> Bool
isNPC NPC {} = True
isNPC _ = False

isPlayer :: Ent -> Bool
isPlayer Player {} = True
isPlayer _ = False

isObject :: Ent -> Bool
isObject Object {} = True
isObject _ = False

isSpell :: Ent -> Bool
isSpell Spell {} = True
isSpell _ = False

entList :: WoW [Ent]
entList = forEnt return

forEnt :: (Ent -> WoW a) -> WoW [a]
forEnt op = fmap catMaybes $ forObj $ \o -> do
    e <- objToEnt o
    if e == UnknownEnt then
        return Nothing
    else 
        Just <$> op e

objToEnt :: OfsObj -> WoW Ent
objToEnt o @ OfsObj { ofs_type = t }
  | t == 3 = liftM4 NPC (objName o)
                        (return $ ofs_guid o)
                        (objUid o)
                        (objPos o)
  | t == 4 = liftM3 Player (objName o)
                           (return $ ofs_guid o)
                           (objPos o)
  | t == 5 = liftM4 Object (objName o)
                           (return $ ofs_guid o)
                           (objUid o)
                           (objPos o)
  | t == 6 = liftM3 Spell (return $ ofs_guid o)
                          (objUid o)
                          (objPos o)
  | otherwise = return UnknownEnt 

entList_ :: WoW [Ent]
entList_ = forEnt_ return 

forEnt_ :: (Ent -> WoW a) -> WoW [a] 
forEnt_ op = fmap catMaybes $ forObj $ \o -> do
    e <- objToEnt_ o
    if e == UnknownEnt then
        return Nothing
    else 
        Just <$> op e

foldEnts_ :: (a -> Ent -> WoW a) -> a -> WoW a
foldEnts_ f initial = entList_ >>= foldM f initial


objToEnt_ :: OfsObj -> WoW Ent
objToEnt_ o @ OfsObj { ofs_type = t }
  | t == 3 = liftM4 NPC (f$ objName o)
                        (return $ ofs_guid o)
                        (f$ objUid o)
                        (f$ objPos o)
  | t == 4 = liftM3 Player (f$ objName o)
                           (return $ ofs_guid o)
                           (f$ objPos o)
  | t == 5 = liftM4 Object (f$ objName o)
                           (return $ ofs_guid o)
                           (f$ objUid o)
                           (f$ objPos o)
  | t == 6 = liftM3 Spell (return $ ofs_guid o)
                          (f$ objUid o)
                          (f$ objPos o)
  | otherwise = return UnknownEnt 
  where
      f = interleaveWoW

entsWithin :: Float -> WoW [Ent]
entsWithin r = do
    pos <- getPos
    L.filter (\o -> ent_pos o `xyDist` pos < r) <$> entList

-- entsCenter :: WoW [Ent] 
-- entsCenter = do
--     pos <- getPos
--     ps <- fmap ent_pos <$> L.filter (\o -> ent_pos o `xyDist` pos < 200) <$> entList_
--     return []

data P =
    P Point
  | T (Maybe Float) (Maybe Float) Float -- radius yaw-off z-off
  | E (Either Word32 ByteString) (Maybe Float) (Maybe Float) Float
  | O P [(Float, P)]
  | R Float Float P
  | N [P]
  | YawAt P P
  deriving (Eq, Show)

instance Read P where
    readsPrec _ = parseReads (($ ()) <$> parseP mzero mzero mzero mzero mzero)

parseP :: (Stream s m Char, Arrow a) => ParsecT s u m (a b Int) -> ParsecT s u m (a b Float) -> ParsecT s u m (a b Point) -> ParsecT s u m (a b P) -> ParsecT s u m (a b [P]) -> ParsecT s u m (a b P)
parseP = parseP' True -- intp flop poip pp psp = P.choice $ P.try <$> parseP_ True intp flop poip pp psp
parseP' :: (Stream s m Char, Arrow a) => Bool -> ParsecT s u m (a b Int) -> ParsecT s u m (a b Float) -> ParsecT s u m (a b Point) -> ParsecT s u m (a b P) -> ParsecT s u m (a b [P]) -> ParsecT s u m (a b P)
parseP' ayaw intp flop poip pp psp = parns $ yaw $ choice
  [ pp
  , do
      optional (P.try $ char 'P' >> space >> spaces)
      mapcat P (parsePoint (mapcat fromIntegral intp) flop <|> poip)
  , do
      char 'T'
      do r <- space >> mflo
         do th <- mflo
            do z <- P.try $ spaces >> parseFloat True flop
               return (liftA3 T r th z)
             <|> return (liftA2 (\a b -> T a b 0) r th)
          <|> return (liftA (\a -> T a n 0) r)
       <|> return (pureArr $ T n n 0)
  , do
      char 'E'
      space 
      obj <- mapcat (Left . fromIntegral) (spaces >> parseInt True intp)
         <|> pureArr . Right . BS.pack <$> parseString
      do r <- mflo
         do th <- mflo
            do z <- P.try $ spaces >> parseFloat True flop
               return (liftA4 E obj r th z)
             <|> return (liftA3 (\a b c -> E a b c 0) obj r th)
          <|> return (liftA2 (\a b -> E a b n 0) obj r)
       <|> return (liftA (\a -> E a n n 0) obj)
  , do
      char 'O'
      space >> spaces
      liftM2 (liftA2 O) 
                (P.try $ P.between (spacesaft $ char '(') (char ')') 
                    (spacesaft $ choice (P.try <$> parseP_ False intp flop poip pp psp))) $
                fmap ((>>^ join) . sequenceArr) $ (spaces >>) $ parselist $ 
                  P.try $ liftM2 (liftA2 (fmap . (,))) 
                         (spacesaft $ parseFloat True flop) $ spacesaft $ P.choice
                          [ P.between (spacesaft (char '(')) (char ')')
                              (spacesaft $ parsePs intp flop poip pp psp) 
                          , mapcat pure (parseP intp flop poip pp psp)
                          , parseSpecialPs intp flop poip pp psp
                          ] 
--              (P.try $ spaces >> parseP' False intp flop poip pp psp) $
--              fmap ((>>^ join) . sequenceArr) $ parns $ 
--                P.try $ flip sepBy1 (spaces >> char ',' >> spaces) $ 
--                P.try $ liftM2 (liftA2 (fmap . (,))) 
--                       (spaces >> parseFloat True flop) 
--                       (spaces >> parsePsOrP intp flop poip pp psp) 
  , do
      char 'R'
      space
      liftM3 (liftA3 R) (spaces >> parseFloat True flop) 
                        (spaces >> parseFloat True flop) 
                        (spaces >> parseP intp flop poip pp psp)
  , do
      char 'N'
      spaces
      liftA N <$> parsePsOrP intp flop poip pp psp
  , parseparen (parseP intp flop poip pp psp)
  ] where
    yaw f 
      | ayaw = (<|> f) $ P.try $ do
          lh <- P.try f
          spaces >> string "->" >> spaces
          rh <- f
          return (liftA2 YawAt lh rh)
      | otherwise = f
    mflo = P.try $ spaces >> (mapcatm id $ parseUMaybe $
        parseFloat True flop)
    n = Nothing
    parns f = P.try f <|> (parseparen f) 

parseP_ :: (Stream s m Char, Arrow a) => Bool -> ParsecT s u m (a b Int) 
  -> ParsecT s u m (a b Float) -> ParsecT s u m (a b Point) 
  -> ParsecT s u m (a b P) -> ParsecT s u m (a b [P]) -> [ParsecT s u m (a b P)]
parseP_ incyaw intp flop poip pp psp = yaw $ join $
  [ [pp]
  , (optional (P.try $ char 'P' >> space >> spaces) >>) <$>
    mapcat P <$> (poip : parsePoint_ (mapcat fromIntegral intp) flop)
  , ((char 'T') >>) <$>
    [ liftM3 (liftA3 T) mflo mflo (spaces >> parseFloat True flop)
    , liftM2 (liftA2 (\r o -> T r o 0)) mflo mflo
    , liftM  (liftA  (\r -> T r n 0)) mflo
    , return (pureArr (T n n 0))
    ]
  , ((char 'E') >>) <$>
    [ liftM4 (liftA4 E) def mflo mflo (P.try $ spaces >> parseFloat True flop)
    , liftM3 (liftA3 (\i r o -> E i r o 0)) def mflo mflo
    , liftM2 (liftA2 (\i r -> E i r n 0)) def mflo
    , liftM  (liftA  (\i -> E i n n 0)) def
    ] 
  , [ do char 'O'
         space >> spaces
         liftM2 (liftA2 O) 
                (P.try $ P.between (spacesaft $ char '(') (char ')') 
                    (spacesaft $ choice (P.try <$> parseP_ False intp flop poip pp psp))) $
                fmap ((>>^ join) . sequenceArr) $ (spaces >>) $ parselist $ 
                  P.try $ liftM2 (liftA2 (fmap . (,))) 
                         (spacesaft $ parseFloat True flop) $ spacesaft $ P.choice
                          [ P.between (spacesaft (char '(')) (char ')')
                              (spacesaft $ parsePs intp flop poip pp psp) 
                          , mapcat pure (parseP intp flop poip pp psp)
                          , parseSpecialPs intp flop poip pp psp
                          ] 
    ]
  , [ do char 'R'
         many1 space
         liftM3 (liftA3 R) 
                  (spaces >> parseFloat True flop)
                  (spaces >> parseFloat True flop)
                  (spaces >> 
                    (P.choice $ P.try <$> parseP_ False intp flop poip pp psp))
    ]
  , [ do char 'N'
         spaces
         liftA N <$> parsePsOrP intp flop poip pp psp
    ]
  , [ parseparen (P.choice $ P.try <$> parseP_ True intp flop poip pp psp) ]
  ] where
  yaw f = (++ f) $ do
      l <- f
      r <- f
      guard incyaw
      return $ do
        lh <- l
        spaces >> string "->" >> spaces
        rh <- r
        return (liftA2 YawAt lh rh)
  mflo = P.try $ spaces >> (mapcatm id $ parseUMaybe $
      parseFloat True flop)
  n = Nothing
  def = do spaces 
           mapcat (Left . fromIntegral) (P.try $ parseInt True intp)
            <|> pureArr . Right . BS.pack <$> (P.try parseString)

parsePs :: (Stream s m Char, Arrow a) => ParsecT s u m (a b Int) -> ParsecT s u m (a b Float) -> ParsecT s u m (a b Point) -> ParsecT s u m (a b P) -> ParsecT s u m (a b [P]) -> ParsecT s u m (a b [P])
parsePs intp flop poip pp psp = choice $ P.try <$>
  [ parseSpecialPs intp flop poip pp psp
  , ((join ^<<) . sequenceArr) <$> do
      flip sepBy2 (char ',' >> spaces) $ spacesaft $ choice
        [ P.try $ do
            mapcat pure (parseP intp flop poip pp psp)
        ,  parseSpecialPs intp flop poip pp psp
        ]
--   , sequenceArr <$> do
--       parseparen $ sepBy1 (mapcat (P . Point Nothing Nothing Nothing Nothing Nothing . Just) $ spacesaft $ parseInt True mzero) (char ',' >> spaces)
  , parseparen $ parsePs intp flop poip pp psp 
  , psp
  ]

parseSpecialPs :: (Stream s m Char, Arrow a) => ParsecT s u m (a b Int) -> ParsecT s u m (a b Float) -> ParsecT s u m (a b Point) -> ParsecT s u m (a b P) -> ParsecT s u m (a b [P]) -> ParsecT s u m (a b [P])
parseSpecialPs intp flop poip pp psp = choice
  [ do
      char 'A' >> space
      liftM5 (liftA5 (\n' r a o p -> arcP p r a o n')) 
             (spaces >> parseInt True intp) 
             (spaces >> parseFloat True flop) 
             (spaces >> parseFloat True flop)
             (spaces >> parseFloat True flop)
             (spaces >> parseP intp flop poip pp psp)
  , do
      char 'C' >> space
      liftM4 (liftA4 (\n' r o p -> circleP p r o n'))
             (spaces >> parseInt True intp) 
             (spaces >> parseFloat True flop) 
             (spaces >> parseFloat True flop)
             (spaces >> parseP intp flop poip pp psp)
  , do
      char 'E' >> space
      spaces
      objs <- between (spacesaft (char '(')) (char ')') $ parns $ flip sepBy2 (spaces >> char ',') $ 
                    do spaces
                       choice [ mapcat (Left . fromIntegral) 
                                 (parseInt True intp)
                              , pureArr . Right . BS.pack <$> 
                                 parseString
                              ]
      f <- do r <- mflo
              do th <- mflo
                 do z <- P.try $ spaces >> parseFloat True flop
                    return (liftA4 (\a b c d -> E d a b c) r th z)
                  <|> return (liftA3 (\a b c -> E c a b 0) r th)
               <|> return (liftA2 (\a b -> E b a n 0) r)
            <|> return (liftA (\a -> E a n n 0)) 
      return $ sequenceArr (f <$> objs)
--   , do
--       char 'O' >> space
--       liftM2 (liftA2 O) 
--              (spaces >> parseP intp flop poip pp psp) $
--              fmap ((>>^ join) . sequenceArr) $ parns $ 
--                P.try $ flip sepBy1 (spaces >> char ',' >> spaces) $ 
--                P.try $ liftM2 (liftA2 (\a b -> (,) a <$> b)) 
--                       (spaces >> parseFloat True flop) 
--                       (spaces >> parsePsOrP intp flop poip pp psp) 
  ] where
  mflo = P.try $ spaces >> (mapcatm id $ parseUMaybe $
      parseFloat True flop)
  n = Nothing
  parns f = P.try f <|> parseparen f

parsePsOrP :: (Stream s m Char, Arrow a) => ParsecT s u m (a b Int) -> ParsecT s u m (a b Float) -> ParsecT s u m (a b Point) -> ParsecT s u m (a b P) -> ParsecT s u m (a b [P]) -> ParsecT s u m (a b [P])
parsePsOrP intp flop poip pp psp = P.try (parsePs intp flop poip pp psp) 
                              <|> (>>^ pure) <$> parseP intp flop poip pp psp

parseparen :: Stream s m Char => ParsecT s u m a -> ParsecT s u m a
parseparen p = between (char '(' >> spaces) (char ')') (spacesaft p)
spacesaft :: Stream s m Char => ParsecT s u m a -> ParsecT s u m a
spacesaft op = op >>= \x -> spaces >> return x

parselist :: Stream s m Char => ParsecT s u m a -> ParsecT s u m [a]
parselist p = P.between (spacesaft $ char '[') (char ']') 
                  $ sepBy (spacesaft p) (spacesaft $ char ',') 

-- partial writes, doesn't override with 0's from Nothing fields
writePoint :: CPid -> Word32 -> Point -> IO Bool
writePoint pid ptr (Point x y z h e mid) = do
    mid' <- peep_ pid (fromIntegral mapid) 
    let true = maybe True (== mid') mid
    when true $ do
        -- printf "writing point to %s\n" (show p)
        mapM_ (w 0) x
        mapM_ (w 4) y
        mapM_ (w 8) z
        mapM_ (w 16) h
        mapM_ (w 20) e
    return true
    where
    w off val = when (not (isNaN val) && not (isInfinite val)) $
                    (plant_ pid $ fromIntegral $ ptr + off) val

instance S.Serialize Point where

newtype AuraObj = AuraObj
  { auo_addr  :: Word32 
  } deriving (Show)

auraCount :: AuraObj -> WoW Word8
auraCount auo = ar (auo_addr auo + 14)

data OfsObj = OfsObj
  { ofs_addr  :: Word32
  , ofs_type  :: Word32
  , ofs_guid  :: Word32
  , ofs_next  :: Word32
  } deriving (Show)

instance Eq OfsObj where
    o == o' = ofs_guid o == ofs_guid o'

readOfsObj :: Word32 -> WoW OfsObj
readOfsObj addr = do
    liftM3 (OfsObj addr) (ar $ addr + ofstype)
                         (ar $ addr + ofsguid)
                         (ar $ addr + ofsnext)

playerObj :: WoW OfsObj
playerObj = readOfsObj =<< wenv_base <$> get

objPos :: OfsObj -> WoW Point
objPos obj = ar (x + ofs_addr obj) where
  x = case ofs_type obj of
      3 -> posx
      4 -> posx
      _ -> ofsx

setObjPos :: OfsObj -> Point -> WoW Bool
setObjPos obj p
  | ofs_type obj == 5 = do
      pid <- wenv_pid <$> get
      io $ liftM2 (&&) (writePoint pid (ofs_addr obj + ofsx) p)
                       (writePoint pid (ofs_addr obj + ofsx') p)
  | otherwise = return False

objName :: OfsObj -> WoW ByteString
objName obj @ OfsObj { ofs_addr = o, ofs_guid = guid } = do
    WEnv { wenv_name = nam, wenv_guid = pguid } <- get
    case ofs_type obj of
        3 -> do
            p <- ar (ofs3namp + o)
            p' <- ar (ofs3namp2 + p) :: WoW Word32
            purge <$> ars p'
        4 -> if guid == pguid then return nam else do
            mask' <- ar (0xc5d940 + 0x24)
            base <- ar (0xc5d940 + 0x1c)
            let off = 12 * (mask' .&. ofs_guid obj)
            curr <- ar (base + off + 8)
            ars (0x20 + curr)
        5 -> do
            p <- ar (ofs5namp + o)
            p' <- ar (ofs5namp2 + p) :: WoW Word32
            purge <$> ars p'
        _ -> return ""

data ThreatStatus = NoThreat | LowThreat | HighThreat | TankingLow | Tanking
  deriving (Show, Eq)

threatToTarget :: WoW (Maybe (ThreatStatus, Word32))
threatToTarget = do
    mtar <- getTar
    forM mtar $ \tar -> do
        guid <- getGuid
        objThreatToGuid tar guid

objThreatToGuid :: OfsObj -> Uid -> WoW (ThreatStatus, Word32)
objThreatToGuid OfsObj { ofs_addr = addr } guid = do
    mask' <- ar (addr + 0xfe0 + 0x24)
    if mask' == -1 then
        return (NoThreat, 0)
    else do
        base <- ar (addr + 0xfe0 + 0x1c)
        entry <- iter (base + 12 * (mask' .&. guid)) 4
        if entry == 0 then 
            return (NoThreat, 0)
        else do
            st <- ar (entry + 0x28) :: WoW Word8
            let threatstatus = case st of
                  1 -> LowThreat
                  2 -> HighThreat
                  3 -> TankingLow
                  4 -> Tanking
                  _ -> error $ printf "objThreatToGuid: threatStatus = %d" st 
            threat <- ar (entry + 0x2c)
            return (threatstatus, threat)
    where
    iter :: Word32 -> Word32 -> WoW Word32
    iter step current = do
        next <- ar (current + step + 4)
        if next == 0 || next `mod` 2 == 1 then
            return 0
        else do
            mguid <- ar (next + 0x8)
            mguid' <- ar (next + 0x20)
            if mguid == guid && mguid' == guid then
                return next
            else
                iter step next

getPickedup :: WoW (Maybe Word32)
getPickedup = (\x -> if x == 0 then Nothing else Just x) <$> ar puitg

getMouseOver :: WoW (Maybe OfsObj)
getMouseOver = ar moverg >>= findGuid

getTar :: WoW (Maybe OfsObj)
getTar = do
    guid <- ar tarstat
    xs <- objList
    return $ L.find (\x -> ofs_guid x == guid) xs

setTar :: OfsObj -> WoW ()
setTar o = do
    let guid = ofs_guid o
    basewrite ofs4tar guid
    aw tarstat guid
    myst <- ar (ofs_addr o + ofsguid + 4) :: WoW Word32
    aw (tarstat+4) myst
    aw tarstatn guid

clearTar :: WoW ()
clearTar = do
    aw tarstat (0 :: Word32)
    aw (tarstat+4) (0 :: Word32)
    aw tarstatn (0 :: Word32)

setPlayerAsTarget :: Word32 -> WoW ()
setPlayerAsTarget g = do
    aw tarstat g
    aw (tarstat+4) (0 :: Word32)
    aw tarstatn (0 :: Word32)

objTar :: OfsObj -> WoW (Maybe OfsObj)
objTar o = findGuid =<< objTarGuid o

objTarGuid :: OfsObj -> WoW Word32
objTarGuid OfsObj { ofs_type = t, ofs_addr = o } 
  | t == 3 = ar (o + ofs3tar)
  | t == 4 = ar (o + ofs4tar)
  | otherwise = return 0

findNpcsBy :: (OfsObj -> WoW Bool) -> WoW [OfsObj]
findNpcsBy f = do
    fmap catMaybes $ forObj $ \o -> do
        if ofs_type o `L.elem` [3,4] then do
            check <- f o
            if check then
                return (Just o)
            else
                return Nothing
        else
            return Nothing 

targetByUid :: Word32 -> WoW (Maybe OfsObj)
targetByUid uid = do
    tars <- fmap catMaybes $ forObj $ \o -> do
        if ofs_type o `L.elem` [3,4,5] then do
            _uid <- objUid o
            if _uid == uid then
                return (Just o)
            else
                return Nothing
        else
            return Nothing 
    pos <- getPos
    sorted <- sortOnM (\o -> pointDist pos <$> objPos o) tars
    let obj = listToMaybe sorted
    forM obj setTar
    return obj

targetByUidAlive :: Word32 -> WoW (Maybe OfsObj)
targetByUidAlive uid = do
    tars <- findNpcsBy $ \o -> do
        liftM2 (&&) ((== uid) <$> objUid o) (((> 0) . fst) <$> objHP o)
    pos <- getPos
    sorted <- sortOnM (\o -> pointDist pos <$> objPos o) tars
    forM (listToMaybe sorted) $ \o -> do
        setTar o
        return o

tarState :: WoW (Maybe Word64)
tarState = getTar >>= mapM objState

tarPos :: WoW (Maybe Point)
tarPos = getTar >>= mapM objPos -- do
--     -- WEnv { wenv_pid = pid } <- get
--     guid <- ar tarstat
--     xs <- objList
--     case L.find (\x -> ofs_guid x == guid) xs of
--         Just ofs -> ar (posx + ofs_addr ofs) 
--         Nothing -> return emptyPoint

tarPos' :: WoW Point
tarPos' = maybe emptyPoint id <$> tarPos 

tarDead :: WoW (Maybe Bool)
tarDead = getTar >>= mapM objDead

tarHP :: WoW (Maybe (Word32, Word32))
tarHP = do
    t <- getTar
    forM t objHP 

tarDist :: WoW Float
tarDist = do
    p <- getPos
    p' <- tarPos
    return $ maybe 0 (pointDist p) p'

tarCasting :: WoW Int32
tarCasting = do
    t <- getTar
    maybe (return 0) objCasting t

objState :: OfsObj -> WoW Word64
objState o = ar (playst2 + ofs_addr o)

objDead :: OfsObj -> WoW Bool
objDead o = (/= 0) <$> (.&. 0x10100800) <$> objState o

objHP :: OfsObj -> WoW (Word32, Word32)
objHP o = liftM2 (,) (ar (hp + ofs_addr o)) (ar (mho + ofs_addr o))
  where mho = if ofs_type o == 3 then mhp3 else mhp

objCasting :: OfsObj -> WoW Int32
objCasting obj = (\i -> if i > 150000 then 0 else i) <$> 
                 ar (castn + ofs_addr obj)

objUid :: OfsObj -> WoW Word32
objUid obj = ar (off + ofs_addr obj) where
  off = case ofs_type obj of
      3 -> uid3
      6 -> uid6
      _ -> uido

findGuid :: Word32 -> WoW (Maybe OfsObj)
findGuid guid 
  | guid == 0 = return Nothing
  | otherwise = L.find ((== guid) . ofs_guid) <$> objList

findUid :: Word32 -> WoW [OfsObj]
findUid uid = do
    xs <- objList
    filterM (\a -> (== uid) <$> objUid a) xs

findObj :: Either Word32 ByteString -> WoW [OfsObj]
findObj eith = filterM f =<< objList where
  f o = case eith of
      Left uid -> (== uid) <$> objUid o
      Right nam -> (== nam) <$> objName o

mgrHead :: WoW OfsObj
mgrHead = readOfsObj =<< acr [connstat, listmgr, mgrhead]

forObj :: (OfsObj -> WoW a) -> WoW [a]
forObj f = do
    head' <- mgrHead 
    chained head' where
      chained obj = do
          if ofs_type obj > 0 && ofs_type obj < 8 then
              liftM2 (:) (f obj) (chained =<< readOfsObj (ofs_next obj))
          else
              return []

objList :: WoW [OfsObj]
objList = forObj return 

playerList :: WoW [OfsObj]
playerList = L.filter (\o -> ofs_type o == 4) <$> objList

otherPlayers :: [Word32] -> WoW [OfsObj]
otherPlayers uids = do
    ps <- playerList
    guid <- getGuid
    let guids = L.union [guid] uids
    return $ L.filter (\o -> not (ofs_guid o `L.elem` guids)) ps

missingPlayers :: [Word32] -> WoW [Word32]
missingPlayers uids = do
    gs <- fmap ofs_guid <$> playerList
    return $ uids L.\\ gs 
    
-- | returns players present around that weren't expected, as well as those 
-- expected that weren't there
diffPlayers :: [Word32] -> WoW ([OfsObj], [Word32])
diffPlayers uids = do
    ps <- playerList
    let le = L.filter (\o -> not (ofs_guid o `L.elem` uids)) ps
        ri = uids L.\\ (ofs_guid <$> ps)
    return (le,ri)

findStrings :: Word32 -> WoW [([Word32], ByteString)]
findStrings addr = do
    let offs = [0..0x4000]
    strs <- fmap L.concat $ forM offs $ \off -> do
        ptr <- ar (addr + off) 
        str <- purgeName <$> ars ptr
        if BS.null str then
            return []
        else
            return [([off, 0], str)]
    return (strs) where

spellName :: Word32 -> WoW ByteString
spellName id' = do
    first' <- ar 0xad49e0
    base <- ar 0xad49f0
    p <- ar (base + 4 * (id' - first'))
    dat <- wtfread 630 p
    if p /= 0 then
      ars (fixw dat 544)
    else
      return ""
    where
    fixw :: [Word8] -> Int -> Word32 
    fixw xs n = L.sum $ L.zipWith (*) (fromIntegral <$> L.drop n xs) ((2^) <$> [0,8 .. 24 :: Int])
    wtfread :: Word32 -> Word32 -> WoW [Word8]
    wtfread sz p = do
        -- buff <- mallocBytes 630
        v <- ar p
        (v:) <$> wtf' (p+1) v
        where
        wtf' p' v 
          | p' >= p + sz = return []
          | otherwise = do
            v' <- ar p'
            if v /= v' then
                (v' :) <$> wtf' (p' + 1) v'
            else do
                n <- ar (p' + 1) :: WoW Word8
                let vs = L.replicate (fromIntegral n) v'
                if p' + 2 < p + sz then do
                    v'' <- ar (p' + 2)
                    (\xs -> [v'] ++ vs ++ [v''] ++ xs) <$> wtf' (p' + 3) v''
                else
                    return (v' : vs)
    
-- getAuras :: WoW AuraObj 
-- getAuras = do
--     base <- wenv_base <$> get
--     maxas <- maxAuras
--       where
--     maxAuras = do
      

getAura :: Word32 -> WoW AuraObj
getAura n = do
    multiple <- baseread 0xdd0 :: WoW Word32
    if multiple == (-1) then do
        p <- baseread 0xc58
        return (AuraObj (p + 24 * n))
    else do
        base <- wenv_base <$> get
        return (AuraObj (base + 0xc50 + 24 * n))

getAuraSpellId :: Word32 -> WoW Word32
getAuraSpellId n = do
    multiple <- baseread 0xdd0 :: WoW Word32
    if multiple == (-1) then do
        p <- baseread 0xc58
        ar (p + 24 * n + 8)
    else
        baseread (24 * n + 0xc58)

getAuraSpellCount :: WoW Word32
getAuraSpellCount = do
    multiple <- baseread 0xdd0 :: WoW Word32
    if multiple == (-1) then do
        baseread 0xc54
    else
        baseread 0xdd0

getBuffs :: WoW [Word32]
getBuffs = objBuffs =<< playerObj

objBuffs :: OfsObj -> WoW [Word32]
objBuffs OfsObj { ofs_addr = 0 } = return []
objBuffs OfsObj { ofs_addr = o } = do
    am <- ar (o + bufs0)
    if am == (0x100 :: Word32) then
        return []
    else if am == 0x20 then do
        p <- ar (o + bufsp)
        at p
    else
        at (o + bufs0) where
    at add = do
        val <- ar add :: WoW Word32
        if val == 0x100 then
            return []
        else
            liftM2 (:) (ar (add + 4)) (at (add + 8))

data Cam = Cam
  { c_yaw   :: Float
  , c_pitch :: Float
  , c_dist  :: Float
  } deriving (Show, Generic)

instance S.Serialize Cam

instance Storable Cam where
    sizeOf _ = 3 * sizeOf (0 :: Float)
    alignment _ = alignment (0 :: Float)
    peek ptr = do
        dist <- peekByteOff ptr 0
        yaw <- peekByteOff ptr 4
        pitch <- peekByteOff ptr 8
        return (Cam yaw pitch dist)
    poke ptr (Cam yaw pitch dist) = do
        pokeByteOff ptr 0 dist
        pokeByteOff ptr 4 yaw
        pokeByteOff ptr 8 pitch

instance Remote Cam where
    peep p a = do dist <- peep p a
                  yaw <- peep p (a + 4)
                  pitch <- peep p (a + 8)
                  return (Cam yaw pitch dist)
    plant p a (Cam yaw pitch dist) = do
        plant p a dist
        plant p (a + 4) yaw
        plant p (a + 8) pitch
    peepUnsafe p a = do dist <- peep p a
                        yaw <- peep p (a + 4)
                        pitch <- peep p (a + 8)
                        return (Cam yaw pitch dist)
    plantUnsafe p a (Cam yaw pitch dist) = do
        plant_ p a dist
        plant_ p (a + 4) yaw
        plant_ p (a + 8) pitch

getCam :: WoW Cam
getCam = acr [camstat, camoff, camdist]

setCam :: Cam -> WoW ()
setCam = acw [camstat, camoff, camdist] 
         
setCamDist :: Float -> WoW ()
setCamDist = acw [camstat, camoff, camdist] 

setCamYaw :: Float -> WoW ()
setCamYaw = acw [camstat, camoff, camdist+4] 

setCamPitch :: Float -> WoW ()
setCamPitch = acw [camstat, camoff, camdist+8] 

rdfTuple :: WoW (Word32, Word32)
rdfTuple = liftM2 (,) (ar rdfqueue) (ar rdfpop)

rdfFree :: WoW Bool
rdfFree = (== (0,0)) <$> rdfTuple

rdfQueued :: WoW Bool
rdfQueued = (== (1,0)) <$> rdfTuple

rdfPrompted :: WoW Bool
rdfPrompted = (\(q,p) -> q == 257 && p /= 0) <$> rdfTuple

rdfMatched :: WoW Bool
rdfMatched = (== (257,0)) <$> rdfTuple

np :: WoW Int
np = Prelude.length . Prelude.filter ((== 4) . ofs_type) <$> objList

nullWEnv :: WEnv
nullWEnv = WEnv 0 0 0 "" 0

newWEnv :: CPid -> IO WEnv
newWEnv 0 = return nullWEnv
newWEnv pid = do
    base <- chainPeep_ pid [playstat, pboff1, pboff2]
    guid <- chainPeep_ pid [base + ofsguid] 
    connd <- (/= (0 :: Word32)) <$> chainPeep_ pid [connstat, listmgr]
    nam <- if base == 0 || not connd then 
        return ""
      else
        purgeName <$> peepByteString_ pid (fromIntegral playnam)
    hWnd <- peep_ pid (fromIntegral hwnd)
    return $ WEnv pid base guid nam hWnd

currentWoWX :: X (Maybe WEnv)
currentWoWX = do
    mpid <- windowPid =<< currentWindow
    case mpid of
        Just pid -> do
            iswow <- io $ isWoW pid
            if iswow then
                Just <$> (io $ newWEnv pid)
            else
                return Nothing
        Nothing -> return Nothing


currentWoW :: IO (Maybe WEnv)
currentWoW = runX currentWoWX

reload :: WoW ()
reload = do
    base <- acr [playstat, pboff1, pboff2]
    nam <- getName
    -- let nam = if BS.all isAlpha nam then nam else ""
    modify $ \w -> w { wenv_base = base, wenv_name = nam }

reloadPlaybase :: WoW Word32
reloadPlaybase = do
    wenv@WEnv { wenv_pid = pid } <- get
    base <- io $ chainPeep_ pid [playstat, pboff1, pboff2]
    put wenv { wenv_base = base }
    return base

getWowsF :: [CPid -> IO Bool] -> IO [CPid]
getWowsF fs = fmap catMaybes $ forStream "/proc/" $ \fp -> do
    fmap join $ forM (validPid fp) $ \p -> do
        is <- and' p
        if is then
            return (Just p)
        else
            return Nothing where
    and' p = tryBool $ foldM (\b op -> if b then op p else return False) True fs 

getWows :: IO [CPid]
getWows = getWowsF [isWoW] 

xWows :: IO [CPid]
xWows = getWowsF [isWoW, x] where
    x pid = do
        env <- procEnv pid
        dpy <- getEnv "DISPLAY"
        return (L.lookup "DISPLAY" env == Just dpy)

wineWows :: IO [CPid]
wineWows = getWowsF [isWoW, x] where
  x pid = do
      env <- procEnv pid
      -- print =<< fixwineproc env
      -- print =<< fixwinethis
      liftM2 (==) (fixwineproc env) fixwinethis
  fixwineproc :: [(String, String)] -> IO String
  fixwineproc env = strips <$> case L.lookup "WINEPREFIX" env of
      Nothing -> do
          let home = maybe (error "no home?") id $ L.lookup "HOME" env
          return $ home ++ "/.wine"
      Just wine -> return $ wine
  fixwinethis = strips <$> do
      wine' <- lookupEnv "WINEPREFIX"
      case wine' of
          Nothing -> do
              home <- getEnv "HOME"
              return $ home ++ "/.wine"
          Just wine -> return wine
  strips :: String -> String
  strips str = if L.last str == '/' then strips (L.init str) else str

-- 
-- data retrieval functions
-- 
getPrevPos :: WoW Point
getPrevPos = do
    wenv <- get
    if nullW wenv then
        return emptyPoint
    else do
        p <- baseread prevx
        i <- getEMapID
        return p { p_mid = Just i }

getPos :: WoW Point
getPos = do
    reloadPlaybase
    wenv <- get
    if nullW wenv then
        return emptyPoint
    else do
        p <- baseread posx
        i <- getMapID
        return p { p_mid = Just i }

getEPos :: WoW Point
getEPos = do
    wenv <- get
    if nullW wenv then
        return emptyPoint
    else do
        p <- baseread eposx
        i <- getEMapID
        return p { p_mid = Just i, p_h = Nothing, p_e = Nothing }

getMovSpeed :: WoW Float
getMovSpeed = baseread mvspeed

posUpdated :: WoW Bool
posUpdated = do
    p <- getPos
    p' <- getEPos
    return (p' == p { p_h = Nothing, p_e = Nothing })

getState :: WoW Word32
getState = do
    reloadPlaybase
    baseread playst2

honorPoints :: WoW Word32
honorPoints = baseread honor

getXP :: WoW Word32
getXP = baseread expe

getLevel :: WoW Word32
getLevel = baseread lvl

getCorpse :: WoW Point
getCorpse = do
    let fil e = if e == 0 then Nothing else Just e
    x <- ar corpx
    y <- ar corpy
    z <- ar corpz
    m <- getMapID
    let p = emptyPoint { p_x = fil x, p_y = fil y, p_z = fil z }
        p' = maybe p t3p $ M.lookup (x,y,z,map_id m) corpseMap
    return p' { p_mid = Just m } 

getName :: WoW ByteString
getName = do
    wenv <- get
    if nullW wenv then
        return ""
    else
        purge <$> ars playnam

getGuid :: WoW Word32
getGuid = baseread ofsguid

getMapID :: WoW MapID
getMapID = ar mapid

getEMapID :: WoW MapID
getEMapID = ar emapid

getZoneID :: WoW Word32
getZoneID = ar zoneid

getMapName :: WoW ByteString
getMapName = do
    bs <- purge <$> ars mapnam
    return $ BS.drop (BS.length "World\\Maps\\") bs

getMMapName :: WoW ByteString
getMMapName = do 
    addr <- ar mipoff
    purge <$> ars addr

inInstance :: WoW Bool
inInstance = do
    i <- getEMapID
    return $ not $ L.elem i [0,1,530,571]

getFarclip :: WoW Float
getFarclip = ar fclip

getClickPos :: WoW Point
getClickPos = ar clkx

isSitting :: WoW Bool
isSitting = (/= 0) <$> (baseread sitting :: WoW Word32)

isDead :: WoW Bool
isDead = (/= 0) <$> (.&. 0x10100800) <$> getState

isStunned :: WoW Bool
isStunned = (/=0) <$> (.&. 0x800) <$> getState

isCorpse :: WoW Bool
isCorpse = (/= 0) <$> (.&. 0x800) <$> getState

isAutoCorpse :: WoW Bool
isAutoCorpse = (/= 0) <$> (.&. 0x100000) <$> getState

isGhost :: WoW Bool
isGhost = (/= 0) <$> (.&. 0x10000000) <$> getState

isFalling :: WoW Bool
isFalling = (/= 0) <$> (.&. 0x3000) <$> getState

inCombat :: WoW Bool
inCombat = (/= 0) <$> (baseread combat :: WoW Word8)

castingID :: WoW Word32
castingID = baseread castn

isCasting :: WoW Bool
isCasting = (/= 0) <$> castingID

channelingID :: WoW Word32
channelingID = baseread chann

isChanneling :: WoW Bool
isChanneling = (/= 0) <$> channelingID

isLoading :: WoW Bool
isLoading = do
    (/= 0) <$> (ar loading :: WoW Word32)
--     m <- getMapID
--     em <- getEMapID
--     return (screen || m /= em)

worldLoading :: WoW Bool
worldLoading = liftM2 (/=) getMapID getEMapID

isOnline :: WoW Bool
isOnline = (/= (0 :: Word32)) <$> acr [connstat, listmgr]

inGroup :: WoW Bool
inGroup = (== 1) <$> (ar wgroup :: WoW Word8)

inRaid :: WoW Bool
inRaid = (== 2) <$> (ar wgroup :: WoW Word8)

inGroupOrRaid :: WoW Bool
inGroupOrRaid = (/= 0) <$> (ar wgroup :: WoW Word8)

partySize :: WoW Int
partySize = do
    ingroup <- inGroupOrRaid
    if ingroup then
        ar groupn
    else
        return 1

getHP :: WoW (Int32, Int32)
getHP = liftM2 (,) (baseread hp) (baseread mhp)

getMP :: WoW (Int32, Int32)
getMP = liftM2 (,) (baseread mp) (baseread mmp)

getEnergy :: WoW Int32
getEnergy = baseread ene

typing :: WoW Bool
typing = (0 /=) <$> (ar typn :: WoW Word32)

--
-- data modification functions
--
setPos :: Point -> WoW Bool
setPos p = do
    reloadPlaybase
    WEnv { wenv_pid = pid, wenv_base = base } <- get
    if (base /= 0) then do
        st <- getState 
        if (st .&. 0xf0ff == 0) then io $ do
            writePoint pid (base+posx) p
            writePoint pid (base+2004) p
        else
            return False
    else
        return False

setPosHard :: Point -> WoW Bool
setPosHard p = do
    WEnv { wenv_pid = pid } <- get
    io $ forM [ [0xADF4E4], [0xCD7778], [0xD38B0C], [0xD38B24], [0xD39454]
              , [0xCD87A8, 0x60], [0xCD87A8, 0x6C]] $ \add -> do
        ptr <- chainOffset_ pid add
        writePoint pid (ptr :: Word32) p
    setPos p

setState :: Word32 -> WoW ()
setState st = basewrite playst2 st

orState :: Word32 -> WoW ()
orState st = do
    st' <- getState
    setState (st' .|. st)

setFarclip :: Float -> WoW ()
setFarclip = aw fclip

setNearclip :: Float -> WoW ()
setNearclip = aw nclip

setMaxfps :: Word32 -> WoW ()
setMaxfps = aw maxfps

setMaxfpsBK :: Word32 -> WoW ()
setMaxfpsBK = aw maxfpsbk

setClickPos :: Point -> WoW ()
setClickPos p = void $ awp clkx p

stepOff :: Float -> Float -> WoW ()
stepOff ang n = getPos >>= \p -> sequence_ $ do
    x <- p_x p
    y <- p_y p
    r <- p_h p
    return $ setPos $ emptyPoint { p_x = 
        (Just $ x + (n * cos (r+ang))), p_y = (Just $ y + (n * sin (r+ang)))  }

moveF :: Float -> WoW ()
moveF = stepOff 0

moveB :: Float -> WoW ()
moveB = stepOff pi

moveL :: Float -> WoW ()
moveL = stepOff (pi/2)

moveR :: Float -> WoW ()
moveR = stepOff (-pi/2)

moveU :: Float -> WoW ()
moveU n = getPos >>= \p -> sequence_ $ do
    z <- p_z p
    return $ setPos emptyPoint { p_z = Just $ n + z }

moveD :: Float -> WoW ()
moveD n = getPos >>= \p -> sequence_ $ do
    z <- p_z p
    return $ setPos emptyPoint { p_z = Just $ z - n }

rot :: Float -> WoW ()
rot th = do
    Point { p_h = mr } <- getPos
    forM_ mr $ \h -> do
        setPos $ Point Nothing Nothing Nothing (Just $ h + th) Nothing Nothing

freezeZ :: WoW ()
freezeZ = do
    st <- getState
    setState (masks 0x400 0x20f00f st)

unfreezeZ :: WoW ()
unfreezeZ = do
    st <- getState
    setState (masks 0 0x400 st)

stopFall :: WoW ()
stopFall = freezeZ >> unfreezeZ

waitFor :: WoW Bool -> Int -> WoW ()
waitFor f n = when (n>0) $ do
    liftIO $ threadDelay 5000
    r <- f
    when r (waitFor f (n-5))

waitForWorld :: WoW ()
waitForWorld = do
    zid <- ar zoneid :: WoW Word32
    waitFor ((==zid) <$> ar ezoneid) 1000
      
setObjects :: Bool -> WoW ()
setObjects = setBitAt rmflags rmf_objects

setRendering :: Bool -> WoW ()
setRendering = setBitAt rmflags rmf_render

setLighting :: Bool -> WoW ()
setLighting = setBitAt rmflags rmf_light

setTexture :: Bool -> WoW ()
setTexture = setBitAt rmflags rmf_texture

setRays :: Bool -> WoW ()
setRays = setBitAt rmflags rmf_ray

setEffects :: Bool -> WoW ()
setEffects = setBitAt roflags rof_effects

setNpcs :: Bool -> WoW ()
setNpcs = setBitAt roflags rof_npcs

resetRender :: WoW ()
resetRender = do
    setFarclip 171
    setObjects True
    setRendering True
    setLighting True
    setTexture True
    setRays False
    setEffects True
    setNpcs True

tuple2p :: (Float, Float) -> Point
tuple2p (x,y) = emptyPoint { p_x = Just x, p_y = Just y }

tuple3p :: (Float, Float, Float) -> Point
tuple3p (x,y,z) = emptyPoint { p_x = Just x, p_y = Just y, p_z = Just z }

tuple4p :: (Float, Float, Float, Word32) -> Point
tuple4p (x,y,z,m) = emptyPoint { p_x = Just x, p_y = Just y, p_z = Just z
                               , p_mid = Just (MapID m) }

p4t :: Point -> (Float, Float, Float, Word32)
p4t Point { p_x = Just x, p_y = Just y, p_z = Just z, p_mid = Just (MapID m) } =
    (x,y,z,m)
p4t p = error $ "p4t " ++ show p

-- utils

tdelay :: (MonadIO io, RealFrac f) => f -> io ()
tdelay factor = io $ threadDelay $ round (factor * 1e6)
getTimen :: MonadIO io => io Int64
getTimen = liftIO $ do
    ElapsedP (Elapsed (Seconds s)) (NanoSeconds n) <- timeCurrentP
    return (s * 1000000000 + n `quot` 1)

getTime :: MonadIO io => io Int64
getTime = liftIO $ do
    ElapsedP (Elapsed (Seconds s)) (NanoSeconds n) <- timeCurrentP
    return (s * 1000 + n `quot` 1000000)

timeToHours :: Int64 -> (Int64, Int64, Int64, Int64)
timeToHours t = (h, m, s, ms)
  where h = (t `quot` 3600000) `mod` 24
        m = (t `quot`   60000) `mod` 60
        s = (t `quot`    1000) `mod` 60
        ms = t `mod` 1000

getTimes :: MonadIO io => io Double
getTimes = (/1000) . fromIntegral <$> getTime

eTimen :: MonadIO io => io a -> io (a, Int64)
eTimen op = liftM3 (\t r t' -> (r, t' - t)) getTimen op getTimen

eTimen_ :: MonadIO io => io a -> io Int64
eTimen_ op = snd <$> eTimen op

eTimen' :: MonadIO io => io a -> io a
eTimen' op = do (x,t) <- eTimen op
                io $ print t
                return x

baseread :: Remote a => Word32 -> WoW a
baseread addr = do
    WEnv { wenv_base = base, wenv_pid = pid } <- get
    liftIO $ peep_ pid (fromIntegral $ base + addr)

basewrite :: Remote a => Word32 -> a -> WoW ()
basewrite addr val = do
    WEnv { wenv_base = base, wenv_pid = pid } <- get
    liftIO $ plant_ pid (fromIntegral $ base + addr) val

io :: MonadIO m => IO a -> m a
io = liftIO

masks :: Bits a => a -> a -> a -> a
masks added removed val = complement removed .&. (added .|. val)

peepNon0 :: (Num a, Eq a, Remote a) => CPid -> Word -> IO (Maybe a)
peepNon0 p a = (\x -> if x == 0 then Nothing else Just x) <$> peep p a

peepNon0_ :: (Num a, Eq a, Remote a) => CPid -> Word -> IO (Maybe a)
peepNon0_ p a = (\x -> if x == 0 then Nothing else Just x) <$> peep_ p a

ar :: Remote a => Word32 -> WoW a
ar addr = do
    wenv <- get
    io $ peep_ (wenv_pid wenv) (fromIntegral addr)

ars :: Word32 -> WoW ByteString
ars addr = do
    wenv <- get
    io $ peepByteString (wenv_pid wenv) (fromIntegral addr)

acr :: Remote a => [Word32] -> WoW a
acr addrs = do
    wenv <- get
    io (chainPeep_ (wenv_pid wenv) addrs)

aw :: Remote a => Word32 -> a -> WoW ()
aw addr val = do
    wenv <- get
    io $ plant_ (wenv_pid wenv) (fromIntegral addr) val

awp :: Word32 -> Point -> WoW Bool
awp addr p = do
    WEnv { wenv_pid = pid } <- get
    io $ writePoint pid addr p

acw :: Remote a => [Word32] -> a -> WoW ()
acw addrs e = do
    WEnv { wenv_pid = pid } <- get
    liftIO $ chainPlant_ pid addrs e

nullW :: WEnv -> Bool
nullW wenv = (wenv_pid wenv == 0) || (wenv_base wenv == 0)

readPoint :: String -> String -> String -> String -> String -> String -> Point
readPoint x y z h e mid = Point (readMaybe x) (readMaybe y) 
    (readMaybe z) (readMaybe h) (readMaybe e) (readMaybe mid)

forStream :: FilePath -> (FilePath -> IO a) -> IO [a]
forStream fp op = do
    dir <- openDirStream fp
    ret <- step dir 
    closeDirStream dir 
    return ret where 
    step dir = do
        paths <- readDirStream dir
        if null paths then
            return []
        else
            liftM2 (:) (op paths) (step dir)

getStreamContents :: DirStream -> IO [FilePath]
getStreamContents stream = do
    str <- readDirStream stream
    if null str then
        return []
    else
        (str:) <$> getStreamContents stream

validPid :: String -> Maybe CPid
validPid str = do
    pid <- readMaybe str
    if pid<100 then
        Nothing
    else
        return pid

tryBool :: IO Bool -> IO Bool
tryBool op = either (\_ -> False) id <$> tryIO op where

isWoW :: CPid -> IO Bool
isWoW pid = do
    let filename = "/proc/" ++ (show pid) ++ "/comm"
        names = ["Wow.exe\n"] 
    either (\_ -> False) (`L.elem` names) <$> tryIO (readFile' filename) where

procEnv :: CPid -> IO [(String, String)]
procEnv pid = do
    let filename = "/proc/" ++ (show pid) ++ "/environ"
    env <- readFile' filename
    return $ uneq <$> unsep env where
    uneq str = case L.break (== '=') str of
        (l, '=' : r) -> (l,r)
        x -> x
    unsep str = case L.break (== '\0') str of
        (l,_:r) -> l : unsep r
        _ -> []

readFile' :: FilePath -> IO String
readFile' fp = do
    h <- openFile fp ReadMode
    contents <- (\s -> L.length s `seq` return s) =<< hGetContents h
    hClose h
    return contents

pointDist :: Point -> Point -> Float
pointDist (Point x1 y1 z1 _ _ m1) (Point x2 y2 z2 _ _ m2) = 
    if cmpm m1 m2 then 
        sqrt $ (cmpf x1 x2) + (cmpf y1 y2) + (cmpf z1 z2)
    else
        100000
    where 
        cmpf m m' = maybe 0 (^(2 :: Int)) $ do
            f1 <- m
            f2 <- m'
            return (f1 - f2)
        cmpm m m' = maybe True id $ do
            i1 <- m
            i2 <- m'
            return (i1 == i2)

xyDist :: Point -> Point -> Float
xyDist p p' = pointDist p p' { p_z = Nothing }

outterPoint :: Point -> [(Float, Point)] -> Point
outterPoint p ps = maybe emptyPoint id $ listToMaybe $ outterPoints p ps 

outterPoints :: Point -> [(Float, Point)] -> [Point]
outterPoints p _ps = (\p' -> p' { p_z = p_z p }) <$> conts 
    where
    conts = 
        let (co, noco) = L.partition (\(r, p') -> xyDist p p' < r) ps
            outter = case co of
                [(r,c)] ->
                  let o = transformP 
                            c { p_h = Nothing } r (maybe 0 id $ lookAtP c p) 0
                  in if L.any (\(r',p') -> xyDist o p' < r'-1e-4) ps then [] else [o]
                _ -> []
        in case co ++ (chained noco `foldMap` co) of
            [] -> [p]
            [_] -> outter
            ps' -> L.sortOn (xyDist p) $ outter ++ circlesInt (L.nub ps')
    chained ps' (r', p') =
        let (co, noco) = L.partition (\(r, p'') -> xyDist p'' p' < r' + r) ps'
        in co ++ (chained noco `foldMap` co)
    ps = L.filter (\p' -> filt (p' `L.delete` _ps) p') $ L.nub _ps
    filt ps' (r,p') = not $ L.any (\(r', p'') -> xyDist p' p'' + r < r') ps'

circlesInt :: [(Float, Point)] -> [Point]
circlesInt ps = L.filter (not . contained) ints
    where
    ints = do
        [p, p'] <- pairs
        (a, b) <- maybeToList (circlesInt2 p p')
        [a, b]
    pairs = L.filter ((== 2) . L.length) $ L.subsequences ps
    contained p = (\(r,p') -> xyDist p p' < r - 1e-4) `L.any` 
                  (L.filter (\(_,p') -> p' /= p) ps )

circlesInt2 :: (Float, Point) -> (Float, Point) -> Maybe (Point, Point)
circlesInt2 (r, p) (r', p') = do
    a <- lookAtP p p'
    return (transformP p'' r (a + b) 0, transformP p'' r (a - b) 0) where
    d = xyDist p p'
    b = acos ((d*d + r*r - r'*r') / (2 * r * d))
    p'' = p { p_h = Nothing }

pPoint :: P -> WoW Point
pPoint p = maybe emptyPoint id . listToMaybe 
           <$> pPoints p

pPoints :: P -> WoW [Point]
pPoints (P p) = return [p]
pPoints (T mrad moff z) = tarPos' >>= \t -> case (mrad, moff) of 
    (Nothing, _) -> do
        pos <- getPos
        return [emptyPoint { p_h = lookAtP pos t }]
    (Just rad, Just off) -> return [transformP t rad off z]
    (Just rad, Nothing) -> do
        pos <- getPos 
        return [transformP t {p_h = Nothing} rad 
                  (maybe 0 (+pi) $ lookAtP pos t) z]
pPoints (E eith mrad moff z) = do
    pos <- getPos
    ps <- sortOnM (fmap (pointDist pos) . objPos) =<< findObj eith 
    forM ps $ \o ->  objPos o >>= \opos -> return $ case (mrad, moff) of
      (Nothing, _) -> pos { p_h = lookAtP pos opos }
      (Just rad, Just off) -> transformP opos rad off z
      (Just rad, Nothing) -> transformP opos {p_h = Nothing} 
                  rad (maybe 0 (+pi) $ lookAtP pos opos) z
pPoints (O orig ps) = do
    p' <- pPoint orig
    p'' <- if p' == emptyPoint then getPos else return p'
    ps' <- fmap join $ forM ps $ \(r, p) -> do
        ps' <- pPoints p
        return $ (,) r <$> L.filter (/= emptyPoint) ps'
    return [ (outterPoint p'' ps') ]
pPoints (R r r' p) = do
    po <- pPoint p
    rad <- io $ randomRIO (r,r')
    a <- io $ randomRIO (0,2*pi)
    return [(transformP po rad a 0) { p_h = Nothing } ]
pPoints (N ps) = do
    points <- mconcat <$> mapM pPoints ps
    pos <- getPos
    return $ L.sortOn (pointDist pos) points
pPoints (YawAt orig dest) = do
    ps <- if orig == P emptyPoint then pure <$> getPos else pPoints orig
    forM ps $ \p -> do
        ps' <- pPoints dest
        return $ case L.uncons $ L.sortOn (pointDist p) ps' of
            Nothing -> p
            Just (p',_) -> p { p_h = lookAtP p p' }

transformP :: Point -> Float -> Float -> Float -> Point
transformP p rad off z = p
  { p_x = (\x -> x + rad * cos th) <$> p_x p
  , p_y = (\y -> y + rad * sin th) <$> p_y p
  , p_z = (+ z) <$> p_z p
  , p_h = Just (th + pi)
  } where
    th = maybe off (+ off) (p_h p)

angleDiff :: Float -> Float -> Float
angleDiff th th' = (th' - th + pi) `mod'` (2*pi) - pi

lookAtP :: Point -> Point -> Maybe Float
lookAtP p p' = do
    x <- p_x p
    y <- p_y p
    x' <- p_x p'
    y' <- p_y p'
    let ox = x' - x
        oy = y' - y
    return $ 
      if ox == 0 then 
        if oy < 0 then 
          -pi/2 
        else 
          pi/2
      else if ox > 0 then
        atan (oy/ox)
      else
        pi + atan (oy/ox)

arcP :: P -> Float -> Float -> Float -> Int -> [P]
arcP p radius amp offset n | n < 2 = pure $ L.head $ arcP p radius amp offset 2
arcP (O center rads)  radius amp offset n = do
    p <- arcP center radius amp offset n
    return (O p rads)
arcP (R r r' center)  radius amp offset n = do
    p <- arcP center radius amp offset n
    return (R r r' p)
arcP (N cs)  radius amp offset n = do
    center <- cs
    return (N $ arcP center radius amp offset n)
arcP (YawAt center obj)  radius amp offset n = do
    p <- arcP center radius amp offset n
    return (YawAt p obj)
arcP p radius amp offset n = do
    i <- [0 .. (n-1)]
    let psi = amp * fromIntegral i / fromIntegral (n-1)
        a +~ b = maybe a (+a) b
    return $ case p of
        P center -> P center
          { p_x = (\x -> x + radius * cos (psi + offset)) <$> p_x center
          , p_y = (\y -> y + radius * sin (psi + offset)) <$> p_y center
          , p_h = 
            if p_x center == Nothing || p_y center == Nothing then
              Nothing
            else
              Just (psi + offset + pi)
          }
        T mrad moff z -> 
            T (Just $ radius +~ mrad) (Just $ offset +~ moff + psi) z
        E id' mrad moff z -> 
            E id' (Just $ radius +~ mrad) (Just $ offset + psi +~ moff) z
        _ -> error "arcP not implemented"

circleP :: P -> Float -> Float -> Int -> [P]
circleP center radius off n = L.take n $ arcP center radius (2 * pi) off (n+1)

memop :: (Num t, Remote t, Num a, Remote a) => Word32 -> (t -> a) -> WoW ()
memop addr op = do
    val' <- ar addr
    aw addr (op val')

browseStr :: Word32 -> Word32 -> WoW [(Word32, Word32, ByteString)]
browseStr add width = f <$> liftM2 (++) (mapM memr adds) (mapM read' adds) where
  adds = [add, add+4 .. add + width - 4]
  memr add' = (,,) (add' - add) 0 <$> purgeName <$> ars add'
  read' add' = do
      add'' <- ar add'
      (,,) (add' - add) (add'' - add') <$> purgeName <$> ars add''
  f = L.filter (\(_,_,s) -> not $ BS.null s)

type Addr = (,) Word32
type Addrs a = [Addr a]

browse :: (Num a, Remote a) => Word32 -> Word32 -> WoW (Addrs a) -- [(Word32, a)]
browse add width = memr `mapM` [add, add+4.. add + width - 4] where
  memr add' = (,) (add' - add) <$> ar add'

basebrowse :: (Num a, Remote a) => WoW (Addrs a)
basebrowse = do b <- wenv_base <$> get
                browse b 0x2314

objbrowsea :: OfsObj -> WoW (Addrs Word32)
objbrowsea = objbrowse

objbrowse :: (Num a, Remote a) => OfsObj -> WoW (Addrs a)
objbrowse o = browse (ofs_addr o) $ case ofs_type o of
    1 -> 0x5a8
    2 -> 0xb88
    3 -> 0x1450
    4 -> 0x2314
    5 -> 0x27c
    6 -> 0x358
    _ -> error $ "objbrowse: ?? " ++ show o
  
filtaddrs :: (a -> Bool) -> Addrs a -> Addrs a
filtaddrs f = L.filter (f . snd)

diffaddrs :: Eq a => [(Word32, a)] -> [(Word32, a)] -> [(Word32, a)]
diffaddrs = (L.\\)

sameaddrs :: Eq a => [(Word32, a)] -> [(Word32, a)] -> [(Word32, a)]
sameaddrs = L.intersect

maskaddrs :: [Word32] -> [(Word32, a)] -> [(Word32, a)]
maskaddrs addrs pairs = L.filter ((`L.elem` addrs) . fst) pairs

basesearch :: Word32 -> WoW [(Word32, Word32)]
basesearch val = do
    L.filter ((== val) . snd) <$> mapM (\a -> (,) a <$> baseread a) [0,4..0xffff]
    

setBitAt :: Word32 -> Word32 -> Bool -> WoW ()
setBitAt addr bitn bool = do
    let op = (if bool then (.|.) else (.&.) . complement) bitn
    memop addr op

purge :: ByteString -> ByteString
purge str = if BS.all isAscii str then str else ""

purgeName :: ByteString -> ByteString
purgeName str = if BS.length str > 2 
                && BS.all isPrint str 
                then str else ""

try' :: a -> (Either String a)
try' a = first (displayException :: SomeException -> String)
    $ unsafePerformIO $ try $ evaluate a

tryIO :: IO a -> IO (Either IOException a)
tryIO = try

interleaveWoW :: WoW a -> WoW a
interleaveWoW op = get >>= io . unsafeInterleaveIO . evalStateT op

t3p :: (Float, Float, Float) -> Point
t3p (x,y,z) = Point (Just x) (Just y) (Just z) Nothing Nothing Nothing

checkChanged :: (Show a, Eq a) => IO a -> IO ()
checkChanged = checkChangedn 5000

checkChangedn :: (Show a, Eq a) => Int -> IO a -> IO ()
checkChangedn n f = do
    es <- f
    tid <- forkIO (step es)
    BS.getLine
    killThread tid where
    step e = do
        e' <- f
        when (e /= e') (print e')
        threadDelay n
        step e'

checkChangedL :: (Show a, Eq a) => IO [a] -> IO ()
checkChangedL f = do
    es <- f
    tid <- forkIO (step es)
    BS.getLine
    killThread tid where
    step e = do
        e' <- f
        when (e /= e') (print $ e' L.\\ e)
        threadDelay 5000
        step e'

testParse :: Show t => Parsec String () (() -> t) -> String -> (String,String)
testParse p s = either ((,) "err" . show) (first $ show . ($ ())) $ runParser ((,) <$> p <*> getInput) () "" s

testParse' :: Parsec String () (() -> t) -> String -> t
testParse' p s = either (error . show) ($ ())  $ runParser (p >>= \r -> eof >> return r) () "" s

parseInt :: (Arrow a, Stream s m Char, Integral i) 
              => Bool -> ParsecT s u m (a b i) -> ParsecT s u m (a b i)
parseInt expSub f = addsub where
  addsub = liftM2 (L.foldr apply) prod $ many $ do
          do (                  (P.try $ spaces >> char '+' >> mapcat       (+)  (spaces >> prod))
              <|> (P.try $ unless expSub spaces >> char '-' >> mapcat (flip (-)) (spaces >> prod)))
  prod = liftM2 (L.foldr apply) value $ many $ P.try $ do
          spaces 
          do (        char '*' >> mapcat        (*)  (spaces >> value))
              <|> (string "//" >> mapcat (flip quot) (spaces >> value))
  value = choice
        [ parseparen (parseInt False f)
        , f
        , pureArr <$> int
        ]

parseFloat :: (Arrow a, Stream s m Char, Real f, Floating f) 
              => Bool -> ParsecT s u m (a b f) 
              -> ParsecT s u m (a b f)
parseFloat expSub fun = either (>>> (arr (fromIntegral :: (Real f, Floating f) => Int -> f))) 
                               id 
                               <$> parseNum expSub (Right <$> fun)

parseNum :: (Arrow a, Stream s m Char, Real i, Integral i, Real f, Floating f) 
              => Bool -> ParsecT s u m (Either (a b i) (a b f))
              -> ParsecT s u m (Either (a b i) (a b f))
parseNum expSub f = addsub where
  addsub = liftM2 (L.foldr ($)) prod $ many $ do
          do (                  (P.try $ spaces >> char '+' >> op (+)        <$> (spaces >> prod)) 
              <|> (P.try $ unless expSub spaces >> char '-' >> op (flip (-)) <$> (spaces >> prod)))
  prod = liftM2 (L.foldr ($)) value $ many $ P.try $ do
          spaces 
          do (     char '*' >> op (*)    <$> (spaces >> value)) 
              <|> (char '/' >> flip divi <$> (spaces >> value))
              <|> (char '%' >> op (flip mod')   <$> (spaces >> value)) 
  value = choice
        [ parseparen (parseNum False f)
        , f
        , choice $ fmap (bimap pureArr pureArr) <$>
          [ string "pi" >> return (Right pi)
          , natFloat
          , char '-' >> bimap negate negate <$> 
                    choice [ natFloat, string "pi" >> return (Right pi) ]
          ]
        ]
  op :: (Arrow a, Real i, Real f, Integral i, Floating f) => (forall n. Real n => n -> n -> n) -> Either (a b i) (a b f) -> Either (a b i) (a b f) -> Either (a b i) (a b f)
  op fun (Left i) (Left i') = Left (i &&& i' >>> arr (uncurry fun))
  op fun x y = Right (tof x &&& tof y >>> arr (uncurry fun))
  divi x y =                Right (tof x &&& tof y >>> arr (uncurry (/)))
  tof :: (Arrow a, Integral i, Floating f) => Either (a b i) (a b f) -> a b f
  tof = either (>>> arr fromIntegral) id

parseNum' :: (Arrow a, Stream s m Char, Real i, Integral i, Real f, Floating f) 
              => Bool -> ParsecT s u m (a b (Either i f))
              -> ParsecT s u m (a b (Either i f))
parseNum' expSub f = addsub where
  addsub = liftM2 (L.foldr apply) prod $ many $ do
          do (                  (P.try $ spaces >> char '+' >> mapcat        add  (spaces >> prod))
              <|> (P.try $ unless expSub spaces >> char '-' >> mapcat (flip subt) (spaces >> prod)))
  prod = liftM2 (L.foldr apply) value $ many $ P.try $ do
          spaces 
          do (     char '*' >> mapcat       mult  (spaces >> value))
              <|> (char '/' >> mapcat (flip divi) (spaces >> value))
              <|> (char '%' >> mapcat (flip modu)  (spaces >> value))
  value = choice $ P.try <$>
        [ parseparen (parseNum' False f)
        , f
        , pureArr <$> choice
          [ string "pi" >> return (Right pi)
          , natFloat
          , char '-' >> bimap negate negate <$> 
                    choice [ natFloat, string "pi" >> return (Right pi) ]
          ]
        ]
  add (Left i) (Left i') =   Left $ i     + i'
  add x y =                 Right $ tof x + tof y
  subt (Left i) (Left i') =  Left $ i     - i'
  subt x y =                Right $ tof x - tof y
  mult (Left i) (Left i') =  Left $ i     * i'
  mult x y =                Right $ tof x * tof y
  divi x y =                Right $ tof x / tof y
  modu (Left i) (Left i') =  Left $ i `mod` i'
  modu x y =                Right $ tof x `mod'` tof y
  tof = either fromIntegral id
mapcat :: (Arrow a, Functor f) => (c -> d) -> f (a b c) -> f (a b d)
mapcat f = fmap (>>^ f)
apply :: Arrow a => a b (c -> d) -> a b c -> a b d
apply f g = f &&& g >>^ uncurry ($)
mapcatm :: (Arrow a, Functor f) => (c -> d) -> f (Maybe (a b c)) -> f (a b (Maybe d))
mapcatm f = fmap (maybe (pureArr Nothing) (>>> arr (Just . f)) )

parseReads :: Parsec String () a -> String -> [(a, String)]
parseReads parser = either (\_ -> []) pure . parse (liftM2 (,) (spaces >> parser) getInput) "ReadS" 

parseMaybe :: ParsecT s u m a -> ParsecT s u m (Maybe a)
parseMaybe p = Just <$> P.try p <|> return Nothing

parseUMaybe :: Stream s m Char => ParsecT s u m a -> ParsecT s u m (Maybe a)
parseUMaybe p = Just <$> P.try p <|> (char '_' >> return Nothing)

parseIdent :: Stream s m Char => ParsecT s u m String
parseIdent = do
    c <- letter <|> char '_'
    b <- many (alphaNum <|> char '_')
    f <- option "" (many (char '\''))
    return (c : (b ++ f))

parseString :: Stream s m Char => ParsecT s u m String
parseString = do
    char '"'
    flip manyTill (char '"') $ do
        c <- anyChar
        if c == '\\' then
            anyChar
        else
            return c

trim :: String -> String
trim = L.reverse . tr . L.reverse . tr where
  tr = L.dropWhile (flip L.elem [' ', '\t'])

pt :: (MonadIO m, Show a) => a -> m ()
pt e = io $ do
    print e
    threadDelay 500000

fromJust' :: String -> Maybe a -> a
fromJust' name = maybe (error $ "fromJust' " ++ name) id

sortOnM :: (Monad m, Ord b) => (a -> m b) -> [a] -> m [a]
sortOnM f xs = fmap fst <$> L.sortOn snd <$> (\a -> (,) a <$> f a) `mapM` xs

sequenceP :: Traversable t => t (IO a) -> IO (t a)
sequenceP ops = do
    mvars <- forM ops $ \op -> do
        mvar <- newEmptyMVar
        forkIO $ do
            putMVar mvar =<< op
        return mvar
    forM mvars takeMVar

forP :: Traversable t => t a -> (a -> IO b) -> IO (t b)
forP xs op = do
    mvars <- forM xs $  \x -> do
        mvar <- newEmptyMVar
        forkIO $ do
            putMVar mvar =<< op x
        return mvar
    forM mvars takeMVar

forP_ :: Traversable t => t a -> (a -> IO b) -> IO ()
forP_ xs op = void $ forP xs op

sequenceS :: (Traversable t) => t (StateT s IO ()) -> StateT s IO (t s)
sequenceS ops = do
    st <- get
    io $ sequenceP (flip execStateT st <$> ops)

sequenceST :: (Traversable t) => t (ST.StateT s IO ()) -> ST.StateT s IO (t s)
sequenceST ops = do
    st <- get
    io $ sequenceP (flip ST.execStateT st <$> ops)

findots :: Word32 -> WoW ()
findots off = do
    wenv <- get
    vals <- mapM baseread range 
    io $ do
        printf "0x%x\n" (wenv_base wenv)
        mapM_ (\(a,v) -> printf "%-20s" $ f a v) (L.zip range vals) 
        printf "\n--------\n"
    where
    range = (+off) <$> [bufs0,bufs0+4..bufs0+0x3fc]
    f :: Word32 -> Word32 -> String
    f a v =  
        if v < 100000 && v > 1000 then 
            printf "%4X %14d" a v 
        else if v <= 1000 then
            printf "%4X %-8x %5d" a v v
        else
            printf "%4X %-10x" a v

dots' :: WoW [(Word32, Word32)]
dots' = do
    base <- wenv_base <$> get
    browse (base + bufs0) 0x200 

colprint :: Addrs Word32 -> IO ()
colprint = colprint_

colprint_ :: (Num a, Ord a, PrintfArg a) => Addrs a -> IO ()
colprint_ xs = do
    io $ do
        forM_ xs $ \(a,v) -> do
            let s = f a v 
            Prelude.putStr $ s -- ++ L.replicate (20 - L.length (L.filter isPrint s)) ' '
        printf "\n--------\n"
    where
    f :: (Num a, Ord a, PrintfArg a) => Word32 -> a -> String
    f a v =  
        if v < 100000 && v > 1000 then 
            printf "\ESC[1;37m\STX%4X\ESC[m\STX %14d " a v 
        else if v <= 1000 then
            printf "\ESC[1;37m\STX%4X\ESC[m\STX %-8x %5d " a v v
        else
            printf "\ESC[1;37m\STX%4X\ESC[m\STX %-10x     " a v

colprintf :: [(Word32, Float)] -> IO ()
colprintf xs = do
    io $ do
        mapM_ (\(a,v) -> printf "%-20s" $ f a v) xs
        printf "\n--------\n"
    where
    -- range = [bufs0,bufs0+4..bufs0+0x3fc]
    f :: Word32 -> Float -> String
    f a v 
      | v < 1e9 = printf " %04X %13.3f" a v
      | otherwise = printf " %04X  %e" a v

-- empirically as of 3.3.5a:
-- 1: 1448 (0x5a8)
-- 2: 2952 (0xb88)
-- 3: 5200 (0x1450)
-- 4: 8980 (0x2314) ?
-- 5: 636  (0x27c)
-- 6: 856  (0x358) ?
estimatePtrSizeOn :: (Num b, Ord b) => [a] -> (a -> b) -> [(a, b)]
estimatePtrSizeOn addrs f = do
    (x, xs) <- maybeToList $ L.uncons (L.sortOn f addrs)
    snd $ L.mapAccumL (\a b -> (b, (b, f b - f a))) x xs

enumBuffs :: IO [CPid] -> IO ()
enumBuffs f = do
    wows <- f
    buffs <- L.concat <$> forM wows (flip runWoW getBuffs)
    let cumul = (\xs -> (L.length xs, L.head xs)) <$> L.group (L.sort buffs)
        sz = L.length (show (Prelude.maximum (fst <$> cumul)))
    forM_ (L.sortOn (negate . fst) cumul) $ \(n, b) -> printf "%*d %d\n" sz n b
    
splitStr :: Char -> String -> [String]
splitStr del word = case L.dropWhile (== del) word of
                        "" -> []
                        word' -> w : splitStr del word''
                                 where (w, word'') = L.break (== del) word'

liftM6 :: Monad m => (a -> b -> c -> d -> e -> f -> g) -> m a -> m b -> m c 
    -> m d -> m e -> m f -> m g
liftM6 fun a b c d e = liftM2 ($) (liftM5 fun a b c d e)

liftA :: Arrow a => (c -> d) -> a b c -> a b d
liftA = (^<<)

liftA2 :: Arrow a => (c -> d -> e) -> a b c -> a b d -> a b e
liftA2 f a1 a2 = a1 &&& a2 >>^ uncurry f

liftA3 :: Arrow a => (c -> d -> e -> f) -> a b c -> a b d -> a b e -> a b f
liftA3 f a1 a2 = liftA2 ($) $ (liftA2 f a1 a2) 

liftA4 :: Arrow a => (c -> d -> e -> f -> g) -> a b c -> a b d -> a b e -> a b f -> a b g
liftA4 f a1 a2 a3 = liftA2 ($) $ liftA3 f a1 a2 a3

liftA5 :: Arrow a => (c -> d -> e -> f -> g -> h) -> a b c -> a b d -> a b e -> a b f -> a b g -> a b h
liftA5 f a1 a2 a3 a4 = liftA2 ($) $ liftA4 f a1 a2 a3 a4

liftA6 :: Arrow a => (c -> d -> e -> f -> g -> h -> i) -> a b c -> a b d -> a b e -> a b f -> a b g -> a b h -> a b i
liftA6 f a1 a2 a3 a4 a5 = liftA2 ($) $ liftA5 f a1 a2 a3 a4 a5

sequenceArr :: (Arrow a, Foldable t) => t (a b c) -> a b [c]
sequenceArr = L.foldr (liftA2 (:)) (pureArr [])

pureArr :: Arrow a => c -> a b c
pureArr = arr . const

sepBy2 :: Stream s m t => ParsecT s u m a -> ParsecT s u m sep -> ParsecT s u m [a]
sepBy2 p sep = liftM2 (:) p (sep >> sepBy1 p sep)

string' :: Stream s m Char => String -> ParsecT s u m String
string' = mapM (\o -> char (toLower o) <|> char (toUpper o))
