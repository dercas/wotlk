{-# LANGUAGE  FlexibleInstances
            , MultiParamTypeClasses
            , GeneralizedNewtypeDeriving 
            , StandaloneDeriving
            #-}

module MCurses 
  ( module MCurses 
  , module Control.Monad.State
  , Attribute (..)
  , Color (..)
  , runCurses
  ) where

import Control.Applicative
import Control.Concurrent
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Reader

import Data.Bifunctor
import Data.Char
import Data.Either
import Data.Function (on)
import Data.Int (Int64)
import Data.IORef
import qualified Data.List as L
import Data.Maybe
import Data.Ratio

import Numeric

import Text.Printf (printf)

import UI.NCurses
import UI.NCurses.Menu (Menu)
import qualified UI.NCurses.Menu as M

import qualified Data.Map as Ma

import WoW.Wotlk.Core (getTime, trim)

data Panel s = Panel
  { p_name    :: String
  , p_win     :: Window
  , on_leave  :: StateW s ()
  , on_enter  :: StateW s ()
  , on_event  :: Event -> StateW s ()
  , wait_ev   :: StateW s Event
  , destroy   :: StateW s ()
  } 

instance Eq (Panel s) where
    (==) = (==) `on` p_name

instance Show (Panel s) where
    show = p_name

data MEnv s = MEnv 
  { menv_cols     :: Ma.Map (Color, Color) ColorID
  , menv_ps       :: Ma.Map String (Panel s)
  , menv_zorder   :: [Panel s]
  , menv_backops  :: [(Int64, Int64, StateW s ())]
  , menv_locked   :: Bool
  } deriving Show

emptyMEnv :: MEnv s
emptyMEnv = MEnv mempty mempty [] [] False

newtype StateW s a = StateW { unStateW :: StateT (MEnv s, s) Curses a }
    deriving (Functor, Applicative, Monad, MonadIO, Alternative, MonadPlus)

instance Show (StateW s a) where
    show _ = "State W * ?"

get' :: StateW s (MEnv s)
get' = fst <$> StateW get

put' :: MEnv s -> StateW s ()
put' s = do
    ~(_, s') <- StateW get
    StateW $ put (s, s')

modify'' :: (MEnv s -> MEnv s) -> StateW s ()
modify'' f = get' >>= put' . f

lift' :: Curses a -> StateW s a
lift' = StateW . lift

port :: s -> StateW s a -> StateW t (a, s)
port env op = do
    me <- get'
    (ret, st, cols) <- lift' $ do
        ((ret, cols), st) <- flip runStateW env $ do
            modify'' $ \m -> m { menv_cols = menv_cols me }
            ret <- op
            reset
            cols <- menv_cols <$> get'
            return (ret, cols)
        return (ret, st, cols)
    put' me { menv_cols = cols }
    return (ret, st)

instance MonadState s (StateW s) where
    get = snd <$> StateW get
    put s = do
        ~(s',_) <- StateW get
        StateW $ put (s',s)

runStateW :: StateW s a -> s -> Curses (a, s)
runStateW op s = fmap snd <$> runStateT (unStateW (initCols >> op)) (emptyMEnv, s)

evalStateW :: StateW s a -> s -> Curses a
evalStateW op s = fst <$> runStateW op s

getPanel :: String -> StateW s (Panel s)
getPanel str = do
    MEnv { menv_ps = ps } <- get'
    maybe (fail $ printf "%s: No such panel\n" str) return $ Ma.lookup str ps

on_enter' :: String -> StateW s ()
on_enter' = (>>= on_enter) . getPanel

on_leave' :: String -> StateW s ()
on_leave' = (>>= on_leave) . getPanel 

on_event' :: String -> Event -> StateW s ()
on_event' name ev = getPanel name >>= flip on_event ev

regPanel :: Panel s -> StateW s ()
regPanel p = do
    menv @ MEnv { menv_ps = ps, menv_zorder = zorder } <- get'
    put' menv { menv_ps = ps `mappend` Ma.fromList [(p_name p, p)] }
    -- selPanel p

regOp :: Int64 -> StateW s () -> StateW s ()
regOp delay op = modify'' $ \menv @ MEnv { menv_backops = ops } 
                 -> menv { menv_backops = (delay, 0, op) : ops }

-- init all possible color combinations, called on runStateW
initCols :: StateW s ()
initCols = do
    menv <- get'
    when (Ma.null (menv_cols menv)) $ do
        let base = [ ColorBlack, ColorRed, ColorGreen, ColorYellow, ColorBlue
                   , ColorCyan, ColorWhite, ColorDefault ]
            cols = [(colorPair (c1, c2), (c1, c2)) | c1 <- base, c2 <- base]
        map <- lift' $ forM cols $ \(n, (c1,c2)) -> do
            id <- unsafeNewColorID c1 c2 n
            return ((c1,c2), id)
        put' menv { menv_cols = Ma.fromList map }

  
getEv :: Window -> StateW s Event
getEv win = do
    menv @ MEnv { menv_backops = ops } <- get'
    t <- getTime
    ops' <- forM ops $ \(delay, time, op) -> do
        if time < t then do
            when (time /= 0) op
            return $ Left (delay, t, op)
        else
            return $ Right (delay, time, op)
    put' menv { menv_backops = either id id <$> ops' }
    mapM_ (\(_,_,op) -> op) $ lefts ops'
    let get_ev = do
        eith <- lift' $ tryCurses $ getEvent win (Just 50)
        either (pure get_ev) return eith
    ev <- get_ev
    case ev of
        Nothing -> do
            getEv win
        Just EventResized -> do
            lift' $ clearBackground
            let zorder = L.reverse $ menv_zorder menv
            forM_ (init zorder) $ \p -> do
                lift' $ updateWindow (p_win p) clear
                on_leave p
            unless (L.null zorder) $ ($ L.last zorder) $ \p -> do
                lift' $ updateWindow (p_win p) clear
                on_enter p
            getEv win
        Just ev' -> return ev'

selPanel' :: String -> StateW s ()
selPanel' name = do
    menv <- get'
    maybe (return ()) selPanel
          $ Ma.lookup name (menv_ps menv)

selPanel :: Panel s -> StateW s ()
selPanel p = do
    menv @ MEnv { menv_zorder = zorder @ order } <- get'
    when (L.null order || head order /= p) $ do
        put' menv { menv_zorder = popL p zorder }
        unless (L.null order || head order == p) $ 
            on_leave (head order)
        on_enter p

atopPanel' :: String -> StateW s ()
atopPanel' name = modify'' $ \menv @ MEnv { menv_zorder = zorder } -> 
                menv { menv_zorder = popP' name zorder }
                

atopPanel :: Panel s -> StateW s ()
atopPanel = atopPanel' . p_name

minPanel' :: String -> StateW s ()
minPanel' name = do
    menv @ MEnv { menv_zorder = zorder } <- get'
    put' menv { menv_zorder = L.filter (\p -> p_name p /= name) zorder }
    when (fmap p_name (listToMaybe zorder) == Just name) $ case zorder of
          (p' : p'' : _) -> do
              on_leave p'
              on_enter p''
          (p' : _) -> do
              on_leave p'
          _ -> nop

minPanel :: Panel s -> StateW s ()
minPanel = minPanel' . p_name

topPanel :: StateW s (Maybe (Panel s))
topPanel = listToMaybe . menv_zorder <$> get'

rend' :: String -> StateW s ()
rend' name = do
    p <- topPanel
    if Just name == (p_name <$> p) then void $ 
        p `forM` on_enter
    else
        on_leave' name

rend :: Panel s -> StateW s ()
rend = rend' . p_name

destroy' :: String -> StateW s ()
destroy' str = destroy =<< getPanel str

reset :: StateW s ()
reset = do
    me <- get'
    forM_ (menv_ps me) destroy
    put' me { menv_ps = mempty, menv_zorder = [] }

lock :: Bool -> StateW s ()
lock b = modify'' $ \m -> m { menv_locked = b }

-- returns True when the click falls outside the current Panel
focusClick :: Event -> StateW s Bool
focusClick (EventMouse _ (MouseState { mouseCoordinates = (x,y,_) })) = do
    MEnv { menv_zorder = zorder } <- get'
    fold True zorder where
        fold _ [] = return False
        fold bool (p : ps) = do
            m <- get'
            (py, px, py', px') <- lift' $ updateWindow (p_win p) $ do
                (y,x) <- windowPosition
                (h,w) <- windowSize
                return (y, x, y+h, x+w)
            -- liftIO $ print (y,x,(py,px,py',px'))
            if y >= py && y < py' && x >= px && x < px' then do
                unless (bool || menv_locked m) $
                    selPanel p
                return bool
            else
                fold False ps

data B a s = B
  { b_text  :: String
  , b_bind  :: [Either Char Key]
  , b_op    :: a -> StateW s ()
  , b_forec :: ColorScheme
  , b_backc :: ColorScheme
  , b_activ :: ColorScheme
  }

instance Show (B a s) where
    show b = "B " ++ b_text b

instance Eq (B a s) where
    (==) = (==) `on` b_text

instance Ord (B a s) where
    compare = compare `on` b_text

type MenuEnv = (Window, Window, Menu, Int)
data MenuPars s = MenuPars
  { m_title     :: String
  , m_bounds    :: RelBounds
  , m_neighs    :: StateW s (String, String)
  , m_bs        :: [B MenuEnv s]
  , m_populate  :: MenuEnv -> StateW s [(String, String)]
  , m_bindings  :: MenuEnv -> Event -> StateW s Bool 
  , m_after     :: MenuEnv -> Event -> StateW s ()
  , m_redir     :: Ma.Map (Either Char Key) String
  , m_boxed     :: Bool
  , m_forec     :: ColorScheme
  , m_backc     :: ColorScheme
  }

data SpecialEv = 
    MenuPopulate 
  | OnEnter
  | OnLeave
    deriving (Show, Eq, Ord, Read, Enum)

isMenuMov :: Event -> Bool
isMenuMov (EventMouse _ _) = True
isMenuMov ev = any' ((Left <$> "jkhlgGudbf\NAK\EOT\STX\ACKKJ") 
        ++ (Right <$> [KeyDownArrow, KeyUpArrow, KeyLeftArrow, KeyRightArrow])) ev 

new_M :: MenuPars s -> StateW s (Panel s)
new_M pars = do
    (h, w, y, x, win, swin, menu, initial_bs, bsh, meenv) <- lift' $ do
        (h,w,y,x) <- getDims (m_bounds pars)
        let initial_bs = distributeBs (m_bs pars) (h-2) (w-2)
            bsh = distributionH initial_bs
        win <- newWindow h w y x
        swin <- subWindow win (h-2-bsh) (w-2) (y+1) (x+1)
        menu <- M.newMenu []
        M.setWin menu win
        M.setSubWin menu swin
        M.setMark menu ""
        M.disableOpts menu [M.NonCyclic, M.RowMajor]
        return 
          (h, w, y, x, win, swin, menu, initial_bs, bsh, (win, swin, menu, 0))

    let pop = do
        pairs <- m_populate pars meenv
        lift' $ forM pairs $ \(nam, desc) -> do
            let pad = if (all isSpace) nam then "" else " "
            if take 13 desc == "[value=False]" then do
                it <- M.newItem (pad ++ nam) (drop 13 desc)
                M.selectable it False
                return it
            else
                M.newItem (pad ++ nam) desc
    its <- pop

    let _sch @ (Scheme back _fore _title deco grey) = convScheme (m_forec pars)
    let _sch' @ (Scheme back' _fore' _title' deco' grey') = 
                                                      convScheme (m_backc pars)

    let title = foldr1 addAtrs [_title, deco, back]
        title' = foldr1 addAtrs [_title', deco', back']
        fore = addAtrs _fore back
        fore' = addAtrs _fore' back'
        sch = _sch { c_fore = fore, c_title = title }
        sch' = _sch' { c_fore = fore', c_title = title' }

--  button attribute map :: Map (B s) (fore, back, active)
    let battmap = Ma.fromList $ flip fmap (m_bs pars) $ \b -> 
          let [fores, backs, actis] =
                  fmap convScheme [b_forec b, b_backc b, b_activ b]
              fold = foldr1 addAtrs
              f s sch = fores 
                { c_back = fold [c_back s, c_back sch ] 
                , c_fore = fold [c_fore s, c_back (f s sch), c_fore sch ]
                , c_deco = fold 
                       [c_deco s, c_back (f s sch), c_fore (f s sch), c_deco sch] }
          in (b, 
            (f fores sch, f backs sch', actis))
    let buttmap = Ma.fromList $ do
          b <- m_bs pars
          bind <- b_bind b
          return (bind, b)

    panel <- lift' $ do
        M.setItems menu its
        M.setFormat menu (fromIntegral $ h - 2 - bsh) 1
        M.setForeground menu fore
        M.setBackground menu back
        M.setUnselectable menu grey

        M.postMenu menu

        ref <- liftIO $ newIORef initial_bs

        let get_ev = getEv win

        let on_ev n ev = case ev of
              EventUnknown n | n == (fromIntegral $ fromEnum MenuPopulate) -> do
                  pairs <- m_populate pars meenv
                  its <- pop
                  lift' $ M.inCurrentIndices menu $ do
                      M.unpostMenu menu
                      M.setItems menu its
                      M.postMenu menu
              EventResized -> (\op -> op >> rend' (m_title pars)) $ lift' $ do
                  (h,w,y,x) <- getDims (m_bounds pars)
                  updateWindow win $ do
                      resizeWindow h w
                      moveWindow y x
                  updateWindow swin $ do
                      resizeWindow (h-2) (w-2)
                      moveWindow (y+1) (x+1)
                  liftIO $ writeIORef ref $ distributeBs (m_bs pars) (h-2) (w-2)
              _ -> do
                didsth <- (m_bindings pars) (win, swin, menu, n) ev
                let repeat n = do
                        m_after pars meenv ev 
                        t <- topPanel
                        when (fmap p_name t == Just (m_title pars)) $ do
                            get_ev >>= on_ev n
                    n1 = if n == 0 then 1 else n
                unless didsth $ do
                  let b = flip Ma.lookup buttmap =<< evToEith ev 
                  if b /= Nothing then do
                    let Just b' = b
                    map <- liftIO $ readIORef ref
                    let [tup] = L.filter (\(_,_,b) -> b' == b) map
                    clickB (win, swin, menu, n) win tup battmap
                  else let mstr = evlookup (m_redir pars) ev in 
                                  if mstr /= Nothing then do
                    let Just str = mstr
                    getPanel str >>= flip on_event ev 
                  else case ev of
                    -- increase repeat buffer
                    EventCharacter c | isDigit c -> do
                        repeat (n * 10 + digitToInt c)
                    -- move down
                    _ | any' [Left 'j', Right KeyDownArrow] ev -> do
                        lift' $ replicateM_ (if n == 0 then 1 else n)
                                $ M.request menu M.DownItem 
                        repeat 0
                    -- move up
                    _ | any' [Left 'k', Right KeyUpArrow] ev -> do
                        lift' $ replicateM_ (if n == 0 then 1 else n) 
                                $ M.request menu M.UpItem 
                        repeat 0
                    -- move left
                    _ | any' [Left 'h', Right KeyLeftArrow] ev -> do
                        (l,_) <- m_neighs pars
                        selPanel' l
                    -- move right
                    _ | any' [Left 'l', Right KeyRightArrow] ev -> do
                        (_,r) <- m_neighs pars
                        selPanel' r
                    -- move to top
                    EventCharacter 'g' -> do
                        lift' $ M.request menu M.FirstItem
                        repeat 0
                    -- move to bottom / line n
                    EventCharacter 'G' -> do
                        lift' $ 
                            if n == 0 then
                                M.request menu M.LastItem
                            else
                                M.setIndex menu (fromIntegral n - 1)
                        repeat 0
                    -- move half a page up
                    _ | any' [Left 'u', Left '\NAK'] ev -> do
                        lift' $ do
                            (n', _) <- M.menuFormat menu
                            let off = (fromIntegral n') `quot` 2
                            replicateM n1 $ do
                                i <- M.getIndex menu 
                                replicateM off $ do
                                    M.request menu M.UpLine
                                i' <- M.getIndex menu 
                                when (i == i') $ do
                                    M.setIndex menu (i - fromIntegral off)
                        repeat 0
                    -- move half a page down
                    _ | any' [Left 'd', Left '\EOT'] ev -> do
                        lift' $ do
                            (n', _) <- M.menuFormat menu
                            let off = (fromIntegral n') `quot` 2
                            replicateM n1 $ do
                                i <- M.getIndex menu 
                                replicateM off $ do
                                    M.request menu M.DownLine
                                i' <- M.getIndex menu 
                                when (i == i') $ do
                                    M.setIndex menu (i + fromIntegral off)
                        repeat 0
                    -- move a page up
                    _ | any' [Left 'b', Left '\STX'] ev -> do
                        lift' $ do
                            (n', _) <- M.menuFormat menu
                            let off = 95 * fromIntegral n' `quot` 100
                            replicateM n1 $ do
                                i <- M.getIndex menu 
                                replicateM off $ do
                                    M.request menu M.UpLine
                                i' <- M.getIndex menu 
                                when (i == i') $ do
                                    M.setIndex menu (i - fromIntegral off)
                        repeat 0
                    -- move a page down
                    _ | any' [Left 'f', Left '\ACK'] ev -> do
                        lift' $ do
                            (n', _) <- M.menuFormat menu
                            let off = 95 * fromIntegral n' `quot` 100
                            replicateM n1 $ do
                                i <- M.getIndex menu 
                                replicateM off $ do
                                    M.request menu M.DownLine
                                i' <- M.getIndex menu 
                                when (i == i') $ do
                                    M.setIndex menu (i + fromIntegral off)
                        repeat 0
                    -- move two items back
                    EventMouse _ MouseState {mouseShift = False
                     , mouseButtons = (4,ButtonPressed):_} -> do
                        lift' $ do
                            (m, _) <- first fromIntegral <$> M.menuFormat menu
                            it <- fromIntegral <$> M.itemCount menu
                            replicateM ((min m it `quot` 10 + 1) * n1) 
                                $ M.request menu M.PrevItem
                        repeat 0
                    -- move two items forward
                    EventMouse _ MouseState 
                     {mouseShift = False, mouseButtons = (5,_):_} -> do
                        lift' $ do
                            (m, _) <- first fromIntegral <$> M.menuFormat menu
                            it <- fromIntegral <$> M.itemCount menu
                            replicateM ((min m it `quot` 10 + 1) * n1) 
                                $ M.request menu M.NextItem
                        repeat 0
                    -- move current item up
                    EventCharacter 'K' -> do
                        lift' $ do
                            M.unpostMenu menu
                            replicateM_ n1 $ moveUp menu
                            M.postMenu menu
                        rend' (m_title pars)
                        repeat 0
                    -- move current item down
                    EventCharacter 'J' -> do
                        lift' $ do
                            M.unpostMenu menu
                            replicateM_ n1 $ moveDown menu
                            M.postMenu menu
                        rend' (m_title pars)
                        repeat 0
                    EventMouse _ 
                     st @ MouseState { mouseButtons = (1, b):_ 
                     , mouseCoordinates = (x,y,_)} 
                     | b `L.elem` [ButtonPressed, ButtonClicked] -> do
                        -- liftIO $ print "asdasd"
                        same <- focusClick ev
                        if same then do
                          bs <- liftIO $ readIORef ref
                          (y', x') <- lift' $ updateWindow win windowPosition 
                          buts <- clickBs meenv win (y-y') (x-x') bs battmap 
                          unless buts $ do
                              lift' $ M.clickMenu 
                                      menu (fromIntegral $ y) (fromIntegral $ x)
                              repeat 0
                        else do
                            Just p <- topPanel
                            when (p_name p /= m_title pars) $ do
                                on_event p ev 
                    _ -> return ()
        
        let on_enter = (>> on_ev 0 (eu OnEnter)) $ lift' $ do
            M.unpostMenu menu
            M.setForeground menu fore
            M.setBackground menu back
            M.setUnselectable menu grey
            updateWindow win $ do
                setBackgroundFilled back
                setBackground $ Glyph ' ' deco
                setAttributes deco
                let f g = if m_boxed pars then
                        Just g { glyphAttributes = deco }
                      else
                        Just (Glyph ' ' deco)
                drawBorder 
                            (f glyphLineV) (f glyphLineV)
                            (f glyphLineH) (f glyphLineH)
                            (f glyphCornerUL) (f glyphCornerUR)
                            (f glyphCornerLL) (f glyphCornerLR)
                unless (L.null $ m_title pars) $ do
                    setBackground $ Glyph ' '
                            $ foldr1 addAtrs [title, deco, back]
                    moveCursor 0 2
                    drawString $ m_title pars
                setBackground $ Glyph ' ' back
                setAttributes back
                bs <- liftIO $ readIORef ref
                drawBs bs battmap 1
            M.postMenu menu
            refreshWindow win
            
        let on_leave = (>> on_ev 0 (eu OnLeave)) $ lift' $ do
                M.unpostMenu menu
                M.setForeground menu fore'
                M.setBackground menu back'
                updateWindow win $ do
                    setBackgroundFilled back' 
                    setBackground $ Glyph ' ' deco'
                    setAttributes deco'
                    let f g = if m_boxed pars then
                            Just g { glyphAttributes = deco' }
                          else
                            Just $ Glyph ' ' deco'
                    drawBorder 
                            (f glyphLineV) (f glyphLineV)
                            (f glyphLineH) (f glyphLineH)
                            (f glyphCornerUL) (f glyphCornerUR)
                            (f glyphCornerLL) (f glyphCornerLR)
                    unless (L.null $ m_title pars) $ do
                        setBackground $ Glyph ' ' 
                                $ foldr1 addAtrs [title', deco', back']
                        moveCursor 0 2
                        drawString (m_title pars)
                    setBackground $ Glyph ' ' back'
                    setAttributes back'
                    bs <- liftIO $ readIORef ref
                    drawBs bs battmap 2
                M.postMenu menu
                refreshWindow win

        let destroy = lift' $ do
            M.freeMenu menu
            closeWindow swin
            closeWindow win

        return $ Panel 
                   (m_title pars)
                   win
                   on_leave
                   on_enter
                   (on_ev 0)
                   (getEv win)
                   destroy
    regPanel panel
    return panel

data PanPars s = PanPars
  { pa_name     :: String 
  , pa_bounds   :: RelBounds
  , pa_populate :: Window -> StateW s ()
  }

new_P :: PanPars s -> StateW s (Panel s)
new_P pars = do
    (h,w,y,x) <- lift' $ getDims (pa_bounds pars)
    p <- lift' $ do
        win <- newWindow h w y x

        let pop = do
            pa_populate pars $ win
            lift' $ refreshWindow win

        let onevent ev = case ev of
              _ | ev == eu MenuPopulate -> pop
              _ -> do
                  menv <- get'
                  let ord = menv_zorder menv
                  case menv_zorder menv of
                      x : x' : xs | p_name x == pa_name pars -> do
                          selPanel x'
                      _ -> return ()
        let waitev = do
              menv <- get'
              let zorder = popP' (pa_name pars) (menv_zorder menv)
              if L.null zorder then
                  getEv win
              else
                  wait_ev $ head zorder
        let destroy = do
            lift' $ closeWindow win

        return $ Panel 
              (pa_name pars)
              win
              pop
              pop
              onevent
              waitev
              destroy
    regPanel p
    return p

type RelPos = (Double, Double)
type RelBounds = (RelPos, RelPos, RelPos, RelPos)

opPos :: RelPos -> Int -> Int
opPos (p, o) x = floor $ (p * (fromIntegral x)) + o

-- returns (h, w, y, x)
getDims :: RelBounds -> Curses (Int, Int, Int, Int)
getDims (y,x,y',x') = do
    (h,w) <- screenSize
    let _y = opPos y (h - 1)
        _x = opPos x (w - 1)
        _y' = opPos y' (h - 1)
        _x' = opPos x' (w - 1)
    return (max 0 $ min h $  _y' - _y + 1
           ,max 0 $ min w $  _x' - _x + 1
           ,max 0 $ min h $  _y
           ,max 0 $ min w $  _x)

dimsToBounds (h,w,y,x) = (y,x,y+h,x+w)

type Atts = [Either (Color, Color) Attribute]

data Scheme att = Scheme
  { c_back    :: att
  , c_fore    :: att
  , c_title   :: att
  , c_deco    :: att
  , c_grey    :: att
  } deriving (Show)

type ColorScheme = Scheme Atts
type AttrScheme = Scheme [Attribute]

emptyScheme :: Scheme Atts
emptyScheme = Scheme [] [Right AttributeReverse] [] [] [] 

invScheme :: Scheme Atts
invScheme = Scheme [i] [i] [i] [i] [i] where i = Right AttributeInvisible
 
convAtts :: Atts -> [Attribute]
convAtts = fmap $ either (AttributeColor . unsafeColorID . colorPair) id

convScheme :: Scheme Atts -> Scheme [Attribute]
convScheme sch = 
    let [back, fore, title, deco, grey] = fmap convAtts $
            ($ sch) <$> [c_back, c_fore, c_title, c_deco, c_grey]
    in Scheme back fore title deco grey


-- 
-- util
--

eu :: Enum e => e -> Event
eu = EventUnknown . fromIntegral . fromEnum

b str = B str [Left $ str !! (length str `quot` 3)] (\_ -> liftIO $ threadDelay 50) 
  emptyScheme emptyScheme { c_fore = [] }  
  emptyScheme { c_back = [Left (ColorRed, ColorWhite)] }

distributeBs :: [B a s] -> Int -> Int -> [(Int, Int, B a s)]
distributeBs bs h w =  concat $ zipWith yto3 [h, h-1 ..] complete where
    pad = fromIntegral $ max 1 (w `quot` 25)
    yto3 :: Int -> [B a s] -> [(Int, Int, B a s)]
    yto3 y xs = fmap (\(x,b) -> (y,x,b)) (fixrow xs)
    complete = fixuppers $ L.reverse $ vert $ L.reverse bs
    vert :: [B a s] -> [[B a s]]
    vert [] = []
    vert bs =
        let (row, rem) = fillrow w bs
        in L.reverse row : vert rem
    fillrow :: Int -> [B a s] -> ([B a s], [B a s])
    fillrow _ [] = ([], [])
    fillrow remw (b : bs) =
        let sz = fromIntegral $ L.length $ b_text b
        in if sz + 2 <= remw then
            first (b :) $ fillrow (remw - sz - 1) bs
        else
            ([], b : bs)
    totsz :: Num n => [B a s] -> n
    totsz bs = fromIntegral $ sum $ fmap (\b -> pad + L.length (b_text b)) bs
    fixrow :: [B a s] -> [(Int, B a s)]
    fixrow bs =
        let leftpad = (1 + w - totsz bs) `quot` 2
            inis = fmap totsz $ L.inits bs
        in zipWith (\x y -> (x + leftpad, y)) inis bs
    fixuppers :: [[B a s]] -> [[B a s]]
    fixuppers (b : (b' @ (h:hs)) : bs) =
        let (uprow, uprem) = fillrow w (h:b)
            (lorow, _) = fillrow w hs
        in if (not $ L.null uprem) || totsz uprow > totsz lorow then
            b : b' : bs
        else
            fixuppers (L.reverse uprow : lorow : bs)
    fixuppers bs = bs

distributionH :: [(Int, Int, B a s)] -> Int
distributionH [] = 0
distributionH bs =
    let xs = (\(x,_,_) -> x) <$> bs 
        mi = minimum (xs)
        ma = maximum (xs)
    in ma - mi + 1

drawB :: (Int, Int, B a s) -> Scheme [Attribute] -> Update ()
drawB (y,x,b) sch = do
    moveCursor y x
    setF c_back
    foldM_ drawChar 0 (b_text b) where
    drawChar n c = do
        if n == pos then do
            setF c_fore
            drawString [c]
            setF c_back
        else if not (isAlphaNum c) then do
            setF c_deco
            drawString [c]
            setF c_back
        else 
            drawString [c]
        return (n+1)
    pos = maybe (-1) id $ L.findIndex (\i -> i `elem` lefts (b_bind b)) (b_text b)
    setF f = do
        setBackground $ Glyph ' ' (f sch)
        setAttributes (f sch)

drawBs :: [(Int, Int, B a s)] 
       -> Ma.Map (B a s) (AttrScheme, AttrScheme, AttrScheme)
       -> Int
       -> Update ()
drawBs bs map n = do
    let f (a,b,c) = if n == 3 then c else if n == 2 then b else a
    forM_ bs (\(y,x,b) -> drawB (y,x,b) $ f $ lookup' b map)

clickB :: a -> Window 
            -> (Int, Int, B a s) 
            -> Ma.Map (B a s) (AttrScheme, AttrScheme, AttrScheme)
            -> StateW s ()
clickB e win (y,x,b) map = do
    let (fores, backs, actis) = lookup' b map
    t <- liftIO $ getTime
    lift' $ do
        updateWindow win $ do
            drawB (y, x, b) actis
        refreshWindow win
    b_op b e
    liftIO $ do
        t' <- getTime
        threadDelay $ fromIntegral $ (120 - t' + t) * 1000
    Just p <- topPanel
    lift' $ do
        updateWindow win $ do
            drawB (y, x, b) $ if p_win p == win then fores else backs
        refreshWindow win 

clickBs :: a -> Window -> Int -> Int -> [(Int, Int, B a s)] 
            -> Ma.Map (B a s) (AttrScheme, AttrScheme, AttrScheme)
            -> StateW s Bool
clickBs e win y x bs map = (|| height) <$> foldM f False bs where
    f bool (y', x', b) = if bool then return True else do
        let sz = fromIntegral $ L.length (b_text b)
        if y == y' && x >= x' && x < x' + sz then do
            clickB e win (y', x', b) map
            return True
        else return False
    height = y `elem` (fmap (\(y', _, _) -> y') bs)


addAtts :: AttrScheme -> AttrScheme -> AttrScheme
addAtts sch sch' = conv $ L.zipWith addAtrs l1 l2 where
    conv [b,f,t,d,g] = Scheme b f t d g
    l1 = ($ sch) <$> fs 
    l2 = ($ sch') <$> fs
    fs = [c_back, c_fore, c_title, c_deco, c_grey]

addAtrs :: [Attribute] -> [Attribute] -> [Attribute]
addAtrs orig xtra = 
    let origc = filtered orig
        xtrac = filtered xtrac
    in if L.null origc then
        xtra ++ orig
    else
        orig
    where
        filter (AttributeColor _) = True
        filter _ = False
        filtered = L.filter filter

drawMult :: Int -> Int -> [(Atts, String)] -> Update ()
drawMult y x pairs = do
    (h,w) <- windowSize
    let mi = max y 0
        ma = min (h - 2) (y + fromIntegral (L.length pairs))
        f n (attr, str) = do
            moveCursor n x
            setBackground $ Glyph ' ' $ convAtts attr
            setAttributes (convAtts attr)
            drawString (L.take (fromIntegral $ w-x-1) str)
    zipWithM_ f [mi..ma] pairs

moveUp :: Menu -> Curses ()
moveUp menu = do
    _it <- M.currentItem menu
    forM_ _it $ \it -> do
        t <- M.topRow menu
        i <- fromIntegral <$> M.itemIndex it
        when (i>0) $ do
            M.swapIndices menu (i-1) i
            when (t>0) $
                M.setTopRow menu (t-1)
            M.setCurrent menu it

moveDown :: Menu -> Curses ()
moveDown menu = do
    _it <- M.currentItem menu
    forM_ _it $ \it -> do
        t <- M.topRow menu
        i <- fromIntegral <$> M.itemIndex it
        n <- M.itemCount menu
        when (i < fromIntegral n-1) $ do
            M.swapIndices menu (i+1) i
            when (t<n-1) $
                M.setTopRow menu (t+1)
            M.setCurrent menu it

clearBackground :: Curses ()
clearBackground = do
    def <- defaultWindow
    updateWindow def $ do
        (h,w) <- windowSize
        clearLines [0..h-1]

mprint :: Show a => a -> StateW s ()
mprint e = lift' $ do
    def <- defaultWindow
    updateWindow def $ do
        (h,w) <- windowSize
        moveCursor (h-4) 0
        clearLine
        drawString $ take (2 * fromIntegral w) $ show e
    render

popL :: Eq a => a -> [a] -> [a]
popL e xs = e : (L.delete e xs)

popP' :: String -> [Panel s] -> [Panel s]
popP' name xs = case L.break (\p -> p_name p == name) xs of
    (l, r:rs) -> r : (l ++ rs)
    (l, _) -> l

lookup' :: Ord k => k -> Ma.Map k a -> a
lookup' e map = maybe (error $ "no such key") id $ Ma.lookup e map

nop :: Monad m => m ()
nop = return ()

populate' :: String -> StateW s ()
populate' name = getPanel name >>= populate

populate :: Panel s -> StateW s ()
populate p = on_event p $ EventUnknown 
                 $ fromIntegral $ fromEnum MenuPopulate

any' :: [Either Char Key] -> Event -> Bool
any' eiths (EventCharacter c) = any (either (== c) (pure False)) eiths
any' eiths (EventSpecialKey k) = any (either (pure False) (== k)) eiths
any' _ _ = False

evToEith :: Event -> Maybe (Either Char Key)
evToEith (EventCharacter c) = Just (Left c)
evToEith (EventSpecialKey k) = Just (Right k)
evToEith _ = Nothing

evlookup :: Ma.Map (Either Char Key) a -> Event -> Maybe a
evlookup map ev = flip Ma.lookup map =<< evToEith ev

deriving instance Ord Color
instance Enum Color where
    fromEnum c = maybe undefined id $ L.lookup c colorInt
    toEnum i = fst $ head $ L.filter ((== i) . snd) colorInt

colorInt :: [(Color, Int)]
colorInt = L.zip [ ColorDefault, ColorBlack, ColorRed, ColorGreen, ColorYellow
                 , ColorBlue , ColorCyan, ColorWhite ] [0..]

colorPair :: (Color, Color) -> Int
colorPair (c1, c2) = 8 * fromEnum c1 + fromEnum c2 + 1

main' = runCurses $ flip runStateW ' ' $ do
    menv <- get'
    put' menv { menv_backops = [(500,0, lift' $ do
        def <- defaultWindow 
        updateWindow def $ do
            moveCursor 0 0
            t <- getTime
            drawString $ printf "%.1fs" (fromIntegral t / 1000 :: Double)
        render )] }
    lift' $ do
        setEcho False
        setCursorMode CursorInvisible
        mouseInterval 50
        def <- defaultWindow
        updateWindow def clear
        render
    let f _ (EventSpecialKey KeyRightArrow) = selPanel' " dsa " >> return True
        f _ (EventCharacter 'q') = mzero
        f _ _ = return False
        g _ (EventSpecialKey KeyLeftArrow) = selPanel' " asd " >> return True
        g _ (EventCharacter 'q') = mzero
        g _ _ = return False
    m <- new_M $ MenuPars
          " dsa "
          ((0,5),(0.125,0),(0.75,5),(0.75,0)) 
          (return (" asd ", " asd "))
          []
          (pure $ return [(show i, (L.concat $ L.permutations ["Nigger", "Faggot", "Kike", "Chink"]) !! ((i + i^2) `mod` 96)) | i <- [30,31..1000]])
          g
          (\_ _ -> nop)
          (Ma.fromList [(Left 'a', " asd ")])
          True
          emptyScheme { c_title = [Left (ColorRed, ColorWhite), Right AttributeBold, Right AttributeBlink]
                      , c_fore = [Left (ColorBlack, ColorRed)]
                      }
          emptyScheme { c_deco = [Left (ColorRed, ColorDefault)] 
                      , c_title = [Left (ColorWhite, ColorRed)]
                      , c_back = [Left (ColorDefault, ColorDefault)]
                      , c_fore = [Left (ColorRed, ColorDefault), Right AttributeUnderline]
                      }
    m' <- new_M $ MenuPars 
          " asd " 
          ((0,2), (0.25,0), (0.75,2), (0.875,0))
          (return (" dsa ", " dsa "))
          [b "[asd]", b "<iajsda>"] 
          (pure $ return [(show i ++ replicate (1 + i `mod` 13) '_', printf "0x%o" i) | i <- [1 :: Int, 4..1065]])
          f
          (\_ _ -> nop)
          mempty
          False
          emptyScheme { c_fore = [Right AttributeReverse, Right AttributeBold] 
                      , c_back = [Left (ColorCyan, ColorBlack)]
                      , c_deco = [Left (ColorBlack, ColorMagenta)]
                      , c_title = [Left (ColorMagenta, ColorBlack)
                                  , Right AttributeBlink
                                  , Right AttributeBold]
                      }
          emptyScheme { c_fore = [Left (ColorBlue, ColorYellow)]
                      , c_back = [Left (ColorGreen, ColorBlack)] 
                      , c_deco = [Left (ColorBlue, ColorBlue)]
                      , c_title = [ Left (ColorBlack, ColorBlue)
                                  , Right AttributeBold
                                  ] 
                      }
    mainLoop
    get'

mainLoop' = mainDo (return False)

mainDo :: StateW s Bool -> StateW s Bool
mainDo op = try $ do
    topPanel >>= maybe (return False) ? \p -> try $ do
        ev <- wait_ev p
        topPanel >>= maybe (return False) ? \p' -> try $ do
            on_event p' ev
            try $ do
                res <- op
                if res then do
                    return True
                else do
                    mainDo op
        where
            infixr 9 ? 
            (?) = ($)
            try :: StateW s Bool -> StateW s Bool
            try = (`mplus` return False)

mainLoop = forever $ do
    p <- topPanel
    forM_ p $ \p' -> do
        ev <- wait_ev p'
        p <- topPanel
        forM_ p $ \p' -> do
            on_event p' ev

x2t :: a -> (a,String)
x2t x = (x,"")

